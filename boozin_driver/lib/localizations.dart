import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_sheet_localization/flutter_sheet_localization.dart';
part 'localizations.g.dart';
//@SheetLocalization("1SOKzFFdEcm7nZ_qoMgIW3cB_qCVDQzQDH0uYuyq7tI8","0") // <- See 1. to get DOCID and SHEETID
@SheetLocalization("1wL5-PQ3Dp77BaqrI3OlXIqK71BLY-YuSLAhpB8YO5Mc","0") // <- See 1. to get DOCID and SHEETID
class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      AppLocalizations.languages.containsKey(locale);
  @override
  Future<AppLocalizations> load(Locale locale) =>
      SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;

}
