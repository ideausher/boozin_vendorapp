// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'localizations.dart';

// **************************************************************************
// SheetLocalizationGenerator
// **************************************************************************

class AppLocalizations {
  AppLocalizations(this.locale) : this.labels = languages[locale];

  final Locale locale;

  static final Map<Locale, AppLocalizations_Labels> languages = {
    Locale.fromSubtags(languageCode: "en"): AppLocalizations_Labels(
      sendOtp: AppLocalizations_Labels_SendOtp(
        hint: AppLocalizations_Labels_SendOtp_Hint(
          phoneNumber: "Enter Mobile Number",
        ),
        button: AppLocalizations_Labels_SendOtp_Button(
          continueText: "Continue",
        ),
        text: AppLocalizations_Labels_SendOtp_Text(
          accepthe: "I accept the ",
          termsAndServices: "Terms & services",
          limitLess: "of\nLimitLess.",
          signUp: "Sign up",
          continueWithOnDemandDriver: "Continue as On-Demand Driver",
          continueWithCommercialVehicleDriver:
              "Continue as Commercial Vehicle Driver",
        ),
        error: AppLocalizations_Labels_SendOtp_Error(
          agreeTermsAndCondition: "I agree to the Terms and Policies",
        ),
      ),
      verifyOtp: AppLocalizations_Labels_VerifyOtp(
        button: AppLocalizations_Labels_VerifyOtp_Button(
          verify: "Verify",
        ),
        text: AppLocalizations_Labels_VerifyOtp_Text(
          verifyMobileNumber: "Verify mobile number",
          enterOtp: "Enter the OTP sent to",
          changeNumber: "Change number",
          resend: "Resend",
          signUp: "Sign Up",
          enterOtpText: "Enter OTP ",
        ),
      ),
      enterDetails: AppLocalizations_Labels_EnterDetails(
        text: AppLocalizations_Labels_EnterDetails_Text(
          enterDetails: "Enter Details",
          enterDetailToProceedFurther: "Enter the details to proceed further.",
          signup: "Sign Up",
        ),
        hint: AppLocalizations_Labels_EnterDetails_Hint(
          enterName: "Enter name",
          chooseDob: "Choose DOB",
        ),
        button: AppLocalizations_Labels_EnterDetails_Button(
          continueText: "Continue",
        ),
      ),
      completeProfile: AppLocalizations_Labels_CompleteProfile(
        text: AppLocalizations_Labels_CompleteProfile_Text(
          youAreOffline: "You are offline.",
          completeProfileToGetOrders: "Complete profile to get orders",
          waitFordocumentVerification:
              "(Please wait until it gets verified from admin)",
          uploadDocAgain: "(Please upload your documents again)",
          receiveOrderText: "(Complete to receive orders)",
          profileCompletion: "Profile Completion",
          completeProfileToReceiveOrders: "Complete profile to receive orders",
          detailsOfDriver: "Details of Driver",
          bankAccountDetails: "Bank account Details",
        ),
        error: AppLocalizations_Labels_CompleteProfile_Error(
          verificationPending: "Document verification is pending yet.",
          documentRejected: "Documents Gets Rejected by Admin due to ",
        ),
        button: AppLocalizations_Labels_CompleteProfile_Button(
          completeProfile: "Complete Profile",
          backgroundVerificationDocuments: "Backgroud Verifcation Documents",
        ),
      ),
      enableLocation: AppLocalizations_Labels_EnableLocation(
        text: AppLocalizations_Labels_EnableLocation_Text(
          locationTitle: "Allow Location",
          giveAccess: "Allow access to your current location",
        ),
        button: AppLocalizations_Labels_EnableLocation_Button(
          allow: "Allow",
        ),
        error: AppLocalizations_Labels_EnableLocation_Error(
          pleaseGrantPermission: "Please grant permission",
          permissionDenied:
              "Permission denied.Please enable it from app settings",
          plsEnableYourLocation: "Please enable your location",
        ),
      ),
      driverDetails: AppLocalizations_Labels_DriverDetails(
        text: AppLocalizations_Labels_DriverDetails_Text(
          driverDetails: "Driver Details",
          noteDetail:
              "Any changes made on the document will DEACTIVATE your profile and send it for company's approval again.(in red) You will not able to receive job request until your profile is approved again, thanks.",
          notUploaded: "Not Uploaded",
          pending: "Pending",
          completed: "Completed",
          rejected: "Rejected",
          vehicleRegistration: "Vehicle Registration",
          drivingLicence: "Driving Licence",
          licenceNumberPlate: "Licence Number Plate",
          vehicleInsurance: "Vehicle Insurance",
          licenceNumber: "Licence Number:",
          expiryDate: "Expiry Date:",
          plateNumber: "Plate Number:",
          policyNumber: "Policy Number:",
          workPermit: "Work Permit/Asylum",
          backgroundVerification: "Background Verification",
          noteText: "Note:",
        ),
        button: AppLocalizations_Labels_DriverDetails_Button(
          proceed: "Proceed",
        ),
      ),
      vehicleRegistration: AppLocalizations_Labels_VehicleRegistration(
        text: AppLocalizations_Labels_VehicleRegistration_Text(
          title: "Vehicle Registration",
        ),
        hint: AppLocalizations_Labels_VehicleRegistration_Hint(
          regno: "Registration Number",
          company: "Vehicle Company",
          model: "Model",
          color: "Color",
          validUpto: "Valid Upto",
        ),
      ),
      drivingLicence: AppLocalizations_Labels_DrivingLicence(
        text: AppLocalizations_Labels_DrivingLicence_Text(
          title: "Driving Licence",
          licenceNumber: "Licence Number",
          expiryDate: "Expiry Date",
        ),
      ),
      vehicleInsurance: AppLocalizations_Labels_VehicleInsurance(
        text: AppLocalizations_Labels_VehicleInsurance_Text(
          title: "Vehicle Insurance",
          policyNumber: "Policy Number",
        ),
        hint: AppLocalizations_Labels_VehicleInsurance_Hint(
          validTill: "Valid Till",
          validFrom: "Valid From",
        ),
        error: AppLocalizations_Labels_VehicleInsurance_Error(
          plsEnterInsuranceValidFrom: "Please enter insurance valid from.",
          plsEnterInsuranceValidTill: "Please enter insurance valid till.",
          validFromAndValidTillMustBeDiff:
              "Valid from and valid till date must be different.",
          validTillGreaterThanValidFrom:
              "Valid till date must be greater than valid from date",
        ),
      ),
      workPermit: AppLocalizations_Labels_WorkPermit(
        text: AppLocalizations_Labels_WorkPermit_Text(
          title: "Work Permit/Asylum",
          plateNumber: "Permit Number",
          idNumber: "ID Number",
          or: "OR",
        ),
        hint: AppLocalizations_Labels_WorkPermit_Hint(
          expiryDate: "Expiry Date",
          capacity: "Capacity (KG)",
        ),
      ),
      backgroundVerification: AppLocalizations_Labels_BackgroundVerification(
        text: AppLocalizations_Labels_BackgroundVerification_Text(
          title: "Background Verification",
          note:
              "Note: The background verification/finger prints document is used to verify previous criminal offences.",
        ),
      ),
      common: AppLocalizations_Labels_Common(
        text: AppLocalizations_Labels_Common_Text(
          notApplicable: "N/A",
          name: "LimitLess",
          email: "Email",
          permissionAlert: "Permission Alert!!",
          setting: "Setting",
          permissionAlertMessage:
              "Location permission is required to get a new order. If you are not allowing the location permission then you are not able to use or earn anything from our platform. So please give permission to use all features of the Application",
          yes: "Yes",
          no: "No",
          driver: "driver",
          ok: "Ok",
          wantToLogOut: "Are you sure you want to log out?",
          appTitle: "LimitLess",
          appSubtitle: "Fast . Simple . Ready",
          userInfoname: "Name",
          phoneNumber: "Phone Number",
          verifyEmail: "Verify your email",
          items: "Items",
          price: "Price",
          quantity: "Quantity",
          subTotal: "Subtotal",
          noDataFound: "No Data Found",
          submit: "Submit",
          mr: "Mr.",
          yourAddress: "Your Address",
          cartValue: "Cart Value",
          deliveryAddress: "Delivery address",
          track: "Track",
          cancel: "Cancel",
          raiseDispute: "Raise Dispute",
          orderDetails: "Order Details",
          next: "Next",
          uploadPhoto: "Upload photo",
          imageRemoved: "Image removed",
          fileAdded: "File Added",
        ),
        error: AppLocalizations_Labels_Common_Error(
          somthingWentWrong: "Something went wrong",
          enterYourNumber: "Please enter valid phone number",
          pleasefillOTP: "Please fill the OTP",
          pleaseEnterEmail: "Please enter your email",
          checkInternet: "Please check your internet connection",
          enterYourName: "Please enter your name",
          somethingWentWrong: "Something went wrong",
          nameLength: "Name must be of atleast 3 charecters",
          phoneNumberLength: "Phone number must be of 10 digit only",
          nameMaxLength: "Name cannot be more than 25 charecter long",
          numberAlreadyExist: "Number already registered on customer app",
          userBlocked: "You are blocked.Please contact admin",
          nameMinLength: "Name must have atleast 3 charecters",
          numberAlreadyRegisteredAsOnDemandDriver:
              "Phone number already registered as On-Demand Driver",
          numberAlreadyRegisteredAsCommercialVehicleDriver:
              "Phone number already registered as Commercial Vehicle Driver",
          numberAlreadyRegisteredAsCustomer:
              "Phone number already registered as customer.",
          numberAlreadyRegisteredAsVendor:
              "Phone number already registered as vendor.",
          pleaseEnterYourDob: "Please enter your date of birth",
        ),
        title: AppLocalizations_Labels_Common_Title(
          ms: "Ms.",
          mrs: "Mrs.",
        ),
      ),
      helpAndSupport: AppLocalizations_Labels_HelpAndSupport(
        appbar: AppLocalizations_Labels_HelpAndSupport_Appbar(
          title: "Help & Support",
        ),
        text: AppLocalizations_Labels_HelpAndSupport_Text(
          previousTicket: "Previous Tickets",
          recentTicket: "Recent Tickets",
          ticketID: "Ticket ID:",
          issueRaisedOn: "Issue Raised On",
          bookingID: "Booking ID",
          serviceName: "Service Name",
          bookingDate: "Booking Date",
          orderId: "Order Id:",
          orderDate: "Order Date:",
          ticketStatus: "Ticket Status:",
          pending: "Pending",
          closed: "Closed",
          query: "Query: ",
          response: "Response: ",
          noQueriesFound: "No Queries Found",
        ),
        button: AppLocalizations_Labels_HelpAndSupport_Button(
          raiseNewTicket: "Raise New Ticket",
        ),
      ),
      sendQuery: AppLocalizations_Labels_SendQuery(
        text: AppLocalizations_Labels_SendQuery_Text(
          pleaseFillTheDetails:
              "Please fill the details that we need to know\nregarding the issue that you are having...",
          tellUsWhyDoYouNeedHelp: "Tell us why do you need help.",
          getInTouch: "Get in Touch!",
          fillDetails: "Fill Details",
          description: "Description",
          uploadPicture: "Upload picture",
          addImage: "Add Image",
          removeImage: "Remove Image",
          areYouSureYouWantToRemoveImage:
              "Are you sure you want to remove this image?",
        ),
        hint: AppLocalizations_Labels_SendQuery_Hint(
          writeHere: "Write here (Minimum 50 charecters)",
        ),
        button: AppLocalizations_Labels_SendQuery_Button(
          submit: "Submit",
        ),
        error: AppLocalizations_Labels_SendQuery_Error(
          writeQuery: "Write here (Minimum 50 charecters)",
          plsEnterTheQuery: "Please enter the query",
          min50Required: "Minimum 50 char required.",
        ),
      ),
      profile: AppLocalizations_Labels_Profile(
        text: AppLocalizations_Labels_Profile_Text(
          profile: "Profile",
          editProfile: "Edit Profile",
          myEarning: "My Earnings",
          subtitleEarning: "See details of your earnings",
          helpAndSupport: "Help & Support",
          subtitleHelpAndSupport: "Get support from us",
          myDocuments: "My Documents",
          subtitleDocuments: "Store you vehicle details",
          termsAndPolicies: "Terms and Policies",
          subtitleTermsAndPolicies: "Get our policy from us",
          logout: "Log out",
        ),
      ),
      editProfile: AppLocalizations_Labels_EditProfile(
        text: AppLocalizations_Labels_EditProfile_Text(
          personalInfo: "Personal Info",
          dob: "DOB",
        ),
        hint: AppLocalizations_Labels_EditProfile_Hint(
          enterEmail: "Enter Email",
        ),
        error: AppLocalizations_Labels_EditProfile_Error(
          enterValidEmail: "Enter valid email",
        ),
      ),
      editPhoneEmail: AppLocalizations_Labels_EditPhoneEmail(
        text: AppLocalizations_Labels_EditPhoneEmail_Text(
          verifyNumber: "Enter number to verify",
          verifyEmail: "Enter email to verify",
          done: "Done",
        ),
        button: AppLocalizations_Labels_EditPhoneEmail_Button(
          sendOtp: "Send Otp",
        ),
        hint: AppLocalizations_Labels_EditPhoneEmail_Hint(
          sendOtp: "Phone Number",
          email: "Email",
        ),
      ),
      stripeAlert: AppLocalizations_Labels_StripeAlert(
        text: AppLocalizations_Labels_StripeAlert_Text(
          appBar: "Link Stripe Account",
          title:
              "To receive orders you have to first\nconnect to stripe account.",
          connectStripe: "Connect Stripe Account",
        ),
      ),
      stripe: AppLocalizations_Labels_Stripe(
        text: AppLocalizations_Labels_Stripe_Text(
          stripeAccount: "Stripe Account",
        ),
      ),
      loginSuccessfully: AppLocalizations_Labels_LoginSuccessfully(
        text: AppLocalizations_Labels_LoginSuccessfully_Text(
          loginSuccessfully: "Login Successfully",
          signUpSuccessfully: "Sign Up Successful",
        ),
      ),
      verification: AppLocalizations_Labels_Verification(
        error: AppLocalizations_Labels_Verification_Error(
          emptyRegNumber: "Please Enter Vehicle Registration Number",
          emptyVehicleCmpny: "Please Enter Vehicle Company",
          emptyVehicleModel: "Please Enter Vehicle Model",
          emptyVehicleColor: "Please Enter Vehicle Color",
          emptyValidity: "Please Enter Vehicle Registration Valid Upto",
          emptyImage: "Please Upload Image",
          emptyLicenceNumber: "Please Enter Licence Number",
          emptyLicenceExpiry: "Please Enter Licence Expiry Date",
          emptyNumberPlate: "Please Enter Number of Plate",
          emptyPolicyNumber: "Please Enter Policy Number",
          emptyPermitNumberOrIdNumber:
              "Please Enter Permit Number Or ID Number.",
          expiryDate: "Please Enter Expiry Date",
        ),
      ),
      polyline: AppLocalizations_Labels_Polyline(
        text: AppLocalizations_Labels_Polyline_Text(
          shopAddress: "Shop Address",
          deliveryAddress: "Delivery Address",
        ),
      ),
      orderDetail: AppLocalizations_Labels_OrderDetail(
        text: AppLocalizations_Labels_OrderDetail_Text(
          pickUpDestination: "PickUp Destination",
          deliveryDestination: "Delivery Destination",
          itemDescription: "Item Description",
          orderDetails: "Order Details",
          totalItems: "Total Items",
          raiseDispute: "Raise Dispute",
          shareFeedback: "Share Feedback",
          plsGiveYourRating: "Please give your Rating!",
          shop: "Shop",
          comment: "Comment",
          customer: "Customer",
          skip: "Skip",
          cancel: "Cancel",
          accept: "Accept",
          reject: "Reject",
        ),
      ),
      customerVerification: AppLocalizations_Labels_CustomerVerification(
        text: AppLocalizations_Labels_CustomerVerification_Text(
          customerVerification: "Customer Verification",
          enterTheOtpSentTo: "Enter the otp sent to",
          resendOtp: "Resend OTP",
          completeDelivery: "Complete Delivery",
        ),
      ),
      notifications: AppLocalizations_Labels_Notifications(
        text: AppLocalizations_Labels_Notifications_Text(
          noNotificationFound: "No Notifications found",
          notifications: "Notifications",
        ),
      ),
      myEarnings: AppLocalizations_Labels_MyEarnings(
        text: AppLocalizations_Labels_MyEarnings_Text(
          totalCreditsIn: "Total credits in",
          calculating: "Calculating...",
          totalEarningOfTheDay: "Total earnings of the day",
          myEarnings: "My Earnings",
        ),
      ),
      intro: AppLocalizations_Labels_Intro(
        text: AppLocalizations_Labels_Intro_Text(
          searchForAnyItemOnline: "Search for any\nitem online",
          placeTheOrderOnlineInstantly: "Place the order\nonline instantly",
          liveTrackYourOrder: "Live track your\nOrder",
          quickSearchAndAddFilters:
              "Quick search and add filters for\nyour comfort of search.",
          searchAndPlaceTheOrder:
              "Search and place the order\ninstant in few steps.",
          seeARealTimeTrackingOfYourCourier:
              "See a real time tracking of your\ncourier on the map during\ndelivery.",
        ),
      ),
      home: AppLocalizations_Labels_Home(
        text: AppLocalizations_Labels_Home_Text(
          myOrders: "My Orders",
          enableToReceiveOrder: "Enable to receive orders",
          availablility: "Availability",
        ),
      ),
      filePicker: AppLocalizations_Labels_FilePicker(
        text: AppLocalizations_Labels_FilePicker_Text(
          gallery: "Gallery",
          camera: "Camera",
          useCamera: "Use Camera",
          maxFiles: "Max files",
          useGallery: "Use Gallery",
          uploadDocument: "Upload Document",
          uploadImage: "Upload Image",
          uploadSuccessfully: "Uploaded Successfully",
          upload: "Upload",
          select: "Select",
          document: "Document",
          image: "Image",
          done: "Done",
          error: "Error",
        ),
      ),
      orders: AppLocalizations_Labels_Orders(
        text: AppLocalizations_Labels_Orders_Text(
          noOrderFound: "No Order found",
          request: "Request",
          shopAddress: "Shop Address",
          deliveryAddress: "Delivery Address",
          orderId: "Order ID",
          earning: "Earning",
          dated: "Dated",
          address: "Address",
          trackOrder: "Track Order",
          pickUp: "Pick Up",
          myOrder: "My Order",
          active: "Active",
          incoming: "Incoming",
          completed: "Completed",
          orderPlaced: "Order Placed",
          orderAcceptedBy: "Order Accepted By",
          orderCanceledBy: "Order Canceled By",
          onProcess: "On Process",
          onTheWay: "On the way",
          reachedAt: "Reached at",
          orderPicked: "Order Picked",
          reachedAtCustomerPlace: "Reached at Customer place",
          delivered: "Delivered",
          canceledByCustomer: "Canceled by Customer",
          canceledByYou: "Canceled by you",
          anyOtherReason: "Any other reason",
          startJourney: "Start Journey",
          reachedVendor: "Reached Vendor",
          reachedCustomer: "Reached Customer",
          completeDelivery: "Complete Delivery",
        ),
      ),
    ),
  };

  final AppLocalizations_Labels labels;

  static AppLocalizations_Labels of(BuildContext context) =>
      Localizations.of<AppLocalizations>(context, AppLocalizations)?.labels;
}

class AppLocalizations_Labels_SendOtp_Hint {
  const AppLocalizations_Labels_SendOtp_Hint({this.phoneNumber});

  final String phoneNumber;
}

class AppLocalizations_Labels_SendOtp_Button {
  const AppLocalizations_Labels_SendOtp_Button({this.continueText});

  final String continueText;
}

class AppLocalizations_Labels_SendOtp_Text {
  const AppLocalizations_Labels_SendOtp_Text(
      {this.accepthe,
      this.termsAndServices,
      this.limitLess,
      this.signUp,
      this.continueWithOnDemandDriver,
      this.continueWithCommercialVehicleDriver});

  final String accepthe;

  final String termsAndServices;

  final String limitLess;

  final String signUp;

  final String continueWithOnDemandDriver;

  final String continueWithCommercialVehicleDriver;
}

class AppLocalizations_Labels_SendOtp_Error {
  const AppLocalizations_Labels_SendOtp_Error({this.agreeTermsAndCondition});

  final String agreeTermsAndCondition;
}

class AppLocalizations_Labels_SendOtp {
  const AppLocalizations_Labels_SendOtp(
      {this.hint, this.button, this.text, this.error});

  final AppLocalizations_Labels_SendOtp_Hint hint;

  final AppLocalizations_Labels_SendOtp_Button button;

  final AppLocalizations_Labels_SendOtp_Text text;

  final AppLocalizations_Labels_SendOtp_Error error;
}

class AppLocalizations_Labels_VerifyOtp_Button {
  const AppLocalizations_Labels_VerifyOtp_Button({this.verify});

  final String verify;
}

class AppLocalizations_Labels_VerifyOtp_Text {
  const AppLocalizations_Labels_VerifyOtp_Text(
      {this.verifyMobileNumber,
      this.enterOtp,
      this.changeNumber,
      this.resend,
      this.signUp,
      this.enterOtpText});

  final String verifyMobileNumber;

  final String enterOtp;

  final String changeNumber;

  final String resend;

  final String signUp;

  final String enterOtpText;
}

class AppLocalizations_Labels_VerifyOtp {
  const AppLocalizations_Labels_VerifyOtp({this.button, this.text});

  final AppLocalizations_Labels_VerifyOtp_Button button;

  final AppLocalizations_Labels_VerifyOtp_Text text;
}

class AppLocalizations_Labels_EnterDetails_Text {
  const AppLocalizations_Labels_EnterDetails_Text(
      {this.enterDetails, this.enterDetailToProceedFurther, this.signup});

  final String enterDetails;

  final String enterDetailToProceedFurther;

  final String signup;
}

class AppLocalizations_Labels_EnterDetails_Hint {
  const AppLocalizations_Labels_EnterDetails_Hint(
      {this.enterName, this.chooseDob});

  final String enterName;

  final String chooseDob;
}

class AppLocalizations_Labels_EnterDetails_Button {
  const AppLocalizations_Labels_EnterDetails_Button({this.continueText});

  final String continueText;
}

class AppLocalizations_Labels_EnterDetails {
  const AppLocalizations_Labels_EnterDetails(
      {this.text, this.hint, this.button});

  final AppLocalizations_Labels_EnterDetails_Text text;

  final AppLocalizations_Labels_EnterDetails_Hint hint;

  final AppLocalizations_Labels_EnterDetails_Button button;
}

class AppLocalizations_Labels_CompleteProfile_Text {
  const AppLocalizations_Labels_CompleteProfile_Text(
      {this.youAreOffline,
      this.completeProfileToGetOrders,
      this.waitFordocumentVerification,
      this.uploadDocAgain,
      this.receiveOrderText,
      this.profileCompletion,
      this.completeProfileToReceiveOrders,
      this.detailsOfDriver,
      this.bankAccountDetails});

  final String youAreOffline;

  final String completeProfileToGetOrders;

  final String waitFordocumentVerification;

  final String uploadDocAgain;

  final String receiveOrderText;

  final String profileCompletion;

  final String completeProfileToReceiveOrders;

  final String detailsOfDriver;

  final String bankAccountDetails;
}

class AppLocalizations_Labels_CompleteProfile_Error {
  const AppLocalizations_Labels_CompleteProfile_Error(
      {this.verificationPending, this.documentRejected});

  final String verificationPending;

  final String documentRejected;
}

class AppLocalizations_Labels_CompleteProfile_Button {
  const AppLocalizations_Labels_CompleteProfile_Button(
      {this.completeProfile, this.backgroundVerificationDocuments});

  final String completeProfile;

  final String backgroundVerificationDocuments;
}

class AppLocalizations_Labels_CompleteProfile {
  const AppLocalizations_Labels_CompleteProfile(
      {this.text, this.error, this.button});

  final AppLocalizations_Labels_CompleteProfile_Text text;

  final AppLocalizations_Labels_CompleteProfile_Error error;

  final AppLocalizations_Labels_CompleteProfile_Button button;
}

class AppLocalizations_Labels_EnableLocation_Text {
  const AppLocalizations_Labels_EnableLocation_Text(
      {this.locationTitle, this.giveAccess});

  final String locationTitle;

  final String giveAccess;
}

class AppLocalizations_Labels_EnableLocation_Button {
  const AppLocalizations_Labels_EnableLocation_Button({this.allow});

  final String allow;
}

class AppLocalizations_Labels_EnableLocation_Error {
  const AppLocalizations_Labels_EnableLocation_Error(
      {this.pleaseGrantPermission,
      this.permissionDenied,
      this.plsEnableYourLocation});

  final String pleaseGrantPermission;

  final String permissionDenied;

  final String plsEnableYourLocation;
}

class AppLocalizations_Labels_EnableLocation {
  const AppLocalizations_Labels_EnableLocation(
      {this.text, this.button, this.error});

  final AppLocalizations_Labels_EnableLocation_Text text;

  final AppLocalizations_Labels_EnableLocation_Button button;

  final AppLocalizations_Labels_EnableLocation_Error error;
}

class AppLocalizations_Labels_DriverDetails_Text {
  const AppLocalizations_Labels_DriverDetails_Text(
      {this.driverDetails,
      this.noteDetail,
      this.notUploaded,
      this.pending,
      this.completed,
      this.rejected,
      this.vehicleRegistration,
      this.drivingLicence,
      this.licenceNumberPlate,
      this.vehicleInsurance,
      this.licenceNumber,
      this.expiryDate,
      this.plateNumber,
      this.policyNumber,
      this.workPermit,
      this.backgroundVerification,
      this.noteText});

  final String driverDetails;

  final String noteDetail;

  final String notUploaded;

  final String pending;

  final String completed;

  final String rejected;

  final String vehicleRegistration;

  final String drivingLicence;

  final String licenceNumberPlate;

  final String vehicleInsurance;

  final String licenceNumber;

  final String expiryDate;

  final String plateNumber;

  final String policyNumber;

  final String workPermit;

  final String backgroundVerification;

  final String noteText;
}

class AppLocalizations_Labels_DriverDetails_Button {
  const AppLocalizations_Labels_DriverDetails_Button({this.proceed});

  final String proceed;
}

class AppLocalizations_Labels_DriverDetails {
  const AppLocalizations_Labels_DriverDetails({this.text, this.button});

  final AppLocalizations_Labels_DriverDetails_Text text;

  final AppLocalizations_Labels_DriverDetails_Button button;
}

class AppLocalizations_Labels_VehicleRegistration_Text {
  const AppLocalizations_Labels_VehicleRegistration_Text({this.title});

  final String title;
}

class AppLocalizations_Labels_VehicleRegistration_Hint {
  const AppLocalizations_Labels_VehicleRegistration_Hint(
      {this.regno, this.company, this.model, this.color, this.validUpto});

  final String regno;

  final String company;

  final String model;

  final String color;

  final String validUpto;
}

class AppLocalizations_Labels_VehicleRegistration {
  const AppLocalizations_Labels_VehicleRegistration({this.text, this.hint});

  final AppLocalizations_Labels_VehicleRegistration_Text text;

  final AppLocalizations_Labels_VehicleRegistration_Hint hint;
}

class AppLocalizations_Labels_DrivingLicence_Text {
  const AppLocalizations_Labels_DrivingLicence_Text(
      {this.title, this.licenceNumber, this.expiryDate});

  final String title;

  final String licenceNumber;

  final String expiryDate;
}

class AppLocalizations_Labels_DrivingLicence {
  const AppLocalizations_Labels_DrivingLicence({this.text});

  final AppLocalizations_Labels_DrivingLicence_Text text;
}

class AppLocalizations_Labels_VehicleInsurance_Text {
  const AppLocalizations_Labels_VehicleInsurance_Text(
      {this.title, this.policyNumber});

  final String title;

  final String policyNumber;
}

class AppLocalizations_Labels_VehicleInsurance_Hint {
  const AppLocalizations_Labels_VehicleInsurance_Hint(
      {this.validTill, this.validFrom});

  final String validTill;

  final String validFrom;
}

class AppLocalizations_Labels_VehicleInsurance_Error {
  const AppLocalizations_Labels_VehicleInsurance_Error(
      {this.plsEnterInsuranceValidFrom,
      this.plsEnterInsuranceValidTill,
      this.validFromAndValidTillMustBeDiff,
      this.validTillGreaterThanValidFrom});

  final String plsEnterInsuranceValidFrom;

  final String plsEnterInsuranceValidTill;

  final String validFromAndValidTillMustBeDiff;

  final String validTillGreaterThanValidFrom;
}

class AppLocalizations_Labels_VehicleInsurance {
  const AppLocalizations_Labels_VehicleInsurance(
      {this.text, this.hint, this.error});

  final AppLocalizations_Labels_VehicleInsurance_Text text;

  final AppLocalizations_Labels_VehicleInsurance_Hint hint;

  final AppLocalizations_Labels_VehicleInsurance_Error error;
}

class AppLocalizations_Labels_WorkPermit_Text {
  const AppLocalizations_Labels_WorkPermit_Text(
      {this.title, this.plateNumber, this.idNumber, this.or});

  final String title;

  final String plateNumber;

  final String idNumber;

  final String or;
}

class AppLocalizations_Labels_WorkPermit_Hint {
  const AppLocalizations_Labels_WorkPermit_Hint(
      {this.expiryDate, this.capacity});

  final String expiryDate;

  final String capacity;
}

class AppLocalizations_Labels_WorkPermit {
  const AppLocalizations_Labels_WorkPermit({this.text, this.hint});

  final AppLocalizations_Labels_WorkPermit_Text text;

  final AppLocalizations_Labels_WorkPermit_Hint hint;
}

class AppLocalizations_Labels_BackgroundVerification_Text {
  const AppLocalizations_Labels_BackgroundVerification_Text(
      {this.title, this.note});

  final String title;

  final String note;
}

class AppLocalizations_Labels_BackgroundVerification {
  const AppLocalizations_Labels_BackgroundVerification({this.text});

  final AppLocalizations_Labels_BackgroundVerification_Text text;
}

class AppLocalizations_Labels_Common_Text {
  const AppLocalizations_Labels_Common_Text(
      {this.notApplicable,
      this.name,
      this.email,
      this.permissionAlert,
      this.setting,
      this.permissionAlertMessage,
      this.yes,
      this.no,
      this.driver,
      this.ok,
      this.wantToLogOut,
      this.appTitle,
      this.appSubtitle,
      this.userInfoname,
      this.phoneNumber,
      this.verifyEmail,
      this.items,
      this.price,
      this.quantity,
      this.subTotal,
      this.noDataFound,
      this.submit,
      this.mr,
      this.yourAddress,
      this.cartValue,
      this.deliveryAddress,
      this.track,
      this.cancel,
      this.raiseDispute,
      this.orderDetails,
      this.next,
      this.uploadPhoto,
      this.imageRemoved,
      this.fileAdded});

  final String notApplicable;

  final String name;

  final String email;

  final String permissionAlert;

  final String setting;

  final String permissionAlertMessage;

  final String yes;

  final String no;

  final String driver;

  final String ok;

  final String wantToLogOut;

  final String appTitle;

  final String appSubtitle;

  final String userInfoname;

  final String phoneNumber;

  final String verifyEmail;

  final String items;

  final String price;

  final String quantity;

  final String subTotal;

  final String noDataFound;

  final String submit;

  final String mr;

  final String yourAddress;

  final String cartValue;

  final String deliveryAddress;

  final String track;

  final String cancel;

  final String raiseDispute;

  final String orderDetails;

  final String next;

  final String uploadPhoto;

  final String imageRemoved;

  final String fileAdded;
}

class AppLocalizations_Labels_Common_Error {
  const AppLocalizations_Labels_Common_Error(
      {this.somthingWentWrong,
      this.enterYourNumber,
      this.pleasefillOTP,
      this.pleaseEnterEmail,
      this.checkInternet,
      this.enterYourName,
      this.somethingWentWrong,
      this.nameLength,
      this.phoneNumberLength,
      this.nameMaxLength,
      this.numberAlreadyExist,
      this.userBlocked,
      this.nameMinLength,
      this.numberAlreadyRegisteredAsOnDemandDriver,
      this.numberAlreadyRegisteredAsCommercialVehicleDriver,
      this.numberAlreadyRegisteredAsCustomer,
      this.numberAlreadyRegisteredAsVendor,
      this.pleaseEnterYourDob});

  final String somthingWentWrong;

  final String enterYourNumber;

  final String pleasefillOTP;

  final String pleaseEnterEmail;

  final String checkInternet;

  final String enterYourName;

  final String somethingWentWrong;

  final String nameLength;

  final String phoneNumberLength;

  final String nameMaxLength;

  final String numberAlreadyExist;

  final String userBlocked;

  final String nameMinLength;

  final String numberAlreadyRegisteredAsOnDemandDriver;

  final String numberAlreadyRegisteredAsCommercialVehicleDriver;

  final String numberAlreadyRegisteredAsCustomer;

  final String numberAlreadyRegisteredAsVendor;

  final String pleaseEnterYourDob;
}

class AppLocalizations_Labels_Common_Title {
  const AppLocalizations_Labels_Common_Title({this.ms, this.mrs});

  final String ms;

  final String mrs;
}

class AppLocalizations_Labels_Common {
  const AppLocalizations_Labels_Common({this.text, this.error, this.title});

  final AppLocalizations_Labels_Common_Text text;

  final AppLocalizations_Labels_Common_Error error;

  final AppLocalizations_Labels_Common_Title title;
}

class AppLocalizations_Labels_HelpAndSupport_Appbar {
  const AppLocalizations_Labels_HelpAndSupport_Appbar({this.title});

  final String title;
}

class AppLocalizations_Labels_HelpAndSupport_Text {
  const AppLocalizations_Labels_HelpAndSupport_Text(
      {this.previousTicket,
      this.recentTicket,
      this.ticketID,
      this.issueRaisedOn,
      this.bookingID,
      this.serviceName,
      this.bookingDate,
      this.orderId,
      this.orderDate,
      this.ticketStatus,
      this.pending,
      this.closed,
      this.query,
      this.response,
      this.noQueriesFound});

  final String previousTicket;

  final String recentTicket;

  final String ticketID;

  final String issueRaisedOn;

  final String bookingID;

  final String serviceName;

  final String bookingDate;

  final String orderId;

  final String orderDate;

  final String ticketStatus;

  final String pending;

  final String closed;

  final String query;

  final String response;

  final String noQueriesFound;
}

class AppLocalizations_Labels_HelpAndSupport_Button {
  const AppLocalizations_Labels_HelpAndSupport_Button({this.raiseNewTicket});

  final String raiseNewTicket;
}

class AppLocalizations_Labels_HelpAndSupport {
  const AppLocalizations_Labels_HelpAndSupport(
      {this.appbar, this.text, this.button});

  final AppLocalizations_Labels_HelpAndSupport_Appbar appbar;

  final AppLocalizations_Labels_HelpAndSupport_Text text;

  final AppLocalizations_Labels_HelpAndSupport_Button button;
}

class AppLocalizations_Labels_SendQuery_Text {
  const AppLocalizations_Labels_SendQuery_Text(
      {this.pleaseFillTheDetails,
      this.tellUsWhyDoYouNeedHelp,
      this.getInTouch,
      this.fillDetails,
      this.description,
      this.uploadPicture,
      this.addImage,
      this.removeImage,
      this.areYouSureYouWantToRemoveImage});

  final String pleaseFillTheDetails;

  final String tellUsWhyDoYouNeedHelp;

  final String getInTouch;

  final String fillDetails;

  final String description;

  final String uploadPicture;

  final String addImage;

  final String removeImage;

  final String areYouSureYouWantToRemoveImage;
}

class AppLocalizations_Labels_SendQuery_Hint {
  const AppLocalizations_Labels_SendQuery_Hint({this.writeHere});

  final String writeHere;
}

class AppLocalizations_Labels_SendQuery_Button {
  const AppLocalizations_Labels_SendQuery_Button({this.submit});

  final String submit;
}

class AppLocalizations_Labels_SendQuery_Error {
  const AppLocalizations_Labels_SendQuery_Error(
      {this.writeQuery, this.plsEnterTheQuery, this.min50Required});

  final String writeQuery;

  final String plsEnterTheQuery;

  final String min50Required;
}

class AppLocalizations_Labels_SendQuery {
  const AppLocalizations_Labels_SendQuery(
      {this.text, this.hint, this.button, this.error});

  final AppLocalizations_Labels_SendQuery_Text text;

  final AppLocalizations_Labels_SendQuery_Hint hint;

  final AppLocalizations_Labels_SendQuery_Button button;

  final AppLocalizations_Labels_SendQuery_Error error;
}

class AppLocalizations_Labels_Profile_Text {
  const AppLocalizations_Labels_Profile_Text(
      {this.profile,
      this.editProfile,
      this.myEarning,
      this.subtitleEarning,
      this.helpAndSupport,
      this.subtitleHelpAndSupport,
      this.myDocuments,
      this.subtitleDocuments,
      this.termsAndPolicies,
      this.subtitleTermsAndPolicies,
      this.logout});

  final String profile;

  final String editProfile;

  final String myEarning;

  final String subtitleEarning;

  final String helpAndSupport;

  final String subtitleHelpAndSupport;

  final String myDocuments;

  final String subtitleDocuments;

  final String termsAndPolicies;

  final String subtitleTermsAndPolicies;

  final String logout;
}

class AppLocalizations_Labels_Profile {
  const AppLocalizations_Labels_Profile({this.text});

  final AppLocalizations_Labels_Profile_Text text;
}

class AppLocalizations_Labels_EditProfile_Text {
  const AppLocalizations_Labels_EditProfile_Text({this.personalInfo, this.dob});

  final String personalInfo;

  final String dob;
}

class AppLocalizations_Labels_EditProfile_Hint {
  const AppLocalizations_Labels_EditProfile_Hint({this.enterEmail});

  final String enterEmail;
}

class AppLocalizations_Labels_EditProfile_Error {
  const AppLocalizations_Labels_EditProfile_Error({this.enterValidEmail});

  final String enterValidEmail;
}

class AppLocalizations_Labels_EditProfile {
  const AppLocalizations_Labels_EditProfile({this.text, this.hint, this.error});

  final AppLocalizations_Labels_EditProfile_Text text;

  final AppLocalizations_Labels_EditProfile_Hint hint;

  final AppLocalizations_Labels_EditProfile_Error error;
}

class AppLocalizations_Labels_EditPhoneEmail_Text {
  const AppLocalizations_Labels_EditPhoneEmail_Text(
      {this.verifyNumber, this.verifyEmail, this.done});

  final String verifyNumber;

  final String verifyEmail;

  final String done;
}

class AppLocalizations_Labels_EditPhoneEmail_Button {
  const AppLocalizations_Labels_EditPhoneEmail_Button({this.sendOtp});

  final String sendOtp;
}

class AppLocalizations_Labels_EditPhoneEmail_Hint {
  const AppLocalizations_Labels_EditPhoneEmail_Hint({this.sendOtp, this.email});

  final String sendOtp;

  final String email;
}

class AppLocalizations_Labels_EditPhoneEmail {
  const AppLocalizations_Labels_EditPhoneEmail(
      {this.text, this.button, this.hint});

  final AppLocalizations_Labels_EditPhoneEmail_Text text;

  final AppLocalizations_Labels_EditPhoneEmail_Button button;

  final AppLocalizations_Labels_EditPhoneEmail_Hint hint;
}

class AppLocalizations_Labels_StripeAlert_Text {
  const AppLocalizations_Labels_StripeAlert_Text(
      {this.appBar, this.title, this.connectStripe});

  final String appBar;

  final String title;

  final String connectStripe;
}

class AppLocalizations_Labels_StripeAlert {
  const AppLocalizations_Labels_StripeAlert({this.text});

  final AppLocalizations_Labels_StripeAlert_Text text;
}

class AppLocalizations_Labels_Stripe_Text {
  const AppLocalizations_Labels_Stripe_Text({this.stripeAccount});

  final String stripeAccount;
}

class AppLocalizations_Labels_Stripe {
  const AppLocalizations_Labels_Stripe({this.text});

  final AppLocalizations_Labels_Stripe_Text text;
}

class AppLocalizations_Labels_LoginSuccessfully_Text {
  const AppLocalizations_Labels_LoginSuccessfully_Text(
      {this.loginSuccessfully, this.signUpSuccessfully});

  final String loginSuccessfully;

  final String signUpSuccessfully;
}

class AppLocalizations_Labels_LoginSuccessfully {
  const AppLocalizations_Labels_LoginSuccessfully({this.text});

  final AppLocalizations_Labels_LoginSuccessfully_Text text;
}

class AppLocalizations_Labels_Verification_Error {
  const AppLocalizations_Labels_Verification_Error(
      {this.emptyRegNumber,
      this.emptyVehicleCmpny,
      this.emptyVehicleModel,
      this.emptyVehicleColor,
      this.emptyValidity,
      this.emptyImage,
      this.emptyLicenceNumber,
      this.emptyLicenceExpiry,
      this.emptyNumberPlate,
      this.emptyPolicyNumber,
      this.emptyPermitNumberOrIdNumber,
      this.expiryDate});

  final String emptyRegNumber;

  final String emptyVehicleCmpny;

  final String emptyVehicleModel;

  final String emptyVehicleColor;

  final String emptyValidity;

  final String emptyImage;

  final String emptyLicenceNumber;

  final String emptyLicenceExpiry;

  final String emptyNumberPlate;

  final String emptyPolicyNumber;

  final String emptyPermitNumberOrIdNumber;

  final String expiryDate;
}

class AppLocalizations_Labels_Verification {
  const AppLocalizations_Labels_Verification({this.error});

  final AppLocalizations_Labels_Verification_Error error;
}

class AppLocalizations_Labels_Polyline_Text {
  const AppLocalizations_Labels_Polyline_Text(
      {this.shopAddress, this.deliveryAddress});

  final String shopAddress;

  final String deliveryAddress;
}

class AppLocalizations_Labels_Polyline {
  const AppLocalizations_Labels_Polyline({this.text});

  final AppLocalizations_Labels_Polyline_Text text;
}

class AppLocalizations_Labels_OrderDetail_Text {
  const AppLocalizations_Labels_OrderDetail_Text(
      {this.pickUpDestination,
      this.deliveryDestination,
      this.itemDescription,
      this.orderDetails,
      this.totalItems,
      this.raiseDispute,
      this.shareFeedback,
      this.plsGiveYourRating,
      this.shop,
      this.comment,
      this.customer,
      this.skip,
      this.cancel,
      this.accept,
      this.reject});

  final String pickUpDestination;

  final String deliveryDestination;

  final String itemDescription;

  final String orderDetails;

  final String totalItems;

  final String raiseDispute;

  final String shareFeedback;

  final String plsGiveYourRating;

  final String shop;

  final String comment;

  final String customer;

  final String skip;

  final String cancel;

  final String accept;

  final String reject;
}

class AppLocalizations_Labels_OrderDetail {
  const AppLocalizations_Labels_OrderDetail({this.text});

  final AppLocalizations_Labels_OrderDetail_Text text;
}

class AppLocalizations_Labels_CustomerVerification_Text {
  const AppLocalizations_Labels_CustomerVerification_Text(
      {this.customerVerification,
      this.enterTheOtpSentTo,
      this.resendOtp,
      this.completeDelivery});

  final String customerVerification;

  final String enterTheOtpSentTo;

  final String resendOtp;

  final String completeDelivery;
}

class AppLocalizations_Labels_CustomerVerification {
  const AppLocalizations_Labels_CustomerVerification({this.text});

  final AppLocalizations_Labels_CustomerVerification_Text text;
}

class AppLocalizations_Labels_Notifications_Text {
  const AppLocalizations_Labels_Notifications_Text(
      {this.noNotificationFound, this.notifications});

  final String noNotificationFound;

  final String notifications;
}

class AppLocalizations_Labels_Notifications {
  const AppLocalizations_Labels_Notifications({this.text});

  final AppLocalizations_Labels_Notifications_Text text;
}

class AppLocalizations_Labels_MyEarnings_Text {
  const AppLocalizations_Labels_MyEarnings_Text(
      {this.totalCreditsIn,
      this.calculating,
      this.totalEarningOfTheDay,
      this.myEarnings});

  final String totalCreditsIn;

  final String calculating;

  final String totalEarningOfTheDay;

  final String myEarnings;
}

class AppLocalizations_Labels_MyEarnings {
  const AppLocalizations_Labels_MyEarnings({this.text});

  final AppLocalizations_Labels_MyEarnings_Text text;
}

class AppLocalizations_Labels_Intro_Text {
  const AppLocalizations_Labels_Intro_Text(
      {this.searchForAnyItemOnline,
      this.placeTheOrderOnlineInstantly,
      this.liveTrackYourOrder,
      this.quickSearchAndAddFilters,
      this.searchAndPlaceTheOrder,
      this.seeARealTimeTrackingOfYourCourier});

  final String searchForAnyItemOnline;

  final String placeTheOrderOnlineInstantly;

  final String liveTrackYourOrder;

  final String quickSearchAndAddFilters;

  final String searchAndPlaceTheOrder;

  final String seeARealTimeTrackingOfYourCourier;
}

class AppLocalizations_Labels_Intro {
  const AppLocalizations_Labels_Intro({this.text});

  final AppLocalizations_Labels_Intro_Text text;
}

class AppLocalizations_Labels_Home_Text {
  const AppLocalizations_Labels_Home_Text(
      {this.myOrders, this.enableToReceiveOrder, this.availablility});

  final String myOrders;

  final String enableToReceiveOrder;

  final String availablility;
}

class AppLocalizations_Labels_Home {
  const AppLocalizations_Labels_Home({this.text});

  final AppLocalizations_Labels_Home_Text text;
}

class AppLocalizations_Labels_FilePicker_Text {
  const AppLocalizations_Labels_FilePicker_Text(
      {this.gallery,
      this.camera,
      this.useCamera,
      this.maxFiles,
      this.useGallery,
      this.uploadDocument,
      this.uploadImage,
      this.uploadSuccessfully,
      this.upload,
      this.select,
      this.document,
      this.image,
      this.done,
      this.error});

  final String gallery;

  final String camera;

  final String useCamera;

  final String maxFiles;

  final String useGallery;

  final String uploadDocument;

  final String uploadImage;

  final String uploadSuccessfully;

  final String upload;

  final String select;

  final String document;

  final String image;

  final String done;

  final String error;
}

class AppLocalizations_Labels_FilePicker {
  const AppLocalizations_Labels_FilePicker({this.text});

  final AppLocalizations_Labels_FilePicker_Text text;
}

class AppLocalizations_Labels_Orders_Text {
  const AppLocalizations_Labels_Orders_Text(
      {this.noOrderFound,
      this.request,
      this.shopAddress,
      this.deliveryAddress,
      this.orderId,
      this.earning,
      this.dated,
      this.address,
      this.trackOrder,
      this.pickUp,
      this.myOrder,
      this.active,
      this.incoming,
      this.completed,
      this.orderPlaced,
      this.orderAcceptedBy,
      this.orderCanceledBy,
      this.onProcess,
      this.onTheWay,
      this.reachedAt,
      this.orderPicked,
      this.reachedAtCustomerPlace,
      this.delivered,
      this.canceledByCustomer,
      this.canceledByYou,
      this.anyOtherReason,
      this.startJourney,
      this.reachedVendor,
      this.reachedCustomer,
      this.completeDelivery});

  final String noOrderFound;

  final String request;

  final String shopAddress;

  final String deliveryAddress;

  final String orderId;

  final String earning;

  final String dated;

  final String address;

  final String trackOrder;

  final String pickUp;

  final String myOrder;

  final String active;

  final String incoming;

  final String completed;

  final String orderPlaced;

  final String orderAcceptedBy;

  final String orderCanceledBy;

  final String onProcess;

  final String onTheWay;

  final String reachedAt;

  final String orderPicked;

  final String reachedAtCustomerPlace;

  final String delivered;

  final String canceledByCustomer;

  final String canceledByYou;

  final String anyOtherReason;

  final String startJourney;

  final String reachedVendor;

  final String reachedCustomer;

  final String completeDelivery;
}

class AppLocalizations_Labels_Orders {
  const AppLocalizations_Labels_Orders({this.text});

  final AppLocalizations_Labels_Orders_Text text;
}

class AppLocalizations_Labels {
  const AppLocalizations_Labels(
      {this.sendOtp,
      this.verifyOtp,
      this.enterDetails,
      this.completeProfile,
      this.enableLocation,
      this.driverDetails,
      this.vehicleRegistration,
      this.drivingLicence,
      this.vehicleInsurance,
      this.workPermit,
      this.backgroundVerification,
      this.common,
      this.helpAndSupport,
      this.sendQuery,
      this.profile,
      this.editProfile,
      this.editPhoneEmail,
      this.stripeAlert,
      this.stripe,
      this.loginSuccessfully,
      this.verification,
      this.polyline,
      this.orderDetail,
      this.customerVerification,
      this.notifications,
      this.myEarnings,
      this.intro,
      this.home,
      this.filePicker,
      this.orders});

  final AppLocalizations_Labels_SendOtp sendOtp;

  final AppLocalizations_Labels_VerifyOtp verifyOtp;

  final AppLocalizations_Labels_EnterDetails enterDetails;

  final AppLocalizations_Labels_CompleteProfile completeProfile;

  final AppLocalizations_Labels_EnableLocation enableLocation;

  final AppLocalizations_Labels_DriverDetails driverDetails;

  final AppLocalizations_Labels_VehicleRegistration vehicleRegistration;

  final AppLocalizations_Labels_DrivingLicence drivingLicence;

  final AppLocalizations_Labels_VehicleInsurance vehicleInsurance;

  final AppLocalizations_Labels_WorkPermit workPermit;

  final AppLocalizations_Labels_BackgroundVerification backgroundVerification;

  final AppLocalizations_Labels_Common common;

  final AppLocalizations_Labels_HelpAndSupport helpAndSupport;

  final AppLocalizations_Labels_SendQuery sendQuery;

  final AppLocalizations_Labels_Profile profile;

  final AppLocalizations_Labels_EditProfile editProfile;

  final AppLocalizations_Labels_EditPhoneEmail editPhoneEmail;

  final AppLocalizations_Labels_StripeAlert stripeAlert;

  final AppLocalizations_Labels_Stripe stripe;

  final AppLocalizations_Labels_LoginSuccessfully loginSuccessfully;

  final AppLocalizations_Labels_Verification verification;

  final AppLocalizations_Labels_Polyline polyline;

  final AppLocalizations_Labels_OrderDetail orderDetail;

  final AppLocalizations_Labels_CustomerVerification customerVerification;

  final AppLocalizations_Labels_Notifications notifications;

  final AppLocalizations_Labels_MyEarnings myEarnings;

  final AppLocalizations_Labels_Intro intro;

  final AppLocalizations_Labels_Home home;

  final AppLocalizations_Labels_FilePicker filePicker;

  final AppLocalizations_Labels_Orders orders;
}
