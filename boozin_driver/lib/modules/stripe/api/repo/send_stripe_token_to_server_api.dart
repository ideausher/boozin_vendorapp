import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';

// used to send token from stripe to attached with merchant
class SendStripeTokenToServerApi {
  // Api
  Future<dynamic> sendStripeTokenToServerApiCall({BuildContext context, String code}) async {
    var path = "v1/linkAccount";

    var result;
    result = await AppConfig.of(context).baseApi.postRequest(
      path,
      context,
      data: {"token_account": code, "country": "ZA", "account_type": "custom"},
    );
    print("linkAccount called");

    return result;
  }
}
