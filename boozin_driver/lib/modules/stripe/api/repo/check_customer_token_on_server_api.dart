import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';

// used to check whether vendor linked to vendor or not
class CheckCustomerTokenOnServerApi {
  // Api
  Future<dynamic> checkCustomerTokenOnServerApiCall({BuildContext context}) async {
    var path = "v1/accountLinked";

    var result = await AppConfig.of(context).baseApi.getRequest(
      path,
      context,
    );

    return result;
  }
}
