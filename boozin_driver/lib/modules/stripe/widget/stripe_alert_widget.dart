import 'package:boozin_driver/modules/bank_details/bank_routes.dart';
import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/auth_routes.dart';
import 'package:boozin_driver/modules/auth/constants/image_constant.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/routes.dart';
import '../../../localizations.dart';
import '../bloc/stripe_linking_bloc.dart';
import '../bloc/stripe_linking_event.dart';
import '../bloc/stripe_linking_state.dart';

class StripeAlertWidget extends StatefulWidget {
  BuildContext context;
  GlobalKey<ScaffoldState> scaffoldVerificationOtpKey;

  StripeAlertWidget(this.context /*this.scaffoldVerificationOtpKey*/);

  @override
  _StripeAlertWidgetState createState() => _StripeAlertWidgetState();
}

class _StripeAlertWidgetState extends State<StripeAlertWidget> {
  var _stripeBloc = StripeLinkingBloc();
  StripeLinkingState _stripeState;
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    _stripeBloc.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // check internet connection
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(widget.context, showNetworkDialog: true)
        .then((value) {
      if (value) {
        // fire an event to check
        _stripeBloc.emitEvent(CheckStripeTokenAvailableOnServerEvent(
            isLoading: true,
            context: widget.context,
            isStripeTokenAvailableOnServer: false));
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocEventStateBuilder<StripeLinkingState>(
      bloc: _stripeBloc,
      builder: (BuildContext context, StripeLinkingState stripeState) {
        if (_stripeState != stripeState) {
          _stripeState = stripeState;

          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (_stripeState?.commonResponseModel != null) {
              if (_stripeState?.commonResponseModel?.status != 200) {
                _showAlertDialog(context);
              } else {
                NavigatorUtils.navigatorUtilsInstance
                    .navigatorClearStack(context, AuthRoutes.HOME_SCREEN_ROOT);
              }
            }
          });
        }

        return ModalProgressHUD(
            inAsyncCall: _stripeState?.isLoading ?? false,
            child: Scaffold(
                backgroundColor: Colors.white,
                body: (_stripeState?.commonResponseModel != null &&
                        _stripeState?.commonResponseModel?.status != 200)
                    ? Container(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppLocalizations.of(context)
                                  .stripeAlert
                                  .text
                                  .appBar,
                              style: AppConfig.of(context)
                                  .themeData
                                  .textTheme
                                  .headline6,
                            ),
                            Text(
                              AppLocalizations.of(context)
                                  .stripeAlert
                                  .text
                                  .title,
                              textAlign: TextAlign.center,
                            ),
                            Image.asset(STRIPE_LOGO,
                                width: CommonUtils.commonUtilsInstance
                                    .getPercentageSize(
                                        context: context,
                                        percentage: SIZE_60,
                                        ofWidth: true)),
                            _showStripeConnectButton()
                          ],
                        ),
                      )
                    : SizedBox()));
      },
    );
  }

  void _showAlertDialog(BuildContext context) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
        context: context,
        title: AppLocalizations.of(context).stripeAlert.text.appBar,
        titleTextStyle: AppConfig.of(context).themeData.textTheme.bodyText2,
        subTitle: AppLocalizations.of(context).stripeAlert.text.title,
        positiveButton: AppLocalizations.of(context).common.text.ok,
        onPositiveButtonTab: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          NavigatorUtils.navigatorUtilsInstance
              .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
      /*    NavigatorUtils.navigatorUtilsInstance
              .navigatorClearStack(context, Routes.STRIPE_LINKING_SCREEN);*/
        });
  }

  _showStripeConnectButton() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_6, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_50, ofWidth: true),
        child: RaisedGradientButton(
          radious: SIZE_30,
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
          ),
          onPressed: () {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
          /*  NavigatorUtils.navigatorUtilsInstance
                .navigatorClearStack(context, Routes.STRIPE_LINKING_SCREEN);*/
          },
          child: Text(
            AppLocalizations.of(context).stripeAlert.text.connectStripe,
            textAlign: TextAlign.center,
            style: AppConfig.of(context).themeData.textTheme.subtitle1,
          ),
        ),
      ),
    );
  }
}
