

import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';

class StripeLinkingState extends BlocState {
  StripeLinkingState({
    this.isLoading: false,
    this.commonResponseModel,
    this.isStripeTokenAvailableOnServer,
  }) : super(isLoading);

  final bool isLoading;
  final CommonResponseModel commonResponseModel;
  final bool isStripeTokenAvailableOnServer;

  factory StripeLinkingState.pageLoading(
      {bool isLoading, CommonResponseModel commonResponseModel, bool isStripeTokenAvailableOnServer}) {
    return StripeLinkingState(
      isLoading: isLoading,
      commonResponseModel: commonResponseModel,
      isStripeTokenAvailableOnServer: isStripeTokenAvailableOnServer,
    );
  }

  factory StripeLinkingState.checkStripeTokenAvailableOnServer(
      {bool isLoading, CommonResponseModel commonResponseModel, bool isStripeTokenAvailableOnServer}) {
    return StripeLinkingState(
        isLoading: isLoading,
        commonResponseModel: commonResponseModel,
        isStripeTokenAvailableOnServer: isStripeTokenAvailableOnServer);
  }

    factory StripeLinkingState.sendStripeTokenToServer(
      {bool isLoading, CommonResponseModel commonResponseModel, bool isStripeTokenAvailableOnServer}) {
    return StripeLinkingState(
        isLoading: isLoading,
        commonResponseModel: commonResponseModel,
        isStripeTokenAvailableOnServer: isStripeTokenAvailableOnServer);
  }
}
