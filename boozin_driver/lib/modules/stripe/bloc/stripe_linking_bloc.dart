import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:boozin_driver/modules/stripe/api/repo/check_customer_token_on_server_api.dart';
import 'package:boozin_driver/modules/stripe/api/repo/send_stripe_token_to_server_api.dart';

import '../bloc/stripe_linking_event.dart';
import 'stripe_linking_state.dart';

class StripeLinkingBloc
    extends BlocEventStateBase<StripeLinkingEvent, StripeLinkingState> {
  StripeLinkingBloc({bool isLoading = false})
      : super(
            initialState: StripeLinkingState.pageLoading(isLoading: isLoading));

  @override
  Stream<StripeLinkingState> eventHandler(
      StripeLinkingEvent event, StripeLinkingState currentState) async* {
    if (event is StripeLinkingPageLoaderEvent) {
      print("the loader is ${event?.isLoading}");
      // used to update ui
      yield StripeLinkingState.pageLoading(
          isLoading: event.isLoading,
          commonResponseModel: event.commonResponseModel,
          isStripeTokenAvailableOnServer: event.isStripeTokenAvailableOnServer);
    }

    if (event is RemoveStripeConnectAlertEvent) {
      // used to update ui
      yield StripeLinkingState.pageLoading(
          isLoading: false,
          commonResponseModel: event.commonResponseModel,
          isStripeTokenAvailableOnServer: true);
    }

    // used to check whether vendor account linked or not
    if (event is CheckStripeTokenAvailableOnServerEvent) {
      // used to update ui so that loader can show
      yield StripeLinkingState.pageLoading(
          isLoading: true,
          commonResponseModel: event.commonResponseModel,
          isStripeTokenAvailableOnServer: event.isStripeTokenAvailableOnServer);

      // call api
      var result = await CheckCustomerTokenOnServerApi()
          .checkCustomerTokenOnServerApiCall(
        context: event
            .context, //event.userModel?.userData?.uploadedDriverDocuments[0],
      );

      CommonResponseModel model;
      bool isStripeTokenAvailableOnServer = true;
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          model = CommonResponseModel.fromJson(result);
          isStripeTokenAvailableOnServer = true /*result["linked"]*/;
        }
        // failure case
        else {
          model = CommonResponseModel.fromJson(result);
          isStripeTokenAvailableOnServer = false;
        }
      } else {
        isStripeTokenAvailableOnServer = false;
      }

      yield StripeLinkingState.checkStripeTokenAvailableOnServer(
        isLoading: false,
        commonResponseModel: model,
        isStripeTokenAvailableOnServer: isStripeTokenAvailableOnServer,
      );
    }

    // used to link vendor account with merhchant account
    if (event is SendStripeTokenToServerEvent) {
      // used to update ui so that loader can show
      yield StripeLinkingState.pageLoading(
          isLoading: true,
          commonResponseModel: event.commonResponseModel,
          isStripeTokenAvailableOnServer: event.isStripeTokenAvailableOnServer);

      // call api
      var result =
          await SendStripeTokenToServerApi().sendStripeTokenToServerApiCall(
        context: event.context,
        code: event.code,
      );

      print('The code is ${event?.code}');
      // check api response
      CommonResponseModel model;
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          model = CommonResponseModel(
              status: ApiStatus.Success.value, message: result["message"]);
        }
        // failure case
        else {
          model = CommonResponseModel.fromJson(result);
        }
      } else {
        model = CommonResponseModel(
//          message: AppLocalizations.of(event.context).common.error.somthingWentWrong,
          message: "",
          status: 0,
        );
      }

      yield StripeLinkingState.sendStripeTokenToServer(
          isLoading: false,
          commonResponseModel: model,
          isStripeTokenAvailableOnServer: event.isStripeTokenAvailableOnServer);
    }
  }
}
