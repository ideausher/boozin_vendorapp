import 'package:flutter/material.dart';

import 'page/stripe_linking_web_view_page.dart';

class StripeVendorRoutes {

  static const String STRIPE_LINKING_SCREEN = '/stripe_linking_screen';
  static const String STRIPE_ALERT_SCREEN = '/stripe_alert_screen';

  static Map<String, WidgetBuilder> routes() {
    return {
      // When we navigate to the "/" route, build the FirstScreen Widget

      STRIPE_LINKING_SCREEN: (context) => StripeLinkingWebViewScreen(context),

    };
  }
}