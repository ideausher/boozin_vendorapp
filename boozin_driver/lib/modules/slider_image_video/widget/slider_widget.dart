import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:boozin_driver/modules/auth/auth_routes.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/intro/constant/image_constant.dart';
import '../../../modules/common/constants/dimens_constants.dart';
import '../../../modules/common/utils/image_utils.dart';
import '../../../modules/slider_image_video/enums/slider_image_video_enum.dart';
import '../../../modules/slider_image_video/model/slider_model.dart';

class SliderWidget extends StatefulWidget {
  BuildContext context;
  Function onPressed;
  SliderImageVideoModel sliderImageVideoModel;

  SliderWidget(
      {@required this.sliderImageVideoModel,
      @required this.context,
      this.onPressed});

  @override
  _SliderWidgetState createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  SliderImageVideoModel _sliderImageVideoModel;
  int _current = 0;
  GlobalKey<CarouselSliderState> _globalKey;
  CarouselControllerImpl controller;

  @override
  void initState() {
    super.initState();
    _globalKey = new GlobalKey<CarouselSliderState>();
    if (widget.sliderImageVideoModel != null) {
      _sliderImageVideoModel = widget.sliderImageVideoModel;
    } else {
      _sliderImageVideoModel = ModalRoute.of(widget.context).settings.arguments;
    }
    controller = new CarouselControllerImpl();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Stack(
        children: <Widget>[
          Material(
            color: Colors.white,
            child: CarouselSlider.builder(
              key: _globalKey,
              carouselController: controller,
              itemCount: _sliderImageVideoModel?.sliderModelList?.length,
              options: CarouselOptions(
                  aspectRatio: 2.0,
                  height: double.maxFinite,

                  enableInfiniteScroll: false,

                  viewportFraction: 1.0,
                  autoPlay: _sliderImageVideoModel?.autoPlay,
                  scrollDirection: _sliderImageVideoModel?.scrollDirection,
                  enlargeCenterPage: _sliderImageVideoModel?.enlargeCenterPage,
                  onPageChanged: (index, reason) {
                      setState(() {
                      print("the idex is $index");
                      _current = index;
                    });
                  }),
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.zero,
                  child: _sliderImageVideoModel
                              ?.sliderModelList[index]?.mediaType ==
                          MediaType.Image
                      ? _showImageView(
                          _sliderImageVideoModel?.sliderModelList[index],
                          index,
                          context,
                        )
                      : _showVideoView(
                          _sliderImageVideoModel?.sliderModelList[index],
                          index,
                          context),
                );
              },
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: double.maxFinite,
              //color: Colors.grey.withOpacity(0.8),
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _sliderImageVideoModel?.sliderModelList?.map((url) {
                  int index =
                      _sliderImageVideoModel?.sliderModelList.indexOf(url);
                  return Container(
                    width: SIZE_9,
                    height: SIZE_9,
                    margin: EdgeInsets.symmetric(
                        vertical: SIZE_10, horizontal: SIZE_2),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
                      color: _current == index
                          ? COLOR_PRIMARY /*Color.fromRGBO(0, 0, 0, 0.9)*/
                          : Colors.white /*Color.fromRGBO(0, 0, 0, 0.4)*/,
                    ),
                  );
                }).toList(),
              ),
            ),
          )
        ],
      );
    });
  }

  //method to return image View
  _showImageView(SliderModel sliderMode, int index, BuildContext context) {
    return InkWell(
        onTap: () {
          if (widget?.onPressed != null) widget?.onPressed(sliderMode);
        },
        child: Container(
          padding: EdgeInsets.all(SIZE_16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        child: SvgPicture.asset(
                          sliderMode.url,
                          fit:BoxFit.fitHeight ,
                        ),
                      )),
                ),
              ),
              Expanded(
                flex:1,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(sliderMode.titleText,
                          style: textStyleSize24GreenColor),
                      SizedBox(
                        height: SIZE_8,
                      ),
                      Text(
                        sliderMode.subTitleText,
                        style: textStyleSize15BlackColor,
                      )
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  if (index == 2) {
                    NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                      context,
                      AuthRoutes.SEND_OTP_SCREEN,
                    );
                  } else {
                    controller?.jumpToPage(++index);
                    setState(() {
                      _current = index;
                    });
                  }
                },
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    margin: EdgeInsets.only(right: SIZE_10),

                    height: CommonUtils.commonUtilsInstance
                        .getPercentageSize(
                            context: context,
                            percentage: SIZE_28,
                            ofWidth: false),
                    width: CommonUtils.commonUtilsInstance
                        .getPercentageSize(
                            context: context,
                            percentage: SIZE_28,
                            ofWidth: true),
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(CIRCLE_BG)),
                      shape: BoxShape.circle,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: SIZE_10),
                      child: Icon(
                        Icons.arrow_forward,
                        color: Colors.white,
                        size: SIZE_20,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ));
  }

  //method to show video view
  _showVideoView(SliderModel sliderMode, int index, BuildContext context) {
    return InkWell(
      onTap: () {
        if (widget?.onPressed != null) widget?.onPressed(sliderMode);
      },
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          //for thumbnail
          ImageUtils.imageUtilsInstance.showCacheNetworkImage(
              url: sliderMode.videoThumbnail,
              context: context,
              placeHolderImage: sliderMode.placeHolderImage),
          Opacity(
            opacity: 0.5,
            child: Center(
              child: FloatingActionButton(
                onPressed: () {},
                child: Icon(
                  Icons.play_arrow,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
