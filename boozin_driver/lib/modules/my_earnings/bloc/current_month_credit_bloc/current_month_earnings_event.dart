import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';


abstract class CurrentMonthEarningsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final String selectedDate;

  CurrentMonthEarningsEvent({
    this.isLoading: false,
    this.context,
    this.selectedDate,
  });
}

// for get completed services
class GetCurrentMonthCompletedServicesListEvent extends CurrentMonthEarningsEvent {
  GetCurrentMonthCompletedServicesListEvent({
    BuildContext context,
    bool isLoading,
    String selectedDate,
  }) : super(
          context: context,
          isLoading: isLoading,
          selectedDate: selectedDate,
        );
}
