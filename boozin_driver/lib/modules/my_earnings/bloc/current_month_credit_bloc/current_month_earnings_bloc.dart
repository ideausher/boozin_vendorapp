import 'package:intl/intl.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:boozin_driver/modules/my_earnings/api/get_all_completed_services/provider/get_current_month_completed_services_provider.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';

import '../../../../localizations.dart';
import 'current_month_earnings_event.dart';
import 'current_month_earnings_state.dart';

class CurrentMonthEarningsBloc extends BlocEventStateBase<
    CurrentMonthEarningsEvent, CurrentMonthEarningsState> {
  CurrentMonthEarningsBloc({bool isLoading = false})
      : super(
            initialState:
                CurrentMonthEarningsState.initiating(isLoading: isLoading));

  @override
  Stream<CurrentMonthEarningsState> eventHandler(
      CurrentMonthEarningsEvent event,
      CurrentMonthEarningsState currentState) async* {
    // used for the get completed services
    if (event is GetCurrentMonthCompletedServicesListEvent) {
      yield CurrentMonthEarningsState.updateUi(
        isLoading: false,
        context: event.context,
      );

      // used for the api call
      var result = await GetCurrentMonthCompletedServicesProvider()
          .getCurrentMonthCompletedServices(
        context: event.context,
        selectedDate: event.selectedDate,
      );
      DateTime _selectedDate = CustomDateUtils.dateUtilsInstance
          .convertStringToDate(dateTime: event.selectedDate);
      String _selectedMonth = DateFormat("MMMM").format(_selectedDate);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          List<Order> _orderListingResponseModel =
              (result[ApiStatusParams.Data.value] as List)
                  .map((itemWord) => Order.fromMap(itemWord))
                  .toList();
          // _responseServiceList.sort((a, b) {
          //   return CustomDateUtils.dateUtilsInstance
          //       .convertStringToDateTime(
          //           dateTime: b.bookingDetails?.bookingStartTime)
          //       .compareTo(CustomDateUtils.dateUtilsInstance.convertStringToDateTime(
          //           dateTime: a.bookingDetails?.bookingStartTime));
          // });

          num _totalEarningOfDay = 0;

          if (_orderListingResponseModel?.isNotEmpty == true) {
            for (var service in _orderListingResponseModel) {
              _totalEarningOfDay = _totalEarningOfDay +
                  (service?.deliveryChargeToDeliveryBoy ?? 0.0) +
                  (service?.tipToDeliveryBoy ?? 0.0) +
                  (service?.amountPaidAtOrderDelivery ?? 0.0) +
                  (service?.amountPaidAtOrderPickUp ?? 0.0);
            }
          }
          yield CurrentMonthEarningsState.updateUi(
            isLoading: false,
            context: event.context,
            orderList: _orderListingResponseModel,
            totalEarningOfDay: _totalEarningOfDay,
            monthName: _selectedMonth,
          );
        } else {
          yield CurrentMonthEarningsState.updateUi(
            isLoading: false,
            context: event.context,
            message: result[ApiStatusParams.Message.value],
            monthName: _selectedMonth,
          );
        }
      } else {
        yield CurrentMonthEarningsState.updateUi(
          isLoading: false,
          context: event.context,
          message:
              AppLocalizations.of(event.context).common.error.somthingWentWrong,
          monthName: _selectedMonth,
        );
      }
    }
  }
}
