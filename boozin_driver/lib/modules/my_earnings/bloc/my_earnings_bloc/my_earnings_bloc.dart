import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:boozin_driver/modules/my_earnings/api/get_all_completed_services/provider/get_current_day_completed_services_provider.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';

import '../../../../localizations.dart';
import 'my_earnings_event.dart';
import 'my_earnings_state.dart';

class MyEarningsBloc
    extends BlocEventStateBase<MyEarningsEvent, MyEarningsState> {
  MyEarningsBloc({bool isLoading = false})
      : super(initialState: MyEarningsState.initiating(isLoading: isLoading));

  @override
  Stream<MyEarningsState> eventHandler(
      MyEarningsEvent event, MyEarningsState currentState) async* {
    // used for the get completed services
    if (event is GetCompletedServicesListEvent) {
      yield MyEarningsState.updateUi(
        isLoading: true,
        context: event.context,
      );

      // used for the api call
      var result = await GetCurrentDayCompletedServicesProvider()
          .getAllCompletedServices(
        context: event.context,
        selectedDate: event.selectedDate,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          List<Order> _responseOrderList =
              (result[ApiStatusParams.Data.value] as List)
                  .map((itemWord) => Order.fromMap(itemWord))
                  .toList();
          // _responseOrderList.sort((a, b) {
          //   return CustomDateUtils.dateUtilsInstance
          //       .convertStringToDateTime(
          //           dateTime: b.orderDetails?.bookingStartTime)
          //       .compareTo(CustomDateUtils.dateUtilsInstance.convertStringToDateTime(
          //           dateTime: a.bookingDetails?.bookingStartTime));
          // });

          num _totalEarningOfDay = 0;

          if (_responseOrderList?.isNotEmpty == true) {
            for (var service in _responseOrderList) {
              _totalEarningOfDay = _totalEarningOfDay +
                  (service?.deliveryChargeToDeliveryBoy ?? 0.0) +
                  (service?.tipToDeliveryBoy ?? 0.0) +
                  (service?.amountPaidAtOrderPickUp ?? 0.0) +
                  (service?.amountPaidAtOrderDelivery ?? 0.0);
            }
          }
          yield MyEarningsState.updateUi(
            isLoading: false,
            context: event.context,
            orderList: _responseOrderList,
            totalEarningOfDay: _totalEarningOfDay,
          );
        } else {
          yield MyEarningsState.updateUi(
            isLoading: false,
            context: event.context,
            message: result[ApiStatusParams.Message.value],
          );
        }
      } else {
        yield MyEarningsState.updateUi(
          isLoading: false,
          context: event.context,
          message:
              AppLocalizations.of(event.context).common.error.somthingWentWrong,
        );
      }
    }
  }
}
