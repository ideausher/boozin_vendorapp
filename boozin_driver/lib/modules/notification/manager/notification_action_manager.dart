import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/notification/api/model/notification_request_model.dart';
import 'package:boozin_driver/modules/notification/notification_bloc/notification_bloc.dart';
import 'package:boozin_driver/modules/notification/notification_bloc/notification_event.dart';
import 'package:boozin_driver/modules/notification/notification_bloc/notification_state.dart';



class NotificationActionManager {
  BuildContext context;
  NotificationBloc notificationBloc;
  NotificationState notificationState;

  // for get notification list
  actionTogetNotificationList({
    ScaffoldState scaffoldState,
    int page,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context,
            showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        NotificationRequestModel _notificationRequestModel =
            NotificationRequestModel(
          page: page,
        );
        notificationBloc.emitEvent(
          FetchNotification(
            context: context,
            isLoading: true,
            notificationRequestModel: _notificationRequestModel,
            notificationResponseModel:
                notificationState?.notificationResponseModel,
          ),
        );
      }
    });
  }

  // for state change in notification list screen
  actionOnNotificationScreenStateChange({
    ScaffoldState scaffoldState,
  }) {
    if (notificationState?.message?.toString()?.trim()?.isNotEmpty == true &&
        notificationState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context,
          scaffoldState: scaffoldState,
          message: notificationState?.message?.toString()?.trim(),
        );
      });
    }
  }
}
