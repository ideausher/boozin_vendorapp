import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/notification/manager/notification_count_manager.dart';
import 'package:boozin_driver/modules/notification/notification_count/notification_count_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/image_constants.dart';


import '../../../routes.dart';

class NotificationReadCountWidget extends StatefulWidget {
  BuildContext context;
  bool rounded = false;

  NotificationReadCountWidget({this.context, this.rounded = false});

  @override
  _NotificationReadCountWidgetState createState() =>
      _NotificationReadCountWidgetState();
}

class _NotificationReadCountWidgetState
    extends State<NotificationReadCountWidget> implements PushReceived {
  NotificationCountManager _notificationCountManager =
      new NotificationCountManager();

  @override
  void initState() {
    super.initState();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _notificationCountManager?.context = widget.context;
    _notificationCountManager?.actionToGetNotificationCount();
  }

  @override
  void didUpdateWidget(covariant NotificationReadCountWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("didUpdateWidget of notification");
    //  _notificationCountManager?.actionToGetNotificationCount();
  }

  @override
  void dispose() {
    _notificationCountManager?.notificationCountBloc?.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _notificationCountManager?.context = context;
    return BlocEventStateBuilder<NotificationCountState>(
      bloc: _notificationCountManager?.notificationCountBloc,
      builder: (BuildContext context,
          NotificationCountState notificationCountState) {
        if (notificationCountState != null &&
            _notificationCountManager?.notificationCountState !=
                notificationCountState) {
          _notificationCountManager?.notificationCountState =
              notificationCountState;
        }
        return ModalProgressHUD(
          inAsyncCall:
              _notificationCountManager?.notificationCountState?.isLoading ??
                  false,
          child: (_notificationCountManager?.notificationCountState?.count !=
                      null &&
                  _notificationCountManager?.notificationCountState?.count > 0)
              ? InkWell(
                  onTap: () async {
                    var result = await NavigatorUtils.navigatorUtilsInstance
                        .navigatorPushedNameResult(
                            context, Routes.NOTIFICATION);
                    _notificationCountManager?.actionToGetNotificationCount();
                  },
                  child: Stack(
                    children: <Widget>[
                      (widget.rounded)
                          ? _roundedNotification()
                          : Container(
                              margin: EdgeInsets.only(right: SIZE_12),
                              alignment: Alignment.center,
                              height: SIZE_30,
                              width: SIZE_30,
                              child: _normalNotification()),
                      Visibility(
                        visible: _notificationCountManager
                                ?.notificationCountState?.count >
                            0,
                        child: Positioned(
                          top: 0,
                          right: 0,
                          child: Container(
                            padding: EdgeInsets.all(SIZE_2),
                            child: Text(
                              _notificationCountManager
                                      ?.notificationCountState?.count
                                      .toString() ??
                                  "",
                              style: TextStyle(
                                  fontSize: SIZE_10, color: Colors.white),
                            ),
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                color: COLOR_ACCENT,
                                border: Border.all(color: COLOR_ACCENT)),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Visibility(
                  visible: _notificationCountManager
                          ?.notificationCountState?.isLoading ==
                      false,
                  child: InkWell(
                    onTap: () async {
                      var result = await NavigatorUtils.navigatorUtilsInstance
                          .navigatorPushedNameResult(
                              context, Routes.NOTIFICATION);
                      _notificationCountManager?.actionToGetNotificationCount();
                    },
                    child: (widget.rounded)
                        ? _roundedNotification()
                        : _normalNotification(),
                  ),
                ),
        );
      },
    );
  }

  Widget _roundedNotification() {
    return Container(
      margin: EdgeInsets.only(right: SIZE_12),
      alignment: Alignment.center,
      height: SIZE_30,
      width: SIZE_30,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
      ),
      child: SvgPicture.asset(
        NOTIFICATION_ICON,
        height: SIZE_14,
        width: SIZE_14,
      ),
    );
  }

  Widget _normalNotification() {
    return SvgPicture.asset(
      NOTIFICATION_ICON,
      height: SIZE_14,
      width: SIZE_14,
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _notificationCountManager?.actionToGetNotificationCount();
  }
}
