

import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/notification/api/provider/notification_provider.dart';


import 'notification_count_event.dart';
import 'notification_count_state.dart';

class NotificationCountBloc extends BlocEventStateBase<NotificationCountEvent, NotificationCountState> {
  NotificationCountBloc({
    bool isLoading = false,
  }) : super(
            initialState: NotificationCountState.initiating(
          isLoading: isLoading,
        ));

  @override
  Stream<NotificationCountState> eventHandler(
      NotificationCountEvent event, NotificationCountState currentState) async* {
    // used to update studio list
    if (event is GetNotificationCountEvent) {
      yield NotificationCountState.updateUi(
          isLoading: false,
          context: event.context,
          count: event?.count ?? 0
      );
      int _count = event?.count ?? 0;

      var result = await NotificationProvider().fetchNotificationCountApiCall(context: event.context);

      if (result != null) {
// check result status
        if (result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          if (result["data"] != null) {
            _count = result["data"]["unreadCount"];
          }

        }

      }

      yield NotificationCountState.updateUi(
        isLoading: false,
        context: event.context,
        count: _count,
      );
    }
  }
}
