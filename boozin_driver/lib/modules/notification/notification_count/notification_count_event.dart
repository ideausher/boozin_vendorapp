import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

abstract class NotificationCountEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int count;

  NotificationCountEvent({
    this.isLoading: false,
    this.context,
    this.count,
  });
}

// used for the get studio listing
class GetNotificationCountEvent extends NotificationCountEvent {
  GetNotificationCountEvent({
    BuildContext context,
    bool isLoading,
    int count,
  }) : super(
          isLoading: isLoading,
          context: context,
          count: count,
        );
}
