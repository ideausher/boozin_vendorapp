import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

class NotificationCountState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final int count;

  NotificationCountState({
    this.isLoading,
    this.context,
    this.count,
  }) : super(isLoading);

  // not authenticated
  factory NotificationCountState.initiating({
    bool isLoading,
  }) {
    return NotificationCountState(
      isLoading: isLoading,
    );
  }

  factory NotificationCountState.updateUi({
    bool isLoading,
    BuildContext context,
    int count,
  }) {
    return NotificationCountState(
      isLoading: isLoading,
      context: context,
      count: count,
    );
  }
}
