// To parse this JSON data, do
//
//     final locationRequestModel = locationRequestModelFromJson(jsonString);

import 'dart:convert';



class LocationRequestModel {
  double latitude;
  double longitude;
  double accuracy;
  double altitude;
  double speed;
  double speedAccuracy;
  double heading;
  double time;

  LocationRequestModel({
    this.latitude,
    this.longitude,
    this.accuracy,
    this.altitude,
    this.speed,
    this.speedAccuracy,
    this.heading,
    this.time,
  });

  factory LocationRequestModel.fromJson(Map<String, dynamic> json) => LocationRequestModel(
        latitude: json["latitude"] == null ? null : json["latitude"]?.toDouble(),
        longitude: json["longitude"] == null ? null : json["longitude"]?.toDouble(),
        accuracy: json["accuracy"] == null ? null : json["accuracy"]?.toDouble(),
        altitude: json["altitude"] == null ? null : json["altitude"]?.toDouble(),
        speed: json["speed"] == null ? null : json["speed"]?.toDouble(),
        speedAccuracy: json["speed_accuracy"] == null ? null : json["speed_accuracy"]?.toDouble(),
        heading: json["heading"] == null ? null : json["heading"]?.toDouble(),
        time: json["time"] == null ? null : json["time"]?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "latitude": latitude == null ? null : latitude?.toString(),
        "longitude": longitude == null ? null : longitude?.toString(),
        "accuracy": accuracy == null ? null : accuracy,
        "altitude": altitude == null ? null : altitude,
        "speed": speed == null ? null : speed,
        "speed_accuracy": speedAccuracy == null ? null : speedAccuracy,
        "heading": heading == null ? null : heading,
        "time": time == null ? null : time,
      };
}
