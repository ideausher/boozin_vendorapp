import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/model.dart';
import 'package:geolocator/geolocator.dart';
import 'package:boozin_driver/modules/common/model/user_current_location_model.dart';

import '../../../modules/common/enum/enums.dart';
import '../../../modules/common/model/common_response_model.dart';
import '../../../modules/current_location_updator/api/model/location_request_model.dart';
import '../../../modules/current_location_updator/api/repo/location_api.dart';

class CurrentLocationManger {
  static CurrentLocationManger _locationManger = CurrentLocationManger();

  static CurrentLocationManger get locationMangerInstance => _locationManger;
  List<LocationRequestModel> locationRequestModelList =
      List<LocationRequestModel>();

  // used to update ui on location
  List<LocationChangeCallBack> _locationCallback = List();

  // used to add callback
  void addLocationCallback(LocationChangeCallBack callBack) {
    _locationCallback.add(callBack);
  }

  double calculateDistance({lat1, lon1, lat2, lon2}) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  // used to remove callback
  void removeLocationCallback(LocationChangeCallBack callBack) {
    _locationCallback.remove(callBack);
  }

  Future<CurrentLocation> getAddressUsingLocation(
      BuildContext context, Position myLocation) async {
    if (myLocation != null) {
      final coordinates =
          new Coordinates(myLocation.latitude, myLocation.longitude);
      var addresses =
          await Geocoder?.local?.findAddressesFromCoordinates(coordinates);

      if (addresses != null) {
        var first = addresses.first;
        CurrentLocation currentLocation = CurrentLocation(
          country: first?.countryName ?? "",
          lat: coordinates?.latitude ?? "",
          lng: coordinates?.longitude ?? "",
          city: first?.locality ?? "",
          postalCode: first?.postalCode,
          currentAddress: first?.addressLine ?? "",
        );
        return currentLocation;
      }
    }
  }

  // used to check service enabled or not
  Future<bool> serviceEnabledOrNot() async {
    var _serviceEnabled = await Geolocator.isLocationServiceEnabled();
    return _serviceEnabled;
  }

  // used to check permission available or not
  Future<LocationPermission> permissionEnabledOrNot() async {
    var _permissionGranted = await Geolocator.checkPermission();
    return _permissionGranted;
  }

  // used to check service enabled or not

  Future<bool> serviceEnabled() async {
    var _serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await Geolocator.openLocationSettings();
    }
    return _serviceEnabled;
  }

  // used to check permission granted or not
  Future<LocationPermission> permissionEnabled() async {
    var _permissionGranted = await Geolocator.checkPermission();
    if (_permissionGranted == LocationPermission.denied ||
        _permissionGranted == LocationPermission.deniedForever) {
      _permissionGranted = await Geolocator.requestPermission();
    }

    if (_permissionGranted == LocationPermission.whileInUse ||
        _permissionGranted == LocationPermission.always)
      await setLocationCallBack();
    return _permissionGranted;
  }

  // used to get current location data
  Future<Position> getCurrentLocationData() async {
    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  // used to set location call  back
  setLocationCallBack() async {
    StreamSubscription<Position> positionStream = Geolocator.getPositionStream(
            intervalDuration: Duration(seconds: 20),
            distanceFilter: 200,
            desiredAccuracy: LocationAccuracy.high)
        .listen((Position position) {
      print("CurrentLocationUpdated ==> ${position}");
      _locationCallback?.forEach((locationCallback) {
        locationCallback?.onLocationChanged(currentLocation: position);
      });
    });
  }

  // used to init location
  initLocationManager() async {
    var _serviceEnabled = await serviceEnabled();
    var _permissionEnabled = await permissionEnabled();
  }
}

// location call  back
class LocationChangeCallBack {
  void onLocationChanged({Position currentLocation}) {}
}

// used to call api whenever location changes
callLocationApi(BuildContext context, LocationRequestModel model) async {
  // call api
  var result = await LocationApi()
      .callLocationApiCall(currentLocationModel: model, context: context);
  print("LOCATION Request=${model.toJson()}");
  String message;
  if (result != null) {
    // check result status
    if (result[ApiStatusParams.Status.value] != null &&
        result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
      CommonResponseModel commonModel = CommonResponseModel.fromJson(result);
      message = commonModel.message;
    }
    // failure case
    else {
      message = result[ApiStatusParams.Message.value];
    }
  } else {
//    message = AppLocalizations.of(context).common.error.somthingWentWrong;
  }
  print("===Message===${message}");
}
