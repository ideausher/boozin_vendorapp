import 'dart:io';

import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/force_update/dialog/force_update_dialog/force_update_dialog.dart';
import 'package:package_info/package_info.dart';

class ForceUpdateActionManager {
  void showForceUpdateDialog({
    BuildContext context,
    bool forcefullyUpdate,
    String message,
  }) {
    showGeneralDialog(
      context: context,
      barrierDismissible: false,
      pageBuilder: (_, __, ___) {
        return ForceUpdateDialog(
          url: (Platform.isAndroid)
              ? "https://play.google.com/store/apps/details?id=com.boozin.driver"
              : "https://apps.apple.com/us/app/",
          message: message,
          forcefullyUpdate: forcefullyUpdate,
          context: context,
        );
      },
    );
  }

  // used to get the need to update or not
  Future<bool> isNeedToUpdate(num latestVersion) async {
    PackageInfo _packageInfo = await PackageInfo.fromPlatform();

    return (int.parse(_packageInfo.buildNumber) < latestVersion) ? true : false;
  }
}
