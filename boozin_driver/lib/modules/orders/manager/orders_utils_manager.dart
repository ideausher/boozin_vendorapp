import 'package:flutter/cupertino.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/enum/order_enum.dart';
import 'package:tuple/tuple.dart';

class OrdersUtilsManager {
  static OrdersUtilsManager _ordersUtilsManager = OrdersUtilsManager();

  static OrdersUtilsManager get ordersUtilsManagerInstance =>
      _ordersUtilsManager;

  // used to get the order status
  String getOrderStatus({Order order, BuildContext context}) {
    if (order?.status == OrderStatus.orderPlaced.value) {
      return AppLocalizations.of(context).orders.text.orderPlaced;
    } else if (order?.status == OrderStatus.orderAcceptedByVendor.value) {
      return "${AppLocalizations.of(context).orders.text.orderAcceptedBy} ${order?.shopDetails?.name}";
    } else if (order?.status == OrderStatus.orderRejectedByVendor.value) {
      return "${AppLocalizations.of(context).orders.text.orderCanceledBy} ${order?.shopDetails?.name}";
    } else if (order?.status == OrderStatus.orderInProgress.value) {
      return AppLocalizations.of(context).orders.text.onProcess;
    } else if (order?.status == OrderStatus.deliveryBoyAssigned.value) {
      return AppLocalizations.of(context).orders.text.onProcess;
    } else if (order?.status == OrderStatus.journeyStarted.value) {
      return AppLocalizations.of(context).orders.text.onTheWay;
    } else if (order?.status == OrderStatus.reachedVendor.value) {
      return "${AppLocalizations.of(context).orders.text.reachedAt} ${order?.shopDetails?.name}";
    } else if (order?.status == OrderStatus.orderPicked.value) {
      return AppLocalizations.of(context).orders.text.orderPicked;
    } else if (order?.status == OrderStatus.reachedCustomer.value) {
      return AppLocalizations.of(context).orders.text.reachedAtCustomerPlace;
    } else if (order?.status == OrderStatus.deliveredProduct.value) {
      return AppLocalizations.of(context).orders.text.delivered;
    } else if (order?.status == OrderStatus.cancelledByCustomer.value) {
      return AppLocalizations.of(context).orders.text.canceledByCustomer;
    } else if (order?.status == OrderStatus.cancelledByDeliveryBoy.value) {
      return AppLocalizations.of(context).orders.text.canceledByYou;
    } else if (order?.status == OrderStatus.other.value) {
      return AppLocalizations.of(context).orders.text.anyOtherReason;
    } else if (order?.status == OrderStatus.autoCancel.value) {
      return "Order Canceled";
    }
    else {
      return "";
    }
  }

  bool canOrderCancel({Order order}) {
    if (order?.status == OrderStatus.cancelledByCustomer?.value ||
        order?.status == OrderStatus.cancelledByDeliveryBoy?.value ||
        order?.status == OrderStatus.autoCancel?.value ||
        order?.status == OrderStatus.orderRejectedByVendor?.value) {
      return false;
    } else if (order?.bookingStatus
            ?.containsKey(OrderStatus.orderPicked?.value?.toString()) ||
        order?.bookingStatus
            ?.containsKey(OrderStatus.reachedCustomer?.value?.toString()) ||
        order?.bookingStatus
            ?.containsKey(OrderStatus.deliveryBoyAssigned?.value?.toString()) ||
        order?.bookingStatus
            ?.containsKey(OrderStatus.deliveredProduct?.value?.toString()) ||
        order?.bookingStatus
            ?.containsKey(OrderStatus.cancelledByCustomer?.value?.toString()) ||
        order?.bookingStatus
            ?.containsKey(OrderStatus.autoCancel?.value?.toString()) ||
        order?.bookingStatus?.containsKey(
            OrderStatus.cancelledByDeliveryBoy?.value?.toString()) ||
        order?.bookingStatus?.containsKey(
            OrderStatus.orderRejectedByVendor?.value?.toString())) {
      return false;
    } else {
      return true;
    }
  }

  // order completed or rejected
  bool orderCompletedOrCanceled({Order order}) {
    if (order?.bookingStatus
            ?.containsKey(OrderStatus.deliveredProduct?.value?.toString()) ||
        order?.bookingStatus
            ?.containsKey(OrderStatus.cancelledByCustomer?.value?.toString()) ||
        order?.bookingStatus?.containsKey(
            OrderStatus.cancelledByDeliveryBoy?.value?.toString()) ||
        order?.bookingStatus?.containsKey(
            OrderStatus.autoCancel?.value?.toString()) ||
        order?.bookingStatus?.containsKey(
            OrderStatus.orderRejectedByVendor?.value?.toString())) {
      return true;
    } else {
      return false;
    }
  }

  //Check order pickup
  bool checkOrderPickedUp(Order order) {
    bool orderPickedUp = false;
    int status = order?.status;
    if (status == OrderStatus.orderAcceptedByVendor.value ||
        status == OrderStatus.orderInProgress.value ||
        status == OrderStatus.deliveryBoyAssigned.value ||
        status == OrderStatus.journeyStarted.value ||
        status == OrderStatus.reachedVendor.value) {
      orderPickedUp = false;
    } else {
      orderPickedUp = true;
    }

    return orderPickedUp;
  }

  //method to get total item count
  int showTotalProductQuantityOrdered({List<OrderDetail> productList}) {
    int totalProductCount = 0;
    for (var product in productList) {
      totalProductCount = totalProductCount + product?.quantity;
    }
    return totalProductCount;
  }

  //method to get total item weight
  num showTotalProductWeight({List<OrderDetail> productList}) {
    num totalProductWeight = 0.0;
    for (var product in productList) {
      totalProductWeight = totalProductWeight + product?.totalWeight;
    }
    return totalProductWeight;
  }

  Tuple2<String, int> getStatusChangeMessage(
      {Order order, BuildContext context}) {
    //   journeyStarted,
    //   reachedVendor,
    //   orderPicked,
    //   reachedCustomer,
    //   deliveredProduct,
    if (orderCompletedOrCanceled(order: order) == true) {
      return Tuple2<String, int>("", -1);
    } else if (order?.bookingStatus
            ?.containsKey(OrderStatus.deliveryBoyAssigned.value?.toString()) ==
        true) {
      // delivery boy assigned
      if (order?.bookingStatus
              ?.containsKey(OrderStatus.journeyStarted.value?.toString()) ==
          false) {
        return Tuple2<String, int>(
            AppLocalizations.of(context).orders.text.startJourney,
            OrderStatus.journeyStarted.value);
      } else if (order?.bookingStatus
              ?.containsKey(OrderStatus.reachedVendor.value?.toString()) ==
          false) {
        return Tuple2<String, int>(
            AppLocalizations.of(context).orders.text.reachedVendor,
            OrderStatus.reachedVendor.value);
      } else if (order?.bookingStatus
              ?.containsKey(OrderStatus.orderPicked.value?.toString()) ==
          false) {
        return Tuple2<String, int>(
            AppLocalizations.of(context).orders.text.orderPicked,
            OrderStatus.orderPicked.value);
      } else if (order?.bookingStatus
              ?.containsKey(OrderStatus.reachedCustomer.value?.toString()) ==
          false) {
        return Tuple2<String, int>(
            AppLocalizations.of(context).orders.text.reachedCustomer,
            OrderStatus.reachedCustomer.value);
      } else if (order?.bookingStatus
              ?.containsKey(OrderStatus.deliveredProduct.value?.toString()) ==
          false) {
        return Tuple2<String, int>(
            AppLocalizations.of(context).orders.text.completeDelivery,
            OrderStatus.deliveredProduct.value);
      } else {
        return Tuple2<String, int>("", -1);
      }
    } else {
      return Tuple2<String, int>("", -1);
    }
  }
}
