import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/orders/bloc/orders_bloc.dart';
import 'package:boozin_driver/modules/orders/bloc/orders_event.dart';
import 'package:boozin_driver/modules/orders/bloc/orders_state.dart';


class OrdersActionManager {
  BuildContext context;
  OrdersBloc ordersBloc;
  OrdersState ordersState;

  // used to perform the action on the tab change
  void actionOnTabChange({int index}) {
    ordersBloc.emitEvent(SelectTabEvent(
      context: context,
      isLoading: false,
      selectedIndex: index,
    ));
  }
}
