import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/tabs/active_tab/bloc/active_orders_bloc.dart';
import 'package:boozin_driver/modules/orders/tabs/active_tab/bloc/active_orders_event.dart';
import 'package:boozin_driver/modules/orders/tabs/active_tab/bloc/active_orders_state.dart';


import '../../../../../routes.dart';
import '../../../order_routes.dart';

class ActiveOrdersActionManager {
  BuildContext context;
  ActiveOrdersBloc activeOrdersBloc;
  ActiveOrdersState activeOrdersState;

  //used to perform the action on the init
  void actionOnInit({
    ScaffoldState scaffoldState,
    int page,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        activeOrdersBloc.emitEvent(GetActiveOrdersEvent(
          context: context,
          isLoading: true,
          page: page,
          listOrders: activeOrdersState?.listOrders,
        ));
      }
    });
  }

  // used to perform the action on the item tap
  void actionOnItemTap(
      {Order order, ScaffoldState scaffoldState, bool raiseAQuery}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        if (raiseAQuery) {
         await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
              context, Routes.SEND_QUERY_PAGE,
              dataToBeSend: order);
        } else {
          await NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
            context,
            OrderRoutes.ORDER_DETAILS,
            dataToBeSend: order,
          );
        }
        actionOnInit(page: 1);
      }
    });
  }

  //used to perform the action on the state change
  void actionOnStateChanged({ScaffoldState scaffoldState}) {
    if (activeOrdersState?.isLoading == false) {
      if (activeOrdersState?.message?.trim()?.isNotEmpty == true) {
        WidgetsBinding.instance.addPostFrameCallback(
          (_) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                message: activeOrdersState?.message,
                scaffoldState: scaffoldState);
          },
        );
      }
    }
  }
}
