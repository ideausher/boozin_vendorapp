import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';


abstract class ActiveOrdersEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int page;
  final List<Order> listOrders;

  ActiveOrdersEvent({
    this.isLoading: false,
    this.context,
    this.page,
    this.listOrders,
  });
}

// for get ongoing orders
class GetActiveOrdersEvent extends ActiveOrdersEvent {
  GetActiveOrdersEvent({
    BuildContext context,
    bool isLoading,
    int page,
    List<Order> listOrders,
  }) : super(
          context: context,
          isLoading: isLoading,
          page: page,
          listOrders: listOrders,
        );
}
