import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';


class ActiveOrdersState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final String message;
  final int page;
  final List<Order> listOrders;

  ActiveOrdersState({
    this.isLoading,
    this.context,
    this.message,
    this.page,
    this.listOrders,
  }) : super(isLoading);

  // not authenticated
  factory ActiveOrdersState.initiating({bool isLoading}) {
    return ActiveOrdersState(
      isLoading: isLoading,
    );
  }

  factory ActiveOrdersState.updateUi({
    bool isLoading,
    BuildContext context,
    String message,
    int page,
    List<Order> listOrders,
  }) {
    return ActiveOrdersState(
      isLoading: isLoading,
      context: context,
      message: message,
      page: page,
      listOrders: listOrders,
    );
  }
}
