import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/tabs/incoming_tab/bloc/incoming_orders_bloc.dart';

import 'package:boozin_driver/modules/orders/tabs/incoming_tab/bloc/incoming_orders_event.dart';
import 'package:boozin_driver/modules/orders/tabs/incoming_tab/bloc/incoming_orders_state.dart';

import '../../../../../localizations.dart';
import '../../../../../routes.dart';
import '../../../order_routes.dart';

class IncomingOrdersActionManager {
  BuildContext context;
  IncomingOrdersBloc incomingOrdersBloc;
  IncomingOrdersState incomingOrdersState;

  //used to perform the action on the init
  void actionOnInit({
    ScaffoldState scaffoldState,
    int page,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        incomingOrdersBloc?.emitEvent(GetIncomingOrdersEvent(
          context: context,
          isLoading: true,
          page: page,
          listOrders: incomingOrdersState?.listOrders,
        ));
      }
    });
  }

  // used to perform the action on the item tap
  void actionOnItemTap(
      {Order order, ScaffoldState scaffoldState, bool raiseAQuery}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        if (raiseAQuery) {
          await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
              context, Routes.SEND_QUERY_PAGE,
              dataToBeSend: order);
        } else {
          await NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
            context,
            OrderRoutes.ORDER_DETAILS,
            dataToBeSend: order,
          );
        }
        actionOnInit(page: 1);
      }
    });
  }

  //used to perform the action on the state change
  void actionOnStateChanged({ScaffoldState scaffoldState}) {
    if (incomingOrdersState?.isLoading == false) {
      if (incomingOrdersState?.message
          ?.trim()
          ?.isNotEmpty == true) {
        WidgetsBinding.instance.addPostFrameCallback(
              (_) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                message: incomingOrdersState?.message,
                scaffoldState: scaffoldState);
          },
        );
      }
    }
  }

  // used to accept or reject order
  void actionOnAcceptOrReject({Order order, bool accept}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        incomingOrdersBloc?.emitEvent(AcceptOrRejectOrder(
          context: context,
          isLoading: true,
          page: incomingOrdersState?.page,
          order: order,
          accept: accept,
          listOrders: incomingOrdersState?.listOrders,
        ));
      }
    });
  }


}
