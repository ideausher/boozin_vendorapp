import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/common_widget/common_image_with_text.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:boozin_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:boozin_driver/modules/common/utils/number_format_utils.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/constant/image_constant.dart';
import 'package:boozin_driver/modules/orders/manager/orders_utils_manager.dart';
import 'package:boozin_driver/modules/orders/tabs/incoming_tab/bloc/incoming_orders_bloc.dart';
import 'package:boozin_driver/modules/orders/tabs/incoming_tab/bloc/incoming_orders_state.dart';
import 'package:boozin_driver/modules/orders/tabs/incoming_tab/manager/incoming_orders_action_manager.dart';

import '../../../../../localizations.dart';

class IncomingOrdersPage extends StatefulWidget {
  BuildContext context;
  ScaffoldState scaffoldState;
  bool raiseANewQuery;

  IncomingOrdersPage({
    this.context,
    this.scaffoldState,
    this.raiseANewQuery,
  });

  @override
  _IncomingOrdersPageState createState() => _IncomingOrdersPageState();
}

class _IncomingOrdersPageState extends State<IncomingOrdersPage>
    implements PushReceived {
  // initialise managers
  IncomingOrdersActionManager _incomingOrdersActionManager =
      IncomingOrdersActionManager();
  OrdersUtilsManager _ordersUtilsManager = OrdersUtilsManager();

  //init bloc
  IncomingOrdersBloc _bloc = IncomingOrdersBloc();

  // initialise controllers
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _incomingOrdersActionManager.context = widget.context;
    _incomingOrdersActionManager.incomingOrdersBloc = _bloc;
    _incomingOrdersActionManager.actionOnInit(
      scaffoldState: widget.scaffoldState,
      page: 1,
    );
    _scrollController.addListener(_scrollListener);
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    super.initState();
  }

  @override
  void dispose() {
    _bloc?.dispose();
    _scrollController?.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    super.dispose();
  }

  // usd for the pagination
  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the bottom");

      _incomingOrdersActionManager.actionOnInit(
        page: _incomingOrdersActionManager.incomingOrdersState.page + 1,
        scaffoldState: widget.scaffoldState,
      );
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the top");
    }
  }

  @override
  Widget build(BuildContext context) {
    _incomingOrdersActionManager.context = context;
    return BlocEventStateBuilder<IncomingOrdersState>(
      bloc: _bloc,
      builder:
          (BuildContext buildContext, IncomingOrdersState incomingOrdersState) {
        _incomingOrdersActionManager.context = context;
        if (incomingOrdersState != null &&
            _incomingOrdersActionManager?.incomingOrdersState !=
                incomingOrdersState) {
          _incomingOrdersActionManager?.incomingOrdersState =
              incomingOrdersState;
          _incomingOrdersActionManager.actionOnStateChanged(
              scaffoldState: widget.scaffoldState);
        }
        // main ui started
        return ModalProgressHUD(
          color: Colors.white,
          inAsyncCall:
              _incomingOrdersActionManager?.incomingOrdersState?.isLoading ??
                  false,
          child: (_incomingOrdersActionManager
                      ?.incomingOrdersState?.listOrders?.isNotEmpty ==
                  true)
              ? _showOrdersHistoryList()
              : (_incomingOrdersActionManager?.incomingOrdersState?.isLoading ==
                      false)
                  ? Center(
                      child: CommonImageWithTextWidget(
                          context: context,
                          localImagePath: NO_INTERNET_ICON,
                          imageHeight: SIZE_15,
                          imageWidth: SIZE_15,
                          textToShow: AppLocalizations.of(
                                  _incomingOrdersActionManager.context)
                              .orders
                              .text
                              .noOrderFound),
                    )
                  : const SizedBox(),
        );
      },
    );
  }

  //this method is used to show active orders list
  Widget _showOrdersHistoryList() {
    return ListView.builder(
        key: PageStorageKey("listing"),
        shrinkWrap: true,
        itemCount: _incomingOrdersActionManager
            ?.incomingOrdersState?.listOrders?.length,
        controller: _scrollController,
        itemBuilder: (BuildContext context, int index) {
          return _getListItem(
              order: _incomingOrdersActionManager
                  ?.incomingOrdersState?.listOrders[index]);
        });
  }

  // used to get the list item
  Widget _getListItem({Order order}) {
    return Padding(
      padding:
          const EdgeInsets.only(left: SIZE_10, right: SIZE_10, bottom: SIZE_12),
      child: InkWell(
        onTap: () {
          _incomingOrdersActionManager.actionOnItemTap(
              order: order,
              scaffoldState: widget.scaffoldState,
              raiseAQuery: widget.raiseANewQuery);
        },
        child: Card(
          elevation: ELEVATION_02,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(SIZE_10),
              ),
              side: BorderSide(width: SIZE_1, color: COLOR_PRIMARY)),
          child: Padding(
            padding: const EdgeInsets.all(SIZE_10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _showOrderStatus(order: order),
                SizedBox(
                  height: SIZE_10,
                ),
                _showOrderDetail(order: order),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //this method will return order id widget view
  Widget _showOrderStatus({Order order}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(SIZE_5)),
            color: COLOR_LIGHT_PINK,
          ),
          padding: EdgeInsets.all(SIZE_6),
          child: Text(
            AppLocalizations.of(_incomingOrdersActionManager.context)
                .orders
                .text
                .request,
            style: textStyleSize14WithDarkOrangeColor,
          ),
        ),
        InkWell(
          onTap: () {
            _incomingOrdersActionManager?.actionOnAcceptOrReject(
                order: order, accept: true);
          },
          child: Text(
            AppLocalizations.of(_incomingOrdersActionManager.context)
                .orderDetail
                .text
                .accept,
            style: textStyleSize14WithGreenColor,
          ),
        ),
        InkWell(
          onTap: () {
            _incomingOrdersActionManager?.actionOnAcceptOrReject(
                order: order, accept: false);
          },
          child: Text(
            AppLocalizations.of(_incomingOrdersActionManager.context)
                .orderDetail
                .text
                .reject,
            style: textStyleSize14WithRedColor,
          ),
        )
      ],
    );
  }

  //method to show Order status
  Widget _showOrderDetail({Order order}) {
    print("order status ${order?.bookingStatus?.toString()}");
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _showDetails(
            title: AppLocalizations.of(_incomingOrdersActionManager.context)
                .orders
                .text
                .shopAddress,
            subTitle: "#${order?.shopDetails?.address ?? ""} "),
        _showDetails(
            title: AppLocalizations.of(_incomingOrdersActionManager.context)
                .orders
                .text
                .deliveryAddress,
            subTitle: "#${order?.deliveryAddress?.formattedAddress ?? ""} "),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _showDetails(
                title: AppLocalizations.of(_incomingOrdersActionManager.context)
                    .orders
                    .text
                    .orderId,
                subTitle: "#${order?.bookingCode ?? ""} "),
            // _showDetails(
            //     title: AppLocalizations.of(_incomingOrdersActionManager.context)
            //         .orders
            //         .text
            //         .earning,
            //     subTitle: NumberFormatUtils.numberFormatUtilsInstance
            //         .formatPriceWithSymbol(
            //         price: ((order?.deliveryChargeToDeliveryBoy ?? 0.0) +
            //             (order?.tipToDeliveryBoy ?? 0.0) +
            //             (order?.amountPaidAtOrderPickUp ?? 0.0) +
            //             (order?.amountPaidAtOrderDelivery ?? 0.0))),
            //     showPaystackText: true),
          ],
        ),
        _showDetails(
            title: AppLocalizations.of(_incomingOrdersActionManager.context)
                .orders
                .text
                .dated,
            subTitle: order?.createdAt?.isNotEmpty == true
                ? CustomDateUtils.dateUtilsInstance.getDateTimeDayFormat(
                    dateTime: order?.createdAt, toLocal: true)
                : ""),
      ],
    );
  }

  //method to show orders details text data
  Widget _showDetails(
      {String title, String subTitle, bool showPaystackText = false}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyleSize14GreyColorFont500,
        ),
        Text(
          subTitle,
          style: AppConfig.of(_incomingOrdersActionManager.context)
              .themeData
              .primaryTextTheme
              .headline1,
        ),
        Visibility(
            visible: showPaystackText == true,
            child: NumberFormatUtils.numberFormatUtilsInstance
                .getPayStackChargesIncludingText())
      ],
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _incomingOrdersActionManager.actionOnInit(
      page: 1,
      scaffoldState: widget.scaffoldState,
    );
  }
}
