import 'package:flutter/material.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/orders/bloc/orders_bloc.dart';
import 'package:boozin_driver/modules/orders/bloc/orders_state.dart';
import 'package:boozin_driver/modules/orders/manager/orders_action_manager.dart';
import 'package:boozin_driver/modules/orders/tabs/active_tab/screen/active_orders_page.dart';
import 'package:boozin_driver/modules/orders/tabs/completed_tab/screen/completed_orders_page.dart';
import 'package:boozin_driver/modules/orders/tabs/incoming_tab/screen/incoming_orders_page.dart';

class OrdersPage extends StatefulWidget {
  BuildContext context;

  OrdersPage(this.context);

  @override
  _OrdersPageState createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  OrdersActionManager _ordersActionManager = OrdersActionManager();
  OrdersBloc _bloc = OrdersBloc();
  bool _raiseANewQuery = false;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    if (ModalRoute.of(widget.context).settings?.arguments != null &&
        ModalRoute.of(widget.context).settings?.arguments is bool)
      _raiseANewQuery = ModalRoute.of(widget.context).settings.arguments;
    _ordersActionManager.context = widget.context;
    _ordersActionManager.ordersBloc = _bloc;
    _ordersActionManager.actionOnTabChange(index: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _ordersActionManager.context = context;
    return BlocEventStateBuilder<OrdersState>(
      bloc: _bloc,
      builder: (BuildContext buildContext, OrdersState ordersState) {
        _ordersActionManager.context = context;
        if (ordersState != null &&
            _ordersActionManager?.ordersState != ordersState) {
          _ordersActionManager?.ordersState = ordersState;
        }
        // main ui started
        return ModalProgressHUD(
          inAsyncCall: _ordersActionManager?.ordersState?.isLoading ?? false,
          child: _showTabBar(),
        );
      },
    );
  }

  //method to show tab bar
  Widget _showTabBar() {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: ELEVATION_0,
          title: _raiseANewQuery
              ? Text(
                  AppLocalizations.of(_ordersActionManager.context).orders.text.myOrder,
                  style: AppConfig.of(_ordersActionManager.context)
                      .themeData
                      .primaryTextTheme
                      .headline3,
                )
              : null,
          backgroundColor: Colors.white,
          leading: _raiseANewQuery
              ? IconButton(
                  onPressed: () {
                    NavigatorUtils.navigatorUtilsInstance
                        .navigatorPopScreen(context);
                  },
                  icon: Icon(Icons.arrow_back),
                )
              : null,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(_raiseANewQuery ? 40 : 0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: TabBar(
                indicatorColor: COLOR_PRIMARY,
                isScrollable: true,
                indicatorSize: TabBarIndicatorSize.label,
                onTap: (int index) {
                  _ordersActionManager.actionOnTabChange(
                    index: index,
                  );
                },
                tabs: [
                  Tab(
                    child: Container(
                      child: Text(
                        AppLocalizations.of(_ordersActionManager.context).orders.text.active,
                        style: textStyleSize14BlackColor,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      AppLocalizations.of(_ordersActionManager.context).orders.text.incoming,
                      style: textStyleSize14BlackColor,
                    ),
                  ),
                  Tab(
                    child: Text(
                      AppLocalizations.of(_ordersActionManager.context).orders.text.completed,
                      style: textStyleSize14BlackColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: TabBarView(
          children: [
            ActiveOrderPage(
              context: _ordersActionManager.context,
              scaffoldState: _scaffoldKey?.currentState,
              raiseAnewQuery: _raiseANewQuery,
            ),
            IncomingOrdersPage(
              context: _ordersActionManager.context,
              scaffoldState: _scaffoldKey?.currentState,
              raiseANewQuery: _raiseANewQuery,
            ),
            CompletedOrdersPage(
              context: _ordersActionManager.context,
              scaffoldState: _scaffoldKey?.currentState,
              raiseANewQuery: _raiseANewQuery,
            ),
          ],
        ),
      ),
    );
  }
}
