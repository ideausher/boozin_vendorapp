// To parse this JSON data, do
//
//     final ratingRequestModel = ratingRequestModelFromJson(jsonString);

import 'dart:convert';

RatingRequestModel ratingRequestModelFromJson(String str) => RatingRequestModel.fromJson(json.decode(str));

String ratingRequestModelToJson(RatingRequestModel data) => json.encode(data.toJson());

class RatingRequestModel {
  RatingRequestModel({
    this.ratings,
    this.bookingId,
  });

  List<Rating> ratings;
  int bookingId;
  factory RatingRequestModel.fromJson(Map<String, dynamic> json) => RatingRequestModel(
    ratings: json["ratings"] == null ? null : List<Rating>.from(json["ratings"].map((x) => Rating.fromJson(x))),
    bookingId: json["booking_id"] == null ? null : json["booking_id"],
  );

  Map<String, dynamic> toJson() => {
    "ratings": ratings == null ? null : List<dynamic>.from(ratings.map((x) => x.toJson())),
    "booking_id": bookingId == null ? null : bookingId,
  };
}

class Rating {
  Rating({
    this.shopId,
    this.rating,
    this.feedback,
    this.deliveryBoyId,
  });

  int shopId;
  num rating;
  String feedback;
  int deliveryBoyId;


  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
    shopId: json["shop_id"] == null ? null : json["shop_id"],
    rating: json["rating"] == null ? null : json["rating"],
    feedback: json["feedback"] == null ? null : json["feedback"],
    deliveryBoyId: json["delivery_boy_id"] == null ? null : json["delivery_boy_id"],

  );

  Map<String, dynamic> toJson() => {
    "shop_id": shopId == null ? null : shopId,
    "rating": rating == null ? null : rating,
    "feedback": feedback == null ? null : feedback,
    "delivery_boy_id": deliveryBoyId == null ? null : deliveryBoyId,

  };
}
