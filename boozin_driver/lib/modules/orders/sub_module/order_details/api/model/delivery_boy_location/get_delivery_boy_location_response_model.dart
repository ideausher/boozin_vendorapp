// To parse this JSON data, do
//
//     final getDeliveryLocationResponseModel = getDeliveryLocationResponseModelFromJson(jsonString);

import 'dart:convert';

GetDeliveryBoyLocationResponseModel getDeliveryLocationResponseModelFromJson(String str) => GetDeliveryBoyLocationResponseModel.fromMap(json.decode(str));

String getDeliveryLocationResponseModelToJson(GetDeliveryBoyLocationResponseModel data) => json.encode(data.toJson());

class GetDeliveryBoyLocationResponseModel {
  GetDeliveryBoyLocationResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory GetDeliveryBoyLocationResponseModel.fromMap(Map<String, dynamic> json) => GetDeliveryBoyLocationResponseModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.lng,
    this.lat,
  });

  String lng;
  String lat;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    lng: json["lng"] == null ? null : json["lng"],
    lat: json["lat"] == null ? null : json["lat"],
  );

  Map<String, dynamic> toJson() => {
    "lng": lng == null ? null : lng,
    "lat": lat == null ? null : lat,
  };
}
