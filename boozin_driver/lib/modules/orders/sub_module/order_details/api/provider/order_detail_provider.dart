import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/api/model/rating/rating_request_model.dart';

class OrderDetailProvider {
  //method to get order details
  Future<dynamic> getOrderDetailApiCall({
    BuildContext context,
    int id,
  }) async {
    var _orderHistory = "booking/detail";

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(_orderHistory, context, queryParameters: {
      "id": id,
    });

    return result;
  }

  //method to get order details
  Future<dynamic> updateOrderApiCall(
      {BuildContext context, int id, int status, String cancelReason}) async {
    var _orderHistory = "booking/update";
    var result = await AppConfig.of(context).baseApi.patchRequest(
        _orderHistory, context, data: {
      "booking_id": id,
      "status": status,
      "reject_reason": cancelReason ?? ""
    });

    return result;
  }

  Future<dynamic> callCustomerRating(
      {BuildContext context, RatingRequestModel ratingRequestModel}) async {
    var _rating = "customer/rating";
    var result = await AppConfig.of(context)
        .baseApi
        .postRequest(_rating, context, data: ratingRequestModel?.toJson());

    return result;
  }

  Future<dynamic> getDeliveryBoyLocation(
      {BuildContext context, int deliveryBoyId}) async {
    var _rating = "getcurrentlocation";
    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(_rating, context, queryParameters: {"user_id": 357});
    return result;
  }
}
