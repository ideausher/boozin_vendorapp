import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';

class OrderDetailsState extends BlocState {
  OrderDetailsState({
    this.isLoading: false,
    this.message,
    this.context,
    this.order,
    this.status,

  }) : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final Order order;
  final int status;


  // used for update order details page state
  factory OrderDetailsState.updateOrderDetails({
    bool isLoading,
    String message,
    BuildContext context,
    Order order,
    int status,
  }) {
    return OrderDetailsState(
      isLoading: isLoading,
      message: message,
      context: context,
      order: order,
      status: status,
    );
  }

  factory OrderDetailsState.initiating() {
    return OrderDetailsState(
      isLoading: false,
    );
  }
}
