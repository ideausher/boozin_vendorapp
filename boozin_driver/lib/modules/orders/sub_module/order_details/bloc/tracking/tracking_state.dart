import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/api/model/delivery_boy_location/get_delivery_boy_location_response_model.dart';

class TackingState extends BlocState {
  TackingState(
      {this.isLoading: false,
      this.message,
      this.context,
      this.order,
      this.getDeliveryLocationResponseModel})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final Order order;
  final GetDeliveryBoyLocationResponseModel getDeliveryLocationResponseModel;

  // used for update order details page state
  factory TackingState.updateTrackingState(
      {bool isLoading,
      String message,
      BuildContext context,
      Order order,
      GetDeliveryBoyLocationResponseModel getDeliveryLocationResponseModel}) {
    return TackingState(
        isLoading: isLoading,
        message: message,
        context: context,
        order: order,
        getDeliveryLocationResponseModel: getDeliveryLocationResponseModel);
  }

  factory TackingState.initiating() {
    return TackingState(
      isLoading: false,
    );
  }
}
