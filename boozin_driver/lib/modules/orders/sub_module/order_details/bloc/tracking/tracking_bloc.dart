import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/api/model/delivery_boy_location/get_delivery_boy_location_response_model.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/api/provider/order_detail_provider.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/bloc/tracking/tracking_event.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/bloc/tracking/tracking_state.dart';


import '../../../../../../localizations.dart';

class TrackingBloc extends BlocEventStateBase<TrackingEvent, TackingState> {
  TrackingBloc({bool initializing = true})
      : super(initialState: TackingState.initiating());

  @override
  Stream<TackingState> eventHandler(TrackingEvent event,
      TackingState currentState) async* {
    //api calling on init state
    if (event is GetOrderDetailsAndDeliveryBoyLocationEvent) {
      String _message = "";
      Order _order = event?.order;
      GetDeliveryBoyLocationResponseModel _deliveryBoyLocationResponseModel =
          event?.getDeliveryBoyLocationResponseModel;

      yield TackingState.updateTrackingState(
          isLoading: false,
          context: event?.context,
          order: event?.order,
          getDeliveryLocationResponseModel:
          event?.getDeliveryBoyLocationResponseModel);

      //here calling order details api
      var _result = await OrderDetailProvider().getOrderDetailApiCall(
        context: event.context,
        id: event.order?.id,
      );

      if (_result != null) {
        // check result status
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _order = Order.fromMap(_result["data"]);
        }
      }

      Position _locationData = await CurrentLocationManger
          .locationMangerInstance
          .getCurrentLocationData();
      _deliveryBoyLocationResponseModel = GetDeliveryBoyLocationResponseModel(
          statusCode: 200,
          message: "",
          data: Data(
              lng: _locationData?.longitude?.toString(),
              lat: _locationData?.latitude?.toString()));

      yield TackingState.updateTrackingState(
          isLoading: false,
          context: event?.context,
          order: _order,
          message: _message,
          getDeliveryLocationResponseModel: _deliveryBoyLocationResponseModel);
    }

    //event to get delivery boy location event
    if (event is GetDeliveryBoyLocationEvent) {
      String _message = "";
      Order _order = event?.order;
      GetDeliveryBoyLocationResponseModel _deliveryBoyLocationResponseModel =
          event?.getDeliveryBoyLocationResponseModel;

      yield TackingState.updateTrackingState(
          isLoading: false,
          context: event?.context,
          order: event?.order,
          getDeliveryLocationResponseModel:
          event?.getDeliveryBoyLocationResponseModel);

      //here calling delivery boy location api to get his lat lng
      Position _locationData = await CurrentLocationManger
          .locationMangerInstance
          .getCurrentLocationData();
      _deliveryBoyLocationResponseModel = GetDeliveryBoyLocationResponseModel(
          statusCode: 200,
          message: "",
          data: Data(
              lng: _locationData?.longitude?.toString(),
              lat: _locationData?.latitude?.toString()));

      yield TackingState.updateTrackingState(
          isLoading: false,
          context: event?.context,
          order: _order,
          message: _message,
          getDeliveryLocationResponseModel: _deliveryBoyLocationResponseModel);
    }

    if (event is UpdateOrderStatusEvent) {
      yield TackingState.updateTrackingState(
          isLoading: true,
          context: event?.context,
          order: event?.order,
          getDeliveryLocationResponseModel:
          event?.getDeliveryBoyLocationResponseModel);

      var _result = await OrderDetailProvider().updateOrderApiCall(
          context: event.context,
          id: event.order?.id,
          status: event?.status
      );

      if (_result != null) {
        // check result status
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          Order _order = Order.fromMap(_result["data"]);
          yield TackingState.updateTrackingState(
              isLoading: false,
              context: event?.context,
              order: _order,
              getDeliveryLocationResponseModel:
              event?.getDeliveryBoyLocationResponseModel);
        }
        // failure case
        else {
          yield TackingState.updateTrackingState(
              isLoading: false,
              context: event?.context,
              order: event?.order,
              message: _result[ApiStatusParams.Message.value],
              getDeliveryLocationResponseModel:
              event?.getDeliveryBoyLocationResponseModel);
        }
      } else {
        yield TackingState.updateTrackingState(
            isLoading: false,
            context: event?.context,
            order: event?.order,
            message: AppLocalizations
                .of(event?.context)
                .common
                .error
                .somethingWentWrong,
            getDeliveryLocationResponseModel:
            event?.getDeliveryBoyLocationResponseModel);
      }
    }
  }
}
