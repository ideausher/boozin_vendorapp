import 'package:boozin_driver/modules/orders/enum/order_enum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/api/model/rating/rating_request_model.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/api/provider/order_detail_provider.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/bloc/order_details/oder_details_state.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/bloc/order_details/order_details_bloc.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/bloc/order_details/order_details_event.dart';

import '../../../../../../localizations.dart';
import '../../../../../../routes.dart';
import '../../../../order_routes.dart';
import 'order_details_widget_manager.dart';

class OrderDetailsActionManager {
  BuildContext context;
  OrderDetailsBloc orderDetailsBloc;
  OrderDetailsState orderDetailsState;

  //used to perform the action on the init
  actionOnInit({Order order}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then(
      (onValue) {
        if (onValue) {
          orderDetailsBloc.emitEvent(GetOrderDetailsEvent(
              isLoading: true, context: context, order: order));
        }
      },
    );
  }

  actionOnUpdateStatusOrder(
      {ScaffoldState scaffoldState, int Status, String cancelReason}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then(
      (onValue) {
        if (onValue) {
          orderDetailsBloc.emitEvent(UpdateStatusEvent(
              isLoading: true,
              context: context,
              status: Status,
              order: orderDetailsState?.order,
              cancelReason: cancelReason));
        }
      },
    );
  }

  // action to verify number
  actionToVerifyCustomerNumber() async {
    await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
        context, OrderRoutes.CUSTOMER_VERIFICATION_PAGE,
        dataToBeSend: orderDetailsState?.order);
    actionOnInit(order: orderDetailsState?.order);
  }

  //action on products list state change
  actionOnOrderDetailsStateChange({
    ScaffoldState scaffoldState,
  }) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) {
        if (orderDetailsState?.isLoading == false) {
          //error case
          if (orderDetailsState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: orderDetailsState?.message);
            print("order details==> ${orderDetailsState?.message}");
          } else {
            if (orderDetailsState?.status ==
                OrderStatus.cancelledByDeliveryBoy.value) {
              NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
            }
          }
        }
      },
    );
  }

  // action to give rating

  actionToRating() {
    OrderDetailsWidgetManager.orderDetailsWidgetManager.showRatingAlertDialog(
        context: context,
        order: orderDetailsState?.order,
        driverFeedBack: new TextEditingController(),
        shopFeedBack: new TextEditingController(),
        orderDetailsActionManager: this);
  }

  // action to raise dispute

  actionToRaiseDispute() {
    NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
        context, Routes.QUERY_LISTING_PAGE,
        dataToBeSend: orderDetailsState?.order);
  }

  // update ui
  actionToUpdateUi({bool loading}) {
    orderDetailsBloc.emitEvent(UpdateOrderDetailEvent(
        isLoading: loading, context: context, order: orderDetailsState?.order));
  }

  // action to rate
  actionToRate({RatingRequestModel requestModel}) async {
    actionToUpdateUi(loading: true);

    var _result = await OrderDetailProvider()
        .callCustomerRating(context: context, ratingRequestModel: requestModel);

    if (_result != null) {
      // check result status
      if (_result[ApiStatusParams.Status.value] != null &&
          _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
        actionOnInit(order: orderDetailsState?.order);
      }
      // failure case
      else {
        actionToUpdateUi(loading: false);
        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
            context: context,
            title: '',
            positiveButton: AppLocalizations.of(context).common.text.ok,
            titleTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
            buttonTextStyle:
                AppConfig.of(context).themeData.textTheme.headline4,
            subTitle: _result[ApiStatusParams.Message.value],
            onPositiveButtonTab: () async {
              NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
            });
      }
    } else {
      actionToUpdateUi(loading: false);
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
          context: context,
          title: '',
          positiveButton: AppLocalizations.of(context).common.text.ok,
          titleTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
          buttonTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
          subTitle:
              AppLocalizations.of(context).common.error.somethingWentWrong,
          onPositiveButtonTab: () async {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          });
    }
  }

  // action on click of track
  actionToTrack() async {
    await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
        context, OrderRoutes.TRACKING_PAGE,
        dataToBeSend: orderDetailsState?.order);
    actionOnInit(order: orderDetailsState?.order);
  }
}
