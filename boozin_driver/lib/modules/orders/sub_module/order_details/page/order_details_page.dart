import 'package:boozin_driver/modules/auth/validator/auth_validator.dart';
import 'package:boozin_driver/modules/common/constants/font_constant.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:boozin_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:boozin_driver/modules/common/utils/image_utils.dart';
import 'package:boozin_driver/modules/common/utils/launcher_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/number_format_utils.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/constant/image_constant.dart';
import 'package:boozin_driver/modules/orders/enum/order_enum.dart';
import 'package:boozin_driver/modules/orders/manager/orders_utils_manager.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/bloc/order_details/oder_details_state.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/bloc/order_details/order_details_bloc.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/managers/order_details/order_details_action_manager.dart';
import 'package:intl/intl.dart';
import 'package:boozin_driver/modules/orders/sub_module/order_details/managers/order_details/order_details_widget_manager.dart';
import 'package:timeline_tile/timeline_tile.dart';

import '../../../../../localizations.dart';
import '../../../order_routes.dart';

class OrderDetailsPage extends StatefulWidget {
  BuildContext context;

  OrderDetailsPage(this.context);

  @override
  _OrderDetailsPageState createState() => _OrderDetailsPageState();
}

class _OrderDetailsPageState extends State<OrderDetailsPage>
    implements PushReceived {
  //Bloc Variables
  OrderDetailsBloc _orderDetailsBloc = new OrderDetailsBloc();
  OrderDetailsActionManager _orderDetailsActionManager =
      OrderDetailsActionManager();
  TextEditingController _orderCancelReasonController =
      new TextEditingController();

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Order _order;

  @override
  void initState() {
    super.initState();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _order = ModalRoute.of(widget.context).settings.arguments;
    _orderDetailsActionManager.context = widget.context;
    _orderDetailsActionManager.orderDetailsBloc = _orderDetailsBloc;
    _orderDetailsActionManager.actionOnInit(order: _order);
  }

  @override
  void dispose() {
    super.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    _orderDetailsBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _orderDetailsActionManager.context = context;
    return BlocEventStateBuilder<OrderDetailsState>(
      bloc: _orderDetailsBloc,
      builder: (BuildContext context, OrderDetailsState orderDetailsState) {
        _orderDetailsActionManager.context = context;
        if (orderDetailsState != null &&
            _orderDetailsActionManager.orderDetailsState != orderDetailsState) {
          _orderDetailsActionManager.orderDetailsState = orderDetailsState;
          _orderDetailsActionManager.actionOnOrderDetailsStateChange(
            scaffoldState: _scaffoldKey?.currentState,
          );

          if (_orderDetailsActionManager?.orderDetailsState?.order != null)
            _order = _orderDetailsActionManager?.orderDetailsState?.order;
        }
        return ModalProgressHUD(
            inAsyncCall: orderDetailsState?.isLoading ?? false,
            child: WillPopScope(
              onWillPop: () {
                return NavigatorUtils.navigatorUtilsInstance
                    .navigatorPopScreen(context);
              },
              child: Scaffold(
                key: _scaffoldKey,
                appBar: _showAppBar(),
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: SIZE_20),
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      alignment: WrapAlignment.center,
                      runSpacing: SIZE_10,
                      spacing: SIZE_2,
                      children: [
                        _showDivider(),
                        _showOrderDetails(),
                        _showOrderCancelReason(),
                        _showCommonHeaderView(
                            titleLeft: AppLocalizations.of(
                                    _orderDetailsActionManager.context)
                                .orderDetail
                                .text
                                .pickUpDestination),
                        _showPickUpAddressView(),
                        _showCommonHeaderView(
                            titleLeft: AppLocalizations.of(
                                    _orderDetailsActionManager.context)
                                .orderDetail
                                .text
                                .deliveryDestination),
                        _showDeliveryAddressView(),
                        _showCommonHeaderView(
                            titleLeft: AppLocalizations.of(
                                    _orderDetailsActionManager.context)
                                .orderDetail
                                .text
                                .itemDescription),
                        _showProductItemsPurchased(),
                        OrderDetailsWidgetManager.orderDetailsWidgetManager
                            .showOutletStoreWidget(
                                context: context,
                                order: _order,
                                onAccept: () {
                                  _orderDetailsActionManager
                                      ?.actionOnUpdateStatusOrder(
                                          scaffoldState:
                                              _scaffoldKey?.currentState,
                                          Status: OrderStatus
                                              .deliveryBoyAssigned.value);
                                },
                                onCancel: () {
                                  _orderDetailsActionManager
                                      ?.actionOnUpdateStatusOrder(
                                          scaffoldState:
                                              _scaffoldKey?.currentState,
                                          Status: OrderStatus
                                              .cancelledByDeliveryBoy.value);
                                },
                                onStatusChange: () {
                                  if (OrdersUtilsManager
                                          .ordersUtilsManagerInstance
                                          .getStatusChangeMessage(
                                              order: _orderDetailsActionManager
                                                      ?.orderDetailsState
                                                      ?.order ??
                                                  _order,
                                              context: context)
                                          ?.item2 ==
                                      OrderStatus.deliveredProduct.value) {
                                    _orderDetailsActionManager
                                        ?.actionToVerifyCustomerNumber();
                                  } else {
                                    _orderDetailsActionManager
                                        ?.actionOnUpdateStatusOrder(
                                            scaffoldState:
                                                _scaffoldKey?.currentState,
                                            Status: OrdersUtilsManager
                                                .ordersUtilsManagerInstance
                                                .getStatusChangeMessage(
                                                    order: _order,
                                                    context: context)
                                                ?.item2);
                                  }
                                }),
                        Visibility(
                          visible: _orderDetailsActionManager
                                      ?.orderDetailsState?.order?.status !=
                                  OrderStatus.orderAcceptedByVendor.value &&
                              _orderDetailsActionManager
                                      ?.orderDetailsState?.order?.status !=
                                  OrderStatus.orderInProgress.value,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              _showRaiseDisputeButton(),

                              //Deleivery boy can cancel order before order picked
                              // Visibility(
                              //   visible: _orderDetailsActionManager
                              //               ?.orderDetailsState
                              //               ?.order
                              //               ?.status ==
                              //       OrderStatus.orderInProgress.value ||
                              //       _orderDetailsActionManager
                              //               ?.orderDetailsState
                              //               ?.order
                              //               ?.status ==
                              //           OrderStatus.deliveryBoyAssigned.value ||
                              //       _orderDetailsActionManager
                              //               ?.orderDetailsState
                              //               ?.order
                              //               ?.status ==
                              //           OrderStatus.journeyStarted.value ||
                              //       _orderDetailsActionManager
                              //               ?.orderDetailsState
                              //               ?.order
                              //               ?.status ==
                              //           OrderStatus.reachedVendor.value,
                              //   child: _showCancelButton(),
                              // )

                              //_shareFeedback()
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: context,
        elevation: ELEVATION_0,
        appBarTitle: AppLocalizations.of(_orderDetailsActionManager.context)
            .orderDetail
            .text
            .orderDetails,
        appBarTitleStyle: AppConfig.of(_orderDetailsActionManager.context)
            .themeData
            .primaryTextTheme
            .headline3,
        popScreenOnTapOfLeadingIcon: true,
        defaultLeadingIcon: Icons.arrow_back,
        actionWidgets: [],
        defaultLeadingIconColor: Colors.black,
        backGroundColor: Colors.transparent);
  }

  //this method is used to show order details
  Widget _showOrderDetails() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_20, right: SIZE_20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _showOrderStatus(),
          SizedBox(
            height: SIZE_20,
          ),
          _showOrderDetail(),
        ],
      ),
    );
  }

  //method to show order status
  Widget _showOrderStatus() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(SIZE_5)),
            color: _order?.status == OrderStatus.cancelledByCustomer.value ||
                    _order?.status ==
                        OrderStatus.cancelledByDeliveryBoy.value ||
                    _order?.status == OrderStatus.orderRejectedByVendor.value
                ? COLOR_LIGHT_RED
                : COLOR_LIGHT_BLUE,
          ),
          padding: EdgeInsets.all(SIZE_6),
          child: Text(
            OrdersUtilsManager.ordersUtilsManagerInstance
                .getOrderStatus(order: _order, context: context),
            style: TextStyle(
                fontSize: textStyleSize14WithBlueColor?.fontSize,
                fontWeight: textStyleSize14WithBlueColor?.fontWeight,
                fontFamily: textStyleSize14WithBlueColor?.fontFamily,
                color:
                    _order?.status == OrderStatus.cancelledByCustomer.value ||
                            _order?.status ==
                                OrderStatus.cancelledByDeliveryBoy.value ||
                            _order?.status ==
                                OrderStatus.orderRejectedByVendor.value
                        ? COLOR_RED
                        : COLOR_DARK_BLUE),
          ),
        ),
      ],
    );
  }

  //method to show Order status
  Widget _showOrderDetail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _showDetails(
                title: AppLocalizations.of(_orderDetailsActionManager.context)
                    .orders
                    .text
                    .orderId,
                subTitle: "#${_order?.bookingCode?.toString() ?? ""}"),
            Visibility(
              visible: OrdersUtilsManager.ordersUtilsManagerInstance
                      .orderCompletedOrCanceled(order: _order) ==
                  true,
              child: _showDetails(
                  title: AppLocalizations.of(_orderDetailsActionManager.context)
                      .orders
                      .text
                      .earning,
                  subTitle: NumberFormatUtils.numberFormatUtilsInstance
                      .formatPriceWithSymbol(
                          price: ((_orderDetailsActionManager.orderDetailsState
                                      ?.order?.deliveryChargeToDeliveryBoy ??
                                  0.0) +
                              (_orderDetailsActionManager.orderDetailsState
                                      ?.order?.tipToDeliveryBoy ??
                                  0.0) +
                              (_orderDetailsActionManager.orderDetailsState
                                      ?.order?.amountPaidAtOrderPickUp ??
                                  0.0) +
                              (_orderDetailsActionManager.orderDetailsState
                                      ?.order?.amountPaidAtOrderDelivery ??
                                  0.0))),
                  showPaystackText: true),
            ),
          ],
        ),
        (_orderDetailsActionManager
                    .orderDetailsState?.order?.bookingDateTime?.isNotEmpty ==
                true)
            ? _showDetails(
                title: "Scheduled At:",
                subTitle:
                    _orderDetailsActionManager.orderDetailsState?.order
                                ?.bookingDateTime?.isNotEmpty ==
                            true
                        ? CustomDateUtils.dateUtilsInstance
                            .getDateAndTimeFormat(
                                dateTime: _orderDetailsActionManager
                                    .orderDetailsState?.order?.bookingDateTime,
                                toLocal: false)
                        : "")
            : const SizedBox(),
        _showDetails(
            title: AppLocalizations.of(_orderDetailsActionManager.context)
                .orders
                .text
                .dated,
            subTitle: _order?.createdAt?.isNotEmpty == true
                ? CustomDateUtils.dateUtilsInstance.getDateTimeDayFormat(
                    dateTime: _order?.createdAt, toLocal: true)
                : ""),
      ],
    );
  }

  //method to show orders details text data
  Widget _showDetails(
      {String title, String subTitle, bool showPaystackText = false}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyleSize14GreyColorFont500,
        ),
        Text(
          subTitle,
          style: AppConfig.of(_orderDetailsActionManager.context)
              .themeData
              .primaryTextTheme
              .headline1,
        ),
        Visibility(
            visible: showPaystackText == true,
            child: NumberFormatUtils.numberFormatUtilsInstance
                .getPayStackChargesIncludingText())
      ],
    );
  }

  Widget _showDivider() {
    return Divider(
      color: COLOR_GREY,
      thickness: SIZE_1,
    );
  }

  //method to show common header view
  Widget _showCommonHeaderView({String titleLeft, String titleRight}) {
    return Container(
      padding: EdgeInsets.only(
          left: SIZE_20, right: SIZE_20, top: SIZE_10, bottom: SIZE_10),
      color: COLOR_GREY,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            titleLeft,
            style: AppConfig.of(_orderDetailsActionManager.context)
                .themeData
                .primaryTextTheme
                .headline1,
          ),
          (titleRight != null)
              ? Text(
                  titleRight,
                  style: textStyleSize12WithPrimary,
                )
              : const SizedBox()
        ],
      ),
    );
  }

  //method to show user address view
  Widget _showPickUpAddressView() {
    return Row(
      children: [
        Expanded(
            child: Container(
          margin: EdgeInsets.only(left: SIZE_20),
          child: Text(
            _order?.shopDetails?.address ?? "",
            style: textStyleSize12WithBlackColor,
          ),
        )),
        OrdersUtilsManager.ordersUtilsManagerInstance
                    .orderCompletedOrCanceled(order: _order) ==
                false
            ? Padding(
                padding: const EdgeInsets.only(left: SIZE_10, right: SIZE_10),
                child: Column(
                  children: [
                    InkWell(
                      child: SvgPicture.asset(
                        TRACK_ICON,
                        color: (OrdersUtilsManager.ordersUtilsManagerInstance
                                    .checkOrderPickedUp(
                                        _orderDetailsActionManager
                                            ?.orderDetailsState?.order) ==
                                false)
                            ? COLOR_PRIMARY
                            : COLOR_GREY_TEXT,
                        height: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_3,
                                ofWidth: false),
                        width: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_3,
                                ofWidth: true),
                      ),
                      onTap: () {
                        if (OrdersUtilsManager.ordersUtilsManagerInstance
                                .checkOrderPickedUp(_orderDetailsActionManager
                                    ?.orderDetailsState?.order) ==
                            false) {
                          _orderDetailsActionManager?.actionToTrack();
                        }
                      },
                    ),
                    SizedBox(
                      height: SIZE_10,
                    ),
                    InkWell(
                      child: SvgPicture.asset(
                        CALL_ICON,
                        color: (OrdersUtilsManager.ordersUtilsManagerInstance
                                    .checkOrderPickedUp(
                                        _orderDetailsActionManager
                                            ?.orderDetailsState?.order) ==
                                false)
                            ? COLOR_PRIMARY
                            : COLOR_GREY_TEXT,
                        height: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_3,
                                ofWidth: false),
                        width: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_3,
                                ofWidth: true),
                      ),
                      onTap: () {
                        if (OrdersUtilsManager.ordersUtilsManagerInstance
                                .checkOrderPickedUp(_orderDetailsActionManager
                                    ?.orderDetailsState?.order) ==
                            false) {
                          LauncherUtils.launcherUtilsInstance.makePhoneCall(
                              phoneNumber: _orderDetailsActionManager
                                  ?.orderDetailsState
                                  ?.order
                                  ?.shopDetails
                                  ?.showOwner
                                  ?.phoneNumber);
                        }
                      },
                    )
                  ],
                ),
              )
            : SizedBox(),
      ],
    );
  }

  //method to show user address view
  Widget _showDeliveryAddressView() {
    return Row(
      children: [
        Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.only(left: SIZE_20),
              child: Text(
                _order?.deliveryAddress?.formattedAddress ?? "",
                style: textStyleSize12WithBlackColor,
              ),
            )),
        OrdersUtilsManager.ordersUtilsManagerInstance
                    .orderCompletedOrCanceled(order: _order) ==
                false
            ? Padding(
                padding: const EdgeInsets.only(left: SIZE_10, right: SIZE_10),
                child: Column(
                  children: [
                    InkWell(
                      child: SvgPicture.asset(
                        TRACK_ICON,
                        color: (OrdersUtilsManager.ordersUtilsManagerInstance
                                    .checkOrderPickedUp(
                                        _orderDetailsActionManager
                                            ?.orderDetailsState?.order) ==
                                true)
                            ? COLOR_PRIMARY
                            : COLOR_GREY_TEXT,
                        height: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_3,
                                ofWidth: false),
                        width: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_3,
                                ofWidth: true),
                      ),
                      onTap: () {
                        if (OrdersUtilsManager.ordersUtilsManagerInstance
                                .checkOrderPickedUp(_orderDetailsActionManager
                                    ?.orderDetailsState?.order) ==
                            true) {
                          _orderDetailsActionManager?.actionToTrack();
                        }
                      },
                    ),
                    SizedBox(
                      height: SIZE_10,
                    ),
                    InkWell(
                      child: SvgPicture.asset(
                        CALL_ICON,
                        color: (OrdersUtilsManager.ordersUtilsManagerInstance
                                    .checkOrderPickedUp(
                                        _orderDetailsActionManager
                                            ?.orderDetailsState?.order) ==
                                true)
                            ? COLOR_PRIMARY
                            : COLOR_GREY_TEXT,
                        height: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_3,
                                ofWidth: false),
                        width: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_3,
                                ofWidth: true),
                      ),
                      onTap: () {
                        if (OrdersUtilsManager.ordersUtilsManagerInstance
                                .checkOrderPickedUp(_orderDetailsActionManager
                                    ?.orderDetailsState?.order) ==
                            true) {
                          LauncherUtils.launcherUtilsInstance.makePhoneCall(
                              phoneNumber: _orderDetailsActionManager
                                  ?.orderDetailsState
                                  ?.order
                                  ?.userDetails
                                  ?.phoneNumber);
                        }
                      },
                    )
                  ],
                ),
              )
            : SizedBox(),
      ],
    );
  }

  // used for the key and value widget
  Widget _getKeyValueWidget({
    String key,
    String value,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          key,
          style: AppConfig.of(_orderDetailsActionManager.context)
              .themeData
              .primaryTextTheme
              .headline4,
        ),
        Text(
          value,
          style: AppConfig.of(_orderDetailsActionManager.context)
              .themeData
              .primaryTextTheme
              .headline4,
        ),
      ],
    );
  }

  // show product items purchased
  Widget _showProductItemsPurchased() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_20, right: SIZE_20),
      child: (_order?.orderDetails?.isNotEmpty == true)
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListView.separated(
                    padding: EdgeInsets.all(SIZE_0),
                    itemCount: _order?.orderDetails?.length,
                    separatorBuilder: (BuildContext context, int index) {
                      return Divider(
                        color: COLOR_BORDER_GREY,
                      );
                    },
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      OrderDetail _orderDetail = _order?.orderDetails[index];
                      return Container(
                        margin: EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                (_orderDetail?.productImage?.fileName?.isNotEmpty == true)
                                    ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                                        context: context,
                                        showProgressBarInPlaceHolder: true,
                                        shape: BoxShape.rectangle,
                                        url:
                                            _orderDetail?.productImage?.fileName ??
                                                "",
                                        height: CommonUtils?.commonUtilsInstance
                                            ?.getPercentageSize(
                                                context: context,
                                                percentage: SIZE_5,
                                                ofWidth: false),
                                        width: CommonUtils.commonUtilsInstance
                                            .getPercentageSize(
                                                context: context,
                                                percentage: SIZE_5,
                                                ofWidth: true))
                                    : SvgPicture.asset("",
                                        height: CommonUtils?.commonUtilsInstance
                                            ?.getPercentageSize(
                                                context: context,
                                                percentage: SIZE_5,
                                                ofWidth: false),
                                        width: CommonUtils.commonUtilsInstance
                                            .getPercentageSize(
                                                context: context,
                                                percentage: SIZE_5,
                                                ofWidth: true)),
                                SizedBox(width: SIZE_20),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            toBeginningOfSentenceCase(
                                              _orderDetail?.productName ??
                                                  'N/A',
                                            ),
                                          ),
                                          Text(
                                            "x${_orderDetail?.quantity?.toString()}" ??
                                                "",
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: SIZE_10,
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              toBeginningOfSentenceCase(
                                                'Product Weight',
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "${_orderDetail?.weight?.toString()} Kg" ??
                                                "",
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      );
                    }),
                Divider(
                  height: SIZE_1,
                  color: COLOR_RATING,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(AppLocalizations.of(
                              _orderDetailsActionManager.context)
                          .orderDetail
                          .text
                          .totalItems),
                      Text(
                          "${OrdersUtilsManager.ordersUtilsManagerInstance.showTotalProductQuantityOrdered(productList: _order?.orderDetails).toString()}" ??
                              "")
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Total Weight'),
                      Text(
                          "${OrdersUtilsManager.ordersUtilsManagerInstance.showTotalProductWeight(productList: _order?.orderDetails).toString()} Kg" ??
                              "")
                    ],
                  ),
                ),
                Divider(height: SIZE_1, color: COLOR_RATING)
              ],
            )
          : const SizedBox(),
    );
  }

  //this method is used to show raise dispute button
  Widget _showRaiseDisputeButton() {
    return Container(
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _orderDetailsActionManager.context,
          ofWidth: true,
          percentage: SIZE_40),
      margin: EdgeInsets.all(SIZE_10),
      child: RaisedButton(
        onPressed: () {
          _orderDetailsActionManager?.actionToRaiseDispute();
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SIZE_80),
          side: BorderSide(color: COLOR_PRIMARY),
        ),
        color: Colors.white,
        padding: EdgeInsets.all(SIZE_0),
        child: Padding(
          padding: const EdgeInsets.only(
              top: SIZE_10, bottom: SIZE_10, left: SIZE_20, right: SIZE_20),
          child: Text(
            AppLocalizations.of(_orderDetailsActionManager.context)
                .orderDetail
                .text
                .raiseDispute,
            textAlign: TextAlign.center,
            style: textStyleSize14TabPrimaryColor,
          ),
        ),
      ),
    );
  }

  Widget _shareFeedback() {
    return Container(
      margin: EdgeInsets.all(SIZE_10),
      child: RaisedButton(
        onPressed: () {
          _orderDetailsActionManager?.actionToRating();
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SIZE_80),
          side: BorderSide(color: COLOR_PRIMARY),
        ),
        color: Colors.white,
        padding: EdgeInsets.all(SIZE_0),
        child: Padding(
          padding: const EdgeInsets.only(
              top: SIZE_10, bottom: SIZE_10, left: SIZE_20, right: SIZE_20),
          child: Text(
            AppLocalizations.of(_orderDetailsActionManager.context)
                .orderDetail
                .text
                .shareFeedback,
            textAlign: TextAlign.center,
            style: textStyleSize14TabPrimaryColor,
          ),
        ),
      ),
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _orderDetailsActionManager.actionOnInit(order: _order);
  }

  //TO show cancel button
  _showCancelButton() {
    return Container(
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _orderDetailsActionManager.context,
          ofWidth: true,
          percentage: SIZE_40),
      margin: EdgeInsets.all(SIZE_10),
      child: RaisedButton(
        onPressed: () {
          actionOnCancelOrder(
              scaffoldState: _scaffoldKey.currentState,
              cancelOrderController: _orderCancelReasonController);
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SIZE_80),
          side: BorderSide(color: COLOR_PRIMARY),
        ),
        color: Colors.white,
        padding: EdgeInsets.all(SIZE_0),
        child: Padding(
          padding: const EdgeInsets.only(
              top: SIZE_10, bottom: SIZE_10, left: SIZE_20, right: SIZE_20),
          child: Text(
            AppLocalizations.of(_orderDetailsActionManager.context)
                .orderDetail
                .text
                .cancel,
            textAlign: TextAlign.center,
            style: textStyleSize14TabPrimaryColor,
          ),
        ),
      ),
    );
  }

  actionOnCancelOrder(
      {ScaffoldState scaffoldState,
      TextEditingController cancelOrderController}) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showCancelAlertDialog(
        context: _orderDetailsActionManager?.orderDetailsState?.context,
        title: 'Cancel Order ',
        negativeButton: 'Confirm',
        positiveButton: 'Cancel',
        buttonTextStyle: textStyleSize14WithRedColor,
        titleTextStyle: textStyleSize14WithRedColor,
        positiveButtonTextStyle: TextStyle(
            color: Color(0xFF34D670),
            fontSize: SIZE_14,
            fontWeight: FontWeight.w600,
            fontFamily: FONT_FAMILY_LATO),
        subTitle: "",
        showWidget: true,
        cancelBookingController: cancelOrderController,
        onNegativeButtonTab: () {
          // check name field is empty or not
          String result = AuthValidator.authValidatorInstance
              .validateCancelOrder(cancelOrderController?.text);
          // if its not empty
          if (result.isEmpty) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
                _orderDetailsActionManager?.orderDetailsState?.context);
            _orderDetailsActionManager?.actionOnUpdateStatusOrder(
                scaffoldState: _scaffoldKey?.currentState,
                Status: OrderStatus.cancelledByDeliveryBoy.value,
                cancelReason: _orderCancelReasonController?.text ?? "");
          } else {
            // cancelOrderController?.text = "";
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
                _orderDetailsActionManager?.orderDetailsState?.context);
            // if error
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: _orderDetailsActionManager?.orderDetailsState?.context,
                scaffoldState: scaffoldState,
                message: result);
          }
        },
        onPositiveButtonTab: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _orderDetailsActionManager?.orderDetailsState?.context);
        });
  }

  //show order cancellation view
  Widget _showOrderCancelReason() {
    if ((_orderDetailsActionManager?.orderDetailsState?.order?.status ==
                OrderStatus.cancelledByCustomer.value ||
            _orderDetailsActionManager?.orderDetailsState?.order?.status ==
                OrderStatus.cancelledByDeliveryBoy.value ||
            _orderDetailsActionManager?.orderDetailsState?.order?.status ==
                OrderStatus.orderRejectedByVendor.value) &&
        _orderDetailsActionManager
                ?.orderDetailsState?.order?.orderRejectReason?.isNotEmpty ==
            true) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _showCommonHeaderView(
              titleLeft: 'Reason of Cancellation', titleRight: ''),
          SizedBox(
            height: SIZE_5,
          ),
          Padding(
            padding: const EdgeInsets.only(left: SIZE_20),
            child: Text(
              '${_orderDetailsActionManager?.orderDetailsState?.order?.orderRejectReason}',
              style: textStyleSize12WithBlackColor,
            ),
          )
        ],
      );
    } else {
      return SizedBox();
    }
  }
}

// return 1;
// case OrderStatus.orderAcceptedByVendor:
// return 2;
// case OrderStatus.orderRejectedByVendor:
// return 3;
// case OrderStatus.orderInProgress:
// return 4;
// case OrderStatus.deliveryBoyAssigned:
// return 5;
// case OrderStatus.journeyStarted:
// return 6;
// case OrderStatus.reachedVendor:
// return 7;
// case OrderStatus.orderPicked:
