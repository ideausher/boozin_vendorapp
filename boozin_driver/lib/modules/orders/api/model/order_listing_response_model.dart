// To parse this JSON data, do
//
//     final orderListingResponseModel = orderListingResponseModelFromMap(jsonString);
import 'dart:convert';

import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';

OrderListingResponseModel orderListingResponseModelFromMap(String str) =>
    OrderListingResponseModel.fromMap(json.decode(str));

String orderListingResponseModelToMap(OrderListingResponseModel data) =>
    json.encode(data.toMap());

class OrderListingResponseModel {
  OrderListingResponseModel({
    this.orders,
    this.message,
    this.statusCode,
  });

  List<Order> orders;
  String message;
  int statusCode;

  factory OrderListingResponseModel.fromMap(Map<String, dynamic> json) =>
      OrderListingResponseModel(
        orders: json["data"] == null
            ? null
            : List<Order>.from(json["data"].map((x) => Order.fromMap(x))),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toMap() => {
        "data": orders == null
            ? null
            : List<dynamic>.from(orders.map((x) => x.toMap())),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}

class Order {
  Order(
      {this.id,
      this.bookingCode,
      this.deliveryBoyId,
      this.bookingType,
      this.bookingDateTime,
      this.amountBeforeDiscount,
      this.discountAmount,
      this.amountAfterDiscount,
      this.deliveryChargeToDeliveryBoy,
      this.deliveryChargeForCustomer,
      this.commissionCharge,
      this.platformCharge,
      this.afterChargeAmount,
      this.stripeCharges,
      this.amountAfterStripeCharges,
      this.tipToDeliveryBoy,
      this.instruction,
      this.status,
      this.orderDetails,
      this.userDetails,
      this.shopDetails,
      this.couponDetails,
      this.deliveryAddress,
      this.deliveryBoyDetails,
      this.bookingStatus,
      this.orderRejectReason,
      this.createdAt,
      this.orderId,
      this.ratings,
      this.amountPaidAtOrderDelivery,
      this.amountPaidAtOrderPickUp});

  int id;
  String orderId;
  String bookingCode;
  int deliveryBoyId;
  int bookingType;
  String orderRejectReason;
  String bookingDateTime;
  num amountBeforeDiscount;
  num discountAmount;
  num amountAfterDiscount;
  num deliveryChargeForCustomer;
  num deliveryChargeToDeliveryBoy;
  num commissionCharge;
  num platformCharge;
  num amountPaidAtOrderPickUp;
  num amountPaidAtOrderDelivery;
  num afterChargeAmount;
  num stripeCharges;
  num amountAfterStripeCharges;
  num tipToDeliveryBoy;
  String instruction;
  int status;
  List<OrderDetail> orderDetails;
  UserDetails userDetails;
  ShopDetailsModel shopDetails;
  CouponData couponDetails;
  DeliveryAddress deliveryAddress;
  DeliveryBoy deliveryBoyDetails;
  Map<String, BookingStatus> bookingStatus;
  String createdAt;
  List<ReviewModel> ratings;

  factory Order.fromMap(Map<String, dynamic> json) => Order(
        id: json["id"] == null ? null : json["id"],
        amountPaidAtOrderPickUp:
            json["amount_paid_to_delivery_boy_on_order_pickup"] == null
                ? null
                : json["amount_paid_to_delivery_boy_on_order_pickup"],
        amountPaidAtOrderDelivery:
            json["amount_paid_to_delivery_boy_on_order_delivery"] == null
                ? null
                : json["amount_paid_to_delivery_boy_on_order_delivery"],
        orderId: json["order_id"] == null ? null : json["order_id"],
        bookingCode: json["booking_code"] == null ? null : json["booking_code"],
        deliveryBoyId:
            json["delivery_boy_id"] == null ? null : json["delivery_boy_id"],
        bookingType: json["booking_type"] == null ? null : json["booking_type"],
        bookingDateTime: json["booking_date_time"] == null
            ? null
            : json["booking_date_time"],
        amountBeforeDiscount: json["amount_before_discount"] == null
            ? null
            : json["amount_before_discount"],
        discountAmount:
            json["discount_amount"] == null ? null : json["discount_amount"],
        amountAfterDiscount: json["amount_after_discount"] == null
            ? null
            : json["amount_after_discount"],
        deliveryChargeToDeliveryBoy:
            json["delivery_charge_to_delivery_boy"] == null
                ? null
                : json["delivery_charge_to_delivery_boy"],
        deliveryChargeForCustomer: json["delivery_charge_for_customer"] == null
            ? null
            : json["delivery_charge_for_customer"],
        commissionCharge: json["commission_charge"] == null
            ? null
            : json["commission_charge"],
        platformCharge:
            json["platform_charge"] == null ? null : json["platform_charge"],
        afterChargeAmount: json["after_charge_amount"] == null
            ? null
            : json["after_charge_amount"],
        stripeCharges: json["stripe_charges"] == null
            ? null
            : json["stripe_charges"].toDouble(),
        orderRejectReason:
            json["reject_reason"] == null ? null : json["reject_reason"],
        amountAfterStripeCharges: json["amount_after_stripe_charges"] == null
            ? null
            : json["amount_after_stripe_charges"],
        tipToDeliveryBoy: json["tip_to_delivery_boy"] == null
            ? null
            : json["tip_to_delivery_boy"],
        instruction: json["instruction"] == null ? null : json["instruction"],
        status: json["status"] == null ? null : json["status"],
        orderDetails: json["order_details"] == null
            ? null
            : List<OrderDetail>.from(
                json["order_details"].map((x) => OrderDetail.fromMap(x))),
        userDetails: json["user_details"] == null
            ? null
            : UserDetails.fromMap(json["user_details"]),
        shopDetails: json["shop_details"] == null
            ? null
            : ShopDetailsModel.fromJson(json["shop_details"]),
        couponDetails: json["coupon_details"] == null
            ? null
            : CouponData.fromJson(json["coupon_details"]),
        deliveryAddress: json["delivery_address"] == null
            ? null
            : DeliveryAddress.fromMap(json["delivery_address"]),
        deliveryBoyDetails: json["delivery_boy_details"] == null
            ? null
            : DeliveryBoy.fromJson(json["delivery_boy_details"]),
        bookingStatus: json["booking_status"] == null
            ? null
            : Map.from(json["booking_status"]).map((k, v) =>
                MapEntry<String, BookingStatus>(k, BookingStatus.fromMap(v))),
        createdAt: json["created_at"] == null ? null : json["created_at"],
        ratings: json["ratings"] == null
            ? null
            : List<ReviewModel>.from(
                json["ratings"].map((x) => ReviewModel.fromJson(x))),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "amount_paid_to_delivery_boy_on_order_pickup":
            amountPaidAtOrderPickUp == null ? null : amountPaidAtOrderPickUp,
        "amount_paid_to_delivery_boy_on_order_delivery":
            amountPaidAtOrderDelivery == null
                ? null
                : amountPaidAtOrderDelivery,
        "order_id": orderId == null ? null : orderId,
        "booking_code": bookingCode == null ? null : bookingCode,
        "delivery_boy_id": deliveryBoyId == null ? null : deliveryBoyId,
        "booking_type": bookingType == null ? null : bookingType,
        "booking_date_time": bookingDateTime == null ? null : bookingDateTime,
        "amount_before_discount":
            amountBeforeDiscount == null ? null : amountBeforeDiscount,
        "discount_amount": discountAmount == null ? null : discountAmount,
        "amount_after_discount":
            amountAfterDiscount == null ? null : amountAfterDiscount,
        "delivery_charge_to_delivery_boy": deliveryChargeToDeliveryBoy == null
            ? null
            : deliveryChargeToDeliveryBoy,
        "delivery_charge_for_customer": deliveryChargeForCustomer == null
            ? null
            : deliveryChargeForCustomer,
        "commission_charge": commissionCharge == null ? null : commissionCharge,
        "platform_charge": platformCharge == null ? null : platformCharge,
        "after_charge_amount":
            afterChargeAmount == null ? null : afterChargeAmount,
        "stripe_charges": stripeCharges == null ? null : stripeCharges,
        "amount_after_stripe_charges":
            amountAfterStripeCharges == null ? null : amountAfterStripeCharges,
        "tip_to_delivery_boy":
            tipToDeliveryBoy == null ? null : tipToDeliveryBoy,
        "instruction": instruction == null ? null : instruction,
        "status": status == null ? null : status,
        "order_details": orderDetails == null
            ? null
            : List<dynamic>.from(orderDetails.map((x) => x.toMap())),
        "user_details": userDetails == null ? null : userDetails.toMap(),
        "shop_details": shopDetails == null ? null : shopDetails.toJson(),
        "coupon_details": couponDetails == null ? null : couponDetails.toJson(),
        "delivery_address":
            deliveryAddress == null ? null : deliveryAddress.toMap(),
        "delivery_boy_details":
            deliveryBoyDetails == null ? null : deliveryBoyDetails.toJson(),
        "booking_status": bookingStatus == null
            ? null
            : Map.from(bookingStatus)
                .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
        "created_at": createdAt == null ? null : createdAt,
        "reject_reason": orderRejectReason == null ? null : orderRejectReason,
        "ratings": ratings == null
            ? null
            : List<dynamic>.from(ratings.map((x) => x.toJson())),
      };
}

class BookingStatus {
  BookingStatus({
    this.id,
    this.bookingId,
    this.reason,
    this.value,
    this.statusValue,
    this.actionTime,
  });

  int id;
  int bookingId;
  String reason;
  String value;
  int statusValue;
  String actionTime;

  factory BookingStatus.fromMap(Map<String, dynamic> json) => BookingStatus(
        id: json["id"] == null ? null : json["id"],
        bookingId: json["bookingId"] == null ? null : json["bookingId"],
        reason: json["reason"] == null ? null : json["reason"],
        value: json["value"] == null ? null : json["value"],
        statusValue: json["status_value"] == null ? null : json["status_value"],
        actionTime: json["action_time"] == null
            ? null
            : CustomDateUtils.dateUtilsInstance.getDateAndTimeFormat(
                dateTime: json["action_time"], toLocal: true),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "bookingId": bookingId == null ? null : bookingId,
        "reason": reason == null ? null : reason,
        "value": value == null ? null : value,
        "status_value": statusValue == null ? null : statusValue,
        "action_time": actionTime == null ? null : actionTime,
      };
}

class DeliveryAddress {
  DeliveryAddress({
    this.id,
    this.userId,
    this.city,
    this.country,
    this.formattedAddress,
    this.additionalInfo,
    this.pincode,
    this.latitude,
    this.longitude,
  });

  int id;
  int userId;
  String city;
  String country;
  String formattedAddress;
  String additionalInfo;
  String pincode;
  num latitude;
  num longitude;

  factory DeliveryAddress.fromMap(Map<String, dynamic> json) => DeliveryAddress(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        city: json["city"] == null ? null : json["city"],
        country: json["country"] == null ? null : json["country"],
        formattedAddress: json["formatted_address"] == null
            ? null
            : json["formatted_address"],
        additionalInfo:
            json["additional_info"] == null ? null : json["additional_info"],
        pincode: json["pincode"] == null ? null : json["pincode"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "city": city == null ? null : city,
        "country": country == null ? null : country,
        "formatted_address": formattedAddress == null ? null : formattedAddress,
        "additional_info": additionalInfo == null ? null : additionalInfo,
        "pincode": pincode == null ? null : pincode,
        "longitude": longitude == null ? null : longitude,
        "latitude": latitude == null ? null : latitude,
      };
}

class OrderDetail {
  OrderDetail(
      {this.id,
      this.bookingId,
      this.productName,
      this.productDescription,
      this.price,
      this.discount,
      this.quantity,
      this.totalWeight,
      this.weight,
      this.newPrice,
      this.productImage});

  int id;
  int bookingId;
  String productName;
  String productDescription;
  num price;
  num discount;
  num newPrice;
  num quantity;
  ProductImage productImage;
  num totalWeight;
  num weight;

  factory OrderDetail.fromMap(Map<String, dynamic> json) => OrderDetail(
        id: json["id"] == null ? null : json["id"],
        bookingId: json["bookingId"] == null ? null : json["bookingId"],
        productName: json["productName"] == null ? null : json["productName"],
        totalWeight: json["total_weight"] == null ? null : json["total_weight"],
        weight: json["weight"] == null ? null : json["weight"],
        productDescription: json["productDescription"] == null
            ? null
            : json["productDescription"],
        price: json["price"] == null ? null : json["price"],
        discount: json["discount"] == null ? null : json["discount"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        newPrice: json["new_price"] == null ? null : json["new_price"],
        productImage: json["product_image"] == null
            ? null
            : ProductImage.fromJson(json['product_image']),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "bookingId": bookingId == null ? null : bookingId,
        "total_weight": totalWeight == null ? null : totalWeight,
        "weight": weight == null ? null : weight,
        "productName": productName == null ? null : productName,
        "productDescription":
            productDescription == null ? null : productDescription,
        "price": price == null ? null : price,
        "discount": discount == null ? null : discount,
        "quantity": quantity == null ? null : quantity,
        "new_price": newPrice == null ? null : newPrice,
        "product_image": productImage == null ? null : productImage.toJson(),
      };
}

class UserDetails {
  UserDetails({
    this.id,
    this.name,
    this.email,
    this.countryCode,
    this.countryISOCode,
    this.phoneNumber,
    this.profilePicture,
    this.resourceUrl,
  });

  int id;
  String name;
  dynamic email;
  String countryCode;
  String countryISOCode;
  String phoneNumber;
  dynamic profilePicture;
  String resourceUrl;

  factory UserDetails.fromMap(Map<String, dynamic> json) => UserDetails(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
        countryISOCode:
            json["country_iso_code"] == null ? null : json["country_iso_code"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        profilePicture: json["profile_picture"],
        resourceUrl: json["resource_url"] == null ? null : json["resource_url"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "email": email,
        "country_code": countryCode == null ? null : countryCode,
        "country_iso_code": countryISOCode == null ? null : countryISOCode,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "profile_picture": profilePicture,
        "resource_url": resourceUrl == null ? null : resourceUrl,
      };
}

class DeliveryBoy {
  DeliveryBoy({
    this.id,
    this.name,
    this.phoneNumber,
  });

  int id;
  String name;
  String phoneNumber;

  factory DeliveryBoy.fromJson(Map<String, dynamic> json) => DeliveryBoy(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "phone_number": phoneNumber == null ? null : phoneNumber,
      };
}

class ShopDetailsModel {
  ShopDetailsModel({
    this.id,
    this.name,
    this.showOwner,
    this.description,
    this.profileImage,
    this.address,
    this.additionalAddress,
    this.latitude,
    this.longitude,
    this.rating,
    this.distance,
    this.expTime,
    this.shopType,
    this.media,
    this.primaryCoupon,
  });

  int id;
  String name;
  ShopOwner showOwner;
  String description;
  ProfileImageModel profileImage;
  String address;
  String additionalAddress;
  String latitude;
  String longitude;
  num rating;
  num distance;
  String expTime;
  int shopType;
  List<Media> media;
  CouponData primaryCoupon;

  factory ShopDetailsModel.fromJson(Map<String, dynamic> json) =>
      ShopDetailsModel(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        showOwner: json["show_owner"] == null
            ? null
            : ShopOwner.fromJson(json["show_owner"]),
        description: json["description"] == null ? null : json["description"],
        profileImage: json["profile_image"] == null
            ? null
            : ProfileImageModel.fromMap(json["profile_image"]),
        address: json["address"] == null ? null : json["address"],
        additionalAddress: json["additional_address"] == null
            ? null
            : json["additional_address"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        rating: json["rating"] == null ? null : json["rating"],
        distance: json["distance"] == null ? null : json["distance"].toDouble(),
        expTime: json["exp_time"] == null ? null : json["exp_time"],
        shopType: json["shop_type"] == null ? null : json["shop_type"],
        media: json["media"] == null
            ? null
            : List<Media>.from(json["media"].map((x) => Media.fromJson(x))),
        primaryCoupon: json["primary_coupon"] == null
            ? null
            : CouponData.fromJson(json["primary_coupon"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "show_owner": showOwner == null ? null : showOwner.toJson(),
        "description": description == null ? null : description,
        "profile_image": profileImage == null ? null : profileImage.toMap(),
        "address": address == null ? null : address,
        "additional_address":
            additionalAddress == null ? null : additionalAddress,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "rating": rating == null ? null : rating,
        "distance": distance == null ? null : distance,
        "exp_time": expTime == null ? null : expTime,
        "shop_type": shopType == null ? null : shopType,
        "media": media == null
            ? null
            : List<dynamic>.from(media.map((x) => x.toJson())),
        "primary_coupon": primaryCoupon == null ? null : primaryCoupon.toJson(),
      };
}

class Media {
  Media({
    this.id,
    this.modelId,
    this.modelType,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String modelType;
  String fileName;
  String name;

  factory Media.fromJson(Map<String, dynamic> json) => Media(
        id: json["id"] == null ? null : json["id"],
        modelId: json["model_id"] == null ? null : json["model_id"],
        modelType: json["model_type"] == null ? null : json["model_type"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "model_id": modelId == null ? null : modelId,
        "model_type": modelType == null ? null : modelType,
        "file_name": fileName == null ? null : fileName,
        "name": name == null ? null : name,
      };
}

class ShopOwner {
  ShopOwner({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNumber,
  });

  int id;
  String firstName;
  String lastName;
  String email;
  String phoneNumber;

  factory ShopOwner.fromJson(Map<String, dynamic> json) => ShopOwner(
        id: json["id"] == null ? null : json["id"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
      };
}

ProfileImageModel profileImageModelFromMap(String str) =>
    ProfileImageModel.fromMap(json.decode(str));

String profileImageModelToMap(ProfileImageModel data) =>
    json.encode(data.toMap());

class ProfileImageModel {
  ProfileImageModel({
    this.id,
    this.modelId,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String fileName;
  String name;

  factory ProfileImageModel.fromMap(Map<String, dynamic> json) =>
      ProfileImageModel(
        id: json["id"] == null ? null : json["id"],
        modelId: json["model_id"] == null ? null : json["model_id"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "model_id": modelId == null ? null : modelId,
        "file_name": fileName == null ? null : fileName,
        "name": name == null ? null : name,
      };
}

class CouponData {
  CouponData({
    this.id,
    this.name,
    this.code,
    this.couponImage,
    this.isPrimary,
    this.isGlobal,
    this.type,
    this.discount,
    this.description,
    this.minAmount,
    this.maxAmount,
    this.maximumCustomerUse,
    this.totalUse,
    this.startDate,
    this.endDate,
  });

  int id;
  String name;
  String code;
  CouponImage couponImage;
  int isPrimary;
  int isGlobal;
  int type;
  int discount;
  String description;
  int minAmount;
  int maxAmount;
  int maximumCustomerUse;
  int totalUse;
  DateTime startDate;
  DateTime endDate;

  factory CouponData.fromJson(Map<String, dynamic> json) => CouponData(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        code: json["code"] == null ? null : json["code"],
        couponImage: json["coupon_image"] == null
            ? null
            : CouponImage.fromJson(json["coupon_image"]),
        isPrimary: json["is_primary"] == null ? null : json["is_primary"],
        isGlobal: json["is_global"] == null ? null : json["is_global"],
        type: json["type"] == null ? null : json["type"],
        discount: json["discount"] == null ? null : json["discount"],
        description: json["description"] == null ? null : json["description"],
        minAmount: json["min_amount"] == null ? null : json["min_amount"],
        maxAmount: json["max_amount"] == null ? null : json["max_amount"],
        maximumCustomerUse: json["maximum_customer_use"] == null
            ? null
            : json["maximum_customer_use"],
        totalUse: json["total_use"] == null ? null : json["total_use"],
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "code": code == null ? null : code,
        "coupon_image": couponImage == null ? null : couponImage.toJson(),
        "is_primary": isPrimary == null ? null : isPrimary,
        "is_global": isGlobal == null ? null : isGlobal,
        "type": type == null ? null : type,
        "discount": discount == null ? null : discount,
        "description": description == null ? null : description,
        "min_amount": minAmount == null ? null : minAmount,
        "max_amount": maxAmount == null ? null : maxAmount,
        "maximum_customer_use":
            maximumCustomerUse == null ? null : maximumCustomerUse,
        "total_use": totalUse == null ? null : totalUse,
        "start_date": startDate == null ? null : startDate.toIso8601String(),
        "end_date": endDate == null ? null : endDate.toIso8601String(),
      };
}

class CouponImage {
  CouponImage({
    this.id,
    this.modelId,
    this.modelType,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String modelType;
  String fileName;
  String name;

  factory CouponImage.fromJson(Map<String, dynamic> json) => CouponImage(
        id: json["id"] == null ? null : json["id"],
        modelId: json["model_id"] == null ? null : json["model_id"],
        modelType: json["model_type"] == null ? null : json["model_type"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "model_id": modelId == null ? null : modelId,
        "model_type": modelType == null ? null : modelType,
        "file_name": fileName == null ? null : fileName,
        "name": name == null ? null : name,
      };
}

class ProductImage {
  ProductImage({
    this.id,
    this.modelId,
    this.modelType,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String modelType;
  String fileName;
  String name;

  factory ProductImage.fromJson(Map<String, dynamic> json) => ProductImage(
        id: json["id"] == null ? null : json["id"],
        modelId: json["model_id"] == null ? null : json["model_id"],
        modelType: json["model_type"] == null ? null : json["model_type"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "model_id": modelId == null ? null : modelId,
        "model_type": modelType == null ? null : modelType,
        "file_name": fileName == null ? null : fileName,
        "name": name == null ? null : name,
      };
}

class ReviewModel {
  ReviewModel({
    this.id,
    this.shopId,
    this.deliveryBoyId,
    this.rating,
    this.feedback,
    this.customerIdBy,
    this.deliveryBoyIdBy,
  });

  var id;
  var shopId;
  var deliveryBoyId;
  var rating;
  var feedback;
  var customerIdBy;
  var deliveryBoyIdBy;

  factory ReviewModel.fromJson(Map<String, dynamic> json) => ReviewModel(
        id: json["id"] == null ? null : json["id"],
        shopId: json["shop_id"] == null ? null : json["shop_id"],
        deliveryBoyId:
            json["delivery_boy_id"] == null ? null : json["delivery_boy_id"],
        rating: json["rating"] == null ? null : json["rating"],
        feedback: json["feedback"] == null ? null : json["feedback"],
        customerIdBy:
            json["customer_id_by"] == null ? null : json["customer_id_by"],
        deliveryBoyIdBy: json["delivery_boy_id_by"] == null
            ? null
            : json["delivery_boy_id_by"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "shop_id": shopId == null ? null : shopId,
        "delivery_boy_id": deliveryBoyId == null ? null : deliveryBoyId,
        "rating": rating == null ? null : rating,
        "feedback": feedback == null ? null : feedback,
        "customer_id_by": customerIdBy == null ? null : customerIdBy,
        "delivery_boy_id_by": deliveryBoyIdBy == null ? null : deliveryBoyIdBy,
      };
}
