const String NOTIFICATION_ICON = 'lib/modules/vehicle_documents/images/notification.svg';
const String CLOCK_ICON = 'lib/modules/vehicle_documents/images/clock.svg';
const String VEHICLE_ICON = 'lib/modules/vehicle_documents/images/vehicle_icon.svg';
const String LICENCE_ICON = 'lib/modules/vehicle_documents/images/license.svg';
const String NUMBER_ICON = 'lib/modules/vehicle_documents/images/number.svg';
const String INSURANCE_ICON = 'lib/modules/vehicle_documents/images/car_insurance.svg';
const String PREFIX_ICON = 'lib/modules/vehicle_documents/images/prefix.png';
const String UPLOAD_ICON = 'lib/modules/vehicle_documents/images/upload.png';
