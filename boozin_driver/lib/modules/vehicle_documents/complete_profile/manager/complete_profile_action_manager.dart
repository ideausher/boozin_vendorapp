import 'dart:io';
import 'package:boozin_driver/modules/bank_details/bank_routes.dart';
import 'package:boozin_driver/modules/bank_details/create_account/api/provider/check_account_linked_provider.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/auth_routes.dart';
import 'package:boozin_driver/modules/auth/enums/auth_enums.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/fetch_prefs_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/complete_profile/bloc/vehicle_details_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/complete_profile/bloc/vehicle_details_event.dart';
import 'package:boozin_driver/modules/vehicle_documents/complete_profile/bloc/vehicle_details_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:boozin_driver/modules/vehicle_documents/registration_routes.dart';
import 'package:boozin_driver/modules/common/model/user_current_location_model.dart';
import 'package:boozin_driver/routes.dart';

class CompleteProfileActionManager {
  BuildContext context;
  GetVehicleDetailsBloc getVehicleDetailsBloc;
  GetVehicleDetailsState getVehicleDetailsState;

  //Action on init
  Future<bool> actionOnInit() async {
    var _network = await NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true);

    if (_network) {
      // hide keyboard
      getVehicleDetailsBloc
          ?.emitEvent(VehicleDetailsEvent(isLoading: true, context: context));
      return true;
    } else {
      return false;
    }
  }

  actionVehicleDetailsState({
    BuildContext context,
    ScaffoldState scaffoldState,
    GetVehicleDetailsState getVehicleDetailsState,
    GetVehicleDetailsBloc getVehicleDetailsBloc,
  }) {
    if (getVehicleDetailsState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (getVehicleDetailsState?.message?.toString()?.trim()?.isNotEmpty ==
            true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            context: context,
            scaffoldState: scaffoldState,
            message: getVehicleDetailsState?.message,
          );
        } else if (getVehicleDetailsState
                ?.driverRegistrationResponseModel?.statusCode ==
            ApiStatus.Success.value) {
          if ((getVehicleDetailsState?.driverRegistrationResponseModel?.data
                          ?.vehicleRegisteration?.vehicleStatus ==
                      DocumentStatus?.Approved?.value &&
                  getVehicleDetailsState?.driverRegistrationResponseModel?.data
                          ?.drivingLicence?.drivingLicenceStatus ==
                      DocumentStatus?.Approved?.value) &&
              (getVehicleDetailsState?.driverRegistrationResponseModel?.data
                          ?.workPermit?.workPermitStatus ==
                      DocumentStatus?.Approved?.value &&
                  getVehicleDetailsState?.driverRegistrationResponseModel?.data
                          ?.vehicleInsurance?.vehicleInsurenceStatus ==
                      DocumentStatus?.Approved?.value &&
                  getVehicleDetailsState
                          ?.driverRegistrationResponseModel
                          ?.data
                          ?.backgroundVerification
                          ?.backgroundVerifcationStatus ==
                      DocumentStatus?.Approved?.value)) {
            checkAccountApprovedOrNot(context: context);
            //here of the status get approved i am navigating to link stripe account
            // NavigatorUtils.navigatorUtilsInstance
            //     .navigatorPushedName(context, BankRoutes.BANK_DETAILS_PAGE);
          } else {
            NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                context, RegistrationRoutes.DRIVER_DETAILS,
                dataToBeSend: HOME);
          }
        }
      });
    }
  }

  checkAccountApprovedOrNot({BuildContext context}) async {
    var result =
    await CheckAccountLinkedOrNotProvider().checkAccountLinkedOrNotProvider(
      context: context,
    );

    CommonResponseModel commonResponseModel;
    if (result != null) {
      // check result status
      if (result[ApiStatusParams.Status.value] != null &&
          result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, AuthRoutes.HOME_SCREEN_ROOT);
      }
      // failure case
      else {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
      }
    } else {
      NavigatorUtils.navigatorUtilsInstance
          .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
    }
  }
  //navigate user form complete profile page to next screen on the basis of document status and form where it is coming
  navigateUserToNextScreen(
      {String comingFrom,
      GetVehicleDetailsState getVehicleDetailsState,
      DriverRegistrationRequestModel driverRegistrationRequestModel,
      ScaffoldState scaffoldState}) async {
    if (comingFrom == HOME) {
      if (getVehicleDetailsState
              ?.driverRegistrationResponseModel?.data?.action?.status ==
          DocumentStatus?.Approved?.value) {
        NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
            context, RegistrationRoutes.DRIVER_DETAILS,
            dataToBeSend: HOME);
      } else {
        if (getVehicleDetailsState
                ?.driverRegistrationResponseModel?.data?.action?.status ==
            DocumentStatus?.Rejected?.value) {
          NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
              context, RegistrationRoutes.DRIVER_DETAILS,
              dataToBeSend: HOME);
        } else if (getVehicleDetailsState
                ?.driverRegistrationResponseModel?.data?.action?.status ==
            DocumentStatus?.Pending?.value) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              scaffoldState: scaffoldState,
              message: AppLocalizations.of(context)
                  .completeProfile
                  .error
                  .verificationPending);
        } else {
          NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
              context, RegistrationRoutes.DRIVER_DETAILS,
              dataToBeSend: HOME);
        }
      }
    } else {
      //here checking if current of location exist in the preference navigate user to dashboard
      print('Calling this..');
      CurrentLocation currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel();
      if (currentLocation != null) {
        if (getVehicleDetailsState
                ?.driverRegistrationResponseModel?.data?.action?.status ==
            DocumentStatus?.Approved?.value) {
          DriverRegistrationRequestModel driverRegistrationRequestModel =
              await FetchPrefsUtils.fetchPrefsUtilsInstance
                  .getVehicleDataModel();
          NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
              context, AuthRoutes.HOME_SCREEN_ROOT,
              dataToBeSend: driverRegistrationRequestModel);
        } else if (getVehicleDetailsState
                ?.driverRegistrationResponseModel?.data?.action?.status ==
            DocumentStatus?.Rejected?.value) {
          NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
              context, RegistrationRoutes.DRIVER_DETAILS,
              dataToBeSend: HOME);
        } else if (getVehicleDetailsState
                ?.driverRegistrationResponseModel?.data?.action?.status ==
            DocumentStatus?.Pending?.value) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              scaffoldState: scaffoldState,
              message: AppLocalizations.of(context)
                  .completeProfile
                  .error
                  .verificationPending);
        }
      }
      //else navigate to enable location screen to get the current location of the user
      else {
        NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
            context, AuthRoutes.ENABLE_LOCATION_ROOT,
            dataToBeSend: driverRegistrationRequestModel);
      }
    }
  }
}
