import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/fetch_prefs_utils.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/provider/vehicle_registration_api_provider.dart';
import 'package:boozin_driver/modules/vehicle_documents/complete_profile/bloc/vehicle_details_event.dart';
import 'package:boozin_driver/modules/vehicle_documents/complete_profile/bloc/vehicle_details_state.dart';

class GetVehicleDetailsBloc extends BlocEventStateBase<GetVehicleDetailsEvent, GetVehicleDetailsState> {
  GetVehicleDetailsBloc({bool initializing = true}) : super(initialState: GetVehicleDetailsState.initiating());

  @override
  Stream<GetVehicleDetailsState> eventHandler(
      GetVehicleDetailsEvent event, GetVehicleDetailsState currentState) async* {
    if (event is VehicleDetailsEvent) {
      String message = "";
      VehicleRegistrationResponseModel driverRegistrationResponseModel;

      yield GetVehicleDetailsState.getVehicleDetails(
        isLoading: event?.isLoading,
        context: event?.context,
        message: message,
      );

      //here i am getting this because i have to store the status of documents verification.
      //when user kills the app on the basis of this status i will navigate user to correct screen.
      DriverRegistrationRequestModel driverInfo = await FetchPrefsUtils.fetchPrefsUtilsInstance.getVehicleDataModel();
      //<-----------------api calling vehicle registration-------------->
      var result = await VehicleRegistrationProvider().getDriverDocStatus(context: event.context);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          driverRegistrationResponseModel = VehicleRegistrationResponseModel.fromMap(result);
          if (driverInfo == null) {
            driverInfo = new DriverRegistrationRequestModel();
            driverInfo.status = driverRegistrationResponseModel?.data?.action?.status;
            // await SharedPrefUtils.sharedPrefUtilsInstance.saveVehicleInfo(driverInfo, PrefsEnum.UserVehicleData.value);
          } else {
            driverInfo?.status = driverRegistrationResponseModel?.data?.action?.status;
            // await SharedPrefUtils.sharedPrefUtilsInstance.saveVehicleInfo(driverInfo, PrefsEnum.UserVehicleData.value);
          }
        }
        // failure case
        else {
          message = result[ApiStatusParams.Message.value];
        }
      } else {
        message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }
      yield GetVehicleDetailsState.getVehicleDetails(
          isLoading: false,
          context: event?.context,
          message: message,
          driverRegistrationResponseModel: driverRegistrationResponseModel);
    }
  }
}
