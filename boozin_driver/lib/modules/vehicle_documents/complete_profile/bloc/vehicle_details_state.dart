import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';

class GetVehicleDetailsState extends BlocState {
  GetVehicleDetailsState({
    this.isLoading: false,
    this.message,
    this.context,
    this.driverRegistrationResponseModel

  }) : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final VehicleRegistrationResponseModel driverRegistrationResponseModel;


  // used for update home page state
  factory GetVehicleDetailsState.getVehicleDetails({bool isLoading,
    String message,
    BuildContext context,
    VehicleRegistrationResponseModel driverRegistrationResponseModel

  }) {
    return GetVehicleDetailsState(
        isLoading: isLoading,
        message: message,
        context: context,
        driverRegistrationResponseModel: driverRegistrationResponseModel

    );
  }

  factory GetVehicleDetailsState.initiating() {
    return GetVehicleDetailsState(
      isLoading: false,
    );
  }
}
