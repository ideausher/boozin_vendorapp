import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/background_verification/page/background_verification_page.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/my_earnings/page/my_earnings_page.dart';
import 'complete_profile/page/complete_profile_page.dart';
import 'driver_details/page/driver_details_page.dart';
import 'driver_details/page/driving_licence/page/driving_licence_page.dart';
import 'driver_details/page/vehicle_insurance/page/insurance_details_page.dart';
import 'driver_details/page/vehicle_registration/page/vehicle_registration_page.dart';
import 'driver_details/page/work_permit/page/work_permit_page.dart';

class RegistrationRoutes {
  static const String COMPLETE_PROFILE = '/COMPLETE_PROFILE';
  static const String DRIVER_DETAILS = '/DRIVER_DETAILS';
  static const String LICENCE_DETAILS = '/LICENCE_DETAILS';
  static const String WORK_PERMIT_PAGE = '/WORK_PERMIT_PAGE';
  static const String INSURANCE_DETAILS = '/INSURANCE_DETAILS';
  static const String BACKGROUND_VERIFICATION = '/BACKGROUND_VERIFICATION';
  static const String VEHICLE_REG = '/VEHICLE_REG';
  static const String MY_EARNING_PAGE = '/MY_EARNING_PAGE';


  static Map<String, WidgetBuilder> routes() {
    Map<String, WidgetBuilder> route = {
      // When we navigate to the "/" route, build the FirstScreen Widget
      COMPLETE_PROFILE: (context) => CompleteProfilePage(context),
      DRIVER_DETAILS: (context) => DriverDetailsPage(context),
      LICENCE_DETAILS: (context) => DrivingLicencePage(context),
      WORK_PERMIT_PAGE: (context) => WorkPermitPage(context),
      INSURANCE_DETAILS: (context) => InsuranceDetailsPage(context),
      VEHICLE_REG: (context) => VehicleRegistrationPage(context),
      BACKGROUND_VERIFICATION: (context) => BackgroundVerificationPage(context),
      MY_EARNING_PAGE: (context) => MyEarningsPage(context),

    };

    return route;
  }
}
