// To parse this JSON data, do
//
//     final driverRegistrationResponseModel = driverRegistrationResponseModelFromJson(jsonString);

import 'dart:convert';

VehicleRegistrationResponseModel driverRegistrationResponseModelFromJson(
        String str) =>
    VehicleRegistrationResponseModel.fromMap(json.decode(str));

String driverRegistrationResponseModelToJson(
        VehicleRegistrationResponseModel data) =>
    json.encode(data.toJson());

class VehicleRegistrationResponseModel {
  VehicleRegistrationResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory VehicleRegistrationResponseModel.fromMap(Map<String, dynamic> json) =>
      VehicleRegistrationResponseModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}

class Data {
  Data({
    this.vehicleRegisteration,
    this.drivingLicence,
    this.workPermit,
    this.vehicleInsurance,
    this.backgroundVerification,
    this.action,
  });

  VehicleRegisteration vehicleRegisteration;
  DrivingLicence drivingLicence;
  WorkPermit workPermit;
  VehicleInsurance vehicleInsurance;
  BackgroundVerification backgroundVerification;
  Action action;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        vehicleRegisteration: json["vehicle_registeration"] == null
            ? null
            : VehicleRegisteration.fromJson(json["vehicle_registeration"]),
        drivingLicence: json["driving_licence"] == null
            ? null
            : DrivingLicence.fromJson(json["driving_licence"]),
        workPermit: json["work_permit"] == null
            ? null
            : WorkPermit.fromJson(json["work_permit"]),
        vehicleInsurance: json["vehicle_insurance"] == null
            ? null
            : VehicleInsurance.fromJson(json["vehicle_insurance"]),
        backgroundVerification: json["background_verification"] == null
            ? null
            : BackgroundVerification.fromJson(json["background_verification"]),
        action: json["action"] == null ? null : Action.fromJson(json["action"]),
      );

  Map<String, dynamic> toJson() => {
        "vehicle_registeration":
            vehicleRegisteration == null ? null : vehicleRegisteration.toJson(),
        "driving_licence":
            drivingLicence == null ? null : drivingLicence.toJson(),
        "work_permit": workPermit == null ? null : workPermit.toJson(),
        "vehicle_insurance":
            vehicleInsurance == null ? null : vehicleInsurance.toJson(),
        "background_verification": backgroundVerification == null
            ? null
            : backgroundVerification.toJson(),
        "action": action == null ? null : action.toJson(),
      };
}

class Action {
  Action({
    this.status,
  });

  int status;

  factory Action.fromJson(Map<String, dynamic> json) => Action(
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
      };
}

class DrivingLicence {
  DrivingLicence({
    this.drivingLicenceImages,
    this.drivingLicenceNo,
    this.drivingLicenceExpiryDate,
    this.drivingLicenceStatus,
    this.drivingLicenceReason,
  });

  String drivingLicenceImages;
  String drivingLicenceNo;
  String drivingLicenceExpiryDate;
  int drivingLicenceStatus;
  String drivingLicenceReason;

  factory DrivingLicence.fromJson(Map<String, dynamic> json) => DrivingLicence(
        drivingLicenceImages: json["driving_licence_images"] == null
            ? null
            : json["driving_licence_images"],
        drivingLicenceNo: json["driving_licence_no"] == null
            ? null
            : json["driving_licence_no"],
        drivingLicenceExpiryDate: json["driving_licence_expiry_date"] == null
            ? null
            : json["driving_licence_expiry_date"],
        drivingLicenceStatus: json["driving_licence_status"] == null
            ? null
            : json["driving_licence_status"],
        drivingLicenceReason: json["driving_licence_reason"] == null
            ? null
            : json["driving_licence_reason"],
      );

  Map<String, dynamic> toJson() => {
        "driving_licence_images":
            drivingLicenceImages == null ? null : drivingLicenceImages,
        "driving_licence_no":
            drivingLicenceNo == null ? null : drivingLicenceNo,
        "driving_licence_expiry_date":
            drivingLicenceExpiryDate == null ? null : drivingLicenceExpiryDate,
        "driving_licence_status":
            drivingLicenceStatus == null ? null : drivingLicenceStatus,
        "driving_licence_reason":
            drivingLicenceReason == null ? null : drivingLicenceReason,
      };
}

class WorkPermit {
  WorkPermit({
    this.workPermitImages,
    this.weight,
    this.workPermitExpiryDate,
    this.idNumber,
    this.permitNumber,
    this.workPermitStatus,
    this.workPermitReason,
  });

  String workPermitImages;
  num weight;
  String workPermitExpiryDate;
  String idNumber;
  String permitNumber;
  int workPermitStatus;
  String workPermitReason;

  factory WorkPermit.fromJson(Map<String, dynamic> json) => WorkPermit(
        workPermitImages: json["work_permit_images"] == null
            ? null
            : json["work_permit_images"],
        weight: json["weight"] == null ? null : json["weight"],
        workPermitExpiryDate: json["work_permit_expiry_date"] == null
            ? null
            : json["work_permit_expiry_date"],
        idNumber: json["id_number"] == null ? null : json["id_number"],
        permitNumber:
            json["permit_number"] == null ? null : json["permit_number"],
        workPermitStatus: json["work_permit_status"] == null
            ? null
            : json["work_permit_status"],
        workPermitReason: json["work_permit_reason"] == null
            ? null
            : json["work_permit_reason"],
      );

  Map<String, dynamic> toJson() => {
        "work_permit_images":
            workPermitImages == null ? null : workPermitImages,
        "weight": weight == null ? null : weight,
        "work_permit_expiry_date":
            workPermitExpiryDate == null ? null : workPermitExpiryDate,
        "id_number": idNumber == null ? null : idNumber,
        "permit_number": permitNumber == null ? null : permitNumber,
        "work_permit_status":
            workPermitStatus == null ? null : workPermitStatus,
        "work_permit_reason":
            workPermitReason == null ? null : workPermitReason,
      };
}

class BackgroundVerification {
  BackgroundVerification({
    this.backgroundVerificationImages,
    this.backgroundVerifcationStatus,
    this.backgroundVerificationReason,
  });

  String backgroundVerificationImages;
  int backgroundVerifcationStatus;
  String backgroundVerificationReason;

  factory BackgroundVerification.fromJson(Map<String, dynamic> json) =>
      BackgroundVerification(
        backgroundVerificationImages:
            json["background_image"] == null ? null : json["background_image"],
        backgroundVerifcationStatus: json["background_image_status"] == null
            ? null
            : json["background_image_status"],
        backgroundVerificationReason: json["background_image_reason"] == null
            ? null
            : json["background_image_reason"],
      );

  Map<String, dynamic> toJson() => {
        "background_image": backgroundVerificationImages == null
            ? null
            : backgroundVerificationImages,
        "background_image_status": backgroundVerifcationStatus == null
            ? null
            : backgroundVerifcationStatus,
        "background_image_reason": backgroundVerificationReason == null
            ? null
            : backgroundVerificationReason,
      };
}

class VehicleInsurance {
  VehicleInsurance(
      {this.vehicleInsurenceImage,
      this.vehicleInsurenceStatus,
      this.vehicleInsurenceReason,
      this.policyNumber,
      this.validTill,
      this.validFrom = ''});

  String vehicleInsurenceImage;
  int vehicleInsurenceStatus;
  String vehicleInsurenceReason;
  String policyNumber;
  String validFrom;
  String validTill;

  factory VehicleInsurance.fromJson(Map<String, dynamic> json) =>
      VehicleInsurance(
        vehicleInsurenceImage: json["vehicle_insurence_image"] == null
            ? null
            : json["vehicle_insurence_image"],
        vehicleInsurenceStatus: json["vehicle_insurence_status"] == null
            ? null
            : json["vehicle_insurence_status"],
        vehicleInsurenceReason: json["vehicle_insurence_reason"] == null
            ? null
            : json["vehicle_insurence_reason"],
        policyNumber:
            json["policy_number"] == null ? null : json["policy_number"],
        validFrom: json["vehicle_insurence_valid_from"] == null
            ? null
            : json["vehicle_insurence_valid_from"],
        validTill: json["vehicle_insurence_valid_upto"] == null
            ? null
            : json["vehicle_insurence_valid_upto"],
      );

  Map<String, dynamic> toJson() => {
        "vehicle_insurence_image":
            vehicleInsurenceImage == null ? null : vehicleInsurenceImage,
        "vehicle_insurence_status":
            vehicleInsurenceStatus == null ? null : vehicleInsurenceStatus,
        "vehicle_insurence_reason":
            vehicleInsurenceReason == null ? null : vehicleInsurenceReason,
        "policy_number": policyNumber == null ? null : policyNumber,
        "vehicle_insurence_valid_from": validFrom == null ? null : validFrom,
        "vehicle_insurence_valid_upto": validTill == null ? null : validTill,
      };
}

class VehicleRegisteration {
  VehicleRegisteration({
    this.registerationNumber,
    this.vehiclePicture,
    this.vehicleCompany,
    this.vehicleStatus,
    this.vehicleRejectReason,
    this.model,
    this.color,
    this.validUpto,
  });

  String registerationNumber;
  String vehiclePicture;
  String vehicleCompany;
  int vehicleStatus;
  String vehicleRejectReason;
  String model;
  String color;
  String validUpto;

  factory VehicleRegisteration.fromJson(Map<String, dynamic> json) =>
      VehicleRegisteration(
        registerationNumber: json["registeration_number"] == null
            ? null
            : json["registeration_number"],
        vehiclePicture:
            json["vehicle_picture"] == null ? null : json["vehicle_picture"],
        vehicleCompany:
            json["vehicle_company"] == null ? null : json["vehicle_company"],
        vehicleStatus:
            json["vehicle_status"] == null ? null : json["vehicle_status"],
        vehicleRejectReason: json["vehicle_reject_reason"] == null
            ? null
            : json["vehicle_reject_reason"],
        model: json["model"] == null ? null : json["model"],
        color: json["color"] == null ? null : json["color"],
        validUpto: json["valid_upto"] == null ? null : json["valid_upto"],
      );

  Map<String, dynamic> toJson() => {
        "registeration_number":
            registerationNumber == null ? null : registerationNumber,
        "vehicle_picture": vehiclePicture == null ? null : vehiclePicture,
        "vehicle_company": vehicleCompany == null ? null : vehicleCompany,
        "vehicle_status": vehicleStatus == null ? null : vehicleStatus,
        "vehicle_reject_reason":
            vehicleRejectReason == null ? null : vehicleRejectReason,
        "model": model == null ? null : model,
        "color": color == null ? null : color,
        "valid_upto": validUpto == null ? null : validUpto,
      };
}
