// To parse this JSON data, do
//
//     final driverRegistrationRequestModel = driverRegistrationRequestModelFromJson(jsonString);

import 'dart:convert';

DriverRegistrationRequestModel driverRegistrationRequestModelFromJson(
        String str) =>
    DriverRegistrationRequestModel.fromMap(json.decode(str));

String driverRegistrationRequestModelToJson(
        DriverRegistrationRequestModel data) =>
    json.encode(data.toMap());

class DriverRegistrationRequestModel {
  DriverRegistrationRequestModel(
      {this.vehicleRegisteration,
      this.drivingLicence,
      this.workPermit,
      this.vehicleInsurance,
      this.backgroundVerification,
      this.status});

  VehicleRegData vehicleRegisteration;
  DrivingLicenceData drivingLicence;
  WorkPermit workPermit;
  VehicleInsuranceData vehicleInsurance;
  BackgroundVerificationData backgroundVerification;
  num status;

  factory DriverRegistrationRequestModel.fromMap(Map<String, dynamic> json) =>
      DriverRegistrationRequestModel(
        vehicleRegisteration: json["vehicle_registeration"] == null
            ? null
            : VehicleRegData.fromMap(json["vehicle_registeration"]),
        drivingLicence: json["driving_licence"] == null
            ? null
            : DrivingLicenceData.fromMap(json["driving_licence"]),
        workPermit: json["work_permit"] == null
            ? null
            : WorkPermit.fromMap(json["work_permit"]),
        vehicleInsurance: json["vehicle_insurance"] == null
            ? null
            : VehicleInsuranceData.fromMap(json["vehicle_insurance"]),
        backgroundVerification: json["background_verification"] == null
            ? null
            : BackgroundVerificationData.fromJson(json["background_verification"]),
      );

  Map<String, dynamic> toMap() => {
        "vehicle_registeration":
            vehicleRegisteration == null ? null : vehicleRegisteration.toMap(),
        "driving_licence":
            drivingLicence == null ? null : drivingLicence.toMap(),
        "work_permit": workPermit == null ? null : workPermit.toMap(),
        "vehicle_insurance":
            vehicleInsurance == null ? null : vehicleInsurance.toMap(),
        "background_verification": backgroundVerification == null
            ? null
            : backgroundVerification.toJson(),
      };
}

class DrivingLicenceData {
  DrivingLicenceData({
    this.drinvinglicenceImages,
    this.drivinglicenceNo,
    this.drivinglicenceExpiryDate,
  });

  String drinvinglicenceImages;
  String drivinglicenceNo;
  String drivinglicenceExpiryDate;

  factory DrivingLicenceData.fromMap(Map<String, dynamic> json) =>
      DrivingLicenceData(
          drinvinglicenceImages: json["drivinglicence_images"] == null
              ? null
              : json["drivinglicence_images"],
          drivinglicenceNo: json["drivinglicence_no"] == null
              ? null
              : json["drivinglicence_no"],
          drivinglicenceExpiryDate: json["drivinglicence_expiry_date"] == null
              ? null
              : json["drivinglicence_expiry_date"]);

  Map<String, dynamic> toMap() => {
        "drivinglicence_images":
            drinvinglicenceImages == null ? null : drinvinglicenceImages,
        "drivinglicence_no": drivinglicenceNo == null ? null : drivinglicenceNo,
        "drivinglicence_expiry_date":
            drivinglicenceExpiryDate == null ? null : drivinglicenceExpiryDate,
      };
}

class BackgroundVerificationData {
  BackgroundVerificationData({
    this.backgroundVerificationImages,
  });

  String backgroundVerificationImages;

  factory BackgroundVerificationData.fromJson(Map<String, dynamic> json) =>
      BackgroundVerificationData(
        backgroundVerificationImages:
            json["background_image"] == null
                ? null
                : json["background_image"],
      );

  Map<String, dynamic> toJson() => {
        "background_image": backgroundVerificationImages == null
            ? null
            : backgroundVerificationImages,
      };
}

class WorkPermit {
  WorkPermit({
    this.workPermitImages,
    this.permitNumber,
    this.weight,
    this.workPermitExpiryDate,
    this.idNumber,
  });

  String workPermitImages;
  String permitNumber;
  String weight;
  String workPermitExpiryDate;
  String idNumber;

  factory WorkPermit.fromMap(Map<String, dynamic> json) => WorkPermit(
        workPermitImages: json["work_permit_images"] == null
            ? null
            : json["work_permit_images"],
        permitNumber:
            json["permit_number"] == null ? null : json["permit_number"],
        weight: json["weight"] == null ? null : json["weight"],
        workPermitExpiryDate: json["work_permit_expiry_date"] == null
            ? null
            : json["work_permit_expiry_date"],
        idNumber: json["id_number"] == null ? null : json["id_number"],
      );

  Map<String, dynamic> toMap() => {
        "work_permit_images":
            workPermitImages == null ? null : workPermitImages,
        "permit_number": permitNumber == null ? null : permitNumber,
        "weight": weight == null ? null : weight,
        "work_permit_expiry_date":
            workPermitExpiryDate == null ? null : workPermitExpiryDate,
        "id_number": idNumber == null ? null : idNumber,
      };
}

class VehicleInsuranceData {
  VehicleInsuranceData(
      {this.vehicleInsurenceImage,
      this.policyNumber,
      this.validFrom,
      this.validTill});

  String vehicleInsurenceImage;
  String policyNumber;
  String validFrom;
  String validTill;

  factory VehicleInsuranceData.fromMap(Map<String, dynamic> json) =>
      VehicleInsuranceData(
        vehicleInsurenceImage: json["vehicle_insurence_image"] == null
            ? null
            : json["vehicle_insurence_image"],
        policyNumber:
            json["policy_number"] == null ? null : json["policy_number"],
        validFrom: json["vehicle_insurence_valid_from"] == null
            ? null
            : json["vehicle_insurence_valid_from"],
        validTill: json["vehicle_insurence_valid_upto"] == null
            ? null
            : json["vehicle_insurence_valid_upto"],
      );

  Map<String, dynamic> toMap() => {
        "vehicle_insurence_image":
            vehicleInsurenceImage == null ? null : vehicleInsurenceImage,
        "policy_number": policyNumber == null ? null : policyNumber,
        "vehicle_insurence_valid_from": validFrom == null ? null : validFrom,
        "vehicle_insurence_valid_upto": validTill == null ? null : validTill,
      };
}

class VehicleRegData {
  VehicleRegData({
    this.registerationNumber,
    this.vehiclePicture,
    this.vehicleCompany,
    this.model,
    this.color,
    this.validUpto,
  });

  String registerationNumber;
  String vehiclePicture;
  String vehicleCompany;
  String model;
  String color;
  String validUpto;

  factory VehicleRegData.fromMap(Map<String, dynamic> json) => VehicleRegData(
        registerationNumber: json["registeration_number"] == null
            ? null
            : json["registeration_number"],
        vehiclePicture:
            json["vehicle_picture"] == null ? null : json["vehicle_picture"],
        vehicleCompany:
            json["vehicle_company"] == null ? null : json["vehicle_company"],
        model: json["model"] == null ? null : json["model"],
        color: json["color"] == null ? null : json["color"],
        validUpto: json["valid_upto"] == null ? null : json["valid_upto"],
      );

  Map<String, dynamic> toMap() => {
        "registeration_number":
            registerationNumber == null ? null : registerationNumber,
        "vehicle_picture": vehiclePicture == null ? null : vehiclePicture,
        "vehicle_company": vehicleCompany == null ? null : vehicleCompany,
        "model": model == null ? null : model,
        "color": color == null ? null : color,
        "valid_upto": validUpto == null ? null : validUpto,
      };
}
