
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';

class GetVehicleDetailsProvider {
  Future<dynamic> getVehicleDetails({BuildContext context}) async {
    var path = "vehicle_details";

    var result = await AppConfig.of(context).baseApi.postRequest(
      path,
      context,

    );
    return result;
  }
}