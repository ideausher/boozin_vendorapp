enum DriverDocumentTypeEnum {
  vehicleRegisteration,
  drivingLicence,
  vehicleInsurance,
  workPermit,
  backgroundVerificationDocument
}

extension DriverDocumentTypeExtension on DriverDocumentTypeEnum {
  int get value {
    switch (this) {
      case DriverDocumentTypeEnum.vehicleRegisteration:
        return 0;
      case DriverDocumentTypeEnum.drivingLicence:
        return 1;
      case DriverDocumentTypeEnum.workPermit:
        return 2;
      case DriverDocumentTypeEnum.vehicleInsurance:
        return 3;
      case DriverDocumentTypeEnum.backgroundVerificationDocument:
        return 4;
      default:
        return null;
    }
  }
}
