import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';

abstract class MyVehicleDetailsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final VehicleRegistrationResponseModel driverRegistrationResponseModel;

  MyVehicleDetailsEvent({this.isLoading: false, this.context, this.driverRegistrationResponseModel});
}

//this event is used for getting current location data
class DetailsEvent extends MyVehicleDetailsEvent {
  DetailsEvent({
    bool isLoading,
    BuildContext context,
    VehicleRegistrationResponseModel driverRegistrationResponseModel,
  }) : super(
    isLoading: isLoading,
    context: context,
    driverRegistrationResponseModel: driverRegistrationResponseModel,
  );
}
