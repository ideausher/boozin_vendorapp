import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';

class MyVehicleDetailsState extends BlocState {
  MyVehicleDetailsState({
    this.isLoading: false,
    this.message,
    this.context,
    this.vehicleRegistrationResponseModel

  }) : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final VehicleRegistrationResponseModel vehicleRegistrationResponseModel;


  // used for update home page state
  factory MyVehicleDetailsState.getVehicleDetails({bool isLoading,
    String message,
    BuildContext context,
    VehicleRegistrationResponseModel driverRegistrationResponseModel

  }) {
    return MyVehicleDetailsState(
        isLoading: isLoading,
        message: message,
        context: context,
        vehicleRegistrationResponseModel: driverRegistrationResponseModel

    );
  }

  factory MyVehicleDetailsState.initiating() {
    return MyVehicleDetailsState(
      isLoading: false,
    );
  }
}
