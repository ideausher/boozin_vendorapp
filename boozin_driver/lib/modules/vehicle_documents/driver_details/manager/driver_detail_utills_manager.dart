import 'package:flutter/material.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/enums/auth_enums.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/enums/driver_document_type_enums.dart';

import 'driver_detail_action_manager.dart';

class DriverDetailUtillManager {
  static DriverDetailUtillManager _driverDetailUtillManager =
      DriverDetailUtillManager();

  static DriverDetailUtillManager get driverDetailUtillInstance =>
      _driverDetailUtillManager;

  //To get the document status (not uploaded, pending, completed or rejected)
  TextSpan getStatusValue({
    int index,
    DriverDetailActionManagers driverDetailActionManagers,
  }) {
    //Vehicle Registration
    if (index == DriverDocumentTypeEnum?.vehicleRegisteration?.value) {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleRegisteration
              ?.vehicleStatus ==
          DocumentStatus?.Pending?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
          style: textStyleSize14WithRED500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleRegisteration
              ?.vehicleStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleRegisteration
              ?.vehicleStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    }
    //Driving Licence
    else if (index == DriverDocumentTypeEnum?.drivingLicence?.value) {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.drivingLicence
              ?.drivingLicenceStatus ==
          DocumentStatus?.Pending?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
          style: textStyleSize14WithRED500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.drivingLicence
              ?.drivingLicenceStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.drivingLicence
              ?.drivingLicenceStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    }
    //WorkPermit
    else if (index == DriverDocumentTypeEnum?.workPermit?.value) {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.workPermit
              ?.workPermitStatus ==
          DocumentStatus?.Pending?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
          style: textStyleSize14WithRED500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.workPermit
              ?.workPermitStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.workPermit
              ?.workPermitStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    }
    //Vehicle Insurance
    else if (index == DriverDocumentTypeEnum?.vehicleInsurance?.value) {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleInsurance
              ?.vehicleInsurenceStatus ==
          DocumentStatus?.Pending?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
          style: textStyleSize14WithRED500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleInsurance
              ?.vehicleInsurenceStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleInsurance
              ?.vehicleInsurenceStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    } else if (driverDetailActionManagers
            ?.myVehicleDetailsState
            ?.vehicleRegistrationResponseModel
            ?.data
            ?.backgroundVerification
            ?.backgroundVerifcationStatus ==
        DocumentStatus?.Pending?.value) {
      return TextSpan(
        text:
            ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
        style: textStyleSize14WithRED500,
      );
    } else {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.backgroundVerification
              ?.backgroundVerifcationStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.backgroundVerification
              ?.backgroundVerifcationStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text:
              ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    }
  }

  //Check need to show proceed button or not
  bool showProceedButton(
      {DriverDetailActionManagers driverDetailActionManagers,
      String comeFrom}) {
    bool showProceedButton = false;
    if (comeFrom == PROFILE) {
      showProceedButton = false;
    } else {
      if (driverDetailActionManagers
                  ?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.vehicleRegisteration ==
              null ||
          driverDetailActionManagers?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel?.data?.drivingLicence ==
              null ||
          driverDetailActionManagers?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel?.data?.workPermit ==
              null ||
          driverDetailActionManagers?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel?.data?.vehicleInsurance ==
              null ||
          driverDetailActionManagers
                  ?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.backgroundVerification ==
              null) {
        showProceedButton = true;
      } else if (((driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.vehicleRegisteration?.vehicleStatus == DocumentStatus?.NotUploaded?.value ||
                  driverDetailActionManagers
                          ?.myVehicleDetailsState
                          ?.vehicleRegistrationResponseModel
                          ?.data
                          ?.drivingLicence
                          ?.drivingLicenceStatus ==
                      DocumentStatus?.NotUploaded?.value) ||
              (driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.workPermit?.workPermitStatus == DocumentStatus?.NotUploaded?.value ||
                  driverDetailActionManagers
                          ?.myVehicleDetailsState
                          ?.vehicleRegistrationResponseModel
                          ?.data
                          ?.vehicleInsurance
                          ?.vehicleInsurenceStatus ==
                      DocumentStatus?.NotUploaded?.value ||
                  driverDetailActionManagers
                          ?.myVehicleDetailsState
                          ?.vehicleRegistrationResponseModel
                          ?.data
                          ?.backgroundVerification
                          ?.backgroundVerifcationStatus ==
                      DocumentStatus?.NotUploaded?.value)) ||
          ((driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.vehicleRegisteration?.vehicleStatus == DocumentStatus?.Approved?.value && driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.drivingLicence?.drivingLicenceStatus == DocumentStatus?.Approved?.value) &&
              (driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.workPermit?.workPermitStatus == DocumentStatus?.Approved?.value &&
                  driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.vehicleInsurance?.vehicleInsurenceStatus == DocumentStatus?.Approved?.value &&
                  driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.backgroundVerification?.backgroundVerifcationStatus == DocumentStatus?.Approved?.value))) {
        showProceedButton = true;
      }
    }
    return showProceedButton;
  }

  //Use to check all document approved or not
  bool checkAllDocumentApprovedOrNot(
      {DriverDetailActionManagers driverDetailActionManagers,
      String comeFrom}) {
    bool allDoucmentApproved = false;
    if (driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.vehicleRegisteration
                ?.vehicleStatus ==
            DocumentStatus.Approved.value &&
        driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.drivingLicence
                ?.drivingLicenceStatus ==
            DocumentStatus.Approved.value &&
        driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.workPermit
                ?.workPermitStatus ==
            DocumentStatus.Approved.value &&
        driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.vehicleInsurance
                ?.vehicleInsurenceStatus ==
            DocumentStatus.Approved.value &&
        driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.backgroundVerification
                ?.backgroundVerifcationStatus ==
            DocumentStatus.Approved.value) {
      allDoucmentApproved = true;
    }
    return allDoucmentApproved;
  }
}
