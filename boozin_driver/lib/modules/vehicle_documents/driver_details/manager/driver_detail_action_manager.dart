import 'package:boozin_driver/modules/auth/auth_routes.dart';
import 'package:boozin_driver/modules/bank_details/bank_routes.dart';
import 'package:boozin_driver/modules/bank_details/create_account/api/provider/check_account_linked_provider.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:boozin_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:boozin_driver/modules/auth/enums/auth_enums.dart';
import 'package:boozin_driver/modules/auth/manager/auth_manager.dart';
import 'package:boozin_driver/modules/common/model/common_pass_data_model.dart';
import 'package:boozin_driver/modules/common/screen_pop_callback.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/bloc/driver_details_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/bloc/driver_details_event.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/bloc/driver_details_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/enums/driver_document_type_enums.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/manager/driver_detail_utills_manager.dart';

import '../../../../routes.dart';
import '../../registration_routes.dart';

class DriverDetailActionManagers {
  BuildContext context;
  MyVehicleDetailsBloc myVehicleDetailsBloc;
  MyVehicleDetailsState myVehicleDetailsState;

  //Get vehicle details
  actionGetVehicleDetails() {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        myVehicleDetailsBloc?.emitEvent(DetailsEvent(
            isLoading: true,
            context: context,
            driverRegistrationResponseModel:
                new VehicleRegistrationResponseModel()));
      }
    });
  }

  //Action on click Proceed Button
  void actionOnClickProceedButton({ScreenPopCallBack screenPopCallBack}) async {
    //Redirect to vehicle registration screen
    if (myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
                ?.vehicleRegisteration ==
            null &&
        myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.drivingLicence ==
            null &&
        myVehicleDetailsState
                ?.vehicleRegistrationResponseModel?.data?.workPermit ==
            null &&
        myVehicleDetailsState
                ?.vehicleRegistrationResponseModel?.data?.vehicleInsurance ==
            null &&
        myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
                ?.backgroundVerification ==
            null) {
      //Redirect to vehicle Registration screen
      navigateToScreens(
          index: DriverDocumentTypeEnum?.vehicleRegisteration?.value,
          screenPopCallBack: screenPopCallBack);
    }
    //this is when all the documents are approved
    else if ((myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
                    ?.vehicleRegisteration?.vehicleStatus ==
                DocumentStatus?.Approved?.value &&
            myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
                    ?.drivingLicence?.drivingLicenceStatus ==
                DocumentStatus?.Approved?.value) &&
        (myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
                    ?.workPermit?.workPermitStatus ==
                DocumentStatus?.Approved?.value &&
            myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
                    ?.vehicleInsurance?.vehicleInsurenceStatus ==
                DocumentStatus?.Approved?.value &&
            myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
                    ?.backgroundVerification?.backgroundVerifcationStatus ==
                DocumentStatus?.Approved?.value)) {
      checkAccountApprovedOrNot(context: context);
      //Navigate to stripe alert
      // NavigatorUtils.navigatorUtilsInstance
      //     .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
    }
    //Navigate to Vehicle registration screen
    else if (myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
            ?.vehicleRegisteration?.vehicleStatus ==
        DocumentStatus?.NotUploaded?.value) {
      navigateToScreens(
          index: DriverDocumentTypeEnum?.vehicleRegisteration?.value,
          screenPopCallBack: screenPopCallBack);
    }
    //Navigate to Driving Licence Screen
    else if (myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
            ?.drivingLicence?.drivingLicenceStatus ==
        DocumentStatus?.NotUploaded?.value) {
      navigateToScreens(
          index: DriverDocumentTypeEnum?.drivingLicence?.value,
          screenPopCallBack: screenPopCallBack);
    }
    //Navigate to Work Permit screen
    else if (myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
            ?.workPermit?.workPermitStatus ==
        DocumentStatus?.NotUploaded?.value) {
      navigateToScreens(
          index: DriverDocumentTypeEnum?.workPermit?.value,
          screenPopCallBack: screenPopCallBack);
    }
    //Navigate to Vehicle Insurance Screen
    else if (myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
            ?.vehicleInsurance?.vehicleInsurenceStatus ==
        DocumentStatus?.NotUploaded?.value) {
      navigateToScreens(
          index: DriverDocumentTypeEnum?.vehicleInsurance?.value,
          screenPopCallBack: screenPopCallBack);
    }
    //Navigate to Background Verification screen
    else if (myVehicleDetailsState?.vehicleRegistrationResponseModel?.data
            ?.backgroundVerification?.backgroundVerifcationStatus ==
        DocumentStatus?.NotUploaded?.value) {
      navigateToScreens(
          index: DriverDocumentTypeEnum?.backgroundVerificationDocument?.value,
          screenPopCallBack: screenPopCallBack);
    }
  }

  checkAccountApprovedOrNot({BuildContext context}) async {
    var result =
        await CheckAccountLinkedOrNotProvider().checkAccountLinkedOrNotProvider(
      context: context,
    );

    CommonResponseModel commonResponseModel;
    if (result != null) {
      // check result status
      if (result[ApiStatusParams.Status.value] != null &&
          result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, AuthRoutes.HOME_SCREEN_ROOT);
      }
      // failure case
      else {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
      }
    } else {
      NavigatorUtils.navigatorUtilsInstance
          .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
    }
  }

  //Redirect to next screen based on index
  void navigateToScreens(
      {int index, ScreenPopCallBack screenPopCallBack}) async {
    CommonPassDataModel commonPassDataModel;

    commonPassDataModel = new CommonPassDataModel(
        vehicleRegistrationResponseModel:
            myVehicleDetailsState?.vehicleRegistrationResponseModel,
        screenPopCallBack: screenPopCallBack);

    bool _update;

    //Vehicle Registration Screen
    if (index == DriverDocumentTypeEnum?.vehicleRegisteration?.value) {
      _update = await NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedNameResult(context, RegistrationRoutes.VEHICLE_REG,
              dataToBeSend: commonPassDataModel);
    }
    //Driving Licence Screen
    else if (index == DriverDocumentTypeEnum?.drivingLicence?.value) {
      _update = await NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
          context, RegistrationRoutes.LICENCE_DETAILS,
          dataToBeSend: commonPassDataModel);
    }
    //Work Permit
    else if (index == DriverDocumentTypeEnum?.workPermit?.value) {
      _update = await NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedNameResult(
              context, RegistrationRoutes.WORK_PERMIT_PAGE,
              dataToBeSend: commonPassDataModel);
    }
    //Vehicle Insurance
    else if (index == DriverDocumentTypeEnum?.vehicleInsurance?.value) {
      _update = await NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
          context, RegistrationRoutes.INSURANCE_DETAILS,
          dataToBeSend: commonPassDataModel);
    }
    //Background verification
    else {
      _update = await NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
          context, RegistrationRoutes.BACKGROUND_VERIFICATION,
          dataToBeSend: commonPassDataModel);
    }

    //Call api to get updated status
    if (_update == true) {
      actionGetVehicleDetails();
    }
  }

  //Action on back click
  void actionOnBackClick(
      {DriverDetailActionManagers driverDetailActionManagers,
      String comeFrom,
      AuthManager authManager,
      AuthBloc authBloc,
      AuthState authState,
      ScaffoldState scaffoldState}) {
    if (comeFrom != PROFILE) {
      authManager?.signOutDialog(
          context: context,
          authState: authState,
          scaffoldState: scaffoldState,
          authBloc: authBloc);
    } else {
      if (DriverDetailUtillManager.driverDetailUtillInstance
          .checkAllDocumentApprovedOrNot(
              driverDetailActionManagers: driverDetailActionManagers,
              comeFrom: comeFrom)) {
        NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
      } else {
        authManager?.signOutDialog(
            context: context,
            authState: authState,
            scaffoldState: scaffoldState,
            authBloc: authBloc);
      }
    }
  }
}
