import 'dart:io';
import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/model/common_pass_data_model.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/date_picker_utils.dart';
import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:boozin_driver/modules/common/utils/image_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/image_constants.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/driving_licence/bloc/driver_licence_detail_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/driving_licence/bloc/driving_licence_detail_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/driving_licence/manager/driving_licence_action_manager.dart';

class DrivingLicencePage extends StatefulWidget {
  VehicleRegistrationResponseModel vehicleRegistrationResponseModel;
  BuildContext context;
  CommonPassDataModel commonPassDataModel;

  DrivingLicencePage(this.context) {
    commonPassDataModel = ModalRoute.of(context).settings.arguments;
    vehicleRegistrationResponseModel =
        commonPassDataModel?.vehicleRegistrationResponseModel;
  }

  @override
  _DrivingLicencePageState createState() => _DrivingLicencePageState();
}

class _DrivingLicencePageState extends State<DrivingLicencePage> {
  // text editing controllers
  final TextEditingController _licenceNumberController =
      new TextEditingController();
  final TextEditingController _expiryController = new TextEditingController();

  //managers
  DrivingLicennceActionManager _drivingLicenceActionManager =
      new DrivingLicennceActionManager();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Bloc
  DrivingLicenceDetailsBloc _drivingLicenceDetailBloc =
      new DrivingLicenceDetailsBloc();

  @override
  void initState() {
    super.initState();

    _drivingLicenceActionManager?.context = widget?.context;
    _drivingLicenceActionManager?.drivingLicenceDetailBloc =
        _drivingLicenceDetailBloc;
    _drivingLicenceActionManager?.commonPassDataModel =
        widget?.commonPassDataModel;

    _drivingLicenceActionManager.actionOnInit(
        currentState: _scaffoldKey?.currentState);
  }

  @override
  void dispose() {
    super.dispose();
    _expiryController?.dispose();
    _licenceNumberController?.dispose();

    _drivingLicenceActionManager?.drivingLicenceDetailBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _drivingLicenceActionManager?.context = context;
    return BlocEventStateBuilder<DrivingLicenceDetailsState>(
      bloc: _drivingLicenceActionManager?.drivingLicenceDetailBloc,
      builder: (BuildContext context,
          DrivingLicenceDetailsState drivingLicenceDetailsState) {
        _drivingLicenceActionManager?.context = context;
        if (drivingLicenceDetailsState != null &&
            _drivingLicenceActionManager?.drivingLicenceDetailsState !=
                drivingLicenceDetailsState) {
          _drivingLicenceActionManager?.drivingLicenceDetailsState =
              drivingLicenceDetailsState;

          if (_drivingLicenceActionManager
                  ?.drivingLicenceDetailsState?.isLoading ==
              false) {
            _setDataToControllers();

            _drivingLicenceActionManager?.actionOnStateChange(
                currentState: _scaffoldKey?.currentState);
          }
        }
        return ModalProgressHUD(
            inAsyncCall: _drivingLicenceActionManager
                    ?.drivingLicenceDetailsState?.isLoading ??
                false,
            child: Scaffold(
              key: _scaffoldKey,
              appBar: _showAppBar(),
              body: WillPopScope(
                onWillPop: () {
                  if (widget.commonPassDataModel.screenPopCallBack != null) {
                    widget.commonPassDataModel.screenPopCallBack.onScreenPop();
                  }
                  return NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(_drivingLicenceActionManager?.context,
                          dataToBeSend: true);
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: SIZE_15, right: SIZE_15, bottom: SIZE_15),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _showDivider(),
                        _showVehicleRegistrationTitle(),
                        SizedBox(
                          height: SIZE_15,
                        ),
                        _showUploadPhotoView(),
                        SizedBox(
                          height: SIZE_20,
                        ),
                        _showVehicleDetailsForm()
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _drivingLicenceActionManager?.context,
        elevation: ELEVATION_0,
        centerTitle: false,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitle:
            AppLocalizations.of(context).driverDetails.text.driverDetails,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () {
          if (widget.commonPassDataModel.screenPopCallBack != null) {
            widget.commonPassDataModel.screenPopCallBack.onScreenPop();
          }
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _drivingLicenceActionManager?.context,
              dataToBeSend: true);
        },
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle: AppConfig.of(_drivingLicenceActionManager?.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show vehicle registration title
  Widget _showVehicleRegistrationTitle() {
    return Text(
        AppLocalizations.of(_drivingLicenceActionManager?.context)
            .drivingLicence
            .text
            .title,
        style: AppConfig.of(_drivingLicenceActionManager?.context)
            .themeData
            .primaryTextTheme
            .headline5);
  }

  //method to show upload photo view
  Widget _showUploadPhotoView() {
    return Card(
      color: COLOR_ORANGE,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(SIZE_15))),
      elevation: ELEVATION_05,
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          _getImageView(),
          InkWell(
            onTap: () {
              _drivingLicenceActionManager?.actionOnUploadImage(
                  currentState: _scaffoldKey?.currentState);
            },
            child: Container(
              decoration: new BoxDecoration(
                color: LIGHT_PINK,
                borderRadius: new BorderRadius.only(
                    bottomRight: Radius.circular(SIZE_15),
                    bottomLeft: Radius.circular(SIZE_15)),
                shape: BoxShape.rectangle,
              ),
              height: SIZE_40,
              alignment: Alignment.center,
              child: Text(
                AppLocalizations.of(_drivingLicenceActionManager?.context)
                    .common
                    .text
                    .uploadPhoto,
                style: TextStyle(color: Colors.black),
              ),
            ),
          )
        ],
      ),
    );
  }

  //method to show vehicle details form widget
  Widget _showVehicleDetailsForm() {
    return Wrap(
      runSpacing: SIZE_10,
      children: [
        _showRegistrationNumber(),
        _showVehicleCompany(),
        _showNextButton()
      ],
    );
  }

  Widget _textFieldForm({
    TextEditingController controller,
    TextInputType keyboardType,
    Widget icon,
    String prefixIcon,
    String hint,
    bool isInputFormater,
    double elevation,
    FormFieldValidator<String> validator,
    bool enable,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_5),
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        style: AppConfig.of(_drivingLicenceActionManager?.context)
            .themeData
            .textTheme
            .headline2,
        enabled: enable,
        controller: controller,
        keyboardType: keyboardType,
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(color: COLOR_BORDER_GREY),
          suffixIcon: icon ?? const SizedBox(),
          contentPadding: EdgeInsets.only(left: SIZE_20),
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(SIZE_30),
            ),
            borderSide:
                new BorderSide(width: SIZE_0_2, color: COLOR_TEXTFIELD_BORDER),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(SIZE_30),
            ),
            borderSide:
                new BorderSide(width: SIZE_1, color: COLOR_TEXTFIELD_BORDER),
          ),
          // enabledBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_RED),
          // ),
          // disabledBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_TEXTFIELD_BORDER),
          // ),
          // errorBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_RED),
          // ),
        ),
      ),
    );
  }

  //registration number
  Widget _showRegistrationNumber() {
    return _textFieldForm(
        controller: _licenceNumberController,
        icon: null,
        isInputFormater: false,
        hint: AppLocalizations.of(_drivingLicenceActionManager?.context)
            .drivingLicence
            .text
            .licenceNumber,
        enable: true);
  }

  //vehicle company
  Widget _showVehicleCompany() {
    return InkWell(
      onTap: () {
        _showDatePicker(_drivingLicenceActionManager?.context);
      },
      child: _textFieldForm(
          controller: _expiryController,
          icon: Icon(
            Icons.calendar_today,
            color: COLOR_PRIMARY,
          ),
          isInputFormater: false,
          hint: AppLocalizations.of(_drivingLicenceActionManager?.context)
              .drivingLicence
              .text
              .expiryDate,
          enable: false),
    );
  }

  //Method to show next button
  Widget _showNextButton() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_20, bottom: SIZE_20),
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_7, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_80, ofWidth: true),
        child: RaisedGradientButton(
          radious: SIZE_30,
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
          ),
          onPressed: () {
            _drivingLicenceActionManager?.actionOnClickNextButton(
                currentState: _scaffoldKey?.currentState,
                licenceNumberController: _licenceNumberController,
                expiryController: _expiryController);
          },
          child: Text(
            AppLocalizations.of(_drivingLicenceActionManager?.context)
                .common
                .text
                .next,
            textAlign: TextAlign.center,
            style: AppConfig.of(context).themeData.textTheme.subtitle1,
          ),
        ),
      ),
    );
  }

  //Method to set data to controllers while update details
  void _setDataToControllers() {
    _licenceNumberController?.text =
        _licenceNumberController?.text?.trim()?.isNotEmpty == true
            ? _licenceNumberController?.text
            : _drivingLicenceActionManager
                    ?.drivingLicenceDetailsState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.drivingLicence
                    ?.drivingLicenceNo ??
                "";
    _expiryController?.text =
        _expiryController?.text?.trim()?.isNotEmpty == true
            ? _expiryController?.text
            : CustomDateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
                    _drivingLicenceActionManager
                            ?.drivingLicenceDetailsState
                            ?.vehicleRegistrationResponseModel
                            ?.data
                            ?.drivingLicence
                            ?.drivingLicenceExpiryDate ??
                        "") ??
                "";
  }

  //Selecting date from date picker
  Future<void> _showDatePicker(BuildContext context) async {
    var picked = await DatePickerUtils.datePickerUtilsInstance.getDatePicker(
        context: context,
        selectedDate: _expiryController.text,
        lastDate: DateTime(2500),
        firstDate: DateTime.now());
    _expiryController.text = picked.isEmpty
        ? CustomDateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
            _drivingLicenceActionManager
                    ?.drivingLicenceDetailsState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.drivingLicence
                    ?.drivingLicenceStatus ??
                "")
        : picked;
    ;
  }

  //method to show view of image uploaded by user or empty image view
  Widget _getImageView() {
    if (widget?.vehicleRegistrationResponseModel?.data?.drivingLicence
                ?.drivingLicenceImages !=
            null &&
        widget?.vehicleRegistrationResponseModel?.data?.drivingLicence
                ?.drivingLicenceImages?.isNotEmpty ==
            true) {
      //this will execute in the case of edit licence doc
      return _getImageWithUrl(
          url: widget?.vehicleRegistrationResponseModel?.data?.drivingLicence
              ?.drivingLicenceImages);
      //this will execute when user upload image or edit image
    } else if (_drivingLicenceActionManager
            ?.drivingLicenceDetailsState
            ?.vehicleRegistrationResponseModel
            ?.data
            ?.drivingLicence
            ?.drivingLicenceImages
            ?.isNotEmpty ==
        true) {
      return _getImageWithUrl(
          url: _drivingLicenceActionManager
              ?.drivingLicenceDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.drivingLicence
              ?.drivingLicenceImages);
    } else {
      //this is default condition when no image uploaded
      return Row(
        children: [
          Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: SIZE_10),
                  child: SvgPicture.asset(
                    VEHICLE_ICON,
                    color: Colors.white,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _drivingLicenceActionManager?.context,
                        percentage: SIZE_10,
                        ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _drivingLicenceActionManager?.context,
                        percentage: SIZE_10,
                        ofWidth: true),
                  ),
                ),
              )),
          Expanded(
              flex: 3,
              child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: SIZE_25, right: SIZE_25),
                  height: CommonUtils.commonUtilsInstance.getPercentageSize(
                      context: _drivingLicenceActionManager?.context,
                      percentage: SIZE_20,
                      ofWidth: false),
                  child: Wrap(
                    runSpacing: SIZE_10,
                    children: [
                      Container(
                        height: SIZE_45,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_5),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      ),
                      Container(
                        height: SIZE_15,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_20),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      ),
                      Container(
                        height: SIZE_15,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_20),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      )
                    ],
                  )))
        ],
      );
    }
  }

  //this widget will return a cache network image
  Widget _showCacheNetworkImage({String imageUrl}) {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
      context: context,
      showProgressBarInPlaceHolder: true,
      shape: BoxShape.rectangle,
      loaderHeight: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _drivingLicenceActionManager?.context,
          percentage: SIZE_40,
          ofWidth: false),
      loaderWidth: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _drivingLicenceActionManager?.context,
          percentage: SIZE_40,
          ofWidth: true),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _drivingLicenceActionManager?.context,
          percentage: SIZE_24,
          ofWidth: false),
      width: double.infinity,
      url: imageUrl ?? "",
    );
  }

  //get widget which will show image
  Widget _getImageWithUrl({String url}) {
    return Container(
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _drivingLicenceActionManager?.context,
            percentage: SIZE_24,
            ofWidth: false),
        width: MediaQuery.of(_drivingLicenceActionManager?.context).size.width,
        child: _showCacheNetworkImage(imageUrl: url));
  }

//method to show divider text
  Widget _showDivider() {
    return Divider(
      color: COLOR_BORDER_GREY,
    );
  }
}
