import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:boozin_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:boozin_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:boozin_driver/modules/auth/manager/auth_manager.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/screen_pop_callback.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/bloc/driver_details_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/bloc/driver_details_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/enums/driver_document_type_enums.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/manager/driver_detail_action_manager.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/manager/driver_detail_utills_manager.dart';

class DriverDetailsPage extends StatefulWidget {
  BuildContext context;
  String cameFrom;

  DriverDetailsPage(this.context) {
    cameFrom = ModalRoute.of(context).settings.arguments;
  }

  @override
  _DriverDetailsPageState createState() => _DriverDetailsPageState();
}

class _DriverDetailsPageState extends State<DriverDetailsPage>
    implements ScreenPopCallBack, PushReceived {
  //Managers
  DriverDetailActionManagers _driverDetailActionManagers =
      DriverDetailActionManagers();
  DriverDetailUtillManager _driverDetailUtillManager =
      new DriverDetailUtillManager();

  //Scaffold Key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AuthBloc _authBloc;
  AuthManager _authManager;
  AuthState _authState;

  @override
  void initState() {
    super.initState();
    _authManager = AuthManager();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _driverDetailActionManagers?.context = widget?.context;
    _driverDetailActionManagers?.myVehicleDetailsBloc =
        new MyVehicleDetailsBloc();

    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    //Call api to get vehicle detail
    _driverDetailActionManagers?.actionGetVehicleDetails();

    //Add Callback listener to refresh screen
    ScreenPopBack.screenPopBack.addCallback(screenPopCallBack: this);
  }

  @override
  void dispose() {
    super.dispose();
    _driverDetailActionManagers?.myVehicleDetailsBloc?.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);

    //Remove callback
    ScreenPopBack.screenPopBack.removeCallback(screenPopCallBack: this);
  }

  @override
  Widget build(BuildContext context) {
    _driverDetailActionManagers?.context = context;

    return WillPopScope(
      onWillPop: () {
        _driverDetailActionManagers?.actionOnBackClick(
            comeFrom: widget?.cameFrom,
            driverDetailActionManagers: _driverDetailActionManagers,
            authBloc: _authBloc,
            authState: _authState,
            scaffoldState: _scaffoldKey.currentState,
            authManager: _authManager);
      },
      child: BlocEventStateBuilder<AuthState>(
        bloc: _authBloc,
        builder: (BuildContext context, AuthState authState) {
          _driverDetailActionManagers.context = context;
          if (_authState != authState) {
            _authState = authState;

            _authManager.actionOnStateUpdate(
                context: _driverDetailActionManagers.context,
                authBloc: _authBloc,
                authState: _authState,
                scaffoldState: _scaffoldKey.currentState);
          }
          return ModalProgressHUD(
              inAsyncCall: _authState?.isLoading ?? false,
              child: BlocEventStateBuilder<MyVehicleDetailsState>(
                bloc: _driverDetailActionManagers?.myVehicleDetailsBloc,
                builder: (BuildContext context,
                    MyVehicleDetailsState myVehicleDetailsState) {
                  _driverDetailActionManagers?.context = context;
                  if (myVehicleDetailsState != null &&
                      _driverDetailActionManagers?.myVehicleDetailsState !=
                          myVehicleDetailsState) {
                    _driverDetailActionManagers?.myVehicleDetailsState =
                        myVehicleDetailsState;
                  }
                  return ModalProgressHUD(
                    inAsyncCall: myVehicleDetailsState?.isLoading ?? false,
                    child: Scaffold(
                        key: _scaffoldKey,
                        appBar: _showAppBar(),
                        body: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.all(SIZE_10),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  _showListView(),
                                  SizedBox(
                                    height: SIZE_60,
                                  ),
                                  _showNote(),
                                  SizedBox(
                                    height: SIZE_30,
                                  ),
                                  _showProceedButton()
                                ]),
                          ),
                        )),
                  );
                },
              ));
        },
      ),
    );
  }

  //Method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _driverDetailActionManagers?.context,
        elevation: ELEVATION_0,
        centerTitle: false,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitle:
            AppLocalizations.of(context).driverDetails.text.driverDetails,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        defaultLeadIconPressed: () {
          _driverDetailActionManagers?.actionOnBackClick(
              comeFrom: widget.cameFrom,
              driverDetailActionManagers: _driverDetailActionManagers,
              authBloc: _authBloc,
              authState: _authState,
              scaffoldState: _scaffoldKey.currentState,
              authManager: _authManager);
        },
        appBarTitleStyle: AppConfig.of(_driverDetailActionManagers?.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  //Method to show proceed Button
  Widget _showProceedButton() {
    return _driverDetailUtillManager.showProceedButton(
            driverDetailActionManagers: _driverDetailActionManagers,
            comeFrom: widget?.cameFrom)
        ? Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.only(top: SIZE_30, bottom: SIZE_20),
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_6, ofWidth: false),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_50, ofWidth: true),
              child: RaisedGradientButton(
                radious: SIZE_30,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
                ),
                onPressed: () {
                  _driverDetailActionManagers?.actionOnClickProceedButton(
                      screenPopCallBack: this);
                },
                child: Text(
                  AppLocalizations.of(_driverDetailActionManagers.context)
                      .driverDetails
                      .button
                      .proceed,
                  textAlign: TextAlign.center,
                  style: AppConfig.of(_driverDetailActionManagers.context)
                      .themeData
                      .textTheme
                      .subtitle1,
                ),
              ),
            ),
          )
        : SizedBox();
  }

  //This method is used to show documents steps
  Widget _showListView() {
    return ListView.builder(
        itemCount: 5,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: CommonUtils?.commonUtilsInstance?.getPercentageSize(
                context: _driverDetailActionManagers?.context,
                percentage: SIZE_9,
                ofWidth: false),
            width: CommonUtils?.commonUtilsInstance?.getPercentageSize(
                context: _driverDetailActionManagers?.context,
                percentage: SIZE_10,
                ofWidth: true),
            child: InkWell(
              onTap: () {
                _driverDetailActionManagers.navigateToScreens(
                    index: index, screenPopCallBack: this);
              },
              child: Card(
                color: LIGHT_PINK,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    side: BorderSide(width: 0.2, color: COLOR_BLACK)),
                elevation: ELEVATION_05,
                child: Padding(
                  padding: const EdgeInsets.all(SIZE_8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: _showItemTitle(index),
                                style: AppConfig.of(
                                        _driverDetailActionManagers?.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline6),
                            _driverDetailUtillManager.getStatusValue(
                                index: index,
                                driverDetailActionManagers:
                                    _driverDetailActionManagers),
                          ],
                        ),
                      ),
                      Icon(Icons.arrow_forward_ios, size: SIZE_16)
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  //To show the item title
  String _showItemTitle(int index) {
    if (index == DriverDocumentTypeEnum?.vehicleRegisteration?.value) {
      return AppLocalizations.of(context)
          .driverDetails
          .text
          .vehicleRegistration;
    } else if (index == DriverDocumentTypeEnum?.drivingLicence?.value) {
      return AppLocalizations.of(context).driverDetails.text.drivingLicence;
    } else if (index == DriverDocumentTypeEnum?.workPermit?.value) {
      return AppLocalizations.of(context).workPermit.text.title;
    } else if (index == DriverDocumentTypeEnum?.vehicleInsurance?.value) {
      return AppLocalizations.of(context).driverDetails.text.vehicleInsurance;
    } else {
      return AppLocalizations.of(context)
          .driverDetails
          .text
          .backgroundVerification;
    }
  }

  //This method is used to show note to the driver
  Widget _showNote() {
    return Text.rich(
      TextSpan(
        children: [
          TextSpan(
              text: AppLocalizations.of(context).driverDetails.text.noteText,
              style: textStyleSize14WithRED),
          TextSpan(
            text: AppLocalizations.of(context).driverDetails.text.noteDetail,
            style: textStyleSize14WithRED500,
          ),
        ],
      ),
    );
  }

  @override
  onScreenPop({int callbackValue}) {
    _driverDetailActionManagers?.actionGetVehicleDetails();
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _driverDetailActionManagers?.actionGetVehicleDetails();
  }
}
