import 'dart:io';

import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/work_permit/bloc/work_permit_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/work_permit/bloc/work_permit_event.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/work_permit/bloc/work_permit_state.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/enums/auth_enums.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/model/common_pass_data_model.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:boozin_driver/modules/file_picker/dialog/single_file_picker_dialog.dart';
import 'package:boozin_driver/modules/file_picker/enums/file_picker_enums.dart';
import 'package:boozin_driver/modules/file_picker/model/file_picker_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/validator/vehicle_registration_validator.dart';

import '../../../../registration_routes.dart';

class WorkPermitActionManager {
  BuildContext context;
  WorkPermitBloc workPermitBloc;
  WorkPermitState workPermitState;
  CommonPassDataModel commonPassDataModel;

  //Action on init
  void actionOnInit({ScaffoldState currentState}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        workPermitBloc?.emitEvent(UpdateWorkPermitEvent(
            context: context,
            isLoading: false,
            vehicleRegistrationResponseModel:
                commonPassDataModel?.vehicleRegistrationResponseModel));
      }
    });
  }

  //Use to upload image
  void actionOnUploadImage({ScaffoldState currentState}) {
    SingleFilePickerDialog.singleFilePickerDialogInstance.showFilePickerDialog(
        filePickerModel: FilePickerModel(
          pickerType: FilePickerTypeEnum.PickerTypeImage,
          pickFrom: FilePickFromEnum.PickFromBoth,
        ),
        context: context,
        fileData: (value) {
          var _image = File(value);
          if (_image != null) {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context, showNetworkDialog: true)
                .then((onValue) async {
              if (onValue) {
                workPermitBloc?.emitEvent(UploadImageWorkPermitEvent(
                    context: context,
                    isLoading: true,
                    file: _image,
                    vehicleRegistrationResponseModel:
                        commonPassDataModel?.vehicleRegistrationResponseModel));
              }
            });
            // _imageFile = _image;
            // _callApi();
          }
        });
  }

  //Action on state change
  void actionOnStateChange({ScaffoldState currentState}) {
    if (workPermitState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        if (workPermitState?.message?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              message: workPermitState?.message?.trim(),
              scaffoldState: currentState);
        }

        if (workPermitState?.status == ApiStatus.Success.value) {
          if (workPermitState?.vehicleRegistrationResponseModel?.data
                  ?.vehicleInsurance?.vehicleInsurenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.INSURANCE_DETAILS,
                dataToBeSend: commonPassDataModel);
          } else if (workPermitState?.vehicleRegistrationResponseModel?.data
                  ?.backgroundVerification?.backgroundVerifcationStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.BACKGROUND_VERIFICATION,
                dataToBeSend: commonPassDataModel);
          } else if (workPermitState?.vehicleRegistrationResponseModel?.data
                  ?.vehicleRegisteration?.vehicleStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.VEHICLE_REG,
                dataToBeSend: commonPassDataModel);
          } else if (workPermitState?.vehicleRegistrationResponseModel?.data
                  ?.drivingLicence?.drivingLicenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.LICENCE_DETAILS,
                dataToBeSend: commonPassDataModel);
          } else {
            if (commonPassDataModel.screenPopCallBack != null) {
              commonPassDataModel.screenPopCallBack.onScreenPop();
            }
            NavigatorUtils.navigatorUtilsInstance
                .navigatorPopScreen(context, dataToBeSend: true);
          }
        }
      });
    }
  }

  //Action click on next button
  void actionOnClickNextButton(
      {ScaffoldState scaffoldState,
      TextEditingController expiryDateController,
      TextEditingController idNumberController,
      TextEditingController vehicleCapacityController,
      TextEditingController permitNumberController}) {
    // check name field is empty or not
    String result = VehicleRegistrationValidator.vehicleRegistrationInstance
        .validateWorkPermitScreen(
            context: context,
            expiryDate: expiryDateController.text,
            idNumber: idNumberController.text,
            vehicleCapacity: vehicleCapacityController.text,
            permitNumber: permitNumberController.text,
            file: workPermitState?.vehicleRegistrationResponseModel?.data
                    ?.vehicleRegisteration?.vehiclePicture ??
                "");

    // if its not empty
    if (result.isEmpty) {
      DriverRegistrationRequestModel driverRegistrationRequestModel =
          new DriverRegistrationRequestModel(
              workPermit: WorkPermit(
        idNumber: idNumberController.text,
        weight: vehicleCapacityController.text,
        workPermitExpiryDate: expiryDateController.text,
        permitNumber: permitNumberController.text,
        workPermitImages: workPermitState?.vehicleRegistrationResponseModel
                ?.data?.workPermit?.workPermitImages ??
            "",
      ));

      //Check internet connection avaibale or not
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          workPermitBloc?.emitEvent(UploadWorkPermitEvent(
              context: context,
              isLoading: true,
              driverRegistrationRequestModel: driverRegistrationRequestModel,
              vehicleRegistrationResponseModel:
                  workPermitState?.vehicleRegistrationResponseModel));
        }
      });
    } else {
      // show error in fields
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }
}
