import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';

class WorkPermitState extends BlocState {
  WorkPermitState({
    this.isLoading: false,
    this.message,
    this.context,
    this.status,
    this.vehicleRegistrationResponseModel

  }) : super(isLoading);

  final bool isLoading;
  final String message;
  final int status;
  final BuildContext context;
  final VehicleRegistrationResponseModel vehicleRegistrationResponseModel;


  // used for update home page state
  factory WorkPermitState.getVehicleDetails({bool isLoading,
    String message,
    BuildContext context,
    int status,
    VehicleRegistrationResponseModel vehicleRegistrationResponseModel

  }) {
    return WorkPermitState(
        isLoading: isLoading,
        message: message,
        context: context,
        status: status,
        vehicleRegistrationResponseModel: vehicleRegistrationResponseModel

    );
  }

  factory WorkPermitState.initiating() {
    return WorkPermitState(
      isLoading: false,
    );
  }
}
