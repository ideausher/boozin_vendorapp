import 'dart:io';

import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';

abstract class WorkPermitEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final File file;
  final DriverRegistrationRequestModel driverRegistrationRequestModel;
  final VehicleRegistrationResponseModel vehicleRegistrationResponseModel;

  WorkPermitEvent(
      {this.isLoading: false,
      this.context,
      this.file,
      this.driverRegistrationRequestModel,
      this.vehicleRegistrationResponseModel});
}

//this event is used update driving licence event
class UpdateWorkPermitEvent extends WorkPermitEvent {
  UpdateWorkPermitEvent({
    bool isLoading,
    BuildContext context,
    VehicleRegistrationResponseModel vehicleRegistrationResponseModel,
  }) : super(
          isLoading: isLoading,
          context: context,
          vehicleRegistrationResponseModel: vehicleRegistrationResponseModel,
        );
}

//this event is use to upload driving licence data
class UploadWorkPermitEvent extends WorkPermitEvent {
  UploadWorkPermitEvent({
    bool isLoading,
    BuildContext context,
    DriverRegistrationRequestModel driverRegistrationRequestModel,
    VehicleRegistrationResponseModel vehicleRegistrationResponseModel,
  }) : super(
          isLoading: isLoading,
          context: context,
          driverRegistrationRequestModel: driverRegistrationRequestModel,
          vehicleRegistrationResponseModel: vehicleRegistrationResponseModel,
        );
}

//use to upload image
class UploadImageWorkPermitEvent extends WorkPermitEvent {
  UploadImageWorkPermitEvent({
    bool isLoading,
    BuildContext context,
    File file,
    VehicleRegistrationResponseModel vehicleRegistrationResponseModel,
  }) : super(
          isLoading: isLoading,
          context: context,
          file: file,
          vehicleRegistrationResponseModel: vehicleRegistrationResponseModel,
        );
}
