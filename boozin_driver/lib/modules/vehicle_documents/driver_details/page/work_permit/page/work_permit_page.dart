import 'dart:io';
import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:boozin_driver/modules/common/utils/date_picker_utils.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/work_permit/bloc/work_permit_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/work_permit/bloc/work_permit_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/work_permit/manager/work_permit_action_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/model/common_pass_data_model.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/image_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/image_constants.dart';

class WorkPermitPage extends StatefulWidget {
  VehicleRegistrationResponseModel driverRegistrationResponseModel;
  CommonPassDataModel commonPassDataModel;
  BuildContext context;

  WorkPermitPage(this.context) {
    commonPassDataModel = ModalRoute.of(context).settings.arguments;
    driverRegistrationResponseModel =
        commonPassDataModel?.vehicleRegistrationResponseModel;
  }

  @override
  _WorkPermitPageState createState() => _WorkPermitPageState();
}

class _WorkPermitPageState extends State<WorkPermitPage> {
  // text editing controllers
  TextEditingController _permitNumberController = new TextEditingController();
  TextEditingController _idNumberController = new TextEditingController();
  TextEditingController _vehicleCapacityController =
      new TextEditingController();
  TextEditingController _expiryDateController = new TextEditingController();

  //for vehicle licence image
  File imageFile;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Manager
  WorkPermitActionManager _workPermitActionManager;
  WorkPermitBloc _workPermitBloc = new WorkPermitBloc();

  @override
  void initState() {
    super.initState();

    _workPermitActionManager = new WorkPermitActionManager();
    _workPermitActionManager.context = widget.context;
    _workPermitActionManager.commonPassDataModel = widget.commonPassDataModel;
    _workPermitActionManager.workPermitBloc = _workPermitBloc;

    _workPermitActionManager.actionOnInit();
  }

  @override
  void dispose() {
    super.dispose();
    _permitNumberController.dispose();
    _vehicleCapacityController.dispose();
    _idNumberController.dispose();
    _expiryDateController.dispose();
    _workPermitActionManager?.workPermitBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _workPermitActionManager.context = context;
    return BlocEventStateBuilder<WorkPermitState>(
      bloc: _workPermitActionManager?.workPermitBloc,
      builder: (BuildContext context, WorkPermitState workPermitState) {
        _workPermitActionManager?.context = context;
        if (workPermitState != null &&
            _workPermitActionManager?.workPermitState != workPermitState) {
          _workPermitActionManager?.workPermitState = workPermitState;

          if (_workPermitActionManager?.workPermitState?.isLoading == false) {
            _setDataToControllers();
            _workPermitActionManager?.actionOnStateChange(
                currentState: _scaffoldKey?.currentState);
          }
        }
        return ModalProgressHUD(
            inAsyncCall:
                _workPermitActionManager?.workPermitState?.isLoading ?? false,
            child: Scaffold(
              key: _scaffoldKey,
              appBar: _showAppBar(),
              body: WillPopScope(
                onWillPop: () {
                  if (widget.commonPassDataModel.screenPopCallBack != null) {
                    widget.commonPassDataModel.screenPopCallBack.onScreenPop();
                  }
                  return NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(_workPermitActionManager.context,
                          dataToBeSend: true);
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: SIZE_15, right: SIZE_15, bottom: SIZE_15),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _showDivider(),
                        _showVehicleRegistrationTitle(),
                        SizedBox(
                          height: SIZE_15,
                        ),
                        _showUploadPhotoView(),
                        SizedBox(
                          height: SIZE_20,
                        ),
                        _showVehicleDetailsForm()
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _workPermitActionManager.context,
        elevation: ELEVATION_0,
        centerTitle: false,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitle: AppLocalizations.of(_workPermitActionManager.context)
            .driverDetails
            .text
            .driverDetails,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () {
          if (widget.commonPassDataModel.screenPopCallBack != null) {
            widget.commonPassDataModel.screenPopCallBack.onScreenPop();
          }
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _workPermitActionManager.context,
              dataToBeSend: true);
        },
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle: AppConfig.of(_workPermitActionManager.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show vehicle registration title
  Widget _showVehicleRegistrationTitle() {
    return Text(
        AppLocalizations.of(_workPermitActionManager.context)
            .workPermit
            .text
            .title,
        style: AppConfig.of(_workPermitActionManager.context)
            .themeData
            .primaryTextTheme
            .headline5);
  }

  //method to show upload photo view
  Widget _showUploadPhotoView() {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(SIZE_15))),
      color: COLOR_ORANGE,
      elevation: 5.0,
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          _getImageView(),
          InkWell(
            onTap: () {
              _workPermitActionManager?.actionOnUploadImage(
                  currentState: _scaffoldKey?.currentState);
            },
            child: Container(
              decoration: new BoxDecoration(
                color: LIGHT_PINK,
                borderRadius: new BorderRadius.only(
                    bottomRight: Radius.circular(SIZE_15),
                    bottomLeft: Radius.circular(SIZE_15)),
                shape: BoxShape.rectangle,
              ),
              height: SIZE_40,
              alignment: Alignment.center,
              child: Text(
                AppLocalizations.of(_workPermitActionManager.context)
                    .common
                    .text
                    .uploadPhoto,
                style: TextStyle(color: Colors.black),
              ),
            ),
          )
        ],
      ),
    );
  }

  //show vehicle details form
  Widget _showVehicleDetailsForm() {
    return Wrap(
      runSpacing: SIZE_10,
      children: [
        _showPermitNumber(),
        _showOrView(),
        _showIdNumber(),
        _showVehicleCapacity(),
        _showExpiryDate(),
        _showNextButton()
      ],
    );
  }

  Widget _textFieldForm({
    TextEditingController controller,
    TextInputType keyboardType,
    Widget icon,
    String prefixIcon,
    String hint,
    bool isInputFormater,
    double elevation,
    FormFieldValidator<String> validator,
    bool enable,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_5),
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        style: AppConfig.of(_workPermitActionManager?.context)
            .themeData
            .textTheme
            .headline2,
        enabled: enable,
        controller: controller,
        keyboardType: keyboardType,
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(color: COLOR_BORDER_GREY),
          suffixIcon: icon ?? const SizedBox(),
          contentPadding: EdgeInsets.only(left: SIZE_20),
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(SIZE_30),
            ),
            borderSide:
                new BorderSide(width: SIZE_0_2, color: COLOR_TEXTFIELD_BORDER),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(SIZE_30),
            ),
            borderSide:
                new BorderSide(width: SIZE_1, color: COLOR_TEXTFIELD_BORDER),
          ),
          // enabledBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_RED),
          // ),
          // disabledBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_TEXTFIELD_BORDER),
          // ),
          // errorBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_RED),
          // ),
        ),
      ),
    );
  }

  //registration number
  Widget _showPermitNumber() {
    return _textFieldForm(
        controller: _permitNumberController,
        icon: null,
        isInputFormater: false,
        hint: AppLocalizations.of(_workPermitActionManager.context)
            .workPermit
            .text
            .plateNumber,
        enable: true);
  }

  //id number
  Widget _showIdNumber() {
    return _textFieldForm(
        controller: _idNumberController,
        icon: null,
        isInputFormater: false,
        hint: AppLocalizations.of(_workPermitActionManager.context)
            .workPermit
            .text
            .idNumber,
        enable: true);
  }

  Widget _showExpiryDate() {
    return InkWell(
      onTap: () {
        _showDatePicker(_workPermitActionManager.context);
      },
      child: _textFieldForm(
          controller: _expiryDateController,
          icon: Icon(Icons.calendar_today, color: COLOR_PRIMARY),
          isInputFormater: false,
          hint: AppLocalizations.of(_workPermitActionManager.context)
              .workPermit
              .hint
              .expiryDate,
          enable: false),
    );
  }

  //Selecting date from date picker
  Future<void> _showDatePicker(BuildContext context) async {
    var picked = await DatePickerUtils.datePickerUtilsInstance.getDatePicker(
        context: context,
        selectedDate: _expiryDateController.text,
        lastDate: DateTime(2500),
        firstDate: DateTime.now());
    _expiryDateController.text = picked.isEmpty
        ? CustomDateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
            _workPermitActionManager
                    ?.workPermitState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.workPermit
                    ?.workPermitStatus ??
                "")
        : picked;
    ;
  }

  //method to show next button
  Widget _showNextButton() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_20, bottom: SIZE_20),
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_7, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_80, ofWidth: true),
        child: RaisedGradientButton(
          radious: SIZE_30,
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
          ),
          onPressed: () {
            _workPermitActionManager?.actionOnClickNextButton(
                scaffoldState: _scaffoldKey.currentState,
                permitNumberController: _permitNumberController,
                expiryDateController: _expiryDateController,
                vehicleCapacityController: _vehicleCapacityController,
                idNumberController: _idNumberController);
          },
          child: Text(
            AppLocalizations.of(_workPermitActionManager.context)
                .common
                .text
                .next,
            textAlign: TextAlign.center,
            style: AppConfig.of(context).themeData.textTheme.subtitle1,
          ),
        ),
      ),
    );
  }

  //method to set data in controllers
  void _setDataToControllers() {
    //Set Permit Number
    _permitNumberController?.text =
        _permitNumberController?.text?.trim()?.isNotEmpty == true
            ? _permitNumberController?.text
            : _workPermitActionManager
                    ?.workPermitState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.workPermit
                    ?.permitNumber ??
                "";

    //Set Id Number
    _idNumberController?.text =
        _idNumberController?.text?.trim()?.isNotEmpty == true
            ? _idNumberController?.text
            : _workPermitActionManager
                    ?.workPermitState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.workPermit
                    ?.idNumber ??
                "";

    //Vehicle Capacity Controller
    _vehicleCapacityController?.text =
        _vehicleCapacityController?.text?.trim()?.isNotEmpty == true
            ? _vehicleCapacityController?.text
            : _workPermitActionManager
                        ?.workPermitState
                        ?.vehicleRegistrationResponseModel
                        ?.data
                        ?.workPermit
                        ?.weight ==
                    null
                ? ""
                : _workPermitActionManager?.workPermitState
                    ?.vehicleRegistrationResponseModel?.data?.workPermit?.weight
                    .toString();

    //Set Expiry Date Controller
    _expiryDateController?.text =
        _expiryDateController?.text?.trim()?.isNotEmpty == true
            ? _expiryDateController?.text
            : CustomDateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
                    _workPermitActionManager
                            ?.workPermitState
                            ?.vehicleRegistrationResponseModel
                            ?.data
                            ?.workPermit
                            ?.workPermitExpiryDate ??
                        "") ??
                "";
  }

  //method to show view of image uploaded by user or empty image view
  Widget _getImageView() {
    if ((widget?.driverRegistrationResponseModel?.data?.workPermit
                ?.workPermitImages !=
            null &&
        widget?.driverRegistrationResponseModel?.data?.workPermit
                ?.workPermitImages?.isNotEmpty ==
            true)) {
      //this will execute in the case of edit licence doc
      return _getWidget(
          url: widget?.driverRegistrationResponseModel?.data?.workPermit
              ?.workPermitImages);
    } else if (_workPermitActionManager
            ?.workPermitState
            ?.vehicleRegistrationResponseModel
            ?.data
            ?.workPermit
            ?.workPermitImages
            ?.isNotEmpty ==
        true) {
      //this will execute when user upload image or edit image
      return _getWidget(
          url: _workPermitActionManager
              ?.workPermitState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.workPermit
              ?.workPermitImages);
    }
    //this is default condition when no image uploaded
    else {
      return Row(
        children: [
          Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: SIZE_10),
                  child: SvgPicture.asset(
                    VEHICLE_ICON,
                    color: Colors.white,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _workPermitActionManager.context,
                        percentage: SIZE_10,
                        ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _workPermitActionManager.context,
                        percentage: SIZE_10,
                        ofWidth: true),
                  ),
                ),
              )),
          Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: SIZE_25, right: SIZE_25),
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _workPermitActionManager.context,
                    percentage: SIZE_20,
                    ofWidth: false),
                child: Wrap(
                  runSpacing: SIZE_10,
                  children: [
                    Container(
                      height: SIZE_45,
                      decoration: new BoxDecoration(
                        color: COLOR_LIGHT_PINK,
                        borderRadius: new BorderRadius.all(
                          Radius.circular(SIZE_5),
                        ),
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      height: SIZE_15,
                      decoration: new BoxDecoration(
                        color: COLOR_LIGHT_PINK,
                        borderRadius: new BorderRadius.all(
                          Radius.circular(SIZE_20),
                        ),
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      height: SIZE_15,
                      decoration: new BoxDecoration(
                        color: COLOR_LIGHT_PINK,
                        borderRadius: new BorderRadius.all(
                          Radius.circular(SIZE_20),
                        ),
                        shape: BoxShape.rectangle,
                      ),
                    )
                  ],
                ),
              ))
        ],
      );
    }
  }

  //this widget will return a cache network image
  Widget _showCacheNetworkImage({String imageUrl}) {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
      context: context,
      showProgressBarInPlaceHolder: true,
      shape: BoxShape.rectangle,
      loaderHeight: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _workPermitActionManager.context,
          percentage: SIZE_40,
          ofWidth: false),
      loaderWidth: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _workPermitActionManager.context,
          percentage: SIZE_40,
          ofWidth: true),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _workPermitActionManager.context,
          percentage: SIZE_24,
          ofWidth: false),
      width: double.infinity,
      url: imageUrl ?? "",
    );
  }

  //get widget which will show image
  Widget _getWidget({String url}) {
    return Container(
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _workPermitActionManager.context,
            percentage: SIZE_24,
            ofWidth: false),
        width: MediaQuery.of(context).size.width,
        child: _showCacheNetworkImage(imageUrl: url));
  }

//method to show divider text
  Widget _showDivider() {
    return Divider(
      color: COLOR_BORDER_GREY,
    );
  }

  Widget _showOrView() {
    return Row(
      children: [
        Expanded(
          child: Divider(
            color: COLOR_LIGHT_GREY,
          ),
        ),
        SizedBox(
          width: SIZE_10,
        ),
        Text(
          AppLocalizations.of(context).workPermit.text.or,
          style: AppConfig.of(context).themeData.primaryTextTheme.headline4,
        ),
        SizedBox(
          width: SIZE_10,
        ),
        Expanded(
          child: Divider(
            color: COLOR_LIGHT_GREY,
          ),
        ),
      ],
    );
  }

  //Total weight
  _showVehicleCapacity() {
    return _textFieldForm(
        controller: _vehicleCapacityController,
        icon: null,
        isInputFormater: false,
        keyboardType: TextInputType.number,
        hint: AppLocalizations.of(context).workPermit.hint.capacity,
        enable: true);
  }
}
