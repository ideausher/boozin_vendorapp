import 'dart:io';
import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/background_verification/bloc/background_verification_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/background_verification/bloc/background_verification_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/background_verification/manager/background_verification_action_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/model/common_pass_data_model.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/date_picker_utils.dart';
import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:boozin_driver/modules/common/utils/image_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/image_constants.dart';

class BackgroundVerificationPage extends StatefulWidget {
  VehicleRegistrationResponseModel vehicleRegistrationResponseModel;
  BuildContext context;
  CommonPassDataModel commonPassDataModel;

  BackgroundVerificationPage(this.context) {
    commonPassDataModel = ModalRoute.of(context).settings.arguments;
    vehicleRegistrationResponseModel =
        commonPassDataModel?.vehicleRegistrationResponseModel;
  }

  @override
  _BackgroundVerificationPageState createState() =>
      _BackgroundVerificationPageState();
}

class _BackgroundVerificationPageState
    extends State<BackgroundVerificationPage> {
  //managers
  BackgroundVerificationActionManager _backgroundVerificationActionManager =
      new BackgroundVerificationActionManager();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Bloc
  BackgroundVerificationBloc _backgroundVerificationBloc =
      new BackgroundVerificationBloc();

  @override
  void initState() {
    super.initState();

    _backgroundVerificationActionManager?.context = widget?.context;
    _backgroundVerificationActionManager?.backgroundVerificationBloc =
        _backgroundVerificationBloc;
    _backgroundVerificationActionManager?.commonPassDataModel =
        widget?.commonPassDataModel;

    _backgroundVerificationActionManager.actionOnInit(
        currentState: _scaffoldKey?.currentState);
  }

  @override
  void dispose() {
    super.dispose();

    _backgroundVerificationActionManager?.backgroundVerificationBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _backgroundVerificationActionManager?.context = context;
    return BlocEventStateBuilder<BackgroundVerificationState>(
      bloc: _backgroundVerificationActionManager?.backgroundVerificationBloc,
      builder: (BuildContext context,
          BackgroundVerificationState backgroundVerificationState) {
        _backgroundVerificationActionManager?.context = context;
        if (backgroundVerificationState != null &&
            _backgroundVerificationActionManager?.backgroundVerificationState !=
                backgroundVerificationState) {
          _backgroundVerificationActionManager?.backgroundVerificationState =
              backgroundVerificationState;

          if (_backgroundVerificationActionManager
                  ?.backgroundVerificationState?.isLoading ==
              false) {
            // _setDataToControllers();

            _backgroundVerificationActionManager?.actionOnStateChange(
                currentState: _scaffoldKey?.currentState);
          }
        }
        return ModalProgressHUD(
            inAsyncCall: _backgroundVerificationActionManager
                    ?.backgroundVerificationState?.isLoading ??
                false,
            child: Scaffold(
              key: _scaffoldKey,
              appBar: _showAppBar(),
              body: WillPopScope(
                onWillPop: () {
                  if (widget.commonPassDataModel.screenPopCallBack != null) {
                    widget.commonPassDataModel.screenPopCallBack.onScreenPop();
                  }
                  return NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(
                          _backgroundVerificationActionManager?.context,
                          dataToBeSend: true);
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: SIZE_15, right: SIZE_15, bottom: SIZE_15),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _showDivider(),
                        _showBackgroundVerificationTitle(),
                        SizedBox(
                          height: SIZE_15,
                        ),
                        _showUploadPhotoView(),
                        SizedBox(
                          height: SIZE_20,
                        ),
                        _showNote(),

                        SizedBox(
                          height: SIZE_40,
                        ),
                        _showNextButton()
                        // _showVehicleDetailsForm()
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _backgroundVerificationActionManager?.context,
        elevation: ELEVATION_0,
        centerTitle: false,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitle:
            AppLocalizations.of(context).driverDetails.text.driverDetails,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () {
          if (widget.commonPassDataModel.screenPopCallBack != null) {
            widget.commonPassDataModel.screenPopCallBack.onScreenPop();
          }
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _backgroundVerificationActionManager?.context,
              dataToBeSend: true);
        },
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle:
            AppConfig.of(_backgroundVerificationActionManager?.context)
                .themeData
                .primaryTextTheme
                .headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show vehicle registration title
  Widget _showBackgroundVerificationTitle() {
    return Text(AppLocalizations.of(context).backgroundVerification.text.title,
        style: AppConfig.of(_backgroundVerificationActionManager?.context)
            .themeData
            .primaryTextTheme
            .headline5);
  }

  //method to show upload photo view
  Widget _showUploadPhotoView() {
    return Card(
      color: COLOR_ORANGE,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(SIZE_15))),
      elevation: ELEVATION_05,
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          _getImageView(),
          InkWell(
            onTap: () {
              _backgroundVerificationActionManager?.actionOnUploadImage(
                  currentState: _scaffoldKey?.currentState);
            },
            child: Container(
              decoration: new BoxDecoration(
                color: LIGHT_PINK,
                borderRadius: new BorderRadius.only(
                    bottomRight: Radius.circular(SIZE_15),
                    bottomLeft: Radius.circular(SIZE_15)),
                shape: BoxShape.rectangle,
              ),
              height: SIZE_40,
              alignment: Alignment.center,
              child: Text(
                AppLocalizations.of(
                        _backgroundVerificationActionManager?.context)
                    .common
                    .text
                    .uploadPhoto,
                style: TextStyle(color: Colors.black),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _textFieldForm({
    TextEditingController controller,
    TextInputType keyboardType,
    Widget icon,
    String prefixIcon,
    String hint,
    bool isInputFormater,
    double elevation,
    FormFieldValidator<String> validator,
    bool enable,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_5),
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        style: AppConfig.of(_backgroundVerificationActionManager?.context)
            .themeData
            .textTheme
            .headline2,
        enabled: enable,
        controller: controller,
        keyboardType: keyboardType,
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(color: COLOR_BORDER_GREY),
          suffixIcon: icon ?? const SizedBox(),
          contentPadding: EdgeInsets.only(left: SIZE_20),
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(SIZE_30),
            ),
            borderSide:
                new BorderSide(width: SIZE_0_2, color: COLOR_TEXTFIELD_BORDER),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(SIZE_30),
            ),
            borderSide:
                new BorderSide(width: SIZE_1, color: COLOR_TEXTFIELD_BORDER),
          ),
          // enabledBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_RED),
          // ),
          // disabledBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_TEXTFIELD_BORDER),
          // ),
          // errorBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_RED),
          // ),
        ),
      ),
    );
  }

  //Method to show next button
  Widget _showNextButton() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_20, bottom: SIZE_20),
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_7, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_80, ofWidth: true),
        child: RaisedGradientButton(
          radious: SIZE_30,
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
          ),
          onPressed: () {
            _backgroundVerificationActionManager?.actionOnClickNextButton(
              currentState: _scaffoldKey?.currentState,
            );
          },
          child: Text(
            AppLocalizations.of(_backgroundVerificationActionManager?.context)
                .common
                .text
                .next,
            textAlign: TextAlign.center,
            style: AppConfig.of(context).themeData.textTheme.subtitle1,
          ),
        ),
      ),
    );
  }

  //method to show view of image uploaded by user or empty image view
  Widget _getImageView() {
    if (widget?.vehicleRegistrationResponseModel?.data?.backgroundVerification
                ?.backgroundVerificationImages !=
            null &&
        widget?.vehicleRegistrationResponseModel?.data?.backgroundVerification
                ?.backgroundVerificationImages?.isNotEmpty ==
            true) {
      //this will execute in the case of edit licence doc
      return _getImageWithUrl(
          url: widget?.vehicleRegistrationResponseModel?.data
              ?.backgroundVerification?.backgroundVerificationImages);
      //this will execute when user upload image or edit image
    } else if (_backgroundVerificationActionManager
            ?.backgroundVerificationState
            ?.vehicleRegistrationResponseModel
            ?.data
            ?.backgroundVerification
            ?.backgroundVerificationImages
            ?.isNotEmpty ==
        true) {
      return _getImageWithUrl(
          url: _backgroundVerificationActionManager
              ?.backgroundVerificationState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.backgroundVerification
              ?.backgroundVerificationImages);
    } else {
      //this is default condition when no image uploaded
      return Row(
        children: [
          Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: SIZE_10),
                  child: SvgPicture.asset(
                    VEHICLE_ICON,
                    color: Colors.white,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _backgroundVerificationActionManager?.context,
                        percentage: SIZE_10,
                        ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _backgroundVerificationActionManager?.context,
                        percentage: SIZE_10,
                        ofWidth: true),
                  ),
                ),
              )),
          Expanded(
              flex: 3,
              child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: SIZE_25, right: SIZE_25),
                  height: CommonUtils.commonUtilsInstance.getPercentageSize(
                      context: _backgroundVerificationActionManager?.context,
                      percentage: SIZE_20,
                      ofWidth: false),
                  child: Wrap(
                    runSpacing: SIZE_10,
                    children: [
                      Container(
                        height: SIZE_45,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_5),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      ),
                      Container(
                        height: SIZE_15,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_20),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      ),
                      Container(
                        height: SIZE_15,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_20),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      )
                    ],
                  )))
        ],
      );
    }
  }

  //this widget will return a cache network image
  Widget _showCacheNetworkImage({String imageUrl}) {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
      context: context,
      showProgressBarInPlaceHolder: true,
      shape: BoxShape.rectangle,
      loaderHeight: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _backgroundVerificationActionManager?.context,
          percentage: SIZE_40,
          ofWidth: false),
      loaderWidth: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _backgroundVerificationActionManager?.context,
          percentage: SIZE_40,
          ofWidth: true),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _backgroundVerificationActionManager?.context,
          percentage: SIZE_24,
          ofWidth: false),
      width: double.infinity,
      url: imageUrl ?? "",
    );
  }

  //get widget which will show image
  Widget _getImageWithUrl({String url}) {
    return Container(
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _backgroundVerificationActionManager?.context,
            percentage: SIZE_24,
            ofWidth: false),
        width: MediaQuery.of(_backgroundVerificationActionManager?.context)
            .size
            .width,
        child: _showCacheNetworkImage(imageUrl: url));
  }

//method to show divider text
  Widget _showDivider() {
    return Divider(
      color: COLOR_BORDER_GREY,
    );
  }

  //This method is used to show note to the driver
  Widget _showNote() {
    return Text.rich(
      TextSpan(
        children: [
          TextSpan(
              text: AppLocalizations.of(context).driverDetails.text.noteText,
              style: textStyleSize14WithRED),
          TextSpan(
            text: AppLocalizations.of(context).backgroundVerification.text.note,
            style: textStyleSize14WithRED500,
          ),
        ],
      ),
    );
  }
}
