import 'dart:io';

import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/enums/auth_enums.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/vehicle_registration/bloc/vehicle_register_detail_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/vehicle_registration/bloc/vehicle_register_detail_event.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/vehicle_registration/bloc/vehicle_register_detail_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/validator/vehicle_registration_validator.dart';
import 'package:boozin_driver/modules/common/model/common_pass_data_model.dart';
import 'package:boozin_driver/modules/file_picker/dialog/single_file_picker_dialog.dart';
import 'package:boozin_driver/modules/file_picker/enums/file_picker_enums.dart';
import 'package:boozin_driver/modules/file_picker/model/file_picker_model.dart';

import '../../../../registration_routes.dart';

class VehicleRegisterActionManager {
  BuildContext context;

  VehicleRegisterDetailBloc vehicleRegisterDetailBloc;
  VehicleRegisterDetailsState vehicleRegisterDetailsState;
  CommonPassDataModel commonPassDataModel;

  //Action on init
  void actionOnInit({ScaffoldState currentState}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        vehicleRegisterDetailBloc?.emitEvent(UpdateVehicleRegisterEvent(
            context: context,
            isLoading: false,
            vehicleRegistrationResponseModel:
                commonPassDataModel?.vehicleRegistrationResponseModel));
      }
    });
  }

  //Action on state change
  void actionOnStateChange({ScaffoldState currentState}) {
    if (vehicleRegisterDetailsState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        if (vehicleRegisterDetailsState?.message?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              message: vehicleRegisterDetailsState?.message?.trim(),
              scaffoldState: currentState);
        }

        if (vehicleRegisterDetailsState?.status == ApiStatus.Success.value) {
          if (vehicleRegisterDetailsState?.vehicleRegistrationResponseModel
                  ?.data?.drivingLicence?.drivingLicenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.LICENCE_DETAILS,
                dataToBeSend: commonPassDataModel);
          } else if (vehicleRegisterDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.workPermit
                  ?.workPermitStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.WORK_PERMIT_PAGE,
                dataToBeSend: commonPassDataModel);
          } else if (vehicleRegisterDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.vehicleInsurance
                  ?.vehicleInsurenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.INSURANCE_DETAILS,
                dataToBeSend: commonPassDataModel);
          } else if (vehicleRegisterDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.backgroundVerification
                  ?.backgroundVerifcationStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.BACKGROUND_VERIFICATION,
                dataToBeSend: commonPassDataModel);
          } else {
            if (commonPassDataModel.screenPopCallBack != null) {
              commonPassDataModel.screenPopCallBack.onScreenPop();
            }
            NavigatorUtils.navigatorUtilsInstance
                .navigatorPopScreen(context, dataToBeSend: true);
          }
        }
      });
    }
  }

  //Use to upload image
  void actionOnUploadImage({ScaffoldState currentState}) {
    SingleFilePickerDialog.singleFilePickerDialogInstance.showFilePickerDialog(
        filePickerModel: FilePickerModel(
          pickerType: FilePickerTypeEnum.PickerTypeImage,
          pickFrom: FilePickFromEnum.PickFromBoth,
        ),
        context: context,
        fileData: (value) {
          var _image = File(value);
          if (_image != null) {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context, showNetworkDialog: true)
                .then((onValue) async {
              if (onValue) {
                vehicleRegisterDetailBloc?.emitEvent(
                    UploadImageVehicleRegisterEvent(
                        context: context,
                        isLoading: true,
                        file: _image,
                        vehicleRegistrationResponseModel: commonPassDataModel
                            ?.vehicleRegistrationResponseModel));
              }
            });
            // _imageFile = _image;
            // _callApi();
          }
        });
  }

  //Action Update Driving Licence detail
  actionUpdateVehicleRegisterDetail(
      {ScaffoldState currentState,
      DriverRegistrationRequestModel driverRegistrationRequestModel,
      VehicleRegistrationResponseModel vehicleRegistrationResponseModel}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        vehicleRegisterDetailBloc?.emitEvent(UploadVehicleRegisterEvent(
            context: context,
            isLoading: true,
            driverRegistrationRequestModel: driverRegistrationRequestModel,
            vehicleRegistrationResponseModel:
                vehicleRegistrationResponseModel));
      }
    });
  }

  //Action on click next button
  void actionOnClickNextButton(
      {ScaffoldState currentState,
      TextEditingController registrationNumberController,
      TextEditingController companyController,
      TextEditingController modelController,
      TextEditingController colorController,
      TextEditingController validController}) {
    //Validation on controllers
    String result = VehicleRegistrationValidator.vehicleRegistrationInstance
        .validateVehicleRegistrationScreen(
      context: context,
      regNumber: registrationNumberController?.text,
      color: colorController?.text,
      company: companyController?.text,
      validUpto: validController?.text,
      model: modelController?.text,
      file: vehicleRegisterDetailsState?.vehicleRegistrationResponseModel?.data
              ?.vehicleRegisteration?.vehiclePicture ??
          "",
    );

    if (result.isEmpty == true) {
      // check internet connection
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) async {
        if (onValue) {
          VehicleRegData vehicleRegistration = new VehicleRegData();
          vehicleRegistration?.registerationNumber =
              registrationNumberController?.text;
          vehicleRegistration?.model = modelController?.text;
          vehicleRegistration?.validUpto = validController?.text;
          vehicleRegistration?.color = colorController?.text;
          vehicleRegistration?.vehicleCompany = companyController?.text;
          vehicleRegistration?.vehiclePicture = vehicleRegisterDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.vehicleRegisteration
                  ?.vehiclePicture ??
              "";

          DriverRegistrationRequestModel driverRegistrationRequestModel =
              new DriverRegistrationRequestModel();

          driverRegistrationRequestModel?.vehicleRegisteration =
              vehicleRegistration;

          //Action on update vehicle Register detail
          actionUpdateVehicleRegisterDetail(
              currentState: currentState,
              driverRegistrationRequestModel: driverRegistrationRequestModel,
              vehicleRegistrationResponseModel: vehicleRegisterDetailsState
                  ?.vehicleRegistrationResponseModel);
        }
      });
    } else {
      // show error in fields
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: currentState, message: result);
    }
  }
}
