import 'dart:io';
import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/model/common_pass_data_model.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/date_picker_utils.dart';
import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:boozin_driver/modules/common/utils/image_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/image_constants.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/vehicle_insurance/bloc/vehicle_insurance_detail_bloc.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/vehicle_insurance/bloc/vehicle_insurance_detail_state.dart';
import 'package:boozin_driver/modules/vehicle_documents/driver_details/page/vehicle_insurance/managers/vehicle_insurance_action_manager.dart';

class InsuranceDetailsPage extends StatefulWidget {
  VehicleRegistrationResponseModel driverRegistrationResponseModel;
  BuildContext context;
  CommonPassDataModel commonPassDataModel;

  InsuranceDetailsPage(this.context) {
    commonPassDataModel = ModalRoute.of(context).settings.arguments;
    driverRegistrationResponseModel =
        commonPassDataModel?.vehicleRegistrationResponseModel;
  }

  @override
  _InsuranceDetailsPageState createState() => _InsuranceDetailsPageState();
}

class _InsuranceDetailsPageState extends State<InsuranceDetailsPage> {
  // text editing controllers
  TextEditingController _policyNumberController = new TextEditingController();
  TextEditingController _validFromController = new TextEditingController();
  TextEditingController _validTillController = new TextEditingController();

  //bloc variables
  VehicleInsuranceDetailBloc _vehicleInsuranceDetailBloc;

  //Managers
  VehicleInsuranceActionManager _vehicleInsuranceActionManager;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _vehicleInsuranceDetailBloc = new VehicleInsuranceDetailBloc();
    _vehicleInsuranceActionManager = new VehicleInsuranceActionManager();
    _vehicleInsuranceActionManager.context = widget?.context;
    _vehicleInsuranceActionManager.vehicleInsuranceDetailBloc =
        _vehicleInsuranceDetailBloc;
    _vehicleInsuranceActionManager.commonPassDataModel =
        widget?.commonPassDataModel;

    _vehicleInsuranceActionManager.actionOnInit();
  }

  @override
  void dispose() {
    super.dispose();
    _policyNumberController.dispose();
    _validFromController.dispose();
    _validTillController.dispose();

    _vehicleInsuranceActionManager?.vehicleInsuranceDetailBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _vehicleInsuranceActionManager.context = context;
    return BlocEventStateBuilder<VehicleInsuranceDetailsState>(
      bloc: _vehicleInsuranceActionManager?.vehicleInsuranceDetailBloc,
      builder: (BuildContext context,
          VehicleInsuranceDetailsState vehicleInsuranceDetailsState) {
        _vehicleInsuranceActionManager.context = context;
        if (vehicleInsuranceDetailsState != null &&
            _vehicleInsuranceActionManager?.vehicleInsuranceDetailsState !=
                vehicleInsuranceDetailsState) {
          _vehicleInsuranceActionManager?.vehicleInsuranceDetailsState =
              vehicleInsuranceDetailsState;

          if (_vehicleInsuranceActionManager
                  ?.vehicleInsuranceDetailsState?.isLoading ==
              false) {
            _setDataToControllers();

            _vehicleInsuranceActionManager?.actionOnStateChange(
                currentState: _scaffoldKey?.currentState);
          }
        }

        return ModalProgressHUD(
          inAsyncCall: _vehicleInsuranceActionManager
                  ?.vehicleInsuranceDetailsState?.isLoading ??
              false,
          child: WillPopScope(
            onWillPop: () {
              if (widget.commonPassDataModel.screenPopCallBack != null) {
                widget.commonPassDataModel.screenPopCallBack.onScreenPop();
              }

              return NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
                  _vehicleInsuranceActionManager.context,
                  dataToBeSend: true);
            },
            child: Scaffold(
              key: _scaffoldKey,
              appBar: _showAppBar(),
              body: Padding(
                padding: const EdgeInsets.only(
                    left: SIZE_15, right: SIZE_15, bottom: SIZE_15),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _showDivider(),
                      _showVehicleRegistrationTitle(),
                      SizedBox(
                        height: SIZE_15,
                      ),
                      _showUploadPhotoView(),
                      SizedBox(
                        height: SIZE_20,
                      ),
                      _showVehicleDetailsForm()
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _vehicleInsuranceActionManager.context,
        elevation: ELEVATION_0,
        centerTitle: false,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitle: AppLocalizations.of(_vehicleInsuranceActionManager.context)
            .driverDetails
            .text
            .driverDetails,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () {
          if (widget.commonPassDataModel.screenPopCallBack != null) {
            widget.commonPassDataModel.screenPopCallBack.onScreenPop();
          }

          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _vehicleInsuranceActionManager.context,
              dataToBeSend: true);
        },
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle: AppConfig.of(_vehicleInsuranceActionManager.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show vehicle registration title
  Widget _showVehicleRegistrationTitle() {
    return Text(
        AppLocalizations.of(_vehicleInsuranceActionManager.context)
            .vehicleInsurance
            .text
            .title,
        style: AppConfig.of(_vehicleInsuranceActionManager.context)
            .themeData
            .primaryTextTheme
            .headline5);
  }

  //method to show upload photo view
  Widget _showUploadPhotoView() {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
              Radius.circular(SIZE_15))),
      color: COLOR_ORANGE,
      elevation: ELEVATION_05,
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          _getImageView(),
          InkWell(
            onTap: () {
              _vehicleInsuranceActionManager?.actionOnUploadInsuranceImage(
                  currentState: _scaffoldKey?.currentState);
            },
            child: Container(
              decoration: new BoxDecoration(
                color:LIGHT_PINK,
                borderRadius: new BorderRadius.only(bottomRight: Radius.circular(SIZE_15), bottomLeft: Radius.circular(SIZE_15)),
                shape: BoxShape.rectangle,
              ),
              height: SIZE_40,
              alignment: Alignment.center,
              child: Text(
                AppLocalizations.of(_vehicleInsuranceActionManager.context)
                    .common
                    .text
                    .uploadPhoto,
                style: TextStyle(color: Colors.black),
              ),
            ),
          )
        ],
      ),
    );
  }

  //show insurance details form
  Widget _showVehicleDetailsForm() {
    return Wrap(
      runSpacing: SIZE_10,
      children: [
        _showRegistrationNumber(),
        _showValidFrom(),
        _showValidTill(),
        _showNextButton()
      ],
    );
  }

  //textformfeild widget
  Widget _textFieldForm({
    TextEditingController controller,
    TextInputType keyboardType,
    Widget icon,
    String prefixIcon,
    String hint,
    bool isInputFormater,
    double elevation,
    FormFieldValidator<String> validator,
    bool enable,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_5),
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        style: AppConfig.of(_vehicleInsuranceActionManager.context)
            .themeData
            .textTheme
            .headline2,
        enabled: enable,
        controller: controller,
        keyboardType: keyboardType,
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(color: COLOR_BORDER_GREY),
          suffixIcon: icon ?? const SizedBox(),
          contentPadding: EdgeInsets.only(left: SIZE_20),
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(SIZE_30),
            ),
            borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_TEXTFIELD_BORDER),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(SIZE_30),
            ),
            borderSide: new BorderSide(width: SIZE_1, color: COLOR_TEXTFIELD_BORDER),
          ),
          // enabledBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_RED),
          // ),
          // disabledBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_TEXTFIELD_BORDER),
          // ),
          // errorBorder: new OutlineInputBorder(
          //   borderRadius: const BorderRadius.all(
          //     const Radius.circular(SIZE_30),
          //   ),
          //   borderSide: new BorderSide(width: SIZE_0_2, color: COLOR_RED),
          // ),
        ),
      ),
    );
  }

  //registration number
  Widget _showRegistrationNumber() {
    return _textFieldForm(
        controller: _policyNumberController,
        icon: null,
        isInputFormater: false,
        hint: AppLocalizations.of(_vehicleInsuranceActionManager.context)
            .vehicleInsurance
            .text
            .policyNumber,
        enable: true);
  }

  //method to show next button
  Widget _showNextButton() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_20, bottom: SIZE_20),
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_7, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_80, ofWidth: true),
        child: RaisedGradientButton(
          radious: SIZE_30,
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
          ),
          onPressed: () {
            _vehicleInsuranceActionManager?.actionOnclickNextButton(
                currentState: _scaffoldKey?.currentState,
                policyNumberController: _policyNumberController,
                validFromController: _validFromController,
                validTillController: _validTillController);
          },
          child: Text(
            AppLocalizations.of(_vehicleInsuranceActionManager.context)
                .common
                .text
                .next,
            textAlign: TextAlign.center,
            style: AppConfig.of(context).themeData.textTheme.subtitle1,
          ),
        ),
      ),
    );
  }

  //method to set data to the controllers
  void _setDataToControllers() {
    _policyNumberController.text =
        (_policyNumberController?.text?.trim()?.isNotEmpty == true)
            ? _policyNumberController?.text
            : _vehicleInsuranceActionManager
                    ?.vehicleInsuranceDetailsState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.vehicleInsurance
                    ?.policyNumber ??
                "";

    _validFromController.text =
        _validFromController?.text?.trim()?.isNotEmpty == true
            ? _validFromController?.text
            : CustomDateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
                    _vehicleInsuranceActionManager
                            ?.vehicleInsuranceDetailsState
                            ?.vehicleRegistrationResponseModel
                            ?.data
                            ?.vehicleInsurance
                            ?.validFrom ??
                        "") ??
                "";

    _validTillController.text =
        _validTillController?.text?.trim()?.isNotEmpty == true
            ? _validTillController?.text
            : CustomDateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
                    _vehicleInsuranceActionManager
                            ?.vehicleInsuranceDetailsState
                            ?.vehicleRegistrationResponseModel
                            ?.data
                            ?.vehicleInsurance
                            ?.validTill ??
                        "") ??
                "";
  }

  //method to show view of image uploaded by user or empty image view
  Widget _getImageView() {
    //this will execute in the case of edit licence doc
    if (widget?.driverRegistrationResponseModel?.data?.vehicleInsurance !=
            null &&
        widget?.driverRegistrationResponseModel?.data?.vehicleInsurance
                ?.vehicleInsurenceImage?.isNotEmpty ==
            true) {
      return _getWidget(
          url: widget?.driverRegistrationResponseModel?.data?.vehicleInsurance
              ?.vehicleInsurenceImage);
    }
    //this will execute when user upload image or edit image
    else if (_vehicleInsuranceActionManager
            .vehicleInsuranceDetailsState
            ?.vehicleRegistrationResponseModel
            ?.data
            ?.vehicleInsurance
            ?.vehicleInsurenceImage
            ?.isNotEmpty ==
        true) {
      return _getWidget(
          url: _vehicleInsuranceActionManager
                  .vehicleInsuranceDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.vehicleInsurance
                  ?.vehicleInsurenceImage ??
              "");
    }
    //this is default condition when no image uploaded
    else {
      return Row(
        children: [
          Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: SIZE_10),
                  child: SvgPicture.asset(
                    VEHICLE_ICON,
                    color: Colors.white,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _vehicleInsuranceActionManager.context,
                        percentage: SIZE_10,
                        ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _vehicleInsuranceActionManager.context,
                        percentage: SIZE_10,
                        ofWidth: true),
                  ),
                ),
              )),
          Expanded(
              flex: 3,
              child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: SIZE_25,right: SIZE_25),
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _vehicleInsuranceActionManager.context,
                    percentage: SIZE_20,
                    ofWidth: false),
                child: Wrap(
                  runSpacing: SIZE_10,
                  children: [
                    Container(
                      height: SIZE_45,
                      decoration: new BoxDecoration(
                        color: COLOR_LIGHT_PINK,
                        borderRadius: new BorderRadius.all(
                          Radius.circular(SIZE_5),
                        ),
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      height: SIZE_15,
                      decoration: new BoxDecoration(
                        color: COLOR_LIGHT_PINK,
                        borderRadius: new BorderRadius.all(
                          Radius.circular(SIZE_20),
                        ),
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      height: SIZE_15,
                      decoration: new BoxDecoration(
                        color: COLOR_LIGHT_PINK,
                        borderRadius: new BorderRadius.all(
                          Radius.circular(SIZE_20),
                        ),
                        shape: BoxShape.rectangle,
                      ),
                    )
                  ],
                )
              ))
        ],
      );
    }
  }

  //this widget will return a cache network image
  Widget _showCacheNetworkImage({String imageUrl}) {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
      context: context,
      showProgressBarInPlaceHolder: true,
      shape: BoxShape.rectangle,
      loaderHeight: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _vehicleInsuranceActionManager.context,
          percentage: SIZE_40,
          ofWidth: false),
      loaderWidth: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _vehicleInsuranceActionManager.context,
          percentage: SIZE_40,
          ofWidth: true),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _vehicleInsuranceActionManager.context,
          percentage: SIZE_24,
          ofWidth: false),
      width: double.infinity,
      url: imageUrl ?? "",
    );
  }

  //get widget which will show image
  Widget _getWidget({String url}) {
    return Container(
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _vehicleInsuranceActionManager.context,
            percentage: SIZE_24,
            ofWidth: false),
        width: MediaQuery.of(context).size.width,
        child: _showCacheNetworkImage(imageUrl: url));
  }

  //method to show divider text
  Widget _showDivider() {
    return Divider(
      color: COLOR_BORDER_GREY,
    );
  }

  Widget _showValidTill() {
    return InkWell(
      onTap: () {
        _showDatePicker(_vehicleInsuranceActionManager.context, false);
      },
      child: _textFieldForm(
          controller: _validTillController,
          icon: Icon(Icons.calendar_today, color: COLOR_PRIMARY),
          isInputFormater: false,
          hint: AppLocalizations.of(_vehicleInsuranceActionManager.context)
              .vehicleInsurance
              .hint
              .validTill,
          enable: false),
    );
  }

  Widget _showValidFrom() {
    return InkWell(
      onTap: () {
        _showDatePicker(_vehicleInsuranceActionManager.context, true);
      },
      child: _textFieldForm(
          controller: _validFromController,
          icon: Icon(Icons.calendar_today, color: COLOR_PRIMARY),
          isInputFormater: false,
          hint: AppLocalizations.of(_vehicleInsuranceActionManager.context)
              .vehicleInsurance
              .hint
              .validFrom,
          enable: false),
    );
  }

  //To show date picker
  void _showDatePicker(BuildContext context, bool isForValidFrom) async {
    var picked = await DatePickerUtils.datePickerUtilsInstance.getDatePicker(
        context: context,
        selectedDate: isForValidFrom
            ? _validFromController.text
            : _validTillController.text,
        lastDate: DateTime(2500),
        firstDate: isForValidFrom
            ? new DateTime(DateTime.now().year - 5, DateTime.now().month,
                DateTime.now().day)
            : DateTime.now());
    if (isForValidFrom) {
      _validFromController.text = picked.isEmpty
          ? CustomDateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
              _vehicleInsuranceActionManager
                      ?.vehicleInsuranceDetailsState
                      ?.vehicleRegistrationResponseModel
                      ?.data
                      ?.vehicleInsurance
                      ?.validFrom ??
                  "")
          : picked;
    } else {
      _validTillController.text = picked.isEmpty
          ? CustomDateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
              _vehicleInsuranceActionManager
                      ?.vehicleInsuranceDetailsState
                      ?.vehicleRegistrationResponseModel
                      ?.data
                      ?.vehicleInsurance
                      ?.validTill ??
                  "")
          : picked;
    }
  }
}
