import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:boozin_driver/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/fetch_prefs_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/current_location_updator/api/model/location_request_model.dart';
import 'package:boozin_driver/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:boozin_driver/modules/auth/auth_routes.dart';
import 'package:boozin_driver/modules/auth/constants/image_constant.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:boozin_driver/modules/orders/screen/orders_page.dart';
import 'package:boozin_driver/modules/home/bloc/home_state.dart';
import 'package:boozin_driver/modules/home/manager/home_action_managers.dart';
import 'package:geolocator/geolocator.dart';

class HomeScreens extends StatefulWidget {
  BuildContext context;

  HomeScreens(this.context);

  @override
  _HomeScreensState createState() => _HomeScreensState();
}

class _HomeScreensState extends State<HomeScreens> with WidgetsBindingObserver {
  // initialise scaffold key
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  HomeActionManagers _homeActionManagers = new HomeActionManagers();
  bool isFirstTime = false;

  @override
  void initState() {
    super.initState();
    isFirstTime = true;
    WidgetsBinding.instance.addObserver(this);
    _homeActionManagers.context = widget.context;
    _homeActionManagers.authBloc = BlocProvider.of<AuthBloc>(widget.context);
    //Call get profile api
    _homeActionManagers.actionOnInit();
    checkLocationPermissionEnabledOrNot(widget.context, true);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    _homeActionManagers?.context = context;
    return BlocEventStateBuilder<HomeState>(
      bloc: _homeActionManagers?.homeBloc,
      builder: (BuildContext context, HomeState homeState) {
        _homeActionManagers?.context = context;
        if (homeState != null && _homeActionManagers?.homeState != homeState) {
          _homeActionManagers?.homeState = homeState;
          _homeActionManagers?.actionOnHomeStateChanged(
            context: _homeActionManagers?.context,
            scaffoldState: _scaffoldKey?.currentState,
          );
        }
        return ModalProgressHUD(
          inAsyncCall: homeState?.isLoading ?? false,
          child: Scaffold(
            key: _scaffoldKey,
            body: Column(
              children: <Widget>[
                // construct the profile details widget here
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: context, percentage: SIZE_5, ofWidth: false),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: SIZE_10, right: SIZE_10, bottom: SIZE_10),
                      child: Row(
                        children: <Widget>[
                          InkWell(
                              onTap: () {
                                NavigatorUtils.navigatorUtilsInstance
                                    .navigatorPushedName(context,
                                        AuthRoutes.PROFILE_SCREEN_ROOT);
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(SIZE_8),
                                child: Image.asset(
                                  DRAWER_ICON,
                                  height: SIZE_20,
                                  width: SIZE_20,
                                ),
                              )),
                          SizedBox(
                            width: SIZE_15,
                          ),
                          Text(
                            AppLocalizations.of(_homeActionManagers.context)
                                .home
                                .text
                                .myOrders,
                            style: AppConfig.of(context)
                                .themeData
                                .primaryTextTheme
                                .headline5,
                          ),
                          Spacer(),
                          NotificationReadCountWidget(
                            context: context,
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: context, percentage: SIZE_2, ofWidth: false),
                    ),
                    Divider(
                      color: Colors.black12,
                      height: 1.0,
                    ),
                    SizedBox(
                      height: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: context, percentage: SIZE_2, ofWidth: false),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: SIZE_10, right: SIZE_10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppLocalizations.of(_homeActionManagers.context)
                                    .home
                                    .text
                                    .availablility,
                                style: AppConfig.of(context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline5,
                              ),
                              Text(
                                  '(${AppLocalizations.of(_homeActionManagers.context).home.text.enableToReceiveOrder})'),
                            ],
                          ),
                          Transform.scale(
                            scale: 0.8,
                            child: CupertinoSwitch(
                              value:
                                  _homeActionManagers?.homeState?.isAvailable ??
                                      false,
                              activeColor: COLOR_PRIMARY,
                              onChanged: (bool value) {
                                _homeActionManagers
                                    ?.actionOnChangeAvailabilityStatus(
                                        availabilityStatus: value);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),

                Expanded(
                  child: OrdersPage(_homeActionManagers?.context),
                )

                // the tab bar with two items
                // SizedBox(
                //   height: 100,
                //   child: AppBar(
                //     backgroundColor: Colors.white,
                //     leading: SizedBox(),
                //     elevation: 0,
                //     bottom: TabBar(
                //       indicatorColor: COLOR_PRIMARY,
                //       tabs: [
                //         Tab(
                //           child: Text('Active',
                //               style: textStyleSize12WithGrey),
                //         ),
                //         Tab(
                //           child: Text('Incoming',
                //               style: textStyleSize12WithGrey),
                //         ),
                //         Tab(
                //           child: Text('Completed',
                //               style: textStyleSize12WithGrey),
                //         )
                //       ],
                //     ),
                //   ),
                // ),
                //
                // // create widgets for each tab bar here
                // Expandedded(
                //   child: TabBarView(
                //     children: [
                //       // first tab bar view widget
                //       Container(
                //         child: Center(
                //           child: Text(
                //             'Active',
                //           ),
                //         ),
                //       ),
                //
                //       // second tab bar viiew widget
                //       Container(
                //         child: Center(
                //           child: Text(
                //             'Incoming',
                //           ),
                //         ),
                //       ),
                //       Container(
                //         child: Center(
                //           child: Text(
                //             'Completed',
                //           ),
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    // TODO: implement didChangeAppLifecycleState

    if (state == AppLifecycleState.resumed) {
      if (isFirstTime == false) {
        checkLocationPermissionEnabledOrNot(widget.context, false);
      }
    } else if (state == AppLifecycleState.paused) {
      isFirstTime = false;
    }
  }

  Future<void> checkLocationPermissionEnabledOrNot(
      BuildContext _context, bool fromInIt) async {
    LocationPermission permission = await CurrentLocationManger
        .locationMangerInstance
        .permissionEnabledOrNot();
    if (permission == LocationPermission.deniedForever ||
        permission == LocationPermission.denied) {
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
          context: _context,
          title: AppLocalizations.of(context).common.text.permissionAlert,
          // negativeButton: AppLocalizations.of(context).common.text.ok,
          dismissOnHardwareButtonClick: false,
          positiveButton: AppLocalizations.of(context).common.text.setting,
          subTitle:
              AppLocalizations.of(context).common.text.permissionAlertMessage,
          // onNegativeButtonTab: () {
          //   NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          // },
          onPositiveButtonTab: () async {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(_context);
            if (Platform.isIOS) {
              AppSettings.openLocationSettings();
            } else {
              AppSettings.openAppSettings();
            }
          });
    }

    if (fromInIt) {
      if (permission == LocationPermission.always ||
          permission == LocationPermission.whileInUse) {
        CurrentLocationManger.locationMangerInstance
            .getCurrentLocationData()
            .then((currentLocation) async {
          LocationRequestModel locationRequestModel = LocationRequestModel(
            latitude: currentLocation?.latitude,
            longitude: currentLocation?.longitude,
            //time: currentLocation?.speedAccuracy,
            speedAccuracy: currentLocation?.speedAccuracy,
            speed: currentLocation?.speed,
            altitude: currentLocation?.altitude,
            heading: currentLocation?.heading,
            accuracy: currentLocation?.accuracy,
          );

          AuthResponseModel _authResponseModel = await FetchPrefsUtils
              .fetchPrefsUtilsInstance
              .getAuthResponseModel();

          if (_authResponseModel != null &&
              _authResponseModel?.userData != null) {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context, showNetworkDialog: false)
                .then((onValue) {
              if (onValue) {
                callLocationApi(
                  widget.context,
                  locationRequestModel,
                );
              }
            });
          }
        });
      }
    }
  }
}
