import 'package:boozin_driver/modules/force_update/model/force_update_response_model.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

class HomeState extends BlocState {
  HomeState(
      {this.isLoading: false,
      this.context,
      this.isAvailable: false,
      this.message,
      this.authResponseModel,
      this.forceUpdateResponseModel,
      this.needToUpdate})
      : super(isLoading);

  final bool isLoading;
  final BuildContext context;
  final bool isAvailable;
  final AuthResponseModel authResponseModel;
  final String message;
  final ForceUpdateResponseModel forceUpdateResponseModel;
  final bool needToUpdate;

  // used for update home page state
  factory HomeState.updateHome({
    bool isLoading,
    String message,
    BuildContext context,
    bool isAvailable,
    AuthResponseModel authResponseModel,
    ForceUpdateResponseModel forceUpdateResponseModel,
    bool needToUpdate,
  }) {
    return HomeState(
      isLoading: isLoading,
      message: message,
      context: context,
      isAvailable: isAvailable,
      authResponseModel: authResponseModel,
      forceUpdateResponseModel: forceUpdateResponseModel,
      needToUpdate: needToUpdate,
    );
  }

  factory HomeState.initiating() {
    return HomeState(
      isLoading: false,
    );
  }
}
