import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/api/is_user_bloc/provider/is_user_blocked_provider.dart';
import 'package:boozin_driver/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:boozin_driver/modules/common/utils/fetch_prefs_utils.dart';
import 'package:boozin_driver/modules/common/utils/shared_prefs_utils.dart';
import 'package:boozin_driver/modules/force_update/api/provider/force_update_provider.dart';
import 'package:boozin_driver/modules/force_update/manager/force_update_action_manager/force_update_action_manager.dart';
import 'package:boozin_driver/modules/force_update/model/force_update_response_model.dart';
import 'package:boozin_driver/modules/home/api/provider/availability_api_provider.dart';
import 'package:boozin_driver/modules/home/bloc/home_event.dart';
import 'package:boozin_driver/modules/home/bloc/home_state.dart';
import 'package:boozin_driver/modules/home/model/availablity_status_response_model.dart';

class HomeBloc extends BlocEventStateBase<HomeEvent, HomeState> {
  HomeBloc({bool initializing = true})
      : super(initialState: HomeState.initiating());

  @override
  Stream<HomeState> eventHandler(
      HomeEvent event, HomeState currentState) async* {
    // update home page UI

    if (event is ChangeAvailabilityEvent) {
      AvailabilityStatusResponseModel availabilityStatusResponseModel;
      String _message = "";
      bool _isDriverAvailable = event?.isAvailable;
      yield HomeState.updateHome(
          isLoading: true,
          context: event?.context,
          message: "",
          authResponseModel: event?.authResponseModel,
          isAvailable: event?.isAvailable);

      var result = await HomeApiProvider().changeDriverAvailability(
          context: event.context,
          availability: (event?.isAvailable == true) ? 1 : 0);
      AuthResponseModel _authResponseModel =
          await FetchPrefsUtils.fetchPrefsUtilsInstance.getAuthResponseModel();
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          availabilityStatusResponseModel =
              AvailabilityStatusResponseModel.fromJson(result);

          if (_authResponseModel != null) {
            _authResponseModel?.userData?.availablity =
                availabilityStatusResponseModel?.data?.availability;

            await SharedPrefUtils.sharedPrefUtilsInstance.saveObject(
                _authResponseModel, PrefsEnum.UserProfileData.value);
          }
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }
      yield HomeState.updateHome(
          isLoading: false,
          context: event?.context,
          message: _message,
          isAvailable: _isDriverAvailable,
          authResponseModel: _authResponseModel);
    }

    //Get Profile event
    if (event is GetProfileEvent) {
      AuthResponseModel authResponseModel;
      String _message = "";
      bool _isDriverAvailable = event?.isAvailable;

      yield HomeState.updateHome(
          isLoading: false,
          context: event?.context,
          message: "",
          isAvailable: event?.isAvailable);
      var result = await IsUserBlockedProvider().getProfileApiCall(
        context: event.context,
      );
      authResponseModel =
          await FetchPrefsUtils.fetchPrefsUtilsInstance.getAuthResponseModel();
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          AuthResponseModel _authResponseModel =
              AuthResponseModel.fromMap(result);

          authResponseModel?.userData?.availablity =
              _authResponseModel?.userData?.availablity;
          authResponseModel?.userData?.id = _authResponseModel?.userData?.id;
          authResponseModel?.userData?.name =
              _authResponseModel?.userData?.name;
          authResponseModel?.userData?.phoneNumber =
              _authResponseModel?.userData?.phoneNumber;
          authResponseModel?.userData?.countryIsoCode =
              _authResponseModel?.userData?.countryIsoCode;
          authResponseModel?.userData?.countryCode =
              _authResponseModel?.userData?.countryCode;
          authResponseModel?.userData?.address =
              _authResponseModel?.userData?.address;
          authResponseModel?.userData?.profilePicture =
              _authResponseModel?.userData?.profilePicture;
          authResponseModel?.userData?.email =
              _authResponseModel?.userData?.email;

          await SharedPrefUtils.sharedPrefUtilsInstance
              .saveObject(authResponseModel, PrefsEnum.UserProfileData.value);

          _isDriverAvailable =
              (authResponseModel?.userData?.availablity == 1) ? true : false;

          print('the availability is $_isDriverAvailable');
        }
      }
      //To Check force update
      var _result = await ForceUpdateProvider()
          .forceUpdateApiCall(context: event.context);
      ForceUpdateResponseModel _forceUpdateResponseModel;
      bool _needToUpdate;
      if (_result != null) {
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          ForceUpdateActionManager _forceUpdateActionManager =
              ForceUpdateActionManager();
          _forceUpdateResponseModel = ForceUpdateResponseModel.fromMap(_result);
          _needToUpdate = await _forceUpdateActionManager
              .isNeedToUpdate(_forceUpdateResponseModel?.data?.version);
        }
      }

      yield HomeState.updateHome(
          isLoading: false,
          context: event?.context,
          message: _message,
          isAvailable: _isDriverAvailable,
          authResponseModel: authResponseModel,
          forceUpdateResponseModel: _forceUpdateResponseModel,
          needToUpdate: _needToUpdate);
    }
  }
}
