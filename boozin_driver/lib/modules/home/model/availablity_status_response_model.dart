// To parse this JSON data, do
//
//     final availabilityStatusResponseModel = availabilityStatusResponseModelFromJson(jsonString);

import 'dart:convert';

AvailabilityStatusResponseModel availabilityStatusResponseModelFromJson(String str) => AvailabilityStatusResponseModel.fromJson(json.decode(str));

String availabilityStatusResponseModelToJson(AvailabilityStatusResponseModel data) => json.encode(data.toJson());

class AvailabilityStatusResponseModel {
  AvailabilityStatusResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory AvailabilityStatusResponseModel.fromJson(Map<String, dynamic> json) => AvailabilityStatusResponseModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.availability,
  });

  int availability;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    availability: json["availability"] == null ? null : json["availability"],
  );

  Map<String, dynamic> toJson() => {
    "availability": availability == null ? null : availability,
  };
}
