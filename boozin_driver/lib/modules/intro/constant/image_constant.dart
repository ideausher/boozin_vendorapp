const String SPLASH_IMAGE = 'lib/modules/intro/images/splash_bg.png';
const String LIMITLESS_LOGO= 'lib/modules/intro/images/limitless_logo.svg';
const String LIMITLESS_TITLE= 'lib/modules/intro/images/limitless_title.svg';
const String INTRO_BACKGROUND= 'lib/modules/intro/images/intro_bg.png';
const String INTRO_LOGO= 'lib/modules/intro/images/intro_logo.png';
const String INTRO_GRADIENT= 'lib/modules/intro/images/intro_gradient.png';
const String SEARCH_ONLINE_LOGO= 'lib/modules/intro/images/search_online.svg';
const String PLACE_ORDER_LOGO= 'lib/modules/intro/images/place_order.svg';
const String TRACK_PROGRESS_LOGO= 'lib/modules/intro/images/track_progress.svg';
const String CIRCLE_BG= 'lib/modules/intro/images/circle_bg.png';
const String PIN_ICON = 'lib/modules/intro/images/pin_icon.png';
const String IN_ICON = 'lib/modules/intro/images/in.png';
const String BOOZ_ICON = 'lib/modules/intro/images/booz.png';




