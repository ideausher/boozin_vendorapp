import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/intro/managers/start_screen_action_manager.dart';
import 'package:boozin_driver/modules/slider_image_video/model/slider_model.dart';
import 'package:boozin_driver/modules/slider_image_video/widget/slider_widget.dart';

class StartScreenPage extends StatefulWidget {
  BuildContext context;
  StartScreenPage(this.context);
  @override
  _StartScreenPageState createState() => _StartScreenPageState();
}

class _StartScreenPageState extends State<StartScreenPage> {

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Declaration of managers
  StartScreenActionManager _screenActionManager;
  SliderImageVideoModel _sliderImageVideoModel = new SliderImageVideoModel();

  @override
  void initState() {
    super.initState();
    _screenActionManager = new StartScreenActionManager(context);
    _sliderImageVideoModel = _screenActionManager.getImageVideoModel();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        child: SliderWidget(
          context: context,
          sliderImageVideoModel: _sliderImageVideoModel,
        ),
      ),
    );
  }
}
