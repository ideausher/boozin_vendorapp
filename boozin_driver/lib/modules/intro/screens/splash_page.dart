import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/manager/auth_manager.dart';
import 'package:boozin_driver/modules/intro/constant/image_constant.dart';

class SplashPage extends StatelessWidget {
  AuthManager _authManager = AuthManager();
  var _onceUiBuild = false;

  @override
  Widget build(BuildContext context) {
    if (!_onceUiBuild) {
      _onceUiBuild = true;
      _authManager.actionOnSplashScreenStateChange(
        context: context,
      );
    }
    return Scaffold(
      body: Container(
        /*decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage(SPLASH_IMAGE),
          fit: BoxFit.cover,
        ))*/
        height: double.infinity,
        width: double.infinity,
        alignment: Alignment.center,
        child: Image.asset(
          PIN_ICON,
          height: CommonUtils.commonUtilsInstance
              .getPercentageSize(context: context,percentage: SIZE_10,ofWidth:false),
          width:CommonUtils.commonUtilsInstance
              .getPercentageSize(context: context,percentage: SIZE_10,ofWidth:true),
        ) /*Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              AppLocalizations.of(context).common.title.appName,
              style: AppConfig.of(context).themeData.primaryTextTheme.bodyText2,
            ),
            Text(
              AppLocalizations.of(context).common.title.appSubtitle,
              style: textStyleSize22WithWhiteColor,
            ),
          ],
        )*/
        ,
      ),
    );
  }
}