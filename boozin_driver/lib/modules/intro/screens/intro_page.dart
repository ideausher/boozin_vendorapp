import 'package:animator/animator.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/manager/auth_manager.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/intro/constant/image_constant.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  AuthManager _authManager = AuthManager();
  var _onceUiBuild = false;
  BuildContext _context;
  bool isAnimationCompleted = false;

  @override
  Widget build(BuildContext context) {
    _context = context;
    if (!_onceUiBuild) {
      _onceUiBuild = true;
      _authManager.actionOnIntroScreenStateChange(
        context: context,
      );
    }
    return Hero(
      tag: "Hero",
      child: Scaffold(
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(

            ),
            Container(
                alignment: Alignment.center,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context, ofWidth: false, percentage: SIZE_100),
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context, ofWidth: true, percentage: SIZE_100),

                child: Padding(
                  padding: const EdgeInsets.all(SIZE_30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          flex: 40,
                          child: Container(
                            color: Colors.transparent,
                          )),
                      Expanded(
                        flex: 60,
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                _showIntroLogo(),
                                SizedBox(height: SIZE_10,),
                                _showLogo()
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }

  //this method will show intro logo
  Widget _showIntroLogo() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset(
          BOOZ_ICON,
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, ofWidth: false, percentage: SIZE_10),
        ),
        Animator(
          tween: Tween<Offset>(begin: Offset(-1, 0), end: Offset(0.0, 0)),
          duration: Duration(milliseconds: 1500),
          // curve: Curves.decelerate,
          repeats: 1,
          cycles: 1,
          builder: (_, animatorState, __) =>
              FractionalTranslation(
                translation: animatorState.value,
                child: Image.asset(
                  PIN_ICON,
                  height: CommonUtils.commonUtilsInstance.getPercentageSize(
                      context: _context,
                      ofWidth: false,
                      percentage: SIZE_10),

                ),
              ),
        ),
        Image.asset(
          IN_ICON,
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, ofWidth: false, percentage: SIZE_10),
        )
      ],
    );
  }

  //this method will show Limit less logo
  Widget _showLogo() {
    return Expanded(
        child: Animator(
          tween: Tween<Offset>(begin: Offset(0, 0.5), end: Offset(0, 0)),
          duration: Duration(milliseconds: 1300),
          repeats: 1,
          cycles: 1,
          builder: (_, animatorState, __) =>
              FractionalTranslation(
                translation: animatorState.value,
                child: Text(
                  AppLocalizations.of(context).common.text.driver,
                  style: textStyleSize30WithDarkBlackColor
                ),
              ),
        ));
  }
}
