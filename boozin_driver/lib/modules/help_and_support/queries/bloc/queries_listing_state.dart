

import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/help_and_support/queries/api/model/query_list_response_model.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';

class QueriesListingState extends BlocState {
  QueriesListingState({
    this.isLoading: false,
    this.message,
    this.context,
    this.listQuery,
    this.page,
    this.order,
  }) : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final List<Query> listQuery;
  final int page;
  final Order order;

  // used for update profile api call
  factory QueriesListingState.updateUi({bool isLoading,
    String message,
    BuildContext context,
    int page, Order order,

    List<Query> listQuery}) {
    return QueriesListingState(
        isLoading: isLoading,
        message: message,
        context: context,
        page: page,
        order: order,
        listQuery: listQuery);
  }

  factory QueriesListingState.initiating() {
    return QueriesListingState(
      isLoading: false,
    );
  }
}
