import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';



class DeleteImageProvider {
  // upload profile pic
  Future<dynamic> deleteImage({BuildContext context, String imageUrl}) async {
    FormData formData = FormData.fromMap({"url": imageUrl});
    var path = "delete-image";
    var result = await AppConfig.of(context)
        .baseApi
        .postRequest(path, context, data: formData);
    return result;
  }
}
