import 'dart:io';

import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';

class SendQueryState extends BlocState {
  SendQueryState({
    this.isLoading: false,
    this.context,
    this.message,
    this.orderModel,
    this.imageFile,
    this.apiStatus,
    this.images,
    this.index,
  }) : super(isLoading);

  final bool isLoading;
  final BuildContext context;
  final String message;
  final Order orderModel;
  final File imageFile;
  final List<String> images;
  final int index;
  final ApiStatus apiStatus;

  // used for update profile api call
  factory SendQueryState.updateQueryState({
    bool isLoading,
    BuildContext context,
    String message,
    Order orderModel,
    File imageFile,
    ApiStatus apiStatus,
    List<String> images,
    int index,
  }) {
    return SendQueryState(
        isLoading: isLoading,
        context: context,
        message: message,
        orderModel: orderModel,
        images: images,
        apiStatus: apiStatus,
        imageFile: imageFile,
        index: index);
  }

  factory SendQueryState.initiating({    Order orderModel,}) {
    return SendQueryState(
        isLoading: false,
        orderModel: orderModel
    );
  }
}
