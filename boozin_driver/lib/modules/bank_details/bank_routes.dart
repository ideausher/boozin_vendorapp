import 'package:boozin_driver/modules/bank_details/bank_listing/screen/bank_list_page.dart';
import 'package:boozin_driver/modules/bank_details/create_account/screens/bank_details_page.dart';
import 'package:flutter/material.dart';
class BankRoutes {
  static const String BANK_DETAILS_PAGE = '/BANK_DETAILS_PAGE';
  static const String BANK_LISTING_PAGE = '/BANK_LISTING_PAGE';


  static Map<String, WidgetBuilder> routes() {
    return {
      BANK_DETAILS_PAGE: (context) => BankDetailsPage(context),
      BANK_LISTING_PAGE: (context) => BankListingPage(context),

    };
  }
}
