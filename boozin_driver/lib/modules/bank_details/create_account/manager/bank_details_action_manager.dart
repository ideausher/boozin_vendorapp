import 'package:boozin_driver/modules/auth/auth_routes.dart';
import 'package:boozin_driver/modules/auth/validator/auth_validator.dart';
import 'package:boozin_driver/modules/bank_details/create_account/api/model/bank_details_request_model.dart';
import 'package:boozin_driver/modules/bank_details/create_account/bloc/bank_details_bloc.dart';
import 'package:boozin_driver/modules/bank_details/create_account/bloc/bank_details_event.dart';
import 'package:boozin_driver/modules/bank_details/create_account/bloc/bank_details_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';

class BankDetailsManager {
  BuildContext context;
  BankDetailsBloc bankDetailsBloc = new BankDetailsBloc();
  BankDetailsState bankDetailsState;

  //method to call on bank details state change
  actionOnBankDetailsStateChange({
    ScaffoldState currentState,
  }) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) {
        if (bankDetailsState?.isLoading == false) {
          if (bankDetailsState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: currentState,
                message: bankDetailsState?.message);
            print("bank details==> ${bankDetailsState?.message}");
          } else if (bankDetailsState?.commonResponseModel != null) {
            if (bankDetailsState?.commonResponseModel?.status ==
                ApiStatus.Success.value) {
              NavigatorUtils.navigatorUtilsInstance
                  .navigatorClearStack(context, AuthRoutes.HOME_SCREEN_ROOT);
            }
          } else if (bankDetailsState?.addSubAccountResponseModel != null) {
            if (bankDetailsState?.addSubAccountResponseModel?.statusCode ==
                ApiStatus.Success.value) {
              NavigatorUtils.navigatorUtilsInstance
                  .navigatorClearStack(context, AuthRoutes.HOME_SCREEN_ROOT);
            }
          }
        }
      },
    );
  }

  //here validating bank details screen and calling save bank details event
  callBankDetailsEvent({
    BuildContext context,
    ScaffoldState scaffoldState,
    String businessName,
    String bankName,
    String bankAccNumber,
    String settlementBankCode,
    String bankDescription,
  }) {
    // check name field is empty or not
    String result = AuthValidator.authValidatorInstance.bankDetailsScreen(
      context: context,
      businessName: businessName,
      bankAccountNumber: bankAccNumber,
      bankName: bankName,
      // description: bankDescription,
    );
    // if its not empty
    if (result.isEmpty) {
      // check internet connection
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);

          BankDetailsRequestModel bankDetailsRequestModel =
              new BankDetailsRequestModel(
                  businessName: businessName,
                  bankAccount: bankAccNumber,
                  settlementBankCode: settlementBankCode);

          bankDetailsBloc.emitEvent(AddSubAccountEvent(
              context: context,
              isLoading: false,
              bankDetailsRequestModel: bankDetailsRequestModel));
        }
      });
    } else {
      // show error in fields
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  void actionOnInit() {
    // check internet connection
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);

        bankDetailsBloc.emitEvent(CheckAccountLinkedOrNot(
          context: context,
          isLoading: true,
        ));
      }
    });
  }
}
