import 'package:boozin_driver/modules/bank_details/create_account/api/model/add_sub_account_response_model.dart';
import 'package:boozin_driver/modules/bank_details/create_account/api/model/bank_details_request_model.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

class BankDetailsState extends BlocState {
  BankDetailsState(
      {this.isLoading: false,
      this.message,
      this.context,
      this.bankDetailsRequestModel,
      this.addSubAccountResponseModel,
      this.commonResponseModel})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final BankDetailsRequestModel bankDetailsRequestModel;
  final CommonResponseModel commonResponseModel;
  final AddSubAccountResponseModel addSubAccountResponseModel;

  // used for update profile api call
  factory BankDetailsState.updateUi(
      {bool isLoading,
      String message,
      BuildContext context,
      BankDetailsRequestModel bankDetailsRequestModel,
      AddSubAccountResponseModel addSubAccountResponseModel,
      CommonResponseModel commonResponseModel}) {
    return BankDetailsState(
        isLoading: isLoading,
        message: message,
        context: context,
        bankDetailsRequestModel: bankDetailsRequestModel,
        addSubAccountResponseModel: addSubAccountResponseModel,
        commonResponseModel: commonResponseModel);
  }

  factory BankDetailsState.initiating() {
    return BankDetailsState(
      isLoading: false,
    );
  }
}
