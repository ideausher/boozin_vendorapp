import 'package:boozin_driver/modules/bank_details/create_account/api/model/add_sub_account_response_model.dart';
import 'package:boozin_driver/modules/bank_details/create_account/api/provider/add_bank_subaccount_api_provider.dart';
import 'package:boozin_driver/modules/bank_details/create_account/api/provider/check_account_linked_provider.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import '../../../../localizations.dart';
import 'bank_details_event.dart';
import 'bank_details_state.dart';

class BankDetailsBloc
    extends BlocEventStateBase<BankDetailsEvent, BankDetailsState> {
  BankDetailsBloc({bool initializing = true})
      : super(initialState: BankDetailsState.initiating());

  @override
  Stream<BankDetailsState> eventHandler(
      BankDetailsEvent event, BankDetailsState currentState) async* {
    // get bank details
    if (event is AddSubAccountEvent) {
      String _message = ""; // message
      yield BankDetailsState.updateUi(
          isLoading: true,
          context: event?.context,
          bankDetailsRequestModel: event.bankDetailsRequestModel);

      //<-----------------api call add subaccount screen-------------->
      var result = await AddSubAccountApiProvider().addSubAccountApiCall(
        context: event.context,
        bankDetailsRequestModel: event.bankDetailsRequestModel,
      );

      AddSubAccountResponseModel addSubAccountResponseModel;
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          addSubAccountResponseModel =
              AddSubAccountResponseModel.fromMap(result);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      yield BankDetailsState.updateUi(
          isLoading: false,
          context: event?.context,
          message: _message,
          addSubAccountResponseModel: addSubAccountResponseModel);
    }

    //Check account linked or not
    if (event is CheckAccountLinkedOrNot) {
      String _message = ""; // message
      yield BankDetailsState.updateUi(
        isLoading: true,
        context: event?.context,
      );

      //<-----------------api call Check account linked or not screen-------------->
      var result = await CheckAccountLinkedOrNotProvider()
          .checkAccountLinkedOrNotProvider(
        context: event.context,
      );
      CommonResponseModel commonResponseModel;
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      yield BankDetailsState.updateUi(
          isLoading: false,
          context: event?.context,
          message: _message,
          commonResponseModel: commonResponseModel);
    }
  }
}
