import 'package:boozin_driver/modules/bank_details/create_account/api/model/bank_details_request_model.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

abstract class BankDetailsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final BankDetailsRequestModel bankDetailsRequestModel;

  BankDetailsEvent(
      {this.isLoading: false, this.context, this.bankDetailsRequestModel});
}

//Event to get bank details of the driver
class AddSubAccountEvent extends BankDetailsEvent {
  AddSubAccountEvent(
      {bool isLoading,
      BuildContext context,
      BankDetailsRequestModel bankDetailsRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            bankDetailsRequestModel: bankDetailsRequestModel);
}


//Event to check account linked or not
class CheckAccountLinkedOrNot extends BankDetailsEvent {
  CheckAccountLinkedOrNot(
      {bool isLoading,
        BuildContext context,
        BankDetailsRequestModel bankDetailsRequestModel})
      : super(
      isLoading: isLoading,
      context: context,
      bankDetailsRequestModel: bankDetailsRequestModel);
}

