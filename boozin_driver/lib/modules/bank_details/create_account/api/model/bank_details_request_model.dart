// To parse this JSON data, do
//
//     final bankDetailsRequestModel = bankDetailsRequestModelFromJson(jsonString);

import 'dart:convert';

BankDetailsRequestModel bankDetailsRequestModelFromJson(String str) =>
    BankDetailsRequestModel.fromJson(json.decode(str));

String bankDetailsRequestModelToJson(BankDetailsRequestModel data) =>
    json.encode(data.toJson());

class BankDetailsRequestModel {
  BankDetailsRequestModel(
      {this.businessName, this.bankAccount, this.settlementBankCode});

  String businessName;
  String bankAccount;
  String settlementBankCode;

  factory BankDetailsRequestModel.fromJson(Map<String, dynamic> json) =>
      BankDetailsRequestModel(
        businessName:
            json["business_name"] == null ? null : json["business_name"],
        bankAccount: json["bank_account"] == null ? null : json["bank_account"],
        settlementBankCode:
            json["settlement_bank"] == null ? null : json["settlement_bank"],
      );

  Map<String, dynamic> toJson() => {
        "business_name": businessName == null ? null : businessName,
        "bank_account": bankAccount == null ? null : bankAccount,
        "settlement_bank":
            settlementBankCode == null ? null : settlementBankCode,
      };
}
