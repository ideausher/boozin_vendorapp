// To parse this JSON data, do
//
//     final addSubAccountResponseModel = addSubAccountResponseModelFromMap(jsonString);

import 'dart:convert';

AddSubAccountResponseModel addSubAccountResponseModelFromMap(String str) => AddSubAccountResponseModel.fromMap(json.decode(str));

String addSubAccountResponseModelToMap(AddSubAccountResponseModel data) => json.encode(data.toMap());

class AddSubAccountResponseModel {
  AddSubAccountResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory AddSubAccountResponseModel.fromMap(Map<String, dynamic> json) => AddSubAccountResponseModel(
    data: json["data"] == null ? null : Data.fromMap(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toMap() => {
    "data": data == null ? null : data.toMap(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.subaccountCode,
  });

  String subaccountCode;

  factory Data.fromMap(Map<String, dynamic> json) => Data(
    subaccountCode: json["subaccount_code"] == null ? null : json["subaccount_code"],
  );

  Map<String, dynamic> toMap() => {
    "subaccount_code": subaccountCode == null ? null : subaccountCode,
  };
}
