import 'package:boozin_driver/modules/bank_details/create_account/api/model/bank_details_request_model.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:flutter/material.dart';

class AddSubAccountApiProvider {
  Future<dynamic> addSubAccountApiCall(
      {BuildContext context,
      BankDetailsRequestModel bankDetailsRequestModel}) async {
    var bankDetails = "v1/addSubAccount";

    var result = await AppConfig.of(context).baseApi.postRequest(
      bankDetails,
      context,
      data: {
        "business_name": bankDetailsRequestModel?.businessName,
        "settlement_bank": bankDetailsRequestModel?.settlementBankCode,
        "account_number": bankDetailsRequestModel?.bankAccount
      },
    );

    return result;
  }
}
