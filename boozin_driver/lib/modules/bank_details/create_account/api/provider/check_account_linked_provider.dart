// used to check whether vendor linked to vendor or not
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:flutter/material.dart';

class CheckAccountLinkedOrNotProvider {
  // Api
  Future<dynamic> checkAccountLinkedOrNotProvider(
      {BuildContext context}) async {
    var path = "v1/accountLinked";

    var result = await AppConfig.of(context).baseApi.getRequest(
          path,
          context,
        );

    return result;
  }
}
