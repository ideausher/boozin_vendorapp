import 'package:boozin_driver/modules/bank_details/bank_listing/api/model/get_bank_listing_response_model.dart';
import 'package:boozin_driver/modules/bank_details/create_account/bloc/bank_details_state.dart';
import 'package:boozin_driver/modules/bank_details/create_account/manager/bank_details_action_manager.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:flutter/material.dart';

import '../../bank_routes.dart';

class BankDetailsPage extends StatefulWidget {
  BuildContext context;

  BankDetailsPage(this.context);

  @override
  _BankDetailsPageState createState() => _BankDetailsPageState();
}

class _BankDetailsPageState extends State<BankDetailsPage> {
  //text editing controllers
  TextEditingController _businessNameController = new TextEditingController();
  TextEditingController _bankNameController = new TextEditingController();
  TextEditingController _accountNumberController = new TextEditingController();
  TextEditingController _accDescriptionController = new TextEditingController();

  //bank action manager declaration
  BankDetailsManager _bankDetailsManager = new BankDetailsManager();
  String _settlementBankCode;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _bankDetailsManager?.context = widget?.context;
    _bankDetailsManager?.actionOnInit();
  }

  @override
  void dispose() {
    super.dispose();
    _businessNameController?.dispose();
    _bankNameController?.dispose();
    _accountNumberController?.dispose();
    _accDescriptionController?.dispose();
    _bankDetailsManager?.bankDetailsBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _bankDetailsManager?.context = context;
    return Scaffold(
        appBar: _showAppBar(),
        key: _scaffoldKey,
        body: BlocEventStateBuilder<BankDetailsState>(
          bloc: _bankDetailsManager?.bankDetailsBloc,
          builder: (BuildContext context, BankDetailsState bankDetailsState) {
            _bankDetailsManager.context = context;
            if (bankDetailsState != null &&
                _bankDetailsManager.bankDetailsState != bankDetailsState) {
              _bankDetailsManager.bankDetailsState = bankDetailsState;
              _bankDetailsManager?.actionOnBankDetailsStateChange(
                currentState: _scaffoldKey?.currentState,
              );
            }

            return ModalProgressHUD(
                inAsyncCall:
                    _bankDetailsManager?.bankDetailsState?.isLoading ?? false,
                child: Padding(
                  padding: const EdgeInsets.all(SIZE_20),
                  child: Column(
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          child: Wrap(
                            spacing: SIZE_10,
                            runSpacing: SIZE_10,
                            children: [
                              _showBusinessNameTextField(),
                              _showBankNameTextField(),
                              _showBankAccTextField(),
                              // _showAccountDescTextField()
                            ],
                          ),
                        ),
                      ),
                      _showNextButton()
                    ],
                  ),
                ));
          },
        ));
  }

  //method used to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: context,
        elevation: ELEVATION_0,
        centerTitle: false,
        appBarTitle: 'Bank Details',
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadingIcon: Icons.arrow_back,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_15),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitleStyle:
            AppConfig.of(context).themeData.primaryTextTheme.headline3,
        defaultLeadingIconColor: Colors.black,
        backGroundColor: Colors.transparent);
  }

  //this method is used to show business name text form field
  Widget _showBusinessNameTextField() {
    return _textFieldForm(
      controller: _businessNameController,
      hint: 'Enter Business Name',
    );
  }

  //method to return text form field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      bool enabled,
      Widget icon,
      FormFieldValidator<String> validator}) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_30),
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        controller: controller,
        textCapitalization: TextCapitalization.words,
        enabled: enabled,
        keyboardType: keyboardType,
        style: AppConfig.of(_bankDetailsManager?.context)
            .themeData
            .textTheme
            .headline2,
        decoration: InputDecoration(
          hintText: hint,
          contentPadding: EdgeInsets.only(
              top: SIZE_8, bottom: SIZE_8, right: SIZE_15, left: SIZE_15),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(SIZE_30)),
              borderSide: BorderSide(
                color: COLOR_BORDER_GREY,
              )),
        ),
      ),
    );
  }

  //bank name
  Widget _showBankNameTextField() {
    return InkWell(
      onTap: () async {
        BankDetailData result = await NavigatorUtils.navigatorUtilsInstance
            .navigatorPushedNameResult(context, BankRoutes.BANK_LISTING_PAGE);

        if (result != null) {
          _bankNameController.text = result.name ?? "";
          _settlementBankCode = result.code ?? "";
        }
      },
      child: _textFieldForm(
        controller: _bankNameController,
        enabled: false,
        hint: 'Select Bank',
      ),
    );
  }

  //bank account number
  Widget _showBankAccTextField() {
    return _textFieldForm(
      controller: _accountNumberController,
      hint: 'Enter Account Number',
    );
  }

  //bank account description
  Widget _showAccountDescTextField() {
    return _textFieldForm(
      controller: _accDescriptionController,
      hint: 'Enter Account Description',
    );
  }

  //method used to show next button
  Widget _showNextButton() {
    return Container(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _bankDetailsManager?.context,
          percentage: SIZE_6,
          ofWidth: false),
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _bankDetailsManager?.context,
          percentage: SIZE_80,
          ofWidth: true),
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () {
          _bankDetailsManager?.callBankDetailsEvent(
              context: _bankDetailsManager?.context,
              bankName: _bankNameController?.text,
              settlementBankCode: _settlementBankCode,
              scaffoldState: _scaffoldKey?.currentState,
              bankAccNumber: _accountNumberController.text,
              // bankDescription: _accDescriptionController?.text,
              businessName: _businessNameController?.text);
        },
        child: Text(
          'Next',
          textAlign: TextAlign.center,
          style: AppConfig.of(_bankDetailsManager?.context)
              .themeData
              .textTheme
              .subtitle1,
        ),
      ),
    );
  }
}
