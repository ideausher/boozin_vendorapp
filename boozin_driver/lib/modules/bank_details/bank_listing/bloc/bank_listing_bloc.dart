import 'package:boozin_driver/modules/bank_details/bank_listing/api/model/get_bank_listing_response_model.dart';
import 'package:boozin_driver/modules/bank_details/bank_listing/api/provider/bank_list_provider.dart';
import 'package:boozin_driver/modules/bank_details/bank_listing/bloc/bank_listing_event.dart';
import 'package:boozin_driver/modules/bank_details/bank_listing/bloc/bank_listing_state.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/help_and_support/queries/api/provider/queries_provider.dart';
import '../../../../localizations.dart';

class BankListingBloc
    extends BlocEventStateBase<BankListingEvent, BankListingState> {
  BankListingBloc({bool initializing = true})
      : super(initialState: BankListingState.initiating());

  @override
  Stream<BankListingState> eventHandler(
      BankListingEvent event, BankListingState currentState) async* {
    //To get bank listing
    if (event is GetBankListing) {
      yield BankListingState.updateUi(
        isLoading: true,
        context: event?.context,
        banksList: event?.banksList,
      );

      // api calling
      var result = await BankListProvider().getBankListApiCall(
        context: event.context,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          List<BankDetailData> resultData =
              (result[ApiStatusParams.Data.value] as List)
                  .map((itemWord) => BankDetailData.fromMap(itemWord))
                  .toList();

          yield BankListingState.updateUi(
            isLoading: false,
            context: event?.context,
            banksList: resultData,
          );
        }
        // failure case
        else {
          yield BankListingState.updateUi(
            isLoading: false,
            context: event?.context,
            message: result[ApiStatusParams.Message.value],
            banksList: event?.banksList,
          );
        }
      } else {
        yield BankListingState.updateUi(
          isLoading: false,
          context: event?.context,
          message: AppLocalizations.of(event.context)
              .common
              .error
              .somethingWentWrong,
          banksList: event?.banksList,
        );
      }
    }

    //To get filtered list
    if (event is GetFilteredBankListing) {
      yield BankListingState.updateUi(
        isLoading: false,
        context: event?.context,
        banksList: event?.banksList,
        searchText: event.searchText,
        filteredList: event.filteredList,
      );

      List<BankDetailData> filteredList = new List();

      filteredList = event?.banksList
          .where((string) => string.name
              .toLowerCase()
              .contains(event.searchText.toLowerCase()))
          .toList();

      yield BankListingState.updateUi(
        isLoading: false,
        context: event?.context,
        banksList: event?.banksList,
        searchText: event.searchText,
        filteredList: filteredList,
      );
    }
  }
}
