import 'package:boozin_driver/modules/bank_details/bank_listing/api/model/get_bank_listing_response_model.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';

abstract class BankListingEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final List<BankDetailData> banksList;
  final List<BankDetailData> filteredList;
  final String searchText;

  BankListingEvent({
    this.isLoading: false,
    this.context,
    this.banksList,
    this.filteredList,
    this.searchText,
  });
}

//Event to get bank listing
class GetBankListing extends BankListingEvent {
  GetBankListing({
    bool isLoading,
    BuildContext context,
    List<BankDetailData> banksList,
  }) : super(
          isLoading: isLoading,
          context: context,
          banksList: banksList,
        );
}

//Event to get bank listing
class GetFilteredBankListing extends BankListingEvent {
  GetFilteredBankListing({
    bool isLoading,
    BuildContext context,
    List<BankDetailData> banksList,
    List<BankDetailData> filteredList,
    String searchText,
  }) : super(
          isLoading: isLoading,
          context: context,
          banksList: banksList,
          filteredList: filteredList,
          searchText: searchText,
        );
}
