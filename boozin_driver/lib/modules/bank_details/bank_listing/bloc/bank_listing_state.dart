import 'package:boozin_driver/modules/bank_details/bank_listing/api/model/get_bank_listing_response_model.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';

class BankListingState extends BlocState {
  BankListingState({
    this.isLoading: false,
    this.message,
    this.context,
    this.banksList,
    this.filteredList,
    this.searchText,
  }) : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final List<BankDetailData> banksList;
  final List<BankDetailData> filteredList;
  final String searchText;

  // used for update profile api call
  factory BankListingState.updateUi({
    bool isLoading,
    String message,
    String searchText,
    BuildContext context,
    List<BankDetailData> banksList,
    List<BankDetailData> filteredList,
  }) {
    return BankListingState(
      isLoading: isLoading,
      message: message,
      searchText: searchText,
      context: context,
      banksList: banksList,
      filteredList: filteredList,
    );
  }

  factory BankListingState.initiating() {
    return BankListingState(
      isLoading: false,
    );
  }
}
