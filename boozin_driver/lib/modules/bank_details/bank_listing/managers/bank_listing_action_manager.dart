import 'package:boozin_driver/modules/bank_details/bank_listing/api/model/get_bank_listing_response_model.dart';
import 'package:boozin_driver/modules/bank_details/bank_listing/bloc/bank_listing_bloc.dart';
import 'package:boozin_driver/modules/bank_details/bank_listing/bloc/bank_listing_event.dart';
import 'package:boozin_driver/modules/bank_details/bank_listing/bloc/bank_listing_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:boozin_driver/modules/orders/order_routes.dart';

import '../../../../routes.dart';

class BankListActionManagers {
  BuildContext context;
  BankListingBloc bankListBloc = new BankListingBloc();
  BankListingState bankListingState;

  //method to call on home page State change method
  actionOnBankListingStateChange({
    ScaffoldState currentState,
  }) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) {
        if (bankListingState?.isLoading == false) {
          if (bankListingState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: currentState,
                message: bankListingState?.message);
            print("login==> ${bankListingState?.message}");
          }
        }
      },
    );
  }

  // used to perform the action on init
  void actionOnInit() {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        bankListBloc.emitEvent(GetBankListing(
          context: context,
          isLoading: true,
          banksList: bankListingState?.banksList,
        ));
      }
    });
  }

  void actionOnGetSearchData(
      {String text,
      List<BankDetailData> banksList,
      List<BankDetailData> filteredList}) {
    bankListBloc.emitEvent(GetFilteredBankListing(
        context: context,
        isLoading: false,
        searchText: text,
        banksList: bankListingState?.banksList,
        filteredList: filteredList));
  }
}
