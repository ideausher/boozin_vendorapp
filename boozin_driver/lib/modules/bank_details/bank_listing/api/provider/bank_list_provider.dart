import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:flutter/material.dart';

class BankListProvider {
  Future<dynamic> getBankListApiCall({BuildContext context}) async {
    var bankList = "v1/banksList";

    var result = await AppConfig.of(context).baseApi.getRequest(
          bankList,
          context,
        );

    return result;
  }
}
