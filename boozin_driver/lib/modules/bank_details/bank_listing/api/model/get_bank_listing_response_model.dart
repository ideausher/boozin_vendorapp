// To parse this JSON data, do
//
//     final getBankListingResponseModel = getBankListingResponseModelFromMap(jsonString);

import 'dart:convert';

GetBankListingResponseModel getBankListingResponseModelFromMap(String str) =>
    GetBankListingResponseModel.fromMap(json.decode(str));

String getBankListingResponseModelToMap(GetBankListingResponseModel data) =>
    json.encode(data.toMap());

class GetBankListingResponseModel {
  GetBankListingResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  List<BankDetailData> data;
  String message;
  int statusCode;

  factory GetBankListingResponseModel.fromMap(Map<String, dynamic> json) =>
      GetBankListingResponseModel(
        data: json["data"] == null
            ? null
            : List<BankDetailData>.from(json["data"].map((x) => BankDetailData.fromMap(x))),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toMap() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toMap())),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}

class BankDetailData {
  BankDetailData({
    this.name,
    this.slug,
    this.code,
    this.longcode,
    this.gateway,
    this.payWithBank,
    this.active,
    this.isDeleted,
    this.country,
    this.currency,
    this.type,
    this.id,
    this.createdAt,
    this.updatedAt,
  });

  String name;
  String slug;
  String code;
  String longcode;
  dynamic gateway;
  bool payWithBank;
  bool active;
  bool isDeleted;
  String country;
  String currency;
  String type;
  int id;
  DateTime createdAt;
  dynamic updatedAt;

  factory BankDetailData.fromMap(Map<String, dynamic> json) => BankDetailData(
        name: json["name"] == null ? null : json["name"],
        slug: json["slug"] == null ? null : json["slug"],
        code: json["code"] == null ? null : json["code"],
        longcode: json["longcode"] == null ? null : json["longcode"],
        gateway: json["gateway"],
        payWithBank:
            json["pay_with_bank"] == null ? null : json["pay_with_bank"],
        active: json["active"] == null ? null : json["active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        country: json["country"] == null ? null : json["country"],
        currency: json["currency"] == null ? null : json["currency"],
        type: json["type"] == null ? null : json["type"],
        id: json["id"] == null ? null : json["id"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"],
      );

  Map<String, dynamic> toMap() => {
        "name": name == null ? null : name,
        "slug": slug == null ? null : slug,
        "code": code == null ? null : code,
        "longcode": longcode == null ? null : longcode,
        "gateway": gateway,
        "pay_with_bank": payWithBank == null ? null : payWithBank,
        "active": active == null ? null : active,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "country": country == null ? null : country,
        "currency": currency == null ? null : currency,
        "type": type == null ? null : type,
        "id": id == null ? null : id,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt,
      };
}
