import 'package:boozin_driver/modules/auth/constants/image_constant.dart';
import 'package:boozin_driver/modules/bank_details/bank_listing/bloc/bank_listing_state.dart';
import 'package:boozin_driver/modules/bank_details/bank_listing/managers/bank_listing_action_manager.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';

class BankListingPage extends StatefulWidget {
  BuildContext context;

  BankListingPage(this.context);

  @override
  _BankListingPageState createState() => _BankListingPageState();
}

class _BankListingPageState extends State<BankListingPage>
    implements PushReceived {
  //Bloc and state variable declaration
  BankListActionManagers _bankListActionManagers = BankListActionManagers();

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //text form field controller
  TextEditingController _searchController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _bankListActionManagers.context = widget.context;
    _bankListActionManagers.actionOnInit();

    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
  }

  @override
  void dispose() {
    super.dispose();
    _bankListActionManagers?.bankListBloc?.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
  }

  @override
  Widget build(BuildContext context) {
    _bankListActionManagers.context = context;
    return Scaffold(
        appBar: _showAppBar(),
        key: _scaffoldKey,
        body: BlocEventStateBuilder<BankListingState>(
          bloc: _bankListActionManagers?.bankListBloc,
          builder: (BuildContext context, BankListingState bankListState) {
            _bankListActionManagers.context = context;
            if (bankListState != null &&
                _bankListActionManagers.bankListingState != bankListState) {
              _bankListActionManagers.bankListingState = bankListState;
              _bankListActionManagers?.actionOnBankListingStateChange(
                currentState: _scaffoldKey?.currentState,
              );
            }
            print("${bankListState?.isLoading}");
            return ModalProgressHUD(
              inAsyncCall: bankListState?.isLoading ?? false,
              child: Padding(
                padding: const EdgeInsets.all(SIZE_12),
                child: Column(
                  children: <Widget>[
                    _showBankListSearch(),
                    SizedBox(
                      height: SIZE_20,
                    ),
                    _showBankListing()
                  ],
                ),
              ),
            );
          },
        ));
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _bankListActionManagers?.context,
        elevation: ELEVATION_0,
        defaultLeadIconPressed: () {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPopScreen(_bankListActionManagers?.context);
        },
        popScreenOnTapOfLeadingIcon: false,
        centerTitle: false,
        appBarTitle: 'Banks List',
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_15),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: Colors.black,
        appBarTitleStyle: AppConfig.of(
          _bankListActionManagers?.context,
        ).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _bankListActionManagers.actionOnInit();
  }

  //method to search bank list
  Widget _showBankListSearch() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_10, right: SIZE_10),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _bankListActionManagers?.context,
          percentage: SIZE_7,
          ofWidth: false),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(SIZE_30)),
        color: COLOR_GREY,
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: _textFieldForm(
                controller: _searchController,
                keyboardType: TextInputType.text,
                context: _bankListActionManagers?.context,
                hint: 'Search Bank'),
          ),
          InkWell(
            onTap: () {
              _searchController?.clear();
              _bankListActionManagers?.actionOnGetSearchData(
                  text: "",
                  banksList:
                      _bankListActionManagers?.bankListingState?.banksList,
                  filteredList:
                      _bankListActionManagers?.bankListingState?.filteredList);
            },
            child: Icon(
              Icons.clear,
              size: SIZE_20,
            ),
          ),
        ],
      ),
    );
  }

  //textform field widget
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      FormFieldValidator<String> validator,
      int maxLines = 1,
      Widget suffixIcon,
      FocusNode currentFocusNode,
      FocusNode nextFocusNode,
      BuildContext context,
      bool enabled = true}) {
    return Padding(
      padding: EdgeInsets.only(left: SIZE_10),
      child: TextField(
        controller: controller,
        keyboardType: keyboardType,
        maxLines: maxLines,
        enabled: enabled,
        style: AppConfig.of(_bankListActionManagers?.context)
            .themeData
            .primaryTextTheme
            .subtitle2,
        focusNode: currentFocusNode,
        onChanged: (value) {
          _bankListActionManagers?.actionOnGetSearchData(
              text: value,
              banksList: _bankListActionManagers?.bankListingState?.banksList,
              filteredList:
                  _bankListActionManagers?.bankListingState?.filteredList);
        },
        onSubmitted: (term) {
          _searchController?.text = term;
          if (term?.isNotEmpty == true) {
            //TODO uncomment later
            /*  _page = 1;
            _actionManager?.actionToGetSearchedData(
                page: _page, text: _searchController?.text, showAlert: true);*/
          }
        },
        decoration: new InputDecoration(
          hintText: hint,
          suffixIcon: suffixIcon,
          border: InputBorder.none,
          hintStyle: TextStyle(
            fontWeight: AppConfig.of(_bankListActionManagers?.context)
                .themeData
                .primaryTextTheme
                .subtitle2
                .fontWeight,
            fontSize: AppConfig.of(_bankListActionManagers?.context)
                .themeData
                .primaryTextTheme
                .subtitle2
                .fontSize,
          ),
        ),
      ),
    );
  }

  Widget _showBankListing() {
    return Expanded(
      child: _searchController?.text.isNotEmpty == false
          ? Visibility(
              visible: _bankListActionManagers?.bankListingState?.banksList !=
                      null &&
                  _bankListActionManagers?.bankListingState?.banksList?.length >
                      0,
              child: ListView.builder(
                  itemCount: _bankListActionManagers
                      ?.bankListingState?.banksList?.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        NavigatorUtils.navigatorUtilsInstance
                            .navigatorPopScreen(
                          _bankListActionManagers?.context,
                          dataToBeSend: _bankListActionManagers
                              ?.bankListingState?.banksList[index],
                        );
                      },
                      child: Card(
                        margin: EdgeInsets.all(SIZE_5),
                        elevation: ELEVATION_02,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(SIZE_10)),
                            side: BorderSide(width: 0.5, color: COLOR_PRIMARY)),
                        child: Container(
                          padding: EdgeInsets.all(SIZE_12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: COLOR_PRIMARY,
                                  shape: BoxShape.circle,
                                ),
                                child: Container(
                                  margin: EdgeInsets.all(SIZE_3),
                                  padding: EdgeInsets.all(SIZE_12),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Image.asset(
                                    BANK_ICON,
                                    height: SIZE_24,
                                    width: SIZE_24,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: SIZE_20,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Bank Name: ${_bankListActionManagers?.bankListingState?.banksList[index].name}",
                                            style: AppConfig.of(context)
                                                .themeData
                                                .primaryTextTheme
                                                .headline6,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Bank Code: ${_bankListActionManagers?.bankListingState?.banksList[index].code}",
                                          style: textStyleSize14TabPrimaryColor,
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            )
          : Visibility(
              visible:
                  _bankListActionManagers?.bankListingState?.filteredList !=
                          null &&
                      _bankListActionManagers
                              ?.bankListingState?.filteredList?.length >
                          0,
              child: ListView.builder(
                  itemCount: _bankListActionManagers
                      ?.bankListingState?.filteredList?.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        NavigatorUtils.navigatorUtilsInstance
                            .navigatorPopScreen(
                          _bankListActionManagers?.context,
                          dataToBeSend: _bankListActionManagers
                              ?.bankListingState?.filteredList[index],
                        );
                      },
                      child: Card(
                        margin: EdgeInsets.all(SIZE_5),
                        elevation: ELEVATION_02,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(SIZE_10)),
                            side: BorderSide(width: 0.5, color: COLOR_PRIMARY)),
                        child: Container(
                          padding: EdgeInsets.all(SIZE_12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: COLOR_PRIMARY,
                                  shape: BoxShape.circle,
                                ),
                                child: Container(
                                  margin: EdgeInsets.all(SIZE_3),
                                  padding: EdgeInsets.all(SIZE_12),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Image.asset(
                                    BANK_ICON,
                                    height: SIZE_24,
                                    width: SIZE_24,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: SIZE_20,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Bank Name: ${_bankListActionManagers?.bankListingState?.filteredList[index].name}",
                                            style: AppConfig.of(context)
                                                .themeData
                                                .primaryTextTheme
                                                .headline6,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Bank Code: ${_bankListActionManagers?.bankListingState?.filteredList[index].code}",
                                          style: textStyleSize14TabPrimaryColor,
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ),
    );
  }
}
