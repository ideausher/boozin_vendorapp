import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/constants/font_constant.dart';
import '../../../modules/common/constants/color_constants.dart';
import '../../../modules/common/constants/dimens_constants.dart';

// host App Theme
final ThemeData appTheme = new ThemeData(
    primaryColor: COLOR_PRIMARY,
    primaryColorDark: COLOR_PRIMARY_DARK,
    backgroundColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    accentColor: COLOR_ACCENT,
    //   primarySwatch: Colors.blue,
    appBarTheme: AppBarTheme(
      color: COLOR_PRIMARY,
      textTheme: TextTheme(
          headline6: TextStyle(color: Colors.white, fontSize: SIZE_15)),
    ),
    buttonTheme: ButtonThemeData(
      disabledColor: COLOR_PRIMARY.withOpacity(0.7),
      buttonColor: COLOR_PRIMARY,
      //padding: EdgeInsets.all(SIZE_20),
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(SIZE_5)),
    ),
    textTheme: TextTheme(
      // black
      headline1: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_22,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_LATO),
      headline2: TextStyle(
          color: Colors.black,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w300,
          fontFamily: FONT_FAMILY_LATO),
      headline3: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_LATO),
      // white
      headline4: TextStyle(
          color: COLOR_RED,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_LATO),
      headline5: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_24,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_LATO),
      headline6: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_24,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_LATO),

      // color accent
      subtitle1: TextStyle(
          color: Colors.white,
          fontSize: SIZE_14,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_LATO),

      subtitle2: TextStyle(
          color: Colors.black,
          fontSize: SIZE_28,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_LATO),
      caption: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_28,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_LATO),
      bodyText1: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_20,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_LATO),
      bodyText2: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_LATO),
    ),
    primaryTextTheme: TextTheme(
      // primary color
      headline1: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_14,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_LATO),
      headline2: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_14,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_LATO),
      headline3: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_18,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_LATO),
      // black 54
      headline4: TextStyle(
          color: COLOR_LIGHT_GREY,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_LATO),
      headline5: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_20,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_LATO),
      headline6: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_16,
          fontFamily: FONT_FAMILY_LATO,
          fontWeight: FontWeight.w600),
      // color white
      subtitle1: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_42,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_LATO),
      subtitle2: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_LATO),
      caption: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_30,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_LATO),
      bodyText1: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_30,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_LATO),
      bodyText2: TextStyle(
          color: Colors.white,
          fontSize: SIZE_48,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_LATO),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: COLOR_PRIMARY,
    ));

TextStyle textStyleSize22WithGreenColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_20,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14WithGreyColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSizeBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_20,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_LATO);
TextStyle textStyleSize15BlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_15,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_LATO);
TextStyle textStyleSize28WithPrimaryColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_28,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO,
    height: 1.2);
TextStyle textStyleSize28WithBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_28,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO,
    height: 1.2);
TextStyle textStyleSize22WithWhiteColor = TextStyle(
    fontSize: SIZE_22,
    color: Colors.white,
    fontFamily: FONT_FAMILY_LATO,
    fontWeight: FontWeight.w600);
TextStyle textStyleSize16WithWhiteColor = TextStyle(
    fontSize: SIZE_16,
    color: Colors.white,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_LATO);


TextStyle textStyleSize30WithWhiteColor = TextStyle(
    fontSize: SIZE_30,
    fontWeight: FontWeight.w500,
    color: Colors.white,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14WithRED = TextStyle(
    fontSize: SIZE_14,
    color: COLOR_RED,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);
TextStyle textStyleSize14WithRED500 = TextStyle(
    fontSize: SIZE_14,
    color: COLOR_RED,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14GREEN500 = TextStyle(
    fontSize: SIZE_14,
    color: COLOR_LIGHT_GREEN,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize12WithGrey = TextStyle(
    fontSize: SIZE_12,
    color: COLOR_BORDER_GREY,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize12WithPrimary = TextStyle(
    fontSize: SIZE_12,
    color: COLOR_PRIMARY,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize24GreenColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_24,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14BlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize12GREY = TextStyle(
    fontSize: SIZE_12,
    color: COLOR_DARK_GREY,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14LightBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14LightGreyColor = TextStyle(
    color: COLOR_LIGHT_GREY,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize18WithPrimaryColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_18,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14WithGREY = TextStyle(
    fontSize: SIZE_14,
    color: COLOR_DARK_GREY,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize12WithBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_12,
    fontWeight: FontWeight.w300,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14WithBlueColor = TextStyle(
    color: COLOR_DARK_BLUE,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14WithGreenColor = TextStyle(
    color: COLOR_GREEN_TEXT,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14GreyColorFont500 = TextStyle(
    color: COLOR_BORDER_GREY,
    fontSize: SIZE_12,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize16WithPrimaryColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_16,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14WithRedColor = TextStyle(
    color: COLOR_RED,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize10WithDarkGreyColor = TextStyle(
    fontSize: SIZE_10,
    fontWeight: FontWeight.w400,
    color: COLOR_DARK_GREY,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize16WithColorHintColor = TextStyle(
    fontSize: SIZE_16,
    fontWeight: FontWeight.w300,
    color: COLOR_BLACK,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize15WithBlackColor = TextStyle(
    fontSize: SIZE_15,
    fontWeight: FontWeight.w200,
    color: COLOR_BLACK,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize5WithBlackColor = TextStyle(
    fontSize: SIZE_5,
    fontWeight: FontWeight.w400,
    color: COLOR_BLACK,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize15WithREDColor = TextStyle(
  fontSize: SIZE_15,
  fontWeight: FontWeight.w300,
  color: COLOR_PRIMARY,
  fontFamily: FONT_FAMILY_LATO,
);

TextStyle textStyleSize15WithGreenColor = TextStyle(
    fontSize: SIZE_15,
    fontWeight: FontWeight.w300,
    color: COLOR_GREEN,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize15WithLightGreyColor = TextStyle(
    fontSize: SIZE_15,
    fontWeight: FontWeight.w400,
    color: COLOR_BORDER_GREY,
    fontFamily: FONT_FAMILY_LATO);
TextStyle textStyleSize14WithWhiteColor = TextStyle(
    color: Colors.white,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14TabPrimaryColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_LATO);
TextStyle textStyleSize26WhiteColor = TextStyle(
    color: Colors.white,
    fontSize: SIZE_26,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize18WithWhiteColor = TextStyle(
    color: Colors.white,
    fontSize: SIZE_18,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize14WithDarkOrangeColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_LATO);

TextStyle textStyleSize30WithDarkBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_30,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_LATO);
