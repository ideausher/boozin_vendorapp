import 'package:flutter/material.dart';

const Color COLOR_PRIMARY = Color(0xFFDF643A);
const Color COLOR_PRIMARY_DARK = Color(0xFF727272);
const Color COLOR_ACCENT = Color(0xFFE99841);
const Color COLOR_BORDER_GREY = Color(0xFF70707066/*E9ECEF*/);
const Color COLOR_BLACK = Color(0xFF303030);
const Color COLOR_WHITE = Color(0xFFFFFFFF);
const Color COLOR_RED = Color(0xFFFF0000);
const Color COLOR_OFF_WHITE = Color(0xFFFAF8EF);
const Color COLOR_LIGHT_GREY = Color(0xFFD6D6D6);
const Color COLOR_LIGHT_FLAME_PEA = Color(0xFFDF643A);
const Color COLOR_DARK_FLAME_PEA = Color(0xFFE1723C);
const Color COLOR_GREY = Color(0xFFf4f4f4);
const Color COLOR_DARK_GREY = Color(0xFF666666);
const Color COLOR_GREY_LIGHT = Color(0xFF757575);
const Color COLOR_RATING = Color(0xFF6e6e6e);
const Color COLOR_FLAME_PEA_TEXT = Color(0xFFDF643A);
const Color COLOR_GREY_TEXT = Color(0xFFd0cdcd);
const Color COLOR_GREY_PICKER = Color(0xFFedebe8);
const Color COLOR_GREY_DEC = Color(0xFFEDEDED);
const Color COLOR_GREY_PRODUCT = Color(0xFFF9F9F9);
const Color COLOR_LIGHT_FLAME_PEA_COLOR = Color(0xFFFCEEE7);
const Color COLOR_LIGHT_BLUE = Color(0xFFE5F2FF);
const Color COLOR_LIGHT_RED = Color(0xFFFFE5E5);
const Color COLOR_DARK_BLUE = Color(0xFF3086FF);
const Color COLOR_GREEN = Color(0xFFF6FFFD);


const Color COLOR_LIGHT_GREEN= Color(0xFF00FFB0);
const Color COLOR_DARK_GREEN= Color(0xFF4BC39D);
const Color LIGHT_PINK= Color(0xFFFFF6F6);
const Color COLOR_LIGHT_PINK= Color(0xFF99FFF6F6);
const Color COLOR_LIGHT_ORANGE= Color(0xFFFFE5D3);
const Color COLOR_DARK_ORANGE= Color(0xFFFF8330);
const Color COLOR_GREEN_TEXT = Color(0xFF00C4A3);
const Color COLOR_GREEN_COLOR = Color(0xFFe3f9f5);
const Color COLOR_MY_EARNING_BACK = Color(0xFFE1413C);
const Color COLOR_BUTTON = Color(0xFFFFBE27);
const Color COLOR_GREY_DIVIDER=Color(0xFF707070);
const Color COLOR_TEXT_LABEL = Color(0xFFA6A6A6);
const Color COLOR_ORANGE = Color(0xFFFF7A3A);
const Color COLOR_TEXTFIELD_BORDER = Color(0xFFE1723C);















