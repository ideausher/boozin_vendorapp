import 'dart:async';
import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:boozin_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:boozin_driver/modules/auth/constants/image_constant.dart';
import 'package:boozin_driver/modules/auth/enums/auth_enums.dart';
import 'package:boozin_driver/modules/auth/manager/auth_manager.dart';
import 'package:boozin_driver/modules/common/app_config/app_config.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/constants/font_constant.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/model/update_ui_data_model.dart';

class VerifyOTPWidget extends StatefulWidget {
  String title;
  String subtitle;
  String userPhoneEmail;
  BuildContext context;
  AuthManager authManager;
  AuthState authState;
  AuthBloc authBloc;
  Function onResendOtpButtonPressed;
  Function onSendOtpButtonPressed;
  Function onChangeNumberPressed;
  bool isForEmail;
  bool isFromEmailEdit;
  TextEditingController verifyOtpController;
  TextEditingController phoneNumberController;

  VerifyOTPWidget(
      {this.context,
      this.title,
      this.subtitle,
      this.authManager,
      this.authState,
      this.authBloc,
      this.userPhoneEmail,
      this.isFromEmailEdit: false,
      this.onSendOtpButtonPressed,
      this.onResendOtpButtonPressed,
      this.isForEmail: false,
      this.verifyOtpController,
      this.phoneNumberController,
      this.onChangeNumberPressed});

  @override
  _VerifyOTPWidgetState createState() => _VerifyOTPWidgetState();
}

class _VerifyOTPWidgetState extends State<VerifyOTPWidget> {
  BuildContext _context;
  UpdateUiDataModel _updateUiDataModel;

  //Controllers for OTP
  StreamController<ErrorAnimationType> _errorController =
      StreamController<ErrorAnimationType>();
  int _endTime = DateTime.now()
      .add(Duration(minutes: Timer?.minute?.value))
      .millisecondsSinceEpoch;

  @override
  void initState() {
    super.initState();
    _updateUiDataModel = new UpdateUiDataModel();
  }

  @override
  void dispose() {
    super.dispose();
    _errorController.close();
    widget?.verifyOtpController?.dispose();
    widget?.phoneNumberController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = widget?.context;
    _updateUiDataModel =
        widget?.authState?.updateUiDataModel ?? _updateUiDataModel;
    return Center(
      child: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Image.asset(
              AUTH_BG,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Padding(
              padding: const EdgeInsets.all(SIZE_20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                /*    alignment: WrapAlignment.center,
                direction: Axis.vertical,*/
                children: <Widget>[
                  _showSpace(size: SIZE_8),
                  _verifyOtpLogo(),
                  _verifyMobileNumberTitleView(),
                  _showPhoneNumberTextField(),
                  _enterOtpView(),
                  _showChangeNumberView(),
                  _showOTPTimerView(),
                  _continueButton(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  //Method to show subtitle text of verify otp screen
  Widget _verifyOtpLogo() {
    return Align(
      alignment: Alignment.center,
      child: Image.asset(
        SIGN_UP_LOGO,
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, ofWidth: true, percentage: SIZE_50),
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, ofWidth: false, percentage: SIZE_24),
      ),
    );
  }

  //This method will return a view to fill OTP
  Widget _enterOtpView() {
    //method to return text for filed of name
    return Container(
        margin: EdgeInsets.only(top: SIZE_16),
        padding: EdgeInsets.only(left: SIZE_10, right: SIZE_5),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.black.withOpacity(0.3),
            border: Border.all(color: Colors.white, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: _textFieldForm(
          keyboardType: TextInputType.number,
          hint: AppLocalizations.of(_context).verifyOtp.text.enterOtp,
          elevation: ELEVATION_0,
          icon: null,
          inputFormatterLength: 4,
          controller: widget?.verifyOtpController,
        ));
  }

  //Method to show continue Raised Button.
  Widget _continueButton() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_30),
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () {
          _callOTPVerification(
            otp: widget?.verifyOtpController?.text.toString(),
          );
        },
        child: Text(
          "Continue",
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to show verify mobile number title view
  Widget _verifyMobileNumberTitleView() {
    return Text(
      widget.title,
      style: textStyleSize26WhiteColor,
    );
  }

  //method to show verify mobile number subtitle view
  Widget _verifyMobileNumberSubtitleView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                    text: widget.subtitle,
                    style: AppConfig.of(_context)
                        .themeData
                        .primaryTextTheme
                        .bodyText1),
                TextSpan(
                    text: widget.userPhoneEmail ?? "",
                    style: textStyleSize14WithWhiteColor),
              ],
            ),
            textScaleFactor: 0.5,
          ),
        )
      ],
    );
  }

  //method to show change number view
  Widget _showChangeNumberView() {
    return Padding(
      padding:
          const EdgeInsets.only(left: SIZE_12, right: SIZE_12, top: SIZE_20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          InkWell(
            onTap: () {
              _onChangeNumberPressed();
            },
            child: Text(
              (widget?.isFromEmailEdit == true)
                  ? ""
                  : AppLocalizations.of(_context).verifyOtp.text.changeNumber,
              style: textStyleSize16WithWhiteColor,
            ),
          ),
          InkWell(
            onTap: () {
              if (widget?.authState?.updateUiDataModel?.resendButton ==
                  COLOR_WHITE) {
                print("CLICKED_1");
                _callOnResendOtp();
              }
            },
            child: Text(AppLocalizations.of(_context).verifyOtp.text.resend,
                style: TextStyle(
                    color: widget?.authState?.updateUiDataModel?.resendButton ??
                        Colors.white54,
                    fontSize: SIZE_16,
                    fontWeight: FontWeight.w600,
                    fontFamily: FONT_FAMILY_LATO)),
          )
        ],
      ),
    );
  }

  //this method will call the validate otp method
  void _callOTPVerification({String otp}) {
    widget?.onSendOtpButtonPressed(otp);
  }

  //this method is used to call resend otp method
  void _callOnResendOtp() {
    _endTime = DateTime.now()
        .add(Duration(minutes: Timer?.minute?.value))
        .millisecondsSinceEpoch;
    _updateUiDataModel.resendButton = Colors.white54;
    widget?.authManager?.updateUI(
        context: _context,
        authBloc: widget?.authBloc,
        authState: widget?.authState,
        updateUiDataModel: _updateUiDataModel);
    widget?.onResendOtpButtonPressed();
  }

  void _onChangeNumberPressed() {
    widget?.onChangeNumberPressed();
  }

  //this method is used to change color when user fills otp
  void _callChangeButtonColorEvent() {
    widget?.authManager?.updateUI(
        context: _context,
        authBloc: widget?.authBloc,
        authState: widget?.authState,
        updateUiDataModel: _updateUiDataModel);
  }

  //method to show otp timer widget view
  Widget _showOTPTimerView() {
    return CountdownTimer(
      endTime: _endTime,
      widgetBuilder: (_, CurrentRemainingTime time) {
        return (time != null)
            ? Container(
                padding: const EdgeInsets.only(right: SIZE_12),
                alignment: Alignment.topRight,
                width: MediaQuery.of(context).size.width,
                child: Text(
                  '0${time?.min ?? "0"} : ${(time?.sec?.toString()?.length == Timer?.sec?.value) ? time?.sec : '0${time?.sec}' ?? "00:00"}',
                  style: textStyleSize16WithWhiteColor,
                ),
              )
            : Container(
                padding: const EdgeInsets.only(right: SIZE_12),
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.topRight,
                child: Text(
                  "00:00",
                  style: textStyleSize16WithWhiteColor,
                  textAlign: TextAlign.end,
                ),
              );
      },
      emptyWidget: Text(
        "00:00",
        style: textStyleSize16WithWhiteColor,
        textAlign: TextAlign.center,
      ),
      onEnd: () {
        UpdateUiDataModel updateUiDataModel = _updateUiDataModel;
        updateUiDataModel.resendButton = COLOR_WHITE;
        widget?.authManager?.updateUI(
            context: _context,
            authBloc: widget?.authBloc,
            authState: widget?.authState,
            updateUiDataModel: updateUiDataModel);
      },
      textStyle: textStyleSize16WithWhiteColor,
    );
  }

  //method to return textform field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      IconData icon,
      String hint,
      double elevation,
      bool enabled,
      int inputFormatterLength: 20,
      bool showTickIcon = false,
      FormFieldValidator<String> validator}) {
    return TextFormField(
      validator: validator,
      textCapitalization: TextCapitalization.words,
      inputFormatters: <TextInputFormatter>[
        WhitelistingTextInputFormatter.digitsOnly,
        new LengthLimitingTextInputFormatter(inputFormatterLength),
      ],
      onChanged: (name) {},
      style: textStyleSize14WithWhiteColor,
      controller: controller,
      enabled: enabled ?? true,
      keyboardType: keyboardType,
      decoration: InputDecoration(
          hintStyle: textStyleSize14WithWhiteColor,
          hintText: hint,
          border: InputBorder.none),
    );
  }

  //show user phone number view
  Widget _showPhoneNumberTextField() {
    widget.phoneNumberController.text = widget?.userPhoneEmail ?? "";
    return Container(
      margin: EdgeInsets.only(top: SIZE_16),
      padding: EdgeInsets.only(left: SIZE_10, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.black.withOpacity(0.3),
          border: Border.all(color: Colors.white, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child: _textFieldForm(
        keyboardType: TextInputType.number,
        enabled: false,
        elevation: ELEVATION_0,
        icon: null,
        controller: widget?.phoneNumberController,
      ),
    );
  }

  //method to show space
  Widget _showSpace({double size}) {
    return SizedBox(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: size ?? SIZE_10, ofWidth: false),
    );
  }
}
