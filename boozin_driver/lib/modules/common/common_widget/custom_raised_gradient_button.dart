
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';

class RaisedGradientButton extends StatelessWidget {
  final Widget child;
  final Gradient gradient;
  final double width;
  final double height;
  final Function onPressed;
  final double radious;

  static final LinearGradient buttonGradient = LinearGradient(
    begin: Alignment.centerLeft, end: Alignment.centerRight,

  );

  const RaisedGradientButton(
      {Key key,
      @required this.child,
      this.gradient,
      this.width,
      this.height = SIZE_50,
      this.onPressed,
      this.radious = SIZE_12})
      : super(key: key);

  @override
  Widget build(BuildContext context) {


   return Container(
      height: SIZE_50,
      child: RaisedButton(
        onPressed:onPressed,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radious)),
        padding: EdgeInsets.all(SIZE_0),
        child: Ink(
          decoration: BoxDecoration(
            gradient: gradient != null ? gradient : buttonGradient,
            borderRadius: BorderRadius.all(
              Radius.circular(radious),
            ),
          ),
          child:  Center(
            child: child,
          ),
        ),
      ),
    );

  }
}
