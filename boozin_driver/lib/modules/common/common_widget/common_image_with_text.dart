import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/image_utils.dart';


class CommonImageWithTextWidget extends StatefulWidget {
  String imageUrl;
  String textToShow;
  String localImagePath;
  BuildContext context;
  TextStyle textStyle;
  double imageHeight;
  double imageWidth;

  CommonImageWithTextWidget(
      {this.context,
      this.imageUrl,
      this.localImagePath,
      this.textStyle,
      this.textToShow,this.imageWidth,this.imageHeight}) {
    this.context = context;
    this.imageUrl = imageUrl;
    this.localImagePath = localImagePath;
    this.textToShow = textToShow;
    this.textStyle = textStyle;
    this.imageHeight = imageHeight;
    this.imageWidth = imageWidth;
  }

  @override
  _CommonImageWithTextWidgetState createState() =>
      _CommonImageWithTextWidgetState();
}

class _CommonImageWithTextWidgetState extends State<CommonImageWithTextWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        (widget.imageUrl?.isNotEmpty == true)
            ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                context: context,
                showProgressBarInPlaceHolder: true,
                shape: BoxShape.circle,
                url: widget?.imageUrl ?? "",
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context,
                    percentage: widget?.imageHeight ?? SIZE_7,
                    ofWidth: false),
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context,
                    percentage: widget?.imageWidth ?? SIZE_7,
                    ofWidth: false))
            : SvgPicture.asset(
                widget?.localImagePath,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context,
                    percentage: widget?.imageHeight ?? SIZE_10,
                    ofWidth: false),
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context,
                    percentage: widget?.imageWidth ?? SIZE_10,
                    ofWidth: true),
              ),
        SizedBox(
          height: SIZE_10,
        ),
        Text(
          widget?.textToShow,
          style: widget?.textStyle,
        )
      ],
    );
  }
}
