import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';


class CommonAppBar extends StatelessWidget {
  String title;
  String leadingImage;
  TextStyle titleTextStyle;
  Color backgroundColor;
  Color dividerColor;
  Function onTap;
  bool stepsDivider;
  int stepNumber ;

  CommonAppBar({
    this.title,
    this.titleTextStyle,
    this.backgroundColor = Colors.black,
    this.dividerColor = COLOR_PRIMARY,
    this.leadingImage = "",
    this.onTap,
    this.stepNumber= 1,
    this.stepsDivider=false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SIZE_80,
      color: backgroundColor,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Visibility(
                  visible: leadingImage?.trim()?.isNotEmpty == true,
                  child: InkWell(
                    onTap: onTap,
                    child: Image.asset(
                      leadingImage,
                      height: SIZE_22,
                      width: SIZE_22,
                    ),
                  )),
              Expanded(
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: titleTextStyle ??
                      TextStyle(
                        color: Colors.white,
                        fontSize: SIZE_22,
                      ),
                ),
              ),
              Visibility(
                visible: leadingImage?.trim()?.isNotEmpty == true,
                child: const SizedBox(
                  width: SIZE_30,
                ),
              )
            ],
          ),

        ],
      ),
    );
  }

  // used for the steps
  getColoredDivider({int selectedStep}) {
    switch (selectedStep) {
      case 1:
        return Row(
          children: <Widget>[
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
          ],
        );
        break;
      case 2:
        return Row(
          children: <Widget>[
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
          ],
        );
        break;
      case 3:
        return Row(
          children: <Widget>[
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
          ],
        );
        break;
      case 4:
        return Row(
          children: <Widget>[
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
          ],
        );
        break;
      case 5:
        return Row(
          children: <Widget>[
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: dividerColor,
              ),
            ),
            Expanded(
              child: Divider(
                color: COLOR_PRIMARY,
              ),
            ),
          ],
        );
        break;
      case 6:
        return Divider(
          color: dividerColor,
        );
        break;
    }
  }
}
