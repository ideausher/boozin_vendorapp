// To parse this JSON data, do
//
//     final searchPlaceAddressModel = searchPlaceAddressModelFromJson(jsonString);

import 'dart:convert';

SearchPlaceAddressModel searchPlaceAddressModelFromJson(String str) =>
    SearchPlaceAddressModel.fromJson(json.decode(str));

String searchPlaceAddressModelToJson(SearchPlaceAddressModel data) => json.encode(data.toJson());

class SearchPlaceAddressModel {
  num latitude;
  num longitude;
  String formattedAddress;
  String country;
  String placeId;
  String name;
  String city;

  SearchPlaceAddressModel({
    this.latitude,
    this.longitude,
    this.formattedAddress,
    this.country,
    this.placeId,
    this.name,
    this.city,
  });

  factory SearchPlaceAddressModel.fromJson(Map<String, dynamic> json) => SearchPlaceAddressModel(
        latitude: json["latitude"] == null ? null : json["latitude"] as num,
        longitude: json["longitude"] == null ? null : json["longitude"] as num,
        formattedAddress: json["formattedAddress"] == null ? null : json["formattedAddress"],
        country: json["country"] == null ? null : json["country"],
        placeId: json["placeId"] == null ? null : json["placeId"],
        name: json["name"] == null ? null : json["name"],
        city: json["city"] == null ? null : json["city"],
      );

  Map<String, dynamic> toJson() => {
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "formattedAddress": formattedAddress == null ? null : formattedAddress,
        "country": country == null ? null : country,
        "placeId": placeId == null ? null : placeId,
        "name": name == null ? null : name,
        "city": city == null ? null : city,
      };
}
