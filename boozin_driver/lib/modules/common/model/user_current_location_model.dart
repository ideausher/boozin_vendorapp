// To parse this JSON data, do
//
//     final currentLocation = currentLocationFromJson(jsonString);

import 'dart:convert';

CurrentLocation currentLocationFromJson(String str) => CurrentLocation.fromMap(json.decode(str));

String currentLocationToJson(CurrentLocation data) => json.encode(data.toMap());

class CurrentLocation {
  CurrentLocation({
    this.lat,
    this.lng,
    this.currentAddress,
    this.city,
    this.country,
    this.postalCode,
    this.id
  });

  num lat;
  num lng;
  String currentAddress;
  String city;
  String country;
  String postalCode;
  String id;

  factory CurrentLocation.fromMap(Map<String, dynamic> json) => CurrentLocation(
    lat: json["lat"] == null ? null : json["lat"],
    lng: json["lng"] == null ? null : json["lng"],
    currentAddress: json["current_address"] == null ? null : json["current_address"],
    city: json["city"] == null ? null : json["city"],
    country: json["country"] == null ? null : json["country"],
    postalCode: json["postal_code"] == null ? null : json["postal_code"],
    id: json["id"] == null ? null : json["id"],
  );

  Map<String, dynamic> toMap() => {
    "lat": lat == null ? null : lat,
    "lng": lng == null ? null : lng,
    "current_address": currentAddress == null ? null : currentAddress,
    "city": city == null ? null : city,
    "country": country == null ? null : country,
    "postal_code": postalCode == null ? null : postalCode,
    "id": id == null ? null : id,
  };
}
