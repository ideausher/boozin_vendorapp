import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';

class DriverVehicleUtils {
  static DriverVehicleUtils _driverVehicleUtils = DriverVehicleUtils();

  static DriverVehicleUtils get addressListUtils => _driverVehicleUtils;

  static DriverRegistrationRequestModel driverRegistrationRequestModel=new DriverRegistrationRequestModel();

  static saveDriverDetailsReuestModel(DriverRegistrationRequestModel data) {
    driverRegistrationRequestModel = data;
  }
}