import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';

import '../../../modules/common/app_config/app_config.dart';
import '../../../modules/common/constants/dimens_constants.dart';
import '../../../modules/common/utils/navigator_utils.dart';

class DialogSnackBarUtils {
  static DialogSnackBarUtils _dialogSnackBarUtils = DialogSnackBarUtils();

  static DialogSnackBarUtils get dialogSnackBarUtilsInstance =>
      _dialogSnackBarUtils;

  // show snackbar
  void showSnackbar(
      {BuildContext context,
      ScaffoldState scaffoldState,
      String message,
      Color backgroundColor}) {
    scaffoldState.showSnackBar(new SnackBar(
        backgroundColor:
            backgroundColor ?? AppConfig.of(context).themeData.accentColor,
        content: new Text(
          message,
          style: AppConfig.of(context).themeData.textTheme.subtitle1,
        )));
  }

  // used to show alert
  void showAlertDialog({
    @required BuildContext context,
    @required String title,
    String subTitle,
    String centerImage,
    String positiveButton,
    TextStyle titleTextStyle,
    TextStyle subTitleTextStyle,
    TextStyle buttonTextStyle,
    String negativeButton,
    bool barrierDismissible = false,
    bool dismissOnHardwareButtonClick = true,
    Function onPositiveButtonTab,
    Function onNegativeButtonTab,
  }) {
    var actionWidgets = <Widget>[];
    if (positiveButton?.isNotEmpty == true) {
      actionWidgets.add(CupertinoDialogAction(
        child: Text(
          positiveButton ?? "",
          style: buttonTextStyle ??
              TextStyle(
                  color: AppConfig.of(context).themeData.primaryColor,
                  fontSize: AppConfig.of(context)
                      .themeData
                      .textTheme
                      .bodyText2
                      .fontSize),
        ),
        onPressed: onPositiveButtonTab,
      ));
    }
    if (negativeButton?.isNotEmpty == true) {
      actionWidgets.add(CupertinoDialogAction(
        child: Text(
          negativeButton ?? "",
          style: buttonTextStyle ??
              TextStyle(
                  color: AppConfig.of(context).themeData.primaryColor,
                  fontSize: AppConfig.of(context)
                      .themeData
                      .textTheme
                      .bodyText2
                      .fontSize),
        ),
        onPressed: onNegativeButtonTab,
      ));
    }

// show the dialog
    showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (BuildContext context) => WillPopScope(
              onWillPop: () {
                if (dismissOnHardwareButtonClick == true) {
                  NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(context);
                }
              },
              child: CupertinoAlertDialog(
                title: Padding(
                  padding: const EdgeInsets.only(bottom: SIZE_15),
                  child: new Text(
                    title,
                    style: titleTextStyle ?? textStyleSize22WithGreenColor,
                  ),
                ),
                content: Visibility(
                    visible: subTitle?.isNotEmpty,
                    child: new Text(
                      subTitle ?? "",
                      style: subTitleTextStyle ??
                          AppConfig.of(context).themeData.textTheme.bodyText2,
                    )),
                actions: actionWidgets,
              ),
            ));
  }

  //Cancel alert dialog
  void showCancelAlertDialog({
    @required BuildContext context,
    @required String title,
    String subTitle,
    String centerImage,
    String positiveButton,
    TextStyle titleTextStyle,
    TextStyle subTitleTextStyle,
    TextEditingController cancelBookingController,
    TextStyle positiveButtonTextStyle,
    TextStyle buttonTextStyle,
    String negativeButton,
    bool showWidget = false,
    bool barrierDismissible = false,
    Function onPositiveButtonTab,
    Function onNegativeButtonTab,
  }) {
    var actionWidgets = <Widget>[];
    if (positiveButton?.isNotEmpty == true) {
      actionWidgets.add(CupertinoDialogAction(
        child: Text(positiveButton ?? "",
            style: positiveButtonTextStyle ??
                TextStyle(
                    color: AppConfig.of(context).themeData.primaryColor,
                    fontSize: AppConfig.of(context)
                        .themeData
                        .textTheme
                        .bodyText2
                        .fontSize)),
        onPressed: onPositiveButtonTab,
      ));
    }
    if (negativeButton?.isNotEmpty == true) {
      actionWidgets.add(CupertinoDialogAction(
        child: Text(negativeButton ?? "",
            style: buttonTextStyle ??
                TextStyle(
                    color: AppConfig.of(context).themeData.primaryColor,
                    fontSize: AppConfig.of(context)
                        .themeData
                        .textTheme
                        .bodyText2
                        .fontSize)),
        onPressed: onNegativeButtonTab,
      ));
    }

// show the dialog
    showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (BuildContext context) => Theme(
              data: ThemeData(
                  primaryColor: Colors.white,
                  accentColor: Colors.white,
                  primaryColorDark: Colors.white,
                  dialogBackgroundColor: Colors.white,
                  dialogTheme: DialogTheme(backgroundColor: Colors.white)),
              child: CupertinoAlertDialog(
                title: Padding(
                  padding: const EdgeInsets.only(bottom: SIZE_10),
                  child: new Text(
                    title,
                    style: titleTextStyle ??
                        AppConfig.of(context).themeData.textTheme.bodyText2,
                  ),
                ),
                content: (showWidget)
                    ? _textFieldForm(
                        context: context,
                        hint: 'Reason(Minimum 30 Characters)',
                        controller: cancelBookingController)
                    : Visibility(
                        visible: subTitle?.isNotEmpty,
                        child: new Text(
                          subTitle ?? "",
                          style: subTitleTextStyle ??
                              AppConfig.of(context)
                                  .themeData
                                  .textTheme
                                  .bodyText2,
                        )),
                actions: actionWidgets,
              ),
            ));
  }

  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      bool enabled,
      Widget icon,
      BuildContext context,
      double elevation,
      FormFieldValidator<String> validator}) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_10),
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        maxLines: 5,
        controller: controller,
        textCapitalization: TextCapitalization.words,
        enabled: enabled,
        keyboardType: keyboardType,
        style: AppConfig.of(context).themeData.textTheme.headline2,
        decoration: InputDecoration(
          hintText: hint,
          contentPadding: EdgeInsets.all(SIZE_10),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(SIZE_10)),
              borderSide: BorderSide(
                color: COLOR_PRIMARY,
              )),
        ),
      ),
    );
  }
}
