import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CustomDateUtils {
  static CustomDateUtils _dateUtils = CustomDateUtils();

  DateFormat defaultServerDateFormat = DateFormat("yyyy-MM-dd");
  DateFormat defaultServerDateTimeFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  DateFormat defaultTimeFormat = DateFormat("h:mm a");
  DateFormat eEEMMMdDDateFormat = DateFormat("EEE, MMM dd");
  DateFormat dMMMhmmaDateFormat = DateFormat("d MMM - h:mm a");
  DateFormat dMMMMhmmaDateFormat = DateFormat("d MMMM, EEEE");
  DateFormat dateFormatDistanceTime = DateFormat('dd-MM-yyyy,kk:mm a');
  DateFormat defaultDDMMYYYYFormat = DateFormat("dd-MM-yyyy");

  static CustomDateUtils get dateUtilsInstance => _dateUtils;

  String convertDateToYYYYMMDD(String date) {
    String convertedDate;
    if (date == null || date.isEmpty || date == "0000-00-00 00:00:00") {
      convertedDate = "";
    } else {
      String dateTime = convertDateTimeToString(
          dateTime: convertStringToDateTime(dateTime: date));
      convertedDate = dateTime.toString();
    }
    return convertedDate;
  }

// convert  String to dateTime
  DateTime convertStringToDate({String dateTime}) {
    return defaultServerDateFormat.parse(dateTime);
  }

  // used to convert gmt to local
  String convertString(String gmtTime) {
    DateTime time = defaultServerDateTimeFormat.parse(gmtTime, true);
    time = time.toLocal();
    return time.toString();
  }

// convert  String to dateTime
  DateTime convertStringToDateTime({String dateTime}) {
    return defaultServerDateTimeFormat.parse(dateTime);
  }

// convert  String to dateTime
  String convertDateTimeToString({DateTime dateTime}) {
    return defaultServerDateFormat.format(dateTime);
  }

  // convert  String to dateTime
  String convertDefaultServerDateTimeToString({DateTime dateTime}) {
    return defaultServerDateTimeFormat.format(dateTime);
  }

  // used to convert gmt to local
  String convertGMtToLocal(String gmtTime) {
    DateTime time = defaultServerDateTimeFormat.parse(gmtTime, true);
    time = time.toLocal();
    return time.toString();
  }

  // get start day of week
  DateTime getStartDayOfWeek({DateTime dateTime}) {
    return getDate(dateTime.subtract(Duration(days: dateTime.weekday - 1)));
  }

  // get start of month
  DateTime getMonthStartDate({DateTime dateTime}) {
    return DateTime(dateTime?.year, dateTime?.month, 1, 0, 0, 0, 0);
  }

// get start of month
  String getMonthandYear({DateTime dateTime}) {
    return DateFormat("MMMM yyyy").format(dateTime);
  }

  DateTime getDate(DateTime d) => DateTime(d.year, d.month, d.day);

  // convert  String to dateTime
  String convertDateTimeToEEEMMMDD({DateTime dateTime}) {
    return eEEMMMdDDateFormat.format(dateTime);
  }

  // used to get time
  String getTimeFormat({String dateTime, bool toLocal = true}) {
    if (toLocal == true) {
      return defaultTimeFormat
          .format(defaultServerDateTimeFormat.parse(dateTime, true).toLocal());
    } else {
      return defaultTimeFormat
          .format(defaultServerDateTimeFormat.parse(dateTime, true));
    }
  } // used to get time

  String getTimeFormatWithOutAmPm({
    String dateTime,
  }) {
    return DateFormat("HH:mm")
        .format(defaultServerDateTimeFormat.parse(dateTime));
  }

  String getDateTimeFormat({String dateTime}) {
    return dMMMhmmaDateFormat
        .format(defaultServerDateTimeFormat.parse(dateTime));
  }

  // used to get the 01 Jan, Tuesday format
  String getDateTimeDayFormat({String dateTime, bool toLocal = true}) {
    if (toLocal == true) {
      return defaultDDMMYYYYFormat
          .format(defaultServerDateTimeFormat.parse(dateTime, true).toLocal());
    } else {
      return defaultDDMMYYYYFormat
          .format(defaultServerDateTimeFormat.parse(dateTime));
    }
  }

  // used to get time
  String getDateAndTimeFormat({String dateTime, bool toLocal = true}) {
    if (toLocal == true) {
      return defaultServerDateTimeFormat
          .format(defaultServerDateTimeFormat.parse(dateTime, true).toLocal());
    } else {
      return defaultServerDateTimeFormat
          .format(defaultServerDateTimeFormat.parse(dateTime, true));
    }
  }

  // used to get the selected date time
  DateTime dateTime({DateTime selectedDate, TimeOfDay timeOfDay}) {
    return new DateTime(selectedDate.year, selectedDate.month, selectedDate.day,
        timeOfDay.hour, timeOfDay.minute);
  }

  String getExpectedDateTime({
    DateTime dateTime,
  }) {
    return dateFormatDistanceTime.format(dateTime);
  }


  // used to convert the date time to string dd-MM-yyyy format
  String convertStringToDefaultServerDateFormat({String dateTime}){
    DateTime time = defaultServerDateTimeFormat.parse(dateTime);
    return defaultDDMMYYYYFormat.format(time);
  }
}
