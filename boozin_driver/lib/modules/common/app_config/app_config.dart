import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:boozin_driver/modules/common/base_api/base_api.dart';
import '../../../modules/common/theme/app_themes.dart';


class AppConfig extends InheritedWidget {
  AppConfig({
    @required this.appName,
    @required this.flavorName,
    @required this.apiBaseUrl,
    @required Widget child,
    @required this.baseApi,
    @required this.termsUrl,

  }) : super(child: child) {
    themeData = appTheme;
    baseApi.setUpOptions(baseUrl: apiBaseUrl);
  }

  final String appName;
  final String flavorName;
  final String apiBaseUrl;
  ThemeData themeData;
  final BaseApi baseApi;
  final String termsUrl;

  static AppConfig of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType(aspect: AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }
}
