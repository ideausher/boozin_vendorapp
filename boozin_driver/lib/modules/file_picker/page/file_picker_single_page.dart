import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import '../../../localizations.dart';
import '../../../modules/common/utils/navigator_utils.dart';
import '../../../modules/file_picker/enums/file_picker_enums.dart';
import '../../../modules/file_picker/model/file_picker_model.dart';
import '../../common/constants/color_constants.dart';
import '../../common/constants/dimens_constants.dart';

class FilePickerSinglePage extends StatefulWidget {
  BuildContext context;
  FilePickerModel filePickerModel;
  Function onPressed;


  FilePickerSinglePage({this.context, this.filePickerModel, this.onPressed});

  @override
  _FilePickerSinglePageState createState() => _FilePickerSinglePageState();
}

class _FilePickerSinglePageState extends State<FilePickerSinglePage> {
  File _file; // selected file
  FilePickerModel _filePickerModel; // user request to get type of file

  @override
  void initState() {
    // check data
    if (widget.filePickerModel != null) {
      _filePickerModel = widget.filePickerModel;
    } else
      _filePickerModel = ModalRoute
          .of(widget.context)
          .settings
          .arguments;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Wrap(
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
              borderRadius:
              new BorderRadius.only(topLeft: Radius.circular(SIZE_30), topRight: Radius.circular(SIZE_30)),
              shape: BoxShape.rectangle,
              boxShadow: [BoxShadow(color: Colors.black54, blurRadius: 6)],
            ),
            child: Material(
              shape: ContinuousRectangleBorder(
                  borderRadius:
                  BorderRadius.only(topLeft: Radius.circular(SIZE_60), topRight: Radius.circular(SIZE_60))),
              child: Padding(
                padding: const EdgeInsets.all(SIZE_20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Visibility(
                      visible: ((_filePickerModel.pickFrom == FilePickFromEnum.PickFromBoth ||
                          _filePickerModel.pickFrom == FilePickFromEnum.PickFromCamera) &&
                          _filePickerModel.pickerType != FilePickerTypeEnum.PickerTypeDoc),
                      child: Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: SIZE_15, right: SIZE_15),
                          child: _showSelectGalleryButtonView(
                              buttonText:AppLocalizations.of(context).filePicker.text.gallery,
                              iconData: Icons.perm_media,
                              radius: SIZE_10,
                              textColor: Colors.black,
                              onPressed: () {
                                _imageSelectorGallery(context);
                              }),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: _filePickerModel.pickFrom == FilePickFromEnum.PickFromBoth ||
                          _filePickerModel.pickFrom == FilePickFromEnum.PickFromGallery,
                      child: Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: SIZE_15, right: SIZE_15),
                          child: _showSelectGalleryButtonView(
                              buttonText: AppLocalizations.of(context).filePicker.text.camera,
                              iconData: Icons.camera_enhance,
                              radius: SIZE_10,
                              textColor: Colors.black,
                              onPressed: () {
                                _imageSelectorCamera(context);
                              }),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  //this method will return documents form gallery View
  _showSelectGalleryButtonView({
    String buttonText,
    IconData iconData,
    double radius,
    Function onPressed,
    Color backgroundColor = Colors.white,
    Color textColor = COLOR_PRIMARY,
    Color iconColor = COLOR_PRIMARY,
  }) {
    return FlatButton(
      color: backgroundColor,
      padding: EdgeInsets.all(SIZE_10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(SIZE_5),
            child: Icon(
              iconData,
              color: iconColor,
            ),
          ),
          new Text(
            buttonText,
            style: TextStyle(color: textColor, fontSize: SIZE_14),
          ),
        ],
      ),
      onPressed: onPressed,
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(
          radius,
        ),
        side: BorderSide(
          color: COLOR_PRIMARY,
        ),
      ),
    );
  }

  //display image selected from gallery
  _imageSelectorGallery(BuildContext context) async {
    _file = await ImagePicker.pickImage(source: ImageSource.gallery, imageQuality: _filePickerModel.compressedQuality);

    if (_file != null) {
      _file = await ImageCropper.cropImage(
        sourcePath: _file.path,
        maxWidth: 512,
        maxHeight: 512,
        compressQuality:  _filePickerModel.compressedQuality,
      );
    }
    if (widget.onPressed != null) {
      widget.onPressed(_file?.path ?? null);
    } else
      NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context, dataToBeSend: _file?.path ?? null);
    print("You selected gallery image : " + _file?.path ?? "");
  }

  //display image selected from camera
  _imageSelectorCamera(BuildContext context) async {
    _file = await ImagePicker.pickImage(source: ImageSource.camera, imageQuality:  _filePickerModel.compressedQuality);

    if (_file != null) {
      _file = await ImageCropper.cropImage(
        sourcePath: _file.path,
        maxWidth: 512,
        maxHeight: 512,
        compressQuality: _filePickerModel.compressedQuality,
      );
    }

    if (widget.onPressed != null) {
      widget.onPressed(_file?.path ?? null);
    } else
      NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context, dataToBeSend: _file?.path ?? null);
    print("You selected camera image : " + _file?.path ?? "");
  }
}
