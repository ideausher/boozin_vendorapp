import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../modules/common/utils/navigator_utils.dart';
import '../../../modules/file_picker/model/file_picker_model.dart';
import '../../../modules/file_picker/page/file_picker_single_page.dart';

class SingleFilePickerDialog {
  static SingleFilePickerDialog _singleFilePickerDialog = SingleFilePickerDialog();

  static SingleFilePickerDialog get singleFilePickerDialogInstance => _singleFilePickerDialog;

  // show single file picker dialog
  showFilePickerDialog({BuildContext context, FilePickerModel filePickerModel, Function fileData, Function cancel}) {
    showDialog(
      child: new FilePickerSinglePage(
        context: context,
        filePickerModel: filePickerModel,
        onPressed: (value) {
          fileData(value);
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        },
      ),
      context: context,
    );
  }
}
