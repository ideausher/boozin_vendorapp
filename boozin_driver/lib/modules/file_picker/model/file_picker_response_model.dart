class FilePickerResponseModel {
  var fileList = List<FileModel>();

  FilePickerResponseModel();
}

class FileModel {
  String localPath;
  String serverPath;
  num fileId;

  FileModel({this.localPath, this.serverPath, this.fileId});
}
