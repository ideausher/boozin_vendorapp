import 'dart:io';

import 'package:flutter/cupertino.dart';
import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../modules/file_picker/model/file_picker_model.dart';
import '../../../modules/file_picker/model/file_picker_response_model.dart';

abstract class UploadDocumentEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final File file;
  final FilePickerResponseModel filePickerResponseModel;
  final FilePickerModel filePickerIntentModel;

  UploadDocumentEvent(
      {this.isLoading,
      this.context,
      this.file,
      this.filePickerResponseModel,
      this.filePickerIntentModel});
}

// sign up api call event
class RemoveDocumentEvent extends UploadDocumentEvent {
  RemoveDocumentEvent(
      {bool isLoading,
      BuildContext context,
      File file,
      FilePickerResponseModel filePickerResponseModel,
      FilePickerModel filePickerIntentModel})
      : super(
            isLoading: isLoading,
            context: context,
            file: file,
            filePickerResponseModel: filePickerResponseModel,
            filePickerIntentModel: filePickerIntentModel);
}

// sign up api call event
class AddFileEvent extends UploadDocumentEvent {
  AddFileEvent(
      {bool isLoading,
      BuildContext context,
      File file,
      FilePickerResponseModel filePickerResponseModel,
      FilePickerModel filePickerIntentModel})
      : super(
            isLoading: isLoading,
            context: context,
            file: file,
            filePickerResponseModel: filePickerResponseModel,
            filePickerIntentModel: filePickerIntentModel);
}
