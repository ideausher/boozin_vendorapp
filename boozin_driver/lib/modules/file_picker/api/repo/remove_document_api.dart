import 'package:flutter/material.dart';

import '../../../../modules/common/app_config/app_config.dart';

class RemoveDocumentApi {
  // RemoveDocumentApi
  Future<dynamic> removeDocumentApiCall({BuildContext context, num fileId, String endPoint}) async {
    var result;
    result = await AppConfig.of(context).baseApi.deleteRequest(
      endPoint,
      context,
      data: {"file_id": fileId},
    );

    return result;
  }
}
