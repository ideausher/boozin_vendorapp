import 'dart:io';

import 'package:flutter/material.dart';

import '../../../../modules/common/app_config/app_config.dart';

class UploadDocumentApi {
  // Signup
  Future<dynamic> uploadDocumentApiCall({BuildContext context, File file, String endPoint}) async {
    var result;
    result = await AppConfig.of(context).baseApi.multiPartRequest(
          endPoint,
          context,
          filePath: file.path,
        );

    return result;
  }
}
