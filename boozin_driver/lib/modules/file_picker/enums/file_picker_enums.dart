enum FilePickerTypeEnum {
  PickerTypeDoc,
  PickerTypeImage,
}
enum FilePickFromEnum {
  PickFromBoth,
  PickFromCamera,
  PickFromGallery,
}

enum DocTypeEnum {
  Pdf,
  Doc,
  Jpg,
  Jpeg,
  Png
}


extension DocTypeEnumExtension on DocTypeEnum {

  String get value {
    switch (this) {
      case DocTypeEnum.Pdf:
        return ".pdf";
      case DocTypeEnum.Doc:
        return ".doc";
      case DocTypeEnum.Jpg:
        return ".jpg";
      case DocTypeEnum.Jpeg:
        return ".jpeg";
      case DocTypeEnum.Png:
        return ".png";

      default:
        return null;
    }
  }

}
