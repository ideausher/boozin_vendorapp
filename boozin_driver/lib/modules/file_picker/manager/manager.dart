//this method will return selecet documents form gallery View
import 'package:flutter/material.dart';
import '../../../modules/common/constants/color_constants.dart';
import '../../../modules/common/constants/dimens_constants.dart';
import '../../../modules/common/theme/app_themes.dart';

showSelectGalleryButtonView({
  String buttonText,
  IconData iconData,
  double radius,
  Function onPressed,
  Color backgroundColor = Colors.white,
  Color iconColor = COLOR_PRIMARY,
})
{
  return FlatButton(
    color: backgroundColor,
    padding: EdgeInsets.all(SIZE_10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(SIZE_5),
          child: Icon(
            iconData,
            color: iconColor,
          ),
        ),
        new Text(
          buttonText,
          style: appTheme.textTheme.headline5,
        ),
      ],
    ),
    onPressed: onPressed,
    shape: new RoundedRectangleBorder(
      borderRadius: new BorderRadius.circular(
        radius,
      ),
      side: BorderSide(
        color: COLOR_PRIMARY,
      ),
    ),
  );
}
