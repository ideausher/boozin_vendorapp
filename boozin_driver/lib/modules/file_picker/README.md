follow this link to add location dependency

https://pub.dev/packages/location#-readme-tab-


# import on yaml file
  image_picker: ^0.6.5+2
  image_cropper: ^1.2.1
  cached_network_image:
  file_picker: ^1.8.0+2

# add this under assets
  assets:
     - lib/modules/file_picker/images/


# Copy this module under modules file_picker

# Add this to Android manifest
    <activity
            android:name="com.yalantis.ucrop.UCropActivity"
            android:screenOrientation="portrait"
            android:theme="@style/Theme.AppCompat.Light.NoActionBar" />


# Calling multiple
NavigatorUtils.navigatorUtilsInstance
            .navigatorPushedName(context, FilePickerRoutes.FILE_PICKER_MULTIPLE_SCREEN_ROOT,
            dataToBeSend: FilePickerModel(
              pickerType: FilePickerTypeEnum.PickerTypeImage,
              pickFrom: FilePickFromEnum.PickFromBoth,
              uploadEnable: false,
              maxFiles: 3,

            ))
        .then((result) {

//result is instance of  FilePickerResponseModel

    });


# Calling single

as a dialog

SingleFilePickerDialog.singleFilePickerDialogInstance.showFilePickerDialog(
              filePickerModel: FilePickerModel(
                pickerType: FilePickerTypeEnum.PickerTypeImage,
                pickFrom: FilePickFromEnum.PickFromBoth,
              ),
              context: context,
              fileData: (value) {

              });


as a page

 var result = await AppUtils.navigatorUtilsInstance.navigatorPushedNameResult(
      context,
      FilePickerRoutes.FILE_PICKER_SINGLE_SCREEN_ROOT,
      dataToBeSend: FilePickerModel(
        pickerType: FilePickerTypeEnum.PickerTypeImage,
        pickFrom: FilePickFromEnum.PickFromBoth,
      ),
    );
    AppUtils.showSnackBar(
        scaffoldState: _scaffoldUploadDocumentKey.currentState, message: result?.toString() ?? 'ERROR');



# And to use it in iOS, you have to add this permission in Info.plist :
#
# NSLocationWhenInUseUsageDescription
# NSLocationAlwaysUsageDescription

