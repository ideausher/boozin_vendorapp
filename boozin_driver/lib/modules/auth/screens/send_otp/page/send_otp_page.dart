import 'package:animator/animator.dart';
import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/shared_prefs_utils.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:boozin_driver/modules/auth/constants/image_constant.dart';
import 'package:boozin_driver/modules/auth/enums/auth_enums.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/model/update_ui_data_model.dart';
import 'package:boozin_driver/modules/common/utils/launcher_utils.dart';
import '../../../../../localizations.dart';
import '../../../../../modules/common/utils/common_utils.dart';
import '../../../../../modules/auth/manager/auth_manager.dart';
import '../../../../common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../../common/app_config/app_config.dart';
import '../../../../common/constants/dimens_constants.dart';
import '../../../auth_bloc/auth_bloc.dart';
import '../../../auth_bloc/auth_state.dart';

class SendOtpPage extends StatefulWidget {
  BuildContext context;

  SendOtpPage(this.context);

  @override
  _SendOtpPageState createState() => _SendOtpPageState();
}

class _SendOtpPageState extends State<SendOtpPage> {
  //Declaration of auth bloc
  var _authBloc;
  AuthState _authState;
  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Declaration text editing controller
  final TextEditingController _phoneController = TextEditingController();

  //Declaration of manager
  AuthManager _authManager = AuthManager();

  //Declaration of UI updation model
  UpdateUiDataModel _updateUiDataModel;
  bool isAnimationFinished = false;

  @override
  void initState() {
    super.initState();
    _updateUiDataModel = new UpdateUiDataModel();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _authManager.actionOnInit(
        context: widget.context,
        authBloc: _authBloc,
        updateUiDataModel: _updateUiDataModel);
  }

  @override
  void dispose() {
    super.dispose();
    _phoneController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      bottom: true,
      top: true,
      child: Scaffold(
        key: _scaffoldKey,
        body: BlocEventStateBuilder<AuthState>(
          bloc: _authBloc,
          builder: (BuildContext context, AuthState authState) {
            _context = context;
            if (authState != null && _authState != authState) {
              _authState = authState;
              _authManager.actionSendOtpStateChange(
                  context: context,
                  scaffoldState: _scaffoldKey?.currentState,
                  phoneController: _phoneController,
                  authBloc: _authBloc,
                  authState: _authState,
                  updateUiDataModel: _updateUiDataModel);
            }
            return ModalProgressHUD(
              inAsyncCall: authState?.isLoading ?? false,
              child: SingleChildScrollView(
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: [
                    Image.asset(AUTH_BG,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height -
                            MediaQuery.of(context).padding.bottom,
                        fit: BoxFit.cover),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: SIZE_20,
                          right: SIZE_20,
                          bottom: SIZE_20,
                          top: SIZE_60),
                      child: Wrap(
                        runSpacing: SIZE_16,
                        alignment: WrapAlignment.center,
                        children: <Widget>[
                          _showLogInLogo(),
                          _form(),
                          _acceptTermsAndConditionWidget(),
                          _continueButton(),
                          _orText(),
                          _continueAsTruckDriverButton(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  // used to create a top text view
  Widget _showLogInLogo() {
    return (isAnimationFinished == true)
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  width: CommonUtils.commonUtilsInstance.getPercentageSize(
                      context: _context, ofWidth: true, percentage: SIZE_50),
                  child: Image.asset(
                    SIGN_UP_LOGO,
                  )),
              Text(
                AppLocalizations.of(context).common.text.driver,
                style: textStyleSize26WhiteColor,
              ),
            ],
          )
        : Container(
            width: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: _context, ofWidth: true, percentage: SIZE_50),
            height: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: _context, ofWidth: false, percentage: SIZE_20),
            child: Animator(
              tween: Tween<Offset>(begin: Offset(0, 0.4), end: Offset(0, 0)),
              duration: Duration(milliseconds: 1500),
              repeats: 1,
              cycles: 1,
              endAnimationListener: (animation) {
                if (animation.controller.isCompleted == true) {
                  isAnimationFinished = true;
                }
              },
              builder: (_, animatorState, __) => FractionalTranslation(
                  translation: animatorState.value,
                  child: Column(
                    children: [
                      Image.asset(
                        SIGN_UP_LOGO,
                      ),
                      Text(
                        AppLocalizations.of(context).common.text.driver,
                        style: textStyleSize26WhiteColor,
                      ),
                    ],
                  )),
            ),
          );
  }

  //used to create a form
  Widget _form() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context).sendOtp.text.signUp,
          style: textStyleSize26WhiteColor,
        ),
        Form(
          child: _showCountryCodePickerAndPhoneNumber(),
        ),
      ],
    );
  }

  // used to create a  phone text field form
  Widget _phoneTextFormField() {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: SIZE_2),
          child: Text(
            '+ ${_updateUiDataModel?.selectedCountry?.dialingCode}' ??
                Country?.ZA?.dialingCode + " ",
            style: textStyleSize14WithWhiteColor,
          ),
        ),
        Expanded(
          child: _textFieldForm(
            keyboardType: TextInputType.number,
            hint: AppLocalizations.of(context).sendOtp.hint.phoneNumber,
            elevation: ELEVATION_0,
            controller: _phoneController,
          ),
        )
      ],
    );
  }

  // used to create a  phone country picker  text field form
  Widget _showCountryCodePickerAndPhoneNumber() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      margin: EdgeInsets.only(top: SIZE_20),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.black.withOpacity(0.3),
          border: Border.all(color: Colors.white, width: SIZE_1_5),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(SIZE_5),
            child: CountryPicker(
              dense: false,
              showFlag: true,
              //displays flag, true by default
              showDialingCode: false,
              //displays dialing code, false by default
              showName: false,
              //displays country name, true by default
              showCurrency: false,
              //eg. 'British pound'
              showCurrencyISO: false,
              //eg. 'GBP'
              onChanged: (Country country) {
                _updateUiDataModel?.selectedCountry = country;
                _updateUiDataModel?.phoneNumber =
                    _phoneController.text.toString();
                _authManager?.updateUI(
                    context: _context,
                    authBloc: _authBloc,
                    authState: _authState,
                    updateUiDataModel: _updateUiDataModel);
                ;
              },
              selectedCountry:
                  _updateUiDataModel?.selectedCountry ?? Country.ZA,
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: SIZE_5),
            child: Image.asset(
              DIVIDER_ICON,
              color: Colors.white,
            ),
            height: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: _context, ofWidth: false, percentage: SIZE_5),
          ),
          Expanded(
            child: _phoneTextFormField(),
          )
        ],
      ),
    );
  }

  //method to show  proceed button
  Widget _continueButton() {
    return Container(
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () async {
          //Save user type in pref
          await SharedPrefUtils.sharedPrefUtilsInstance
              .setInt(UserRoles.OnDemandDriver.value, PrefsEnum.UserType.value);

          _validateSignUp();
        },
        child: Text(
          AppLocalizations.of(context).sendOtp.text.continueWithOnDemandDriver,
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to show  proceed button
  Widget _continueAsTruckDriverButton() {
    return Container(
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () async {
          //Save user type in pref
          await SharedPrefUtils.sharedPrefUtilsInstance.setInt(
              UserRoles.CommercialVehicleDriver.value,
              PrefsEnum.UserType.value);
          _validateSignUp();
        },
        child: Text(
          AppLocalizations.of(context)
              .sendOtp
              .text
              .continueWithCommercialVehicleDriver,
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to return text form field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      bool showTickIcon,
      String hint,
      double elevation,
      FormFieldValidator<String> validator}) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_5),
      elevation: elevation,
      color: Colors.transparent,
      child: TextFormField(
        validator: validator,
        onFieldSubmitted: _showTickMark,
        onChanged: (phoneNumber) {
          if (phoneNumber.length >= MaxLength.MinPhoneLength.value) {
            _showTickMark(phoneNumber);
          } else {
            _showTickMark(phoneNumber);
          }
        },
        controller: controller,
        keyboardType: keyboardType,
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly,
          new LengthLimitingTextInputFormatter(14),
        ],
        style: textStyleSize14WithWhiteColor,
        decoration: InputDecoration(
          suffixIcon: (_updateUiDataModel?.phoneNumber != null &&
                  _updateUiDataModel?.phoneNumber?.length >=
                      MaxLength.MinPhoneLength.value)
              ? Icon(Icons.check, color: Colors.white, size: SIZE_20)
              : null,
          hintText: hint,
          hintStyle: textStyleSize14WithWhiteColor,
          contentPadding: EdgeInsets.all(SIZE_0),
          border: OutlineInputBorder(borderSide: BorderSide.none),
        ),
      ),
    );
  }

  //this method will show the tick mark icon when user hits done button while typing number
  void _showTickMark(String values) {
    _updateUiDataModel.phoneNumber = values;
    _authManager?.updateUI(
      context: _context,
      updateUiDataModel: _authState?.updateUiDataModel ?? _updateUiDataModel,
      authBloc: _authBloc,
      authState: _authState,
    );
  }

  //method to return accept terms and services widget
  Widget _acceptTermsAndConditionWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Theme(
          data: Theme.of(_context).copyWith(
            unselectedWidgetColor: Colors.white,
          ),
          child: Checkbox(
              value: _authState?.updateUiDataModel?.isTermsAccepted ?? false,
              activeColor: COLOR_PRIMARY,
              onChanged: (bool value) {
                _updateUiDataModel?.isTermsAccepted = value;
                _updateUiDataModel?.phoneNumber =
                    _phoneController?.text.toString();
                _authManager?.updateUI(
                    context: _context,
                    authState: _authState,
                    updateUiDataModel: _updateUiDataModel,
                    authBloc: _authBloc);
              }),
        ),
        Padding(
          padding: const EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
          child: Center(
              child: InkWell(
            onTap: () {
              LauncherUtils.launcherUtilsInstance
                  .launchInBrowser(url: AppConfig.of(context).termsUrl);
            },
            child: Text(
              AppLocalizations.of(context).sendOtp.error.agreeTermsAndCondition,
              style: textStyleSize14WithWhiteColor,
            ),
          )),
        ),
      ],
    );
  }

  //method to call validate for when proceed button is pressed
  void _validateSignUp() {
    _authManager.sendOtpCall(
      context: _context,
      authBloc: _authBloc,
      phoneNumber: _phoneController?.text,
      updateUiDataModel: _updateUiDataModel,
      selectedCountry: _updateUiDataModel?.selectedCountry ?? Country.ZA,
      scaffoldState: _scaffoldKey?.currentState,
    );
  }

  //Show or text
  _orText() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_5, bottom: SIZE_5),
      child: Text(
        "OR",
        textAlign: TextAlign.center,
        style: textStyleSize14WithWhiteColor,
      ),
    );
  }
}
