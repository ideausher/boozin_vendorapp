import 'package:boozin_driver/modules/auth/enums/auth_enums.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:boozin_driver/modules/vehicle_documents/registration_routes.dart';
import 'package:boozin_driver/modules/auth/auth_routes.dart';
import 'package:boozin_driver/modules/auth/constants/image_constant.dart';
import 'package:boozin_driver/modules/common/constants/dimens_constants.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/shared_prefs_utils.dart';
import 'package:boozin_driver/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:boozin_driver/modules/common/model/user_current_location_model.dart';

import '../../../../../localizations.dart';

class LogInSuccessFullScreen extends StatefulWidget {
  BuildContext _context;

  LogInSuccessFullScreen(this._context);

  @override
  _LogInSuccessFullScreenState createState() => _LogInSuccessFullScreenState();
}

class _LogInSuccessFullScreenState extends State<LogInSuccessFullScreen> {
  bool _serviceEnabled;
  Position _myLocation;
  CurrentLocation _currentLocation;
  int comingFrom = ComingFrom.EnterDetailScreen.value;

  //class variables
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    comingFrom = ModalRoute.of(widget._context).settings.arguments;

    //here starting the timer of 2 sec after this navigate to next screen
    Future.delayed(Duration(seconds: 2), () async {
      _checkLocationPermissionStatus();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      top: false,
      child: Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        body: Stack(
          alignment: Alignment.topCenter,
          children: [
            Image.asset(AUTH_BG,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.bottom,
                fit: BoxFit.cover),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: SIZE_25,
                ),
                Image.asset(LOGIN_SUCCESSFUL_LOGO,
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: context, percentage: SIZE_20, ofWidth: true)),
                SizedBox(
                  height: SIZE_15,
                ),
                Text(
                  comingFrom == ComingFrom.VerifyOtpScreen.value
                      ? AppLocalizations.of(context)
                          .loginSuccessfully
                          .text
                          .loginSuccessfully
                      : AppLocalizations.of(context)
                          .loginSuccessfully
                          .text
                          .signUpSuccessfully,
                  style: textStyleSize26WhiteColor,
                )
              ],
            ),
            _showLogInLogo(),
          ],
        ),
      ),
    );
  }

  //this method will check the location permission status if enables navigate to home else navigate to enable location screen
  void _checkLocationPermissionStatus() async {
    _serviceEnabled = await CurrentLocationManger.locationMangerInstance
        .serviceEnabledOrNot();
    if (!_serviceEnabled) {
      NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
          widget?._context, AuthRoutes.ENABLE_LOCATION_ROOT);
    } else {
      //if location permission is allowed only then get user location else show grant location meassage
      LocationPermission permissionStatus = await CurrentLocationManger
          .locationMangerInstance
          .permissionEnabled();

      if (permissionStatus == LocationPermission.always ||
          permissionStatus == LocationPermission.whileInUse) {
        _getLocationAndFindAddress();
      }
      //if user denies the permission navigate to location screen
      else {
        NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
            widget?._context, AuthRoutes.ENABLE_LOCATION_ROOT);
      }
    }
  }

  //method to get location and find address
  void _getLocationAndFindAddress() async {
    _myLocation = await CurrentLocationManger.locationMangerInstance
        .getCurrentLocationData();
    _currentLocation = await CurrentLocationManger?.locationMangerInstance
        ?.getAddressUsingLocation(widget?._context, _myLocation);
    print('current location is ${_currentLocation?.currentAddress}');
    await SharedPrefUtils.sharedPrefUtilsInstance
        .saveLocationObject(_currentLocation, PrefsEnum.UserLocationData.value);
    NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
        context, RegistrationRoutes.COMPLETE_PROFILE,
        dataToBeSend: HOME);
    /*  NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
        widget?._context, AuthRoutes.HOME_SCREEN, */ /*dataToBeSend: _currentLocation*/ /*);*/
  }

  // used to create a top text view
  Widget _showLogInLogo() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_70),
      child: Column(
        children: [
          Container(
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, ofWidth: true, percentage: SIZE_50),
              child: Image.asset(
                SIGN_UP_LOGO,
              )),
          Text(
            AppLocalizations.of(context).common.text.driver,
            style: textStyleSize26WhiteColor,
          ),
        ],
      ),
    );
  }
}
