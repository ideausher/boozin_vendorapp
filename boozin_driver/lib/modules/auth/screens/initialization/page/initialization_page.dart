import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import '../../../../../modules/auth/auth_bloc/auth_bloc.dart';
import '../../../../../modules/auth/manager/auth_manager.dart';
import '../../../../../modules/auth/auth_bloc/auth_event.dart';
import '../../../../../modules/auth/auth_bloc/auth_state.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';

class InitializationPage extends StatefulWidget {
  AuthBloc authBloc;

  InitializationPage(this.authBloc);

  @override
  _InitializationPageState createState() => _InitializationPageState();
}

class _InitializationPageState extends State<InitializationPage> {
  AuthState _authState;
  AuthManager _authManager = AuthManager();

  @override
  void initState() {
    super.initState();
    widget.authBloc.emitEvent(
      CheckAuthentication(
        authBloc: widget.authBloc,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocEventStateBuilder<AuthState>(
        bloc: widget.authBloc,
        builder: (BuildContext context, AuthState state) {
          if (state != null && _authState != state) {
            _authState = state;
            _authManager.actionOnIntializationScreenStateChange(
              authState: _authState,
              context: context,
            );
          }

          return ModalProgressHUD(
            inAsyncCall: state?.isLoading ?? false,
            child: Container(
              color: Colors.white,
            ),
          );
        },
      ),
    );
  }
}
