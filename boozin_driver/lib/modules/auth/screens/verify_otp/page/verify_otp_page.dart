import 'package:flutter/material.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/common_widget/verify_otp_widget.dart';
import '../../../../../modules/auth/manager/auth_manager.dart';
import '../../../../../modules/auth/auth_bloc/auth_bloc.dart';
import '../../../../../modules/auth/auth_bloc/auth_state.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';

class VerifyOtpPage extends StatefulWidget {
  BuildContext context;

  VerifyOtpPage(BuildContext context) {
    this.context = context;
  }

  @override
  _VerifyOtpPageState createState() => _VerifyOtpPageState();
}

class _VerifyOtpPageState extends State<VerifyOtpPage> {
  //Controllers for OTP
  final TextEditingController _verifyOtpController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var _authBloc;
  AuthState _authState;
  BuildContext _context;
  AuthManager _authManager = AuthManager();

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return BlocEventStateBuilder<AuthState>(
      bloc: _authBloc,
      builder: (BuildContext context, AuthState authState) {
        if (authState != null && _authState != authState) {
          _context = context;
          _authState = authState;
          _authManager.actionOnVerifyOtpStateChanged(
              scaffoldState: _scaffoldKey?.currentState,
              authState: _authState,
              verifyOtpController: _verifyOtpController,
              phoneController: _phoneNumberController,
              context: _context,
              authBloc: _authBloc);
        }
        return ModalProgressHUD(
          inAsyncCall: authState?.isLoading ?? false,
          child: WillPopScope(
            onWillPop: () {
              return _authManager?.changeNumberEventCall(
                  context, _scaffoldKey?.currentState, _authBloc, authState);
            },
            child: SafeArea(
              bottom: true,
              top: true,
              child: Scaffold(
                key: _scaffoldKey,
                body: VerifyOTPWidget(
                    context: _context,
                    authState: _authState,
                    phoneNumberController: _phoneNumberController,
                    authBloc: _authBloc,
                    authManager: _authManager,
                    verifyOtpController: _verifyOtpController,
                    isForEmail: false,
                    //call for verify otp call
                    onSendOtpButtonPressed: (String otp) {
                      print('The Otp is ${otp}');
                      _validateOtp(otp: otp);
                    },
                    //call for resend otp call
                    onResendOtpButtonPressed: () {
                      _resendOtpCall();
                    },
                    onChangeNumberPressed: () {
                      _authManager?.changeNumberEventCall(context,
                          _scaffoldKey?.currentState, _authBloc, authState);
                    },
                    title: AppLocalizations.of(context).verifyOtp.text.signUp,
                    userPhoneEmail:
                        '(+${_authState?.updateUiDataModel?.selectedCountry?.dialingCode ?? ""}) ${_authState?.updateUiDataModel?.phoneNumber ?? ""}',
                    subtitle: AppLocalizations.of(context)
                        .verifyOtp
                        .text
                        .enterOtpText),
              ),
            ),
          ),
        );
      },
    );
  }

  // validate otp and api call
  void _validateOtp({String otp}) {
    _authManager.verifyOtpCall(
      context: _context,
      authState: _authState,
      authBloc: _authBloc,
      otp: otp,
      scaffoldState: _scaffoldKey?.currentState,
    );
  }

  // resend otp call
  void _resendOtpCall() {
    _authManager.resendOtpCall(
      context: _context,
      authState: _authState,
      authBloc: _authBloc,
      scaffoldState: _scaffoldKey?.currentState,
    );
  }
}
