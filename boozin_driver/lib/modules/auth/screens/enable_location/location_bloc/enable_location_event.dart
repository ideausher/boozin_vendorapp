import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/common/model/user_current_location_model.dart';

abstract class EnableCurrentLocationEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final CurrentLocation currentLocation;

  EnableCurrentLocationEvent(
      {this.isLoading: false, this.context, this.currentLocation});
}

//this event is used for getting current location data
class GetCurrentLocationEvent extends EnableCurrentLocationEvent {
  GetCurrentLocationEvent(
      {bool isLoading,
      BuildContext context,
      bool networkConnected,
      CurrentLocation currentLocation})
      : super(
            isLoading: isLoading,
            context: context,
            currentLocation: currentLocation);
}
