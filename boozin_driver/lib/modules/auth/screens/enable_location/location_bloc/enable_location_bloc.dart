
import 'package:geolocator/geolocator.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/screens/enable_location/location_bloc/enable_location_event.dart';
import 'package:boozin_driver/modules/auth/screens/enable_location/location_bloc/enable_location_state.dart';
import 'package:boozin_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:boozin_driver/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:boozin_driver/modules/common/model/user_current_location_model.dart';

class EnableCurrentLocationBloc extends BlocEventStateBase<
    EnableCurrentLocationEvent, EnableCurrentLocationState> {
  EnableCurrentLocationBloc({bool initializing = true})
      : super(initialState: EnableCurrentLocationState.initiating());

  @override
  Stream<EnableCurrentLocationState> eventHandler(
      EnableCurrentLocationEvent event,
      EnableCurrentLocationState currentState) async* {
    //get current location
    if (event is GetCurrentLocationEvent) {
      yield EnableCurrentLocationState.updateLocation(
          isLoading: event?.isLoading, context: event?.context);
      String _message = "";
      CurrentLocation currentLocation;
      Position geolocator;
      LocationPermission permission;

      permission = await CurrentLocationManger.locationMangerInstance
          .permissionEnabledOrNot();
      if (permission == LocationPermission.deniedForever ||
          permission == LocationPermission.denied) {
        _message = AppLocalizations.of(event?.context)
            .enableLocation
            .error
            .pleaseGrantPermission;
      }

      //if location permission is allowed only then get user location else show grant location meassage
      // LocationPermission permissionStatus = await CurrentLocationManger
      //     .locationMangerInstance
      //     .permissionEnabled();

      if (permission == LocationPermission.always ||
          permission == LocationPermission.whileInUse) {
        try {
          geolocator = await CurrentLocationManger.locationMangerInstance
              .getCurrentLocationData();
          currentLocation = await CurrentLocationManger?.locationMangerInstance
              ?.getAddressUsingLocation(event?.context, geolocator);
          print('the current location is ${currentLocation?.currentAddress}');
        } catch (e) {
          _message = AppLocalizations.of(event.context)
              .enableLocation
              .error
              .plsEnableYourLocation;
        }
      }

      yield EnableCurrentLocationState.updateLocation(
          isLoading: false,
          context: event?.context,
          message: _message,
          currentLocation: currentLocation);
    }
  }
}
