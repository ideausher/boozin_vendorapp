import 'package:boozin_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:boozin_driver/modules/common/theme/app_themes.dart';
import 'package:boozin_driver/modules/common/utils/date_picker_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_country_picker/country.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:boozin_driver/modules/auth/constants/image_constant.dart';
import 'package:boozin_driver/modules/common/constants/color_constants.dart';
import 'package:boozin_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:boozin_driver/modules/common/model/update_ui_data_model.dart';
import '../../../../../localizations.dart';
import '../../../../../modules/common/utils/common_utils.dart';
import '../../../../../modules/auth/enums/auth_enums.dart';
import '../../../../../modules/auth/manager/auth_manager.dart';
import '../../../../auth/auth_bloc/auth_bloc.dart';
import '../../../../auth/auth_bloc/auth_state.dart';
import '../../../../common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../../common/app_config/app_config.dart';
import '../../../../common/constants/dimens_constants.dart';

class EnterDetailsPage extends StatefulWidget {
  BuildContext _context;

  EnterDetailsPage(this._context);

  @override
  _EnterDetailsPageState createState() => _EnterDetailsPageState();
}

class _EnterDetailsPageState extends State<EnterDetailsPage> {
  //Declaration of auth bloc
  var _authBloc;
  AuthState _authState;
  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Declaration of edit text controller
  final TextEditingController _dobController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();

  //Class variables
  AuthManager _authManager = AuthManager();
  Country _selectedCountry = Country.ZA;
  UpdateUiDataModel updateUiDataModel;
  String userName = "";
  DateTime _selectedDate = DateTime.now();

  //Dropdown item
  List<int> _dropdownValues = [
    UserGender.Mrs.value,
    UserGender.Mr.value,
    UserGender.Ms.value
  ];

  @override
  Future<void> initState() {
    super.initState();
    updateUiDataModel = UpdateUiDataModel();
    _authBloc = BlocProvider.of<AuthBloc>(widget._context);
  }

  @override
  void dispose() {
    super.dispose();
    _dobController.dispose();
    _nameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;

    return WillPopScope(
      onWillPop: () {
        return _authManager?.signOutDialog(
            context: _context,
            authState: _authState,
            scaffoldState: _scaffoldKey.currentState,
            authBloc: _authBloc);
      },
      child: SafeArea(
        bottom: true,
        top: true,
        child: Scaffold(
          key: _scaffoldKey,
          body: BlocEventStateBuilder<AuthState>(
            bloc: _authBloc,
            builder: (BuildContext context, AuthState authState) {
              _context = context;
              if (authState != null && _authState != authState) {
                _authState = authState;

                if (authState?.authResponseModel?.userData != null &&
                    _authState?.authResponseModel?.userData?.countryIsoCode
                        ?.isNotEmpty ==
                        true) {
                  _selectedCountry = Country.findByIsoCode(authState
                      ?.authResponseModel?.userData?.countryIsoCode ??
                      authState?.updateUiDataModel?.selectedCountry?.isoCode);
                }


                _authManager.actionOnEnterDetailsStateChanged(
                    context: context,
                    authBloc: _authBloc,
                    authState: authState,
                    dobController: _dobController,
                    updateUiDataModel: authState?.updateUiDataModel,
                    scaffoldState: _scaffoldKey?.currentState,
                    selectedCountry: _selectedCountry);
              }
              return ModalProgressHUD(
                inAsyncCall: authState?.isLoading ?? false,
                child: SingleChildScrollView(
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: [
                      Image.asset(
                        AUTH_BG,
                        height: MediaQuery.of(context).size.height,
                        fit: BoxFit.cover,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: SIZE_20,
                            right: SIZE_20,
                            bottom: SIZE_20,
                            top: SIZE_60),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            _showLoginLogo(),
                            _showSignUpTitle(),
                            _showSpace(size: SIZE_2),
                            _showNameTextField(),
                            _showDobTextField(),
                            _showCountryCodePickerAndPhoneNumber(),
                            _continueButton(),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  // used to create a top text view
  Widget _showLoginLogo() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            width: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: _context, ofWidth: true, percentage: SIZE_50),
            child: Image.asset(
              SIGN_UP_LOGO,
            )),
        Text(
          AppLocalizations.of(context).common.text.driver,
          style: textStyleSize26WhiteColor,
        ),
        SizedBox(
          height: SIZE_15,
        ),
      ],
    );
  }

  // used to create a  phone text field form
  Widget _phoneTextFormField() {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: SIZE_2),
          child: Row(
            children: <Widget>[
              Text(
                '(${_authState?.authResponseModel?.userData?.countryCode ?? ""}) ',
                style: AppConfig.of(_context).themeData.textTheme.headline2,
              ),
            ],
          ),
        ),
        Expanded(
          child: _textFieldForm(
            keyboardType: TextInputType.phone,
            elevation: ELEVATION_0,
            enabled: false,
            showTickIcon: true,
            controller: _dobController,
          ),
        ),
      ],
    );
  }

  // used to create a  phone country picker  text field form
  Widget _showCountryCodePickerAndPhoneNumber() {
    return Visibility(
      visible: false,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_12),
        padding: EdgeInsets.only(right: SIZE_5, left: SIZE_10),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_10)),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 22,
              child: CountryPicker(
                  dense: false,
                  showFlag: true,
                  //isShowDialog: false,
                  //displays flag, true by default
                  showDialingCode: false,
                  //displays dialing code, false by default
                  showName: false,
                  //displays country name, true by default
                  showCurrency: false,
                  //eg. 'British pound'
                  showCurrencyISO: false,
                  //eg. 'GBP'
                  onChanged: null,
                  selectedCountry: _selectedCountry ?? Country?.ZA),
            ),
            Expanded(
              flex: 5,
              child: Container(
                margin: EdgeInsets.only(right: SIZE_5),
                child: Image.asset(DIVIDER_ICON),
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, ofWidth: false, percentage: SIZE_5),
              ),
            ),
            Expanded(
              flex: 77,
              child: _phoneTextFormField(),
            )
          ],
        ),
      ),
    );
  }

  //method to show continue button
  Widget _continueButton() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_40),
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () {
          _validateInput();
        },
        child: Text(
          AppLocalizations.of(_context).enterDetails.button.continueText,
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to return textform field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      IconData icon,
      String hint,
      double elevation,
      bool enabled,
      bool showTickIcon = false,
      FormFieldValidator<String> validator}) {
    return TextFormField(
      validator: validator,
      onFieldSubmitted: _updateUserName,
      textCapitalization: TextCapitalization.words,
      onChanged: (name) {
        userName = name;
        if (name.length >= MinLength.Username.value) {
          _updateUserName(name);
        } else {
          _updateUserName(name);
        }
      },
      style: textStyleSize14WithWhiteColor,
      controller: controller,
      enabled: enabled ?? true,
      keyboardType: keyboardType,
      decoration: InputDecoration(
          suffixIcon: (icon != null)
              ? Icon(
                  Icons.calendar_today,
                  color: Colors.white,
                )
              : (userName.isNotEmpty &&
                      userName?.length >= MinLength.Username.value)
                  ? Icon(Icons.check, color: Colors.white, size: SIZE_20)
                  : null,
          hintText: hint,
          contentPadding: EdgeInsets.all(SIZE_6),
          border: OutlineInputBorder(borderSide: BorderSide.none),
          hintStyle: textStyleSize14WithWhiteColor),
    );
  }

  //method to check validation of phone field
  void _validateInput() {
    _authManager.enterDetailsUpdate(
        scaffoldState: _scaffoldKey?.currentState,
        context: _context,
        authBloc: _authBloc,
        updateUiDataModel: _authState?.updateUiDataModel,
        authResponseModel: _authState?.authResponseModel,
        name: _nameController?.text,
        dateOfBirth: _dobController?.text);
  }

  //method to show login title
  Widget _showLoginTitle() {
    return Text(
      AppLocalizations.of(_context).enterDetails.text.enterDetails,
      textAlign: TextAlign.center,
      style: AppConfig.of(_context).themeData.textTheme.headline1,
    );
  }

  //method to show login screen subtitle
  Widget _showSignUpTitle() {
    return Text(
      AppLocalizations.of(_context).enterDetails.text.signup,
      style: textStyleSize26WhiteColor,
    );
  }

  //method to return text for filed of name
  Widget _showNameTextField() {
    return Container(
        margin: EdgeInsets.only(top: SIZE_16),
        padding: EdgeInsets.only(left: SIZE_10, right: SIZE_5),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.black.withOpacity(0.3),
            border: Border.all(color: Colors.white, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: _nameTextFormField());
  }

  //enter name field
  Widget _nameTextFormField() {
    return _textFieldForm(
      keyboardType: TextInputType.text,
      hint: AppLocalizations.of(_context).enterDetails.hint.enterName,
      elevation: ELEVATION_0,
      icon: null,
      controller: _nameController,
    );
  }

  //this method will give the space between the widgets
  Widget _showSpace({double size}) {
    return SizedBox(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, ofWidth: false, percentage: size ?? SIZE_10),
    );
  }

  void _updateUserName(String userName) {
    _authManager?.updateUI(
        context: _context,
        authBloc: _authBloc,
        authState: _authState,
        updateUiDataModel: _authState?.updateUiDataModel);
  }

  //method to show dob widget
  Widget _showDobTextField() {
    return Container(
        margin: EdgeInsets.only(top: SIZE_16),
        padding: EdgeInsets.only(left: SIZE_10, right: SIZE_5),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.black.withOpacity(0.3),
            border: Border.all(color: Colors.white, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: InkWell(
          onTap: () {
            _selectDateOfBirth();
          },
          child: _textFieldForm(
              keyboardType: TextInputType.text,
              hint: AppLocalizations.of(_context).enterDetails.hint.chooseDob,
              elevation: ELEVATION_0,
              enabled: false,
              controller: _dobController,
              icon: Icons.calendar_today),
        ));
  }

  //this method will show calender to select date of birth of a user
  Future<void> _selectDateOfBirth() async {
    var picked = await DatePickerUtils.datePickerUtilsInstance.getDatePicker(
        context: context,
        selectedDate: DateTime.now().toString(),
        lastDate: DateTime.now(),
        firstDate: new DateTime(DateTime.now().year - 100, DateTime.now().month,
            DateTime.now().day));
    _dobController.text = picked?.toString();
    _authManager?.updateUI(
        context: _context,
        authBloc: _authBloc,
        authState: _authState,
        updateUiDataModel: _authState?.updateUiDataModel);
  }
}
