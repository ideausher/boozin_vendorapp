// To parse this JSON data, do
//
//     final checkUserRequestModel = checkUserRequestModelFromJson(jsonString);

import 'dart:convert';

CheckUserRequestModel checkUserRequestModelFromJson(String str) => CheckUserRequestModel.fromJson(json.decode(str));

String checkUserRequestModelToJsonForPhone(CheckUserRequestModel data) => json.encode(data.toJsonForPhone());
String checkUserRequestModelToJsonForEmail(CheckUserRequestModel data) => json.encode(data.toJsonForEmail());

class CheckUserRequestModel {
  CheckUserRequestModel({
    this.phoneNumber,
    this.countryCode,
    this.email,
  });

  String phoneNumber;
  String countryCode;
  String email;

  factory CheckUserRequestModel.fromJson(Map<String, dynamic> json) => CheckUserRequestModel(
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    email: json["email"] == null ? null : json["email"],
  );

  Map<String, dynamic> toJsonForPhone() => {
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "country_code": countryCode == null ? null : countryCode,
  };
  Map<String, dynamic> toJsonForEmail() => {
    "email": email == null ? null : email,
  };
}
