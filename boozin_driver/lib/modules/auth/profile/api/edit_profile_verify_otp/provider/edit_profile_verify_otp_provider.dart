import 'package:flutter/material.dart';
import '../../../../../../modules/auth/profile/api/edit_profile_verify_otp/model/edit_profile_verify_otp_request_model.dart';

import '../../../../../common/app_config/app_config.dart';

class EditProfileVerifyOtpProvider {
  Future<dynamic> editProfileVerifyOtpApiCall(
      {BuildContext context,
      EditProfileVerifyOtpRequestModel
          editProfileVerifyOtpRequestModel}) async {
    var confirmOtp = "verify-email-or-phone";
    var data;

    if (editProfileVerifyOtpRequestModel?.email?.isEmpty == true) {
      data = {
        "otp": editProfileVerifyOtpRequestModel.otp,
        "phone_number": editProfileVerifyOtpRequestModel.phoneNumber,
        "country_code": editProfileVerifyOtpRequestModel.countryCode,
        "country_iso_code":editProfileVerifyOtpRequestModel.countryIsoCode,
        "type": "1",
        "action": "1"
      };
    } else {
      data = {
        "otp": editProfileVerifyOtpRequestModel.otp,
        "email": editProfileVerifyOtpRequestModel.email,
        "type": "2",
        "action": "1"
      };
    }
    var result = await AppConfig.of(context).baseApi.postRequest(
          confirmOtp,
          context,
          data: data,
        );

    return result;
  }
}
