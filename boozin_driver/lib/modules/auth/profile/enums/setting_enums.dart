enum ProfileItemEnum {
  MyEarnings,
  HelpAndSupport,
  MyDocuments,
  TermsAndPolicy,
  LogOut
}

extension ProfileItemExtension on ProfileItemEnum {
  int get value {
    switch (this) {
      case ProfileItemEnum.MyEarnings:
        return 0;
      case ProfileItemEnum.HelpAndSupport:
        return 1;
      case ProfileItemEnum.MyDocuments:
        return 2;
      case ProfileItemEnum.TermsAndPolicy:
        return 3;
      case ProfileItemEnum.LogOut:
        return 4;
      default:
        return null;
    }
  }
}
