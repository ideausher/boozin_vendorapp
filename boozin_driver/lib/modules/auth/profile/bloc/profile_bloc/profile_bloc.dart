import 'dart:math';

import 'package:flutter_country_picker/country.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:boozin_driver/modules/auth/api/sign_in/model/enter_details_request_model.dart';
import 'package:boozin_driver/modules/auth/api/sign_in/model/enter_details_response_model.dart';
import 'package:boozin_driver/modules/auth/api/sign_up/provider/sign_up_provider.dart';
import 'package:boozin_driver/modules/auth/profile/model/edit_profile_response_model.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import '../../../../../modules/auth/profile/api/edit_user_profile/model/edit_email_phone_response_model.dart';
import '../../../../../modules/auth/profile/api/edit_user_profile/repo/edit_profile_provider.dart';
import '../../../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../../../modules/auth/profile/bloc/profile_bloc/profile_event.dart';
import '../../../../../modules/auth/profile/bloc/profile_bloc/profile_state.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../../../modules/common/enum/enums.dart';
import '../../../../../modules/common/utils/shared_prefs_utils.dart';

class ProfileBloc extends BlocEventStateBase<ProfileEvent, ProfileState> {
  ProfileBloc({bool initializing = true}) : super(initialState: ProfileState.initiating());

  @override
  Stream<ProfileState> eventHandler(ProfileEvent event, ProfileState currentState) async* {
    // update message event
    if (event is UpdateEvent) {
      yield ProfileState.updateProfile(
          isLoading: false,
          isSendOtp: false,
          authResponseModel: event.authResponseModel,
          enableProfileEditing: event?.enableProfileEditing,
          enableDobEditing: event?.enableDobEditing,
          commonResponseModel: event?.commonResponseModel,
          message: "",
          status: ApiStatus.NoChange);
    }

    // for enable or disable editing
    if (event is EnableUserProfileEditing) {
      yield ProfileState.updateProfile(
          isLoading: false,
          isSendOtp: false,
          authResponseModel: event.authResponseModel,
          enableProfileEditing: event.enableProfileEditing,
          status: ApiStatus.NoChange);
    }

    // for enable or disable editing
    if (event is EditUserNameEvent) {
      var _message = '';
      bool _enableProfileEditing = event.enableProfileEditing;
      var _status;
      AuthResponseModel _authResponseModel = event?.authResponseModel;
      EnterDetailsResponseModel _enterDetailsResponseModel;
      List<Address> addresses = new List();

      yield ProfileState.updateProfile(
          isLoading: true,
          context: event?.context,
          isSendOtp: false,
          authResponseModel: event?.authResponseModel,
          enableProfileEditing: event?.enableProfileEditing,
          enableDobEditing: event?.enableDobEditing,
          status: ApiStatus.NoChange);

      //TODO uncomment code later
      // call api
      var result = await EditUserProfileProvider().editUserName(
          context: event.context,
          enterDetailsRequestModel: new EnterDetailsRequestModel(
              name: event?.userName,
              dateOfBirth: event?.dob,
              countryCode: event?.authResponseModel?.userData?.countryCode,
              countryIsoCode: event?.authResponseModel?.userData?.countryIsoCode ?? Country.ZA.isoCode));
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _enterDetailsResponseModel = EnterDetailsResponseModel.fromMap(result);
          _authResponseModel?.userData?.name = _enterDetailsResponseModel?.data?.name;
          _authResponseModel?.userData?.id = _enterDetailsResponseModel?.data?.id;
          _authResponseModel?.userData?.phoneNumber = _enterDetailsResponseModel?.data?.phoneNumber;
          _authResponseModel?.userData?.profilePicture = _enterDetailsResponseModel?.data?.profilePicture;
          _authResponseModel?.userData?.userType = _enterDetailsResponseModel?.data?.userType;
          _authResponseModel?.userData?.countryIsoCode = _enterDetailsResponseModel?.data?.countryIsoCode;
          _authResponseModel?.userData?.countryCode = _enterDetailsResponseModel?.data?.countryCode;
          _authResponseModel?.userData?.verified = _enterDetailsResponseModel?.data?.verified;
          _authResponseModel?.userData?.email = _enterDetailsResponseModel?.data?.email;
          _authResponseModel?.userData?.dateOfBirth=_enterDetailsResponseModel?.data?.dateOfBirth;
          if (_enterDetailsResponseModel?.data?.userAddress?.isNotEmpty == true) {
            for (int index = 0; index < _enterDetailsResponseModel?.data?.userAddress?.length; index++) {
              Address address = new Address();
              address?.addressId = _enterDetailsResponseModel?.data?.userAddress[index].addressId;
              address?.additionalInfo = (_enterDetailsResponseModel?.data?.userAddress[index].additionalInfo);
              address?.city = _enterDetailsResponseModel?.data?.userAddress[index].city;
              address?.country = _enterDetailsResponseModel?.data?.userAddress[index].country;
              address?.addressType = _enterDetailsResponseModel?.data?.userAddress[index].addressType;

              address?.latitude = _enterDetailsResponseModel?.data?.userAddress[index].latitude;
              address?.longitude = _enterDetailsResponseModel?.data?.userAddress[index].longitude;
              address?.primary = _enterDetailsResponseModel?.data?.userAddress[index].primary;
              address?.pincode = _enterDetailsResponseModel?.data?.userAddress[index].pincode;
              addresses.add(address);
            }
          }
          _authResponseModel?.userData?.address = addresses;

          /* _authResponseModel?.userData?.address =
              List.from(_enterDetailsResponseModel?.data?.userAddress);*/
          await SharedPrefUtils.sharedPrefUtilsInstance.saveObject(_authResponseModel, PrefsEnum.UserProfileData.value);
          _status = ApiStatus.Updated;
          _message = _enterDetailsResponseModel?.message;
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        // if result ull
        _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
        _authResponseModel = event.authResponseModel;
        _status = ApiStatus.NoChange;
      }

      yield ProfileState.updateProfile(
          isLoading: false,
          isSendOtp: false,
          authResponseModel: _authResponseModel,
          enableProfileEditing: _enableProfileEditing,
          enableDobEditing: event?.enableDobEditing,
          message: _message,
          status: _status);
    }

    if (event is EditUserImage) {
      var _message = '';
      AuthResponseModel _authResponseModel = event?.authResponseModel;
      EditProfileResponseModel _editProfileResponseModel;
      var _status;
      yield ProfileState.updateProfile(
          isLoading: true,
          isSendOtp: false,

          authResponseModel: event.authResponseModel,
          enableProfileEditing: event.enableProfileEditing,
          status: ApiStatus.NoChange);
      _authResponseModel = event.authResponseModel;

      //<*------API call-------*>
      var result = await EditUserProfileProvider().uploadProfileImage(
          context: event.context, file: event.userProfileImage);
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _editProfileResponseModel = EditProfileResponseModel.fromMap(result);
          _authResponseModel?.userData?.profilePicture =
              _editProfileResponseModel?.data?.image;
          await SharedPrefUtils.sharedPrefUtilsInstance
              .saveObject(_authResponseModel, PrefsEnum.UserProfileData.value);
          _message = _editProfileResponseModel?.message;
          _status = ApiStatus.Updated;
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
          _authResponseModel = event.authResponseModel;
          _status = ApiStatus.NoChange;
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
        _authResponseModel = event.authResponseModel;
        _status = ApiStatus.NoChange;
      }

      yield ProfileState.updateProfile(
          isLoading: false,
          isSendOtp: false,

          authResponseModel: _authResponseModel,
          enableProfileEditing: event.enableProfileEditing,
          context: event.context,
          message: _message,
          status: _status);
    }

    // used for the call edit phone and email api
    if (event is EditUserEmailPhoneEvent) {
      String _message = "";
      EditEmailPhoneResponseModel _editPhoneNumberResponseModel;
      CommonResponseModel commonResponseModel;
      var _status;
      AuthResponseModel authResponseModel = event?.authResponseModel;
      yield ProfileState.updateProfile(isSendOtp: false, isLoading: true, message: "", status: ApiStatus.NoChange);

      // call api
      var result = await EditUserProfileProvider()
          .checkUserExistance(context: event.context, checkUserRequestModel: event.checkUserRequestModel);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _status = ApiStatus.Updated;
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
          _message = commonResponseModel?.message;
        } else if (result[ApiStatusParams.Status.value] == ApiStatus.NotFound.value) {
          _status = ApiStatus.NoChange;
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
          _message = commonResponseModel?.message;
        }
        // failure case
        else {
          _status = ApiStatus.NoChange;
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _status = ApiStatus.NoChange;
        _message = AppLocalizations.of(event.context).common.error.somethingWentWrong;
      }

      yield ProfileState.updateProfile(
          isLoading: false,
          message: _message,
          isSendOtp: false,
          emailPhoneResponseModel: _editPhoneNumberResponseModel,
          status: _status,
          commonResponseModel: commonResponseModel,
          authResponseModel: authResponseModel);
    }

    if (event is UpdateUserCountryEvent) {
      yield ProfileState.updateProfile(
          isLoading: false,
          authResponseModel: event?.authResponseModel,
          selectedCountry: event?.selectedCountry,
          message: "",
          isSendOtp: false,
          enableProfileEditing: false,
          context: event?.context,
          status: ApiStatus.NoChange);
    }

    // event for send otp
    if (event is SendProfileOtpEvent) {
      String _message = ""; // message
      CommonResponseModel commonResponseModel = new CommonResponseModel();
      // update UI
      yield ProfileState.updateProfile(
          isLoading: true,
          message: "",
          isSendOtp: false,
          context: event.context,
          authResponseModel: event?.authResponseModel,
          commonResponseModel: commonResponseModel);

      // api calling
      var result = await SendOtpProvider().sendOtp(
        context: event.context,
        sendOtpRequestModel: event.sendOtpRequestModel,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      // update UI
      yield ProfileState.updateProfile(
        isLoading: false,
        message: _message,
        context: event.context,
        isSendOtp: true,
        authResponseModel: event?.authResponseModel,
        sendOtpRequestModel: event?.sendOtpRequestModel,
        commonResponseModel: commonResponseModel,
      );
    }
  }
}
