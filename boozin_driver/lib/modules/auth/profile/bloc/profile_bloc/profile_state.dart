import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:boozin_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:boozin_driver/modules/common/model/update_ui_data_model.dart';
import '../../../../../modules/auth/profile/api/edit_user_profile/model/edit_email_phone_response_model.dart';
import '../../../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../../../modules/common/enum/enums.dart';

class ProfileState extends BlocState {
  ProfileState(
      {this.isLoading: false,
        this.message,
        this.context,
        this.isSendOtp,
        this.authResponseModel,
        this.commonResponseModel,
        this.enableNameEditing = false,
        this.enableDobEditing = false,
        this.emailPhoneResponseModel,
        this.sendOtpRequestModel,
        this.selectedCountry,
        this.status = ApiStatus.NoChange})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final AuthResponseModel authResponseModel;
  final bool enableNameEditing,enableDobEditing;
  final ApiStatus status;
  final EditEmailPhoneResponseModel emailPhoneResponseModel;
  final CommonResponseModel commonResponseModel;
  final Country selectedCountry;
  final bool isSendOtp;
  final SendOtpRequestModel sendOtpRequestModel;

  // used for update profile api call
  factory ProfileState.updateProfile(
      {bool isLoading,
        String message,
        BuildContext context,
        AuthResponseModel authResponseModel,
        bool enableProfileEditing,
        Country selectedCountry,
        bool isSendOtp,
        EditEmailPhoneResponseModel emailPhoneResponseModel,
        SendOtpRequestModel sendOtpRequestModel,
        bool enableDobEditing,

        CommonResponseModel commonResponseModel,
        ApiStatus status}) {
    return ProfileState(
        isLoading: isLoading,
        message: message,
        context: context,
        authResponseModel: authResponseModel,
        enableNameEditing: enableProfileEditing,
        selectedCountry: selectedCountry,
        isSendOtp: isSendOtp,
        enableDobEditing: enableDobEditing,
        commonResponseModel: commonResponseModel,
        sendOtpRequestModel: sendOtpRequestModel,
        emailPhoneResponseModel: emailPhoneResponseModel,
        status: status);
  }

  factory ProfileState.initiating() {
    return ProfileState(
      isLoading: false,
    );
  }
}
