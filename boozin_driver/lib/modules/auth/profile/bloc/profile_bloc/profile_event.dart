import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:boozin_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:boozin_driver/modules/auth/profile/model/check_user_request_model.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:boozin_driver/modules/common/model/update_ui_data_model.dart';
import '../../../../../modules/auth/profile/api/edit_user_profile/model/edit_email_phone_request_model.dart';
import '../../../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

abstract class ProfileEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final AuthResponseModel authResponseModel;
  final bool enableProfileEditing, enableDobEditing;
  final String userName;
  final File userProfileImage;
  final EditEmailPhoneRequestModel editEmailPhoneRequest;
  final CheckUserRequestModel checkUserRequestModel;
  final UpdateUiDataModel signUpDataModel;
  final Country selectedCountry;
  final SendOtpRequestModel sendOtpRequestModel;
  final UpdateUiDataModel updateUiDataModel;
  final CommonResponseModel commonResponseModel;
  final bool isSendOtp;
  final String dob;

  ProfileEvent(
      {this.isLoading: false,
        this.context,
        this.authResponseModel,
        this.enableProfileEditing = false,
        this.userName,
        this.userProfileImage,
        this.signUpDataModel,
        this.editEmailPhoneRequest,
        this.checkUserRequestModel,
        this.commonResponseModel,
        this.selectedCountry,
        this.sendOtpRequestModel,
        this.updateUiDataModel,
        this.enableDobEditing: false,
        this.isSendOtp: false,
        this.dob});
}

class EnableUserProfileEditing extends ProfileEvent {
  EnableUserProfileEditing(
      {BuildContext context,
        AuthResponseModel authResponseModel,
        bool enableProfileEditing,
        bool enableDobEditing,
        bool isLoading,
        bool isSendOtp})
      : super(
      authResponseModel: authResponseModel,
      enableProfileEditing: enableProfileEditing,
      isLoading: isLoading,
      enableDobEditing: enableDobEditing,
      context: context,
      isSendOtp: isSendOtp);
}

//Event to edit user name
class UpdateEvent extends ProfileEvent {
  UpdateEvent(
      {bool isLoading,
        BuildContext context,
        AuthResponseModel authResponseModel,
        CommonResponseModel commonResponseModel,
        bool enableProfileEditing,
        bool enableDobEditing,
        bool isSendOtp})
      : super(
      isLoading: isLoading,
      context: context,
      authResponseModel: authResponseModel,
      enableProfileEditing: enableProfileEditing,
      enableDobEditing: enableDobEditing,
      commonResponseModel: commonResponseModel,
      isSendOtp: isSendOtp);
}

//Event to edit user name
class EditUserNameEvent extends ProfileEvent {
  EditUserNameEvent(
      {bool isLoading,
        BuildContext context,
        AuthResponseModel authResponseModel,
        String userName,
        String dob,
        bool enableProfileEditing,
        bool enableDobEditing,
        bool isSendOtp})
      : super(
      isLoading: isLoading,
      context: context,
      authResponseModel: authResponseModel,
      userName: userName,
      dob: dob,
      enableDobEditing: enableDobEditing,
      enableProfileEditing: enableProfileEditing,
      isSendOtp: isSendOtp);
}

//Event to edit user profile pic
class EditUserImage extends ProfileEvent {
  EditUserImage(
      {bool isLoading,
        BuildContext context,
        File userProfileImage,
        AuthResponseModel authResponseModel,
        bool enableProfileEditing,
        bool enableDobEditing,
        bool isSendOtp})
      : super(
      isLoading: isLoading,
      userProfileImage: userProfileImage,
      context: context,
      enableDobEditing: enableDobEditing,
      authResponseModel: authResponseModel,
      enableProfileEditing: enableProfileEditing,
      isSendOtp: isSendOtp);
}

//Event to edit user email or phone
class EditUserEmailPhoneEvent extends ProfileEvent {
  EditUserEmailPhoneEvent(
      {bool isLoading,
        BuildContext context,
        EditEmailPhoneRequestModel editEmailPhoneRequest,
        CheckUserRequestModel checkUserRequestModel,
        UpdateUiDataModel signUpDataModel,
        bool enableDobEditing,
        AuthResponseModel authResponseModel,
        bool isSendOtp})
      : super(
      isLoading: isLoading,
      editEmailPhoneRequest: editEmailPhoneRequest,
      context: context,
      signUpDataModel: signUpDataModel,
      enableDobEditing: enableDobEditing,
      checkUserRequestModel: checkUserRequestModel,
      authResponseModel: authResponseModel,
      isSendOtp: isSendOtp);
}

class UpdateUserCountryEvent extends ProfileEvent {
  UpdateUserCountryEvent(
      {bool isLoading,
        BuildContext context,
        AuthResponseModel authResponseModel,
        bool enableDobEditing,
        Country selectedCountry,
        bool isSendOtp})
      : super(
      isLoading: isLoading,
      context: context,
      authResponseModel: authResponseModel,
      enableDobEditing: enableDobEditing,
      selectedCountry: selectedCountry,
      isSendOtp: isSendOtp);
}

// for sign up
class SendProfileOtpEvent extends ProfileEvent {
  SendProfileOtpEvent(
      {BuildContext context,
        bool isLoading,
        bool isSendOtp,
        AuthResponseModel authResponseModel,
        bool enableDobEditing,
        SendOtpRequestModel sendOtpRequestModel,
        UpdateUiDataModel updateUiDataModel})
      : super(
      context: context,
      isLoading: isLoading,
      isSendOtp: isSendOtp,
      enableDobEditing: enableDobEditing,
      authResponseModel: authResponseModel,
      sendOtpRequestModel: sendOtpRequestModel,
      updateUiDataModel: updateUiDataModel);
}



