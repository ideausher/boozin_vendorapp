import 'package:flutter/cupertino.dart';
import 'package:boozin_driver/modules/auth/api/is_user_bloc/model/is_user_blocked_request_model.dart';
import 'package:boozin_driver/modules/auth/api/login_with_otp/model/login_with_otp_request_model.dart';
import 'package:boozin_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:boozin_driver/modules/common/model/update_ui_data_model.dart';
import '../../../modules/auth/auth_bloc/auth_bloc.dart';
import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../modules/auth/api/sign_in/model/enter_details_request_model.dart';
import '../../../modules/auth/api/verify_otp/model/verify_otp_request_model.dart';

abstract class AuthEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final EnterDetailsRequestModel enterDetailsRequestModel;
  final SendOtpRequestModel sendOtpRequestModel;
  final VerifyOtpRequestModel verifyOtpRequestModel;
  final AuthResponseModel authResponseModel;
  final AuthBloc authBloc;
  final UpdateUiDataModel updateUiDataModel;
  final CommonResponseModel commonResponseModel;
  final LoginWithOtpRequestModel loginWithOtpRequestModel;
  final UserBlocRequestModel isUserBlockedRequestModel;
  final bool isUserBlock, isResendOtp;

  AuthEvent(
      {this.isLoading: false,
      this.verifyOtpRequestModel,
      this.enterDetailsRequestModel,
      this.context,
      this.authResponseModel,
      this.sendOtpRequestModel,
      this.commonResponseModel,
      this.loginWithOtpRequestModel,
      this.isUserBlockedRequestModel,
      this.isUserBlock: false,
      this.isResendOtp: false,
      this.authBloc,
      this.updateUiDataModel});
}

//UpdateBlocData
class UpdateBlocEvent extends AuthEvent {
  UpdateBlocEvent(
      {bool isLoading,
      BuildContext context,
      EnterDetailsRequestModel enterDetailsRequestModel,
      VerifyOtpRequestModel verifyOtpRequestModel,
      AuthResponseModel authResponseModel,
      UpdateUiDataModel updateUiDataModel,
      bool isUserBlock,
      bool isResendOtp,
      CommonResponseModel commonResponseModel,
      AuthBloc authBloc})
      : super(
            isLoading: isLoading,
            context: context,
            isUserBlock: isUserBlock,
            isResendOtp: isResendOtp,
            enterDetailsRequestModel: enterDetailsRequestModel,
            verifyOtpRequestModel: verifyOtpRequestModel,
            authResponseModel: authResponseModel,
            updateUiDataModel: updateUiDataModel,
            authBloc: authBloc,
            commonResponseModel: commonResponseModel);
}

// for check authentication
class CheckAuthentication extends AuthEvent {
  CheckAuthentication({AuthBloc authBloc})
      : super(
          authBloc: authBloc,
        );
}

// for sign up
class SendOtpEvent extends AuthEvent {
  SendOtpEvent(
      {BuildContext context,
      bool isLoading,
      SendOtpRequestModel sendOtpRequestModel,
      bool isUserBlock,
      bool isResendOtp,
      AuthResponseModel authResponseModel,
      UpdateUiDataModel updateUiDataModel})
      : super(
            context: context,
            isLoading: isLoading,
            isUserBlock: isUserBlock,
            isResendOtp: isResendOtp,
            sendOtpRequestModel: sendOtpRequestModel,
            authResponseModel: authResponseModel,
            updateUiDataModel: updateUiDataModel);
}

// for sign in
class UpdateDetailsEvent extends AuthEvent {
  UpdateDetailsEvent({
    BuildContext context,
    UpdateUiDataModel updateUiDataModel,
    bool isLoading,
    bool isUserBlock,
    bool isResendOtp,
    AuthResponseModel authResponseModel,
    EnterDetailsRequestModel enterDetailsRequestModel,
  }) : super(
          isLoading: isLoading,
          context: context,
          isResendOtp: isResendOtp,
          updateUiDataModel: updateUiDataModel,
          isUserBlock: isUserBlock,
          authResponseModel: authResponseModel,
          enterDetailsRequestModel: enterDetailsRequestModel,
        );
}

// for verify otp
class VerifyOtpEvent extends AuthEvent {
  VerifyOtpEvent({
    BuildContext context,
    bool isLoading,
    bool isUserBlock,
    bool isResendOtp,
    UpdateUiDataModel updateUiDataModel,
    VerifyOtpRequestModel verifyOtpRequestModel,
  }) : super(
          context: context,
          isLoading: isLoading,
          isUserBlock: isUserBlock,
          isResendOtp: isResendOtp,
          updateUiDataModel: updateUiDataModel,
          verifyOtpRequestModel: verifyOtpRequestModel,
        );
}

// for resend otp
class ResendOtpEvent extends AuthEvent {
  ResendOtpEvent({
    BuildContext context,
    bool isLoading,
    bool isUserBlock,
    bool isResendOtp,
    UpdateUiDataModel updateUiDataModel,
  }) : super(
          isLoading: isLoading,
          context: context,
          isUserBlock: isUserBlock,
          isResendOtp: isResendOtp,
          updateUiDataModel: updateUiDataModel,
        );
}

// for login with otp request model
class LoginWithOtpEvent extends AuthEvent {
  LoginWithOtpEvent(
      {BuildContext context,
      bool isLoading,
      bool isUserBlock,
      bool isResendOtp,
      UpdateUiDataModel updateUiDataModel,
      LoginWithOtpRequestModel loginWithOtpRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            isUserBlock: isUserBlock,
            isResendOtp: isResendOtp,
            updateUiDataModel: updateUiDataModel,
            loginWithOtpRequestModel: loginWithOtpRequestModel);
}

// for sign out
class SignOutEvent extends AuthEvent {
  SignOutEvent({
    BuildContext context,
    bool isLoading,
    bool isUserBlock,
    bool isResendOtp,
    AuthResponseModel authResponseModel,
  }) : super(
          isLoading: isLoading,
          context: context,
          isUserBlock: isUserBlock,
          isResendOtp: isResendOtp,
          authResponseModel: authResponseModel,
        );
}

// update UI event
class UpdateUIEvent extends AuthEvent {
  UpdateUIEvent({
    BuildContext context,
    bool isLoading,
    bool isUserBlock,
    bool isResendOtp,
    AuthResponseModel authResponseModel,
    UpdateUiDataModel updateUiDataModel,
  }) : super(
            isLoading: isLoading,
            context: context,
            isUserBlock: isUserBlock,
            isResendOtp: isResendOtp,
            authResponseModel: authResponseModel,
            updateUiDataModel: updateUiDataModel);
}

// change number event
class ChangeNumberEvent extends AuthEvent {
  ChangeNumberEvent({
    BuildContext context,
    bool isLoading,
    bool isUserBlock,
    bool isResendOtp,
    UpdateUiDataModel updateUiDataModel,
    CommonResponseModel commonResponseModel,
  }) : super(
            isLoading: isLoading,
            isUserBlock: isUserBlock,
            isResendOtp: isResendOtp,
            context: context,
            updateUiDataModel: updateUiDataModel,
            commonResponseModel: commonResponseModel);
}

// check is user blocked or not
class IsUserBlocEvent extends AuthEvent {
  IsUserBlocEvent({
    BuildContext context,
    bool isLoading,
    bool isUserBlock,
    bool isResendOtp,
    UpdateUiDataModel updateUiDataModel,
    UserBlocRequestModel isUserBlockedRequestModel,
    CommonResponseModel commonResponseModel,
  }) : super(
            isLoading: isLoading,
            context: context,
            isResendOtp: isResendOtp,
            updateUiDataModel: updateUiDataModel,
            isUserBlock: isUserBlock,
            isUserBlockedRequestModel: isUserBlockedRequestModel,
            commonResponseModel: commonResponseModel);
}

// get user data event
class GetProfileEvent extends AuthEvent {
  GetProfileEvent({
    BuildContext context,
    bool isLoading,
    bool isUserBlock,
    bool isResendOtp,
    UpdateUiDataModel updateUiDataModel,
    AuthResponseModel authResponseModel,
    UserBlocRequestModel isUserBlockedRequestModel,
    CommonResponseModel commonResponseModel,
  }) : super(
            isLoading: isLoading,
            context: context,
            updateUiDataModel: updateUiDataModel,
            isUserBlock: isUserBlock,
            isResendOtp: isResendOtp,
            isUserBlockedRequestModel: isUserBlockedRequestModel,
            commonResponseModel: commonResponseModel,
            authResponseModel: authResponseModel);
}

//event to check update
class CheckUpdateEvent extends AuthEvent {
  CheckUpdateEvent({
    BuildContext context,
    bool isLoading,
    UpdateUiDataModel updateUiDataModel,
  }) : super(
          isLoading: isLoading,
          context: context,
          updateUiDataModel: updateUiDataModel,
        );
}
