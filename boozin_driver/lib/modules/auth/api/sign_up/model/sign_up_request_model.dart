// To parse this JSON data, do
//
//     final signUpRequestModel = signUpRequestModelFromJson(jsonString);

import 'dart:convert';

SignUpRequestModel signUpRequestModelFromJson(String str) => SignUpRequestModel.fromMap(json.decode(str));

String signUpRequestModelToJson(SignUpRequestModel data) => json.encode(data.toMap());

class SignUpRequestModel {
  String name;
  String email;
  String phone;
  String countryCode;
  int role;
  int loginType;

  SignUpRequestModel({
    this.name,
    this.email,
    this.phone,
    this.countryCode,
    this.role,
    this.loginType,
  });

  factory SignUpRequestModel.fromMap(Map<String, dynamic> json) => SignUpRequestModel(
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    phone: json["phone"] == null ? null : json["phone"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    role: json["role"] == null ? null : json["role"],
    loginType: json["login_type"] == null ? null : json["login_type"],
  );

  Map<String, dynamic> toMap() => {
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "phone": phone == null ? null : phone,
    "country_code": countryCode == null ? null : countryCode,
    "role": role == null ? null : role,
    "login_type": loginType == null ? null : loginType,
  };
}
