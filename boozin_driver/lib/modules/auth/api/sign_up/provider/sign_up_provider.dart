import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import '../../../../common/app_config/app_config.dart';

class SendOtpProvider {
  //method to send otp on the number user entered on the screen
  Future<dynamic> sendOtp(
      {BuildContext context, SendOtpRequestModel sendOtpRequestModel}) async {
    var sendOtp = "send-otp";
    var result;
    if (sendOtpRequestModel?.type == "1") {
      result = await AppConfig.of(context).baseApi.postRequest(
        sendOtp,
        context,
        data: {
          "type": sendOtpRequestModel?.type,
          "phone_number": sendOtpRequestModel?.phoneNumber,
          "country_code": sendOtpRequestModel?.countryCode,
          "country_iso_code": sendOtpRequestModel?.countryIsoCode
        },
      );
    } else {
      result = await AppConfig.of(context).baseApi.postRequest(
        sendOtp,
        context,
        data: {
          "email": sendOtpRequestModel?.email,
          "type": sendOtpRequestModel?.type
        },
      );
    }

    return result;
  }
}
