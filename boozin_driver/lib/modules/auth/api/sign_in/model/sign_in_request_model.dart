// To parse this JSON data, do
//
//     final signInRequestModel = signInRequestModelFromJson(jsonString);

import 'dart:convert';

import 'package:flutter_country_picker/country.dart';

SignInRequestModel signInRequestModelFromJson(String str) => SignInRequestModel.fromMap(json.decode(str));

String signInRequestModelToJson(SignInRequestModel data) => json.encode(data.toMap());

class SignInRequestModel {
  Country country;
  String name;

  SignInRequestModel({
    this.country,
    this.name
  });

  factory SignInRequestModel.fromMap(Map<String, dynamic> json) => SignInRequestModel(
    country: json["country"] == null ? null : json["country"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toMap() => {
    "country": country == null ? null : country,
    "name": name == null ? null : name,
  };
}
