import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/api/sign_in/model/enter_details_request_model.dart';
import '../../../../common/app_config/app_config.dart';

class EnterDetailsProvider {
  Future<dynamic> updateProfileApiCall({BuildContext context,EnterDetailsRequestModel enterDetailsRequestModel}) async {
    var path = "update-profile";

    var result = await AppConfig.of(context).baseApi.postRequest(
      path,
      context,
      data: enterDetailsRequestModelToJson(enterDetailsRequestModel),
    );
    return result;
  }
}
