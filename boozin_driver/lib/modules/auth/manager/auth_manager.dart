import 'package:boozin_driver/modules/bank_details/bank_routes.dart';
import 'package:boozin_driver/modules/bank_details/create_account/api/provider/check_account_linked_provider.dart';
import 'package:boozin_driver/modules/common/utils/shared_prefs_utils.dart';
import 'package:boozin_driver/modules/force_update/manager/force_update_action_manager/force_update_action_manager.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:boozin_driver/modules/auth/api/is_user_bloc/model/is_user_blocked_request_model.dart';
import 'package:boozin_driver/modules/auth/api/login_with_otp/model/login_with_otp_request_model.dart';
import 'package:boozin_driver/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:boozin_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/model/common_response_model.dart';
import 'package:boozin_driver/modules/common/utils/fetch_prefs_utils.dart';
import 'package:boozin_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:boozin_driver/modules/vehicle_documents/registration_routes.dart';
import 'package:boozin_driver/modules/common/model/update_ui_data_model.dart';
import 'package:boozin_driver/modules/common/model/user_current_location_model.dart';
import '../../../modules/auth/enums/auth_enums.dart';
import '../../../modules/common/utils/common_utils.dart';
import '../../../modules/auth/api/verify_otp/model/verify_otp_request_model.dart';
import '../../../modules/auth/api/sign_in/model/enter_details_request_model.dart';
import '../../../modules/auth/auth_bloc/auth_bloc.dart';
import '../../../modules/auth/auth_bloc/auth_event.dart';
import '../../../modules/auth/auth_bloc/auth_state.dart';
import '../../../modules/auth/auth_routes.dart';
import '../../../modules/auth/validator/auth_validator.dart';
import '../../../modules/common/utils/dialog_snackbar_utils.dart';
import '../../../modules/common/utils/navigator_utils.dart';
import '../../../modules/common/utils/network_connectivity_utils.dart';
import 'package:flutter/material.dart';
import '../../../localizations.dart';
import '../../../routes.dart';

class AuthManager {
  // for initialization screen authentication check
  actionOnIntializationScreenStateChange(
      {AuthState authState, BuildContext context}) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        if (authState?.isLoading == false) {
          //if user is not logged in and auth model is null
          if (authState?.authResponseModel?.userData == null) {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorClearStack(context, AuthRoutes.SPLASH_SCREEN_ROOT);
          } else {
            if (authState?.authResponseModel?.userData?.name?.isNotEmpty ==
                true) {
              //here checking if current of location exist in the preference.
              CurrentLocation currentLocation = await FetchPrefsUtils
                  .fetchPrefsUtilsInstance
                  .getCurrentLocationModel();
              if (currentLocation != null) {
                DriverRegistrationRequestModel driverRegistrationRequestModel =
                    await FetchPrefsUtils.fetchPrefsUtilsInstance
                        .getVehicleDataModel();

                //here i am checking if document verification status gets approved navigate to home screen
                if (driverRegistrationRequestModel?.status ==
                    DocumentStatus?.Approved?.value) {
                  checkAccountApprovedOrNot(context: context);
                  // NavigatorUtils.navigatorUtilsInstance
                  //     .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
                }
                //if not approved navigate to complete profile screen in case of pending,reject and new user
                else {
                  NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                      context, RegistrationRoutes.COMPLETE_PROFILE,
                      dataToBeSend:
                          getStatusValue(driverRegistrationRequestModel));
                }
              }
              //else navigate to enable location screen to get the current location of the user
              else {
                NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                    context, AuthRoutes.ENABLE_LOCATION_ROOT,
                    dataToBeSend: currentLocation);
              }
            }

            //if otp is verified but the name is empty firs fill the name of the user then move to next screen
            else {
              NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                context,
                AuthRoutes.ENTER_DETAILS_SCREEN,
                /*dataToBeSend: authState?.updateUiDataModel*/
              );
            }
          }
        }
      },
    );
  }

  checkAccountApprovedOrNot({BuildContext context}) async {
    var result =
        await CheckAccountLinkedOrNotProvider().checkAccountLinkedOrNotProvider(
      context: context,
    );

    CommonResponseModel commonResponseModel;
    if (result != null) {
      // check result status
      if (result[ApiStatusParams.Status.value] != null &&
          result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, AuthRoutes.HOME_SCREEN_ROOT);
      }
      // failure case
      else {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
      }
    } else {
      NavigatorUtils.navigatorUtilsInstance
          .navigatorClearStack(context, BankRoutes.BANK_DETAILS_PAGE);
    }
  }

  actionOnIntroScreenStateChange({BuildContext context}) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(Duration(seconds: 2), () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
      });
    });
  }

  // for splash screen state changed
  actionOnSplashScreenStateChange({BuildContext context}) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(Duration(seconds: 4), () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, AuthRoutes.INTRO_ROOT);
      });
    });
  }

  // for sign in

  enterDetailsUpdate({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    String name,
    String dateOfBirth,
    UpdateUiDataModel updateUiDataModel,
    AuthResponseModel authResponseModel,
  }) {
    // check name field is empty or not
    String result = AuthValidator.authValidatorInstance.enterDetailsScreen(
        context: context, name: name, dateOfBirth: dateOfBirth);
    // if its not empty
    if (result.isEmpty) {
      // check internet connection
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
          // if its available
          authBloc.emitEvent(UpdateDetailsEvent(
            isLoading: true,
            updateUiDataModel: updateUiDataModel,
            context: context,
            authResponseModel: authResponseModel,
            enterDetailsRequestModel: EnterDetailsRequestModel(
                name: name,
                dateOfBirth: dateOfBirth,
                title: (authResponseModel?.userData?.title?.isNotEmpty == true)
                    ? authResponseModel?.userData?.title
                    : "",
                countryCode: authResponseModel?.userData?.countryCode,
                countryIsoCode: updateUiDataModel?.selectedCountry?.isoCode ??
                    Country.ZA?.isoCode),
          ));
        }
      });
    } else {
      // show error in fields
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  // for sign in state change
  actionOnEnterDetailsStateChanged(
      {BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      TextEditingController dobController,
      AuthState authState,
      Country selectedCountry,
      UpdateUiDataModel updateUiDataModel}) {
    if (authState.isLoading == false) {
      updateUiDataModel = authState?.updateUiDataModel ?? updateUiDataModel;
      WidgetsBinding.instance.addPostFrameCallback(
        (_) {
          if (authState?.authResponseModel == null) {
            // check message not empty so that it will loop again and again
            if (authState?.message?.isNotEmpty == true) {
              NavigatorUtils.navigatorUtilsInstance
                  .navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
              // update bloc to update message
              authBloc.emitEvent(UpdateBlocEvent(
                  isLoading: false,
                  context: context,
                  authResponseModel: authState.authResponseModel));
            }
          } else if (authState?.authResponseModel?.userData?.name?.isNotEmpty ==
              true) {
            NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                context, AuthRoutes.LOGIN_SUCCESSFUL_ROOT,
                dataToBeSend: ComingFrom.EnterDetailScreen.value);
          } else if (authState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: authState?.message);
            print("login==> ${authState?.message}");
            // update bloc to update message
            authBloc.emitEvent(UpdateBlocEvent(
                isLoading: false,
                context: context,
                updateUiDataModel: authState?.updateUiDataModel,
                authResponseModel: authState.authResponseModel));
          }
        },
      );
    }
  }

  // for send otp call
  sendOtpCall(
      {BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      String phoneNumber,
      Country selectedCountry,
      UpdateUiDataModel updateUiDataModel}) {
    // validate data
    String result = AuthValidator.authValidatorInstance.validateSendOtpScreen(
        phoneNumber: phoneNumber,
        context: context,
        country: updateUiDataModel?.selectedCountry ?? Country.ZA,
        isConditionAgreed: updateUiDataModel?.isTermsAccepted ?? false);
    // if no error
    if (result.isEmpty) {
      // connection check
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          // hide keyboard
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
          authBloc?.emitEvent(IsUserBlocEvent(
            isLoading: true,
            context: context,
            isUserBlockedRequestModel: UserBlocRequestModel(
                type: 1,
                phoneNumber: AuthValidator.authValidatorInstance
                    .trimValue(value: phoneNumber),
                countryCode: "+${selectedCountry?.dialingCode}"),
            updateUiDataModel: updateUiDataModel,
          ));
        }
      });
    } else {
      // if error
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  // for send otp state change
  actionSendOtpStateChange(
      {BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      TextEditingController phoneController,
      AuthState authState,
      UpdateUiDataModel updateUiDataModel}) {
    if (authState.isLoading == false) {
      updateUiDataModel = authState?.updateUiDataModel ?? updateUiDataModel;
      //phoneController.text = updateUiDataModel?.phoneNumber ?? "";
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        //case 1: when calling is block api
        if (authState?.isUserBlock == true) {
          //when calling is bloc api when number is entered first check whether number doest exist on any of the app
          if (authState?.commonResponseModel != null &&
              authState?.commonResponseModel?.status ==
                  ApiStatus.NotFound.value) {
            //call send otp api if number does not exist on delivery app
            sendOtp(context, scaffoldState, authBloc, phoneController.text,
                updateUiDataModel);
          }
          //this case will be called when number is already exist in db
          else if (authState?.isUserBlockedResponseModel != null &&
              authState?.isUserBlockedResponseModel?.statusCode ==
                  ApiStatus.Success.value) {
            var userType = await SharedPrefUtils.sharedPrefUtilsInstance
                .getInt(PrefsEnum.UserType.value);

            if (userType ==
                authState?.isUserBlockedResponseModel?.data?.userType) {
              if (authState?.isUserBlockedResponseModel?.data?.isBlocked == 0) {
                sendOtp(context, scaffoldState, authBloc, phoneController.text,
                    updateUiDataModel);
              } else {
                DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                    context: context,
                    scaffoldState: scaffoldState,
                    message:
                        AppLocalizations.of(context).common.error.userBlocked);
              }
            } else {
              String message = "";
              if (authState?.isUserBlockedResponseModel?.data?.userType ==
                  UserRoles?.OnDemandDriver?.value) {
                message = AppLocalizations.of(context)
                    .common
                    .error
                    .numberAlreadyRegisteredAsOnDemandDriver;
              } else if (authState
                      ?.isUserBlockedResponseModel?.data?.userType ==
                  UserRoles?.Customer?.value) {
                message = AppLocalizations.of(context)
                    .common
                    .error
                    .numberAlreadyRegisteredAsCustomer;
              } else if (authState
                      ?.isUserBlockedResponseModel?.data?.userType ==
                  UserRoles?.Vendor?.value) {
                message = AppLocalizations.of(context)
                    .common
                    .error
                    .numberAlreadyRegisteredAsVendor;
                ;
              } else {
                message = AppLocalizations.of(context)
                    .common
                    .error
                    .numberAlreadyRegisteredAsCommercialVehicleDriver;
              }

              DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                  context: context,
                  scaffoldState: scaffoldState,
                  message: message);
            }

            //check if user type is coming 2 i.e on demand driver for 3 i.e commericial driver
            // if (authState?.isUserBlockedResponseModel?.data?.userType ==
            //         UserRoles?.OnDemandDriver?.value ||
            //     authState?.isUserBlockedResponseModel?.data?.userType ==
            //         UserRoles?.CommercialVehicleDriver?.value) {
            //   if (authState?.isUserBlockedResponseModel?.data?.isBlocked == 0) {
            //     sendOtp(context, scaffoldState, authBloc, phoneController.text,
            //         updateUiDataModel);
            //   } else {
            //     DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            //         context: context,
            //         scaffoldState: scaffoldState,
            //         message:
            //             AppLocalizations.of(context).common.error.userBlocked);
            //   }
            // }
            // //if user type value is coming 2 then show a message
            // else if (authState?.isUserBlockedResponseModel?.data?.userType ==
            //     1) {
            //   DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            //       context: context,
            //       scaffoldState: scaffoldState,
            //       message: AppLocalizations.of(context)
            //           .common
            //           .error
            //           .numberAlreadyExist);
            // }
          } else {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: authState?.message);
          }
        }
        //Case to check force update required or not
        else if (authState?.forceUpdateResponseModel != null &&
            authState?.forceUpdateResponseModel?.data != null &&
            authState?.needToUpdate == true) {
          ForceUpdateActionManager _forceUpdateActionManager =
              ForceUpdateActionManager();
          _forceUpdateActionManager.showForceUpdateDialog(
            context: context,
            forcefullyUpdate:
                (authState?.forceUpdateResponseModel?.data?.forceUpdate == 1)
                    ? true
                    : false,
            message: authState?.forceUpdateResponseModel?.data?.message,
          );
        }
        //Case 2:this else is for verifying send otp api call conditions
        else if (authState?.commonResponseModel != null &&
            authState?.commonResponseModel?.status == ApiStatus.Success.value) {
          authBloc.emitEvent(UpdateBlocEvent(
              isLoading: false,
              context: context,
              commonResponseModel: new CommonResponseModel(),
              updateUiDataModel: authState?.updateUiDataModel,
              authResponseModel: authState.authResponseModel));
          NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
            context,
            AuthRoutes.VERIFY_OTP_SCREEN,
          );
        } else if (authState?.message?.toString()?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              scaffoldState: scaffoldState,
              message: authState?.message);
          print("send otp==> ${authState?.message}");
        }
      });
    }
  }

  // for verify otp
  verifyOtpCall({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    String otp,
    AuthState authState,
  }) {
    // check otp
    String result = AuthValidator.authValidatorInstance.validateVerifyOtpScreen(
      value: otp,
      context: context,
    );
    // if no error
    if (result.isEmpty) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          // hide keyboard
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
          authBloc.emitEvent(
            VerifyOtpEvent(
              isLoading: true,
              context: context,
              updateUiDataModel: authState?.updateUiDataModel,
              verifyOtpRequestModel: VerifyOtpRequestModel(
                  otp:
                      AuthValidator.authValidatorInstance.trimValue(value: otp),
                  phoneNumber: authState?.updateUiDataModel?.phoneNumber,
                  action: "0",
                  countryCode:
                      "+${authState?.updateUiDataModel?.selectedCountry?.dialingCode}",
                  type: LoginWithPhoneOrEmail.phone.value.toString()),
            ),
          );
        }
      });
    } else {
      // show error
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  // for state  update
  actionOnStateUpdate({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthState authState,
    AuthBloc authBloc,
  }) {
    if (authState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (authState?.authResponseModel == null) {
          // check message not empty so that it will loop again and again
          if (authState?.message?.isNotEmpty == true) {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
            // update bloc to update message
            authBloc.emitEvent(UpdateBlocEvent(
                isLoading: false,
                context: context,
                authResponseModel: authState.authResponseModel));
          }
        } else if (authState?.message?.toString()?.trim()?.isNotEmpty == true) {
          // update bloc to update message
          authBloc.emitEvent(UpdateBlocEvent(
              isLoading: false,
              context: context,
              authResponseModel: authState.authResponseModel));
        }
      });
    }
  }

  // for state change in verify otp
  // for state change in verify otp
  actionOnVerifyOtpStateChanged({
    BuildContext context,
    ScaffoldState scaffoldState,
    TextEditingController verifyOtpController,
    TextEditingController phoneController,
    AuthState authState,
    AuthBloc authBloc,
  }) {
    if (authState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (authState?.commonResponseModel != null &&
            authState?.commonResponseModel?.status == ApiStatus.Success.value) {
          //if user call resend otp api just show the message here
          if (authState?.isResend == true) {
            verifyOtpController?.clear();
            phoneController.text =
                '(+${authState?.updateUiDataModel?.selectedCountry?.dialingCode ?? ""}) ${authState?.updateUiDataModel?.phoneNumber ?? ""}';
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              scaffoldState: scaffoldState,
              message: authState?.commonResponseModel?.message,
            );
          }
          //this else will be called when the otp is verified successfully then call login api.not in the case of resend otp
          else {
            //if user otp is verified successfully call login with otp api
            callLoginWithOtp(authBloc, context, authState?.updateUiDataModel);
          }
        } else if (authState?.authResponseModel != null) {
          if (authState?.message?.isNotEmpty == true) {
            // update bloc to update message
            authBloc.emitEvent(UpdateBlocEvent(
                isLoading: false,
                context: context,
                commonResponseModel: new CommonResponseModel(),
                authResponseModel: authState.authResponseModel));
          }

          //here check a condition if username is empty then navigate user to
          // sign in screen and update user name there.
          if (authState?.authResponseModel?.userData?.name?.isEmpty == true) {
            print(
                'the name is ${authState?.authResponseModel?.userData?.name}');
            NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                context, AuthRoutes.ENTER_DETAILS_SCREEN,
                dataToBeSend: authState?.updateUiDataModel);
          }
          //else if name is present then navigate user to login successful screen
          else {
            NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                context, AuthRoutes.LOGIN_SUCCESSFUL_ROOT,
                dataToBeSend: ComingFrom.VerifyOtpScreen.value);
          }
        } else if (authState?.commonResponseModel?.status ==
            ApiStatus.Failure.value) {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
        } else if (authState?.message?.toString()?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            context: context,
            scaffoldState: scaffoldState,
            message: authState?.message,
          );
          // update bloc to update message
          authBloc.emitEvent(UpdateBlocEvent(
              isLoading: false,
              context: context,
              updateUiDataModel: authState?.updateUiDataModel,
              authResponseModel: authState.authResponseModel));
        }
      });
    }
  }

  // for resend otp
  resendOtpCall({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    AuthState authState,
  }) {
    // check internet connection
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);

        // bloc update
        authBloc.emitEvent(
          ResendOtpEvent(
              isLoading: true,
              context: context,
              updateUiDataModel: authState?.updateUiDataModel),
        );
      }
    });
  }

  // for sign out
  signOutCall({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    AuthState authState,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        authBloc.emitEvent(
          SignOutEvent(
            isLoading: true,
            context: context,
            authResponseModel: authState?.authResponseModel,
          ),
        );
      }
    });
  }

  //method to call change number event
  changeNumberEventCall(
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    AuthState authState,
  ) {
    authBloc?.emitEvent(ChangeNumberEvent(
        isLoading: true,
        context: context,
        commonResponseModel: authState?.commonResponseModel,
        updateUiDataModel: authState?.updateUiDataModel));
  }

  // open logout Dialog
  signOutDialog({
    @required BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    AuthState authState,
  }) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
        context: context,
        title: AppLocalizations.of(context).profile.text.logout,
        negativeButton: AppLocalizations.of(context).common.text.no,
        positiveButton: AppLocalizations.of(context).common.text.yes,
        subTitle: AppLocalizations.of(context).common.text.wantToLogOut,
        onNegativeButtonTab: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        },
        onPositiveButtonTab: () async {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          signOutCall(
              context: context,
              authBloc: authBloc,
              scaffoldState: scaffoldState,
              authState: authState);
        });
  }

  //method to update ui
  updateUI(
      {BuildContext context,
      AuthState authState,
      UpdateUiDataModel updateUiDataModel,
      AuthResponseModel authResponseModel,
      AuthBloc authBloc}) {
    authBloc?.emitEvent(UpdateUIEvent(
      context: context,
      authResponseModel: authState?.authResponseModel,
      updateUiDataModel: updateUiDataModel,
      isLoading: false,
    ));
  }

  //call login with otp api event
  Future<void> callLoginWithOtp(AuthBloc authBloc, BuildContext context,
      UpdateUiDataModel updateUiDataModel) async {
    var userType = await SharedPrefUtils.sharedPrefUtilsInstance
        .getInt(PrefsEnum.UserType.value);

    authBloc?.emitEvent(LoginWithOtpEvent(
        context: context,
        isLoading: true,
        updateUiDataModel: updateUiDataModel,
        loginWithOtpRequestModel: new LoginWithOtpRequestModel(
            type: "3",
            countryIsoCode: updateUiDataModel?.selectedCountry?.isoCode,
            phoneNumber: updateUiDataModel?.phoneNumber,
            countryCode: "+${updateUiDataModel?.selectedCountry?.dialingCode}",
            token: "1",
            registration: 1,
            userType: userType)));
  }

  void sendOtp(
      BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      String phoneNumber,
      UpdateUiDataModel updateUiDataModel) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        authBloc.emitEvent(SendOtpEvent(
          isLoading: true,
          context: context,
          updateUiDataModel: updateUiDataModel,
          sendOtpRequestModel: SendOtpRequestModel(
              type: "1",
              countryIsoCode: updateUiDataModel?.selectedCountry?.isoCode,
              phoneNumber: AuthValidator.authValidatorInstance
                  .trimValue(value: phoneNumber),
              countryCode:
                  "+${updateUiDataModel?.selectedCountry?.dialingCode}"),
        ));
      }
    });
  }

  // //to get title in the form of string
  // getTitle(int value) {
  //   switch (value) {
  //     case 0:
  //       return "Mr.";
  //       break;
  //     case 1:
  //       return "Mrs.";
  //       break;
  //     case 2:
  //       return "Ms.";
  //       break;
  //   }
  // }

  getStatusValue(
      DriverRegistrationRequestModel driverRegistrationRequestModel) {
    if (driverRegistrationRequestModel?.status ==
        DocumentStatus?.Pending?.value)
      return INSURANCE;
    else if (driverRegistrationRequestModel?.status ==
        DocumentStatus?.Rejected?.value)
      return HOME;
    else
      return HOME;
  }

  void actionOnInit(
      {BuildContext context,
      AuthBloc authBloc,
      UpdateUiDataModel updateUiDataModel}) {
    // connection check
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: false)
        .then((onValue) {
      if (onValue) {
        authBloc?.emitEvent(CheckUpdateEvent(
          context: context,
          isLoading: false,
        ));
      }
    });
  }
}
