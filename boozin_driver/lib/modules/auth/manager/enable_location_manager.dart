import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:boozin_driver/localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:boozin_driver/modules/auth/auth_routes.dart';
import 'package:boozin_driver/modules/auth/screens/enable_location/location_bloc/enable_location_bloc.dart';
import 'package:boozin_driver/modules/auth/screens/enable_location/location_bloc/enable_location_event.dart';
import 'package:boozin_driver/modules/auth/screens/enable_location/location_bloc/enable_location_state.dart';
import 'package:boozin_driver/modules/common/enum/enums.dart';
import 'package:boozin_driver/modules/common/utils/common_utils.dart';
import 'package:boozin_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:boozin_driver/modules/common/utils/navigator_utils.dart';
import 'package:boozin_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:boozin_driver/modules/common/utils/shared_prefs_utils.dart';
import 'package:boozin_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:boozin_driver/modules/vehicle_documents/registration_routes.dart';
import 'package:boozin_driver/modules/common/model/user_current_location_model.dart';
import 'package:boozin_driver/routes.dart';

class EnableLocationManager {
  BuildContext context;
  EnableCurrentLocationBloc enableCurrentLocationBloc;
  ScaffoldState scaffoldState;

  EnableLocationManager(
      {this.context, this.enableCurrentLocationBloc, this.scaffoldState});

  //action on products list state change
  actionOnInitializationScreenStateChange(
      {EnableCurrentLocationState enableCurrentLocationState,
      ScaffoldState scaffoldState}) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        if (enableCurrentLocationState?.isLoading == false) {
          if (enableCurrentLocationState?.message
                  ?.toString()
                  ?.trim()
                  ?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
                context: context,
                title: AppLocalizations.of(context).common.text.permissionAlert,
                negativeButton: AppLocalizations.of(context).common.text.ok,
                dismissOnHardwareButtonClick: false,
                positiveButton:
                    AppLocalizations.of(context).common.text.setting,
                subTitle: AppLocalizations.of(context)
                    .common
                    .text
                    .permissionAlertMessage,
                onNegativeButtonTab: () {
                  NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(context);
                },
                onPositiveButtonTab: () async {
                  NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(context);
                  if (Platform.isIOS) {
                    AppSettings.openLocationSettings();
                  } else {
                    AppSettings.openAppSettings();
                  }
                });

            print("location update==> ${enableCurrentLocationState?.message}");
          } else {
            if (enableCurrentLocationState?.currentLocation != null) {
              await SharedPrefUtils.sharedPrefUtilsInstance.saveLocationObject(
                  enableCurrentLocationState?.currentLocation,
                  PrefsEnum.UserLocationData.value);
              NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                  context, RegistrationRoutes.COMPLETE_PROFILE,
                  dataToBeSend: HOME);
            }
          }
        }
      },
    );
  }

  //call enable location emit event
  callEnableLocationEvent() {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        // enable location emit event
        enableCurrentLocationBloc?.emitEvent(GetCurrentLocationEvent(
            isLoading: true,
            context: context,
            currentLocation: new CurrentLocation()));
      }
    });
  }
}
