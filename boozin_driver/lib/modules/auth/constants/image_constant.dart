const String SIGN_UP_LOGO = 'lib/modules/auth/images/sign_up_logo.png';
const String LIMIT_LESS_LOGO = 'lib/modules/auth/images/limitless.svg';
const String LIMIT_LESS_TITLE = 'lib/modules/auth/images/limitless_title.svg';
const String LOGIN_LOGO = 'lib/modules/auth/images/login_logo.png';
const String VERIFY_OTP_LOGO = 'lib/modules/auth/images/verify_otp_logo.png';
const String LOGIN_SUCCESSFUL_LOGO =
    'lib/modules/auth/images/login_successful.png';
const String STRIPE_LOGO = 'lib/modules/auth/images/stripe.png';
const String ENABLE_LOCATION_LOGO =
    'lib/modules/auth/images/enable_location.png';
const String EMAIL_ICON = 'lib/modules/auth/images/email.png';
const String DIVIDER_ICON = 'lib/modules/auth/images/divider.png';
const String TICK_ICON = 'lib/modules/auth/images/tick_icon.png';
const String USER_PROFILE_ICON = 'lib/modules/auth/images/user.png';
const String LOADING_ICON = 'lib/modules/auth/images/loading.gif';
const String APP_ICON = 'lib/modules/auth/images/app_icon.png';
const String DEFAULT_PROFILE_ICON =
    'lib/modules/auth/images/default_profile_image.png';

const String BANK_DETAIL_ICON = 'lib/modules/auth/images/bank_details_icon.png';
const String HELP_AND_SUPPORT_ICON =
    'lib/modules/auth/images/help_and_support_icon.png';
const String LOGOUT_ICON = 'lib/modules/auth/images/logout_icon.png';
const String MY_DOCUMENT_ICON = 'lib/modules/auth/images/my_documents_icon.png';
const String MY_EARNING_ICON = 'lib/modules/auth/images/my_earning_icon.png';
const String TERMS_AND_POLICY_ICON =
    'lib/modules/auth/images/terms_and_policy_icon.png';
const String ARROW_RIGHT_ICON = 'lib/modules/auth/images/arrow_right_icon.png';
const String EDIT_ICON = 'lib/modules/auth/images/edit_icon_icon.png';
const String UPLOAD_ICON = 'lib/modules/auth/images/upload_icon.png';
const String DRAWER_ICON = 'lib/modules/auth/images/drawer.png';
const String HELP_AND_SUPPORT =
    'lib/modules/auth/images/help_and_support.svg';
const String NOTIFICATION = 'lib/modules/auth/images/notification.svg';
const String BANK_ICON = 'lib/modules/auth/images/bank.png';




//Boozin
const String SPLASH_PIN_ICON = 'lib/modules/auth/images/splash_pin_icon.png';
const String AUTH_BG = 'lib/modules/auth/images/auth_bg.png';
const String ALLOW_LOCATION_ICON = 'lib/modules/auth/images/allow_location_icon.png';