import 'package:boozin_driver/modules/common/utils/custom_date_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:boozin_driver/localizations.dart';
import '../../../modules/auth/enums/auth_enums.dart';

class AuthValidator {
  static AuthValidator _authValidator = AuthValidator();

  static AuthValidator get authValidatorInstance => _authValidator;

  final String _emailvalidRegex =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  final int _nameMinLength = 3;

  final int _phoneMinLength = 14;

// used to check empty field
  bool checkEmptyField({String value}) {
    return value?.trim()?.isEmpty;
  }

  //method to check valid phone number
  bool checkValidPhoneNumber({String value}) {
    if (value?.length > _phoneMinLength ||
        value?.length < MaxLength.MinPhoneLength.value) {
      return true;
    }
  }

  //method to check valid phone number
  bool checkValidPhoneLength({String value}) {
    print('The phone number is $value');
    if (value?.length > _phoneMinLength) {
      return true;
    }
  }

// used to check valid email or not
  bool checkValidEmail({String value}) {
    RegExp regExp = new RegExp(_emailvalidRegex);
    return regExp.hasMatch(value.toString().trim());
  }

  // used to check name Validation
  bool checkValidName({String value}) {
    return value.length < _nameMinLength;
  }

  // check otpLength
  bool checkOtpLength({String value}) {
    return value.length < MaxLength.OTP.value;
  }

// used to trim string
  String trimValue({String value}) {
    return value?.trim();
  }

  // validate signup screen
  String validateSendOtpScreen(
      {String name,
      String email,
      String phoneNumber,
      Country country,
      BuildContext context,
      bool isConditionAgreed}) {
    String result = "";

    // validate phone is empty
    if (checkValidPhoneNumber(value: phoneNumber) == true)
      return result = AppLocalizations.of(context).common.error.enterYourNumber;
    if (checkValidPhoneLength(value: phoneNumber) == true)
      return result =
          AppLocalizations.of(context).common.error.phoneNumberLength;
    if (isConditionAgreed == false)
      return result =
          AppLocalizations.of(context).sendOtp.error.agreeTermsAndCondition;

    return result;
  }

  String validateVerifyOtpScreen({String value, BuildContext context}) {
    String result = "";
    // validate otp is empty
    if (checkEmptyField(value: value) == true)
      return result = AppLocalizations.of(context).common.error.pleasefillOTP;

    // validate otp is valid
    if (checkOtpLength(value: value) == true)
      return result = AppLocalizations.of(context).common.error.pleasefillOTP;

    return result;
  }

  String enterDetailsScreen(
      {String name,
      Country country,
      BuildContext context,
      String dateOfBirth}) {
    String result = "";
// validate otp is empty
    if (checkEmptyField(value: name) == true)
      return result = AppLocalizations.of(context).common.error.enterYourName;
    if (checkNameLength(value: name) == true)
      return result = AppLocalizations.of(context).common.error.nameMaxLength;
    if (checkValidName(value: name) == true)
      return result = AppLocalizations.of(context).common.error.nameMinLength;

    if (checkEmptyField(value: dateOfBirth) == true)
      return result =
          AppLocalizations.of(context).common.error.pleaseEnterYourDob;
    // if (checkValidDob(value: dateOfBirth) == true)
    //   return result = 'Valid age is 18 years.';
    return result;
  }

  bool checkNameLength({String value}) {
    return value.length > 25;
  }

  String validateSendQuery({String queryString, BuildContext context}) {
    var result;
    // validate name is empty
    result = (checkEmptyField(value: queryString) == true)
        ? AppLocalizations.of(context).sendQuery.error.plsEnterTheQuery
        : (queryString?.trim()?.length < 5)
            ? "Minimum 5 char required."
            : "";
    return result;
  }

//check valid age
  checkValidDob({String value}) {
    DateTime currentDate = DateTime.now();
    double daysinEighteenYears = 6574;
    DateTime date = DateTime.now();
    DateTime pickeDate =
        CustomDateUtils.dateUtilsInstance.convertStringToDate(dateTime: value);
    final difference = currentDate.difference(pickeDate).inDays;
    if (difference < daysinEighteenYears) {
      date = currentDate;
      print('invalid age');
      return true;
    } else {
      date = pickeDate;
      print('valid age');
      return false;
    }
  }

//validation for bank account details of driver
  String bankDetailsScreen({
    String businessName,
    String bankName,
    String bankAccountNumber,
    // String description,
    BuildContext context,
  }) {
    String result = "";
    if (checkEmptyField(value: businessName) == true)
      return result = 'Please enter your business name';
    if (checkEmptyField(value: bankName) == true)
      return result = 'Please select your bank';
    if (checkEmptyField(value: bankAccountNumber) == true)
      return result = 'Please enter your bank account number';
    // if (checkEmptyField(value: description) == true)
    //   return result = 'Please enter account description';

    return result;
  }

  String validateCancelOrder(String cancelOrderReason) {
    String result = "";
    //check cancel order
    if (checkEmptyField(value: cancelOrderReason) == true)
      return result = 'Please enter a reason to cancel order.';

    if (checkValidReason(value: cancelOrderReason) == true)
      return result = 'Reason must be of minimum 30 charecters.';

    return result;
  }

  bool checkValidReason({String value}) {
    if (value?.length < 30) {
      return true;
    }
  }
}
