enum SocialLogin { facebook, google, apple }

enum UserRoles { Customer, Vendor, OnDemandDriver, CommercialVehicleDriver }

enum UserGender { Mr, Mrs, Ms }
enum Timer { sec, minute }

enum LocationStatus { denied, neverAsk }

enum LoginWithPhoneOrEmail { email, phone }

extension UserGnederExtension on UserGender {
  int get value {
    switch (this) {
      case UserGender.Mr:
        return 0;
      case UserGender.Mrs:
        return 1;
      case UserGender.Ms:
        return 2;
      default:
        return null;
    }
  }
}

extension PermissionExtention on LocationStatus {
  String get value {
    switch (this) {
      case LocationStatus.denied:
        return 'PERMISSION_DENIED';
        break;
      case LocationStatus.neverAsk:
        return 'PERMISSION_DENIED_NEVER_ASK';
        break;
      default:
        return null;
    }
  }
}

extension TimerExtention on Timer {
  int get value {
    switch (this) {
      case Timer.sec:
        return 2;
      case Timer.minute:
        return 2;

      default:
        return null;
    }
  }
}

extension LoginWithPhoneOrEmailExtension on LoginWithPhoneOrEmail {
  int get value {
    switch (this) {
      case LoginWithPhoneOrEmail.phone:
        return 1;
      case LoginWithPhoneOrEmail.email:
        return 2;

      default:
        return null;
    }
  }
}

// NormalUser = 1;
// DeliveryBoyUser = 2;
// tuckDriverUser = 3;
// vendorAsUser = 4;
extension UserRolesExtension on UserRoles {
  int get value {
    switch (this) {
      case UserRoles.Customer:
        return 1;
      case UserRoles.OnDemandDriver:
        return 2;
      case UserRoles.CommercialVehicleDriver:
        return 3;
      case UserRoles.Vendor:
        return 4;

      default:
        return null;
    }
  }
}

enum LoginType { EmailPassword, onlyPhone }

extension LoginTypeExtension on LoginType {
  int get value {
    switch (this) {
      case LoginType.EmailPassword:
        return 1;
      case LoginType.onlyPhone:
        return 0;
      default:
        return null;
    }
  }
}

enum MaxLength { OTP, Phone, OtpErrorLength, MinPhoneLength }

extension MaxLengthExtension on MaxLength {
  int get value {
    switch (this) {
      case MaxLength.OTP:
        return 4;
      case MaxLength.Phone:
        return 14;
      case MaxLength.OtpErrorLength:
        return 3;
      case MaxLength.MinPhoneLength:
        return 9;

      default:
        return null;
    }
  }
}

enum MinLength { Username }

extension MinLengthExtension on MinLength {
  int get value {
    switch (this) {
      case MinLength.Username:
        return 3;
      default:
        return null;
    }
  }
}

enum VerifyOtpFrom { Email, PhoneNumber, Normal }

extension VerifyOtpFromExtension on VerifyOtpFrom {
  int get value {
    switch (this) {
      case VerifyOtpFrom.Email:
        return 1;
      case VerifyOtpFrom.PhoneNumber:
        return 0;
      case VerifyOtpFrom.Normal:
        return 2;
      default:
        return null;
    }
  }
}

enum DocumentStatus { Pending, Approved, Rejected, NotUploaded }

extension DocumentStatusExtension on DocumentStatus {
  int get value {
    switch (this) {
      case DocumentStatus.NotUploaded:
        return 0;
      case DocumentStatus.Pending:
        return 1;
      case DocumentStatus.Approved:
        return 2;
      case DocumentStatus.Rejected:
        return 3;
      default:
        return null;
    }
  }
}

enum RegistrationStep { Registration, Licence, Policy, Insurance }

extension RegistrationStepExtension on RegistrationStep {
  int get value {
    switch (this) {
      case RegistrationStep.Registration:
        return 1;
      case RegistrationStep.Licence:
        return 2;
      case RegistrationStep.Policy:
        return 3;
      case RegistrationStep.Insurance:
        return 4;
      default:
        return null;
    }
  }
}

enum ProfileEditType { Email, PhoneNumber, Name, DOB }

extension ProfileEditTypeExtension on ProfileEditType {
  int get value {
    switch (this) {
      case ProfileEditType.Name:
        return 1;
      case ProfileEditType.PhoneNumber:
        return 2;
      case ProfileEditType.Email:
        return 3;
      case ProfileEditType.DOB:
        return 4;
      default:
        return null;
    }
  }
}

enum ComingFrom { VerifyOtpScreen, EnterDetailScreen }

extension ComingFromTypeExtension on ComingFrom {
  int get value {
    switch (this) {
      case ComingFrom.VerifyOtpScreen:
        return 1;
      case ComingFrom.EnterDetailScreen:
        return 2;
      default:
        return null;
    }
  }
}
