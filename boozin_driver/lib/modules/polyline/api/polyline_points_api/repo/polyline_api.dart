import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../../../../modules/polyline/api/polyline_points_api/model/poly_lat_long_model.dart';

enum TravelMode { driving, bicycling, transit, walking }

const String STATUS_OK = "ok";

class PolyLineLatLongApi {
  // logout
  Future<List<PolyPointLatLngModel>> getLatLongApi(
      {BuildContext context,
      String googleApiKey,
      PolyPointLatLngModel originLatLong,
      PolyPointLatLngModel destLatLong,
      TravelMode travelMode,
      List<PolyPointLatLngModel> wayPosPoints}) async {
    String mode = travelMode.toString().replaceAll('TravelMode.', '');
    List<PolyPointLatLngModel> polylinePoints = [];

    String wayPoint = "";
    if (wayPosPoints?.isNotEmpty == true) {
      var via = "via:";
      var comma = "%2C";
      var colon = "%7C";
      wayPoint = "&waypoints=";
      for (int index = 0; index < wayPosPoints.length; index++) {
        if (index == wayPosPoints.length - 1) {
          wayPoint += "${via}${wayPosPoints[index].latitude}${comma}${wayPosPoints[index].longitude}";
        } else {
          wayPoint += "${via}${wayPosPoints[index].latitude}${comma}${wayPosPoints[index].longitude}${colon}";
        }
      }
    }

    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" +
        originLatLong.latitude.toString() +
        "," +
        originLatLong.longitude.toString() +
        "&destination=" +
        destLatLong.latitude.toString() +
        "," +
        destLatLong.longitude.toString() +
        wayPoint +
        "&mode=$mode" +
        "&key=$googleApiKey";

    print('GOOGLE MAPS URL: ' + url);
    var response = await Dio().get(url);
    if (response?.statusCode == 200) {
      var parsedJson = response.data;
      if (parsedJson["status"]?.toLowerCase() == STATUS_OK &&
          parsedJson["routes"] != null &&
          parsedJson["routes"].isNotEmpty) {
        polylinePoints = decodeEncodedPolyline(parsedJson["routes"][0]["overview_polyline"]["points"]);
      } else {
        print("GOOGLE MAPS error>> " + response?.toString());
      }
    } else {
      print("GOOGLE MAPS error>> " + response?.toString());
    }

    return polylinePoints;
  }

  ///decode the google encoded string using Encoded Polyline Algorithm Format
  /// for more info about the algorithm check https://developers.google.com/maps/documentation/utilities/polylinealgorithm
  ///
  ///return [List]
  List<PolyPointLatLngModel> decodeEncodedPolyline(String encoded) {
    List<PolyPointLatLngModel> poly = [];
    int index = 0, len = encoded.length;
    int lat = 0, lng = 0;

    while (index < len) {
      int b, shift = 0, result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;
      PolyPointLatLngModel p =
          new PolyPointLatLngModel(latitude: (lat / 1E5).toDouble(), longitude: (lng / 1E5).toDouble());
      poly.add(p);
    }
    return poly;
  }
}
