import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../modules/polyline/api/polyline_points_api/model/poly_lat_long_model.dart';
import '../../../modules/polyline/api/polyline_points_api/repo/polyline_api.dart';
import 'polyline_event.dart';
import 'polyline_state.dart';

class PolylineBloc extends BlocEventStateBase<PolyLineEvent, PolyLineState> {
  PolylineBloc(
      {bool isLoading,
      String googleApiKey,
      PolyPointLatLngModel originLatLong,
      PolyPointLatLngModel destLatLong,
      TravelMode travelMode,
      List<PolyPointLatLngModel> wayPosPoints,
      Set<Polyline> polylines,
      BuildContext context})
      : super(
            initialState: PolyLineState.initiating(
                isLoading: isLoading,
                wayPosPoints: wayPosPoints,
                travelMode: travelMode,
                googleApiKey: googleApiKey,
                originLatLong: originLatLong,
                destLatLong: destLatLong,
                polylines: polylines,
                context: context));

  @override
  Stream<PolyLineState> eventHandler(PolyLineEvent event, PolyLineState currentState) async* {
    if (event is FetchPolyLineEvent) {
      // show loader on map view
//      yield PolyLineState.UpdatedPolyLines(
//          isLoading: true,
//          wayPosPoints: event.wayPosPoints,
//          travelMode: event.travelMode,
//          googleApiKey: event.googleApiKey,
//          originLatLong: event.originLatLong,
//          destLatLong: event.destLatLong,
//          polylines: event.polylines,
//          context: event.context);
      // create new instance of polycordinates
      List<LatLng> _polylineCoordinates = [];
      // create new instance of polylines
      Set<Polyline> _polylines = {};
      // get result from api
      List<PolyPointLatLngModel> result = await new PolyLineLatLongApi().getLatLongApi(
          destLatLong: event.destLatLong,
          originLatLong: event.originLatLong,
          googleApiKey: event.googleApiKey,
          travelMode: event.travelMode,
          wayPosPoints: event.wayPosPoints);
      if (result.isNotEmpty) {
        // loop through all PointLatLng points and convert them
        // to a list of LatLng, required by the Polyline
        result.forEach((PolyPointLatLngModel point) {
          _polylineCoordinates.add(LatLng(point.latitude, point.longitude));
        });
      }

      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      Polyline polyline =
          Polyline(polylineId: PolylineId("poly"), color: Colors.blue, width: 3, points: _polylineCoordinates);

      // add the constructed polyline as a set of points
      // to the polyline set, which will eventually
      // end up showing up on the map
      _polylines.add(polyline);

      yield PolyLineState.UpdatedPolyLines(
          isLoading: false,
          wayPosPoints: event.wayPosPoints,
          travelMode: event.travelMode,
          googleApiKey: event.googleApiKey,
          originLatLong: event.originLatLong,
          destLatLong: event.destLatLong,
          polylines: _polylines,
          context: event.context);
    }
  }
}
