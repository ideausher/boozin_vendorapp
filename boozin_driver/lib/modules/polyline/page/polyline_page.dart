import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:boozin_driver/localizations.dart';
import '../../../modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../modules/common/common_widget/async_call_parent_widget.dart';
import '../../../modules/polyline/bloc/polyline_event.dart';
import '../../../modules/polyline/bloc/polyline_state.dart';
import '../../../modules/polyline/bloc/polyline_bloc.dart';

import '../../../modules/polyline/api/polyline_points_api/model/poly_lat_long_model.dart';
import '../../../modules/polyline/api/polyline_points_api/repo/polyline_api.dart';

const double CAMERA_ZOOM = 13;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 30;
const int TIMER_VALUE = 4;

class PolyLineLatLongMapPage extends StatefulWidget {
  static UpdatePolyPointsCallback updatePolyPointsCallback;

  String googleApiKey; // web api key to get polyline
  PolyPointLatLngModel originLatLong; // source location
  PolyPointLatLngModel destLatLong; // destination location
  TravelMode travelMode; // travel mode
  List<PolyPointLatLngModel>
      wayPosPoints; // way points in between source and destination

  PolyLineLatLongMapPage(
      {this.googleApiKey,
      this.originLatLong,
      this.destLatLong,
      this.travelMode,
      this.wayPosPoints});

  @override
  _PolyLineLatLongMapPageState createState() => _PolyLineLatLongMapPageState();
}

class _PolyLineLatLongMapPageState extends State<PolyLineLatLongMapPage>
    implements UpdatePolyPointsCallback {
  Completer<GoogleMapController> _controller =
      Completer(); // map complete controller list

  // this set will hold my markers
  Set<Marker> _markers = {};

  PolylineBloc _polylineBloc; // bloc  instance
  PolyLineState _polyLineState; // state instance

  Timer _timer; // timer to get polyline
  int _start = TIMER_VALUE;
  GoogleMapController _googleMapController; // map controller

  // used to start timer whenver we get polyline so that we have gap
  void _startTimer() {
    _timer = new Timer.periodic(
      Duration(seconds: TIMER_VALUE),
      (Timer timer) {
        print("LocationUpdate =>> Timer value ${_start}");
        // recursion if location updated
        if (_start < 1) {
          _start = TIMER_VALUE;
          timer.cancel(); // cancel timer
          print("LocationUpdate =>> Timercancelled");
          // if (_polyLineState?.originLatLong?.longitude !=
          //     widget.originLatLong.longitude) {
          print("LocationUpdate =>> Timer started");
          _startTimer(); // start timer again
          // get data from api again
          _polylineBloc?.emitEvent(FetchPolyLineEvent(
              wayPosPoints: widget.wayPosPoints,
              travelMode: widget.travelMode,
              googleApiKey: widget.googleApiKey,
              originLatLong: widget.originLatLong,
              destLatLong: widget.destLatLong,
              polylines: _polyLineState?.polylines ?? {},
              isLoading: true));
          //}
        } else {
          // decrease count down
          _start = _start - 1;
        }
      },
    );
  }

  CameraPosition _initialLocation;

  @override
  void dispose() {
    PolyLineLatLongMapPage.updatePolyPointsCallback = null;
    _timer?.cancel();
    _start = TIMER_VALUE;
    _polylineBloc?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    print("init caled poly");
    // get data from google api
    _polylineBloc = PolylineBloc(
        isLoading: false,
        polylines: {},
        destLatLong: widget.destLatLong,
        originLatLong: widget.originLatLong,
        googleApiKey: widget.googleApiKey,
        travelMode: widget.travelMode,
        wayPosPoints: widget.wayPosPoints);
    PolyLineLatLongMapPage.updatePolyPointsCallback = this;

    _initialLocation = CameraPosition(
        zoom: CAMERA_ZOOM,
        bearing: CAMERA_BEARING,
        tilt: CAMERA_TILT,
        target: new LatLng(
            widget.originLatLong.latitude, widget.originLatLong.longitude));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocEventStateBuilder<PolyLineState>(
      bloc: _polylineBloc,
      builder: (BuildContext context, PolyLineState state) {
        if (_polyLineState != state) {
          _polyLineState = state;
          // set map pins
          _setMapPins();
        }

        return ModalProgressHUD(
          inAsyncCall: state.isLoading,
          child: GoogleMap(
            initialCameraPosition: _initialLocation,
            myLocationEnabled: true,
            compassEnabled: true,
            tiltGesturesEnabled: false,
            markers: _markers,
            polylines: _polyLineState?.polylines ?? {},
            mapType: MapType.normal,
            onMapCreated: onMapCreated,
            zoomGesturesEnabled: true,
            scrollGesturesEnabled: true,
          ),
        );
      },
    );
  }

  @override
  void updatePolyDetails(
      {String googleApiKey,
      PolyPointLatLngModel originLatLong,
      PolyPointLatLngModel destLatLong,
      TravelMode travelMode,
      List<PolyPointLatLngModel> wayPosPoints}) {
    print("LocationUpdate");
    if (googleApiKey != null) {
      widget.googleApiKey = googleApiKey;
    }
    if (originLatLong != null) {
      widget.originLatLong = originLatLong;
    }
    if (destLatLong != null) {
      widget.destLatLong = destLatLong;
    }
    if (travelMode != null) {
      widget.travelMode = travelMode;
    }
    if (wayPosPoints != null) {
      widget.wayPosPoints = wayPosPoints;
    }

    // if (
    //     _start == TIMER_VALUE) {
    //   print("LocationUpdate =>> Timer started ${widget.wayPosPoints}");
    //   _startTimer();
    _polylineBloc?.emitEvent(FetchPolyLineEvent(
        wayPosPoints: widget.wayPosPoints,
        travelMode: widget.travelMode,
        googleApiKey: widget.googleApiKey,
        originLatLong: widget.originLatLong,
        destLatLong: widget.destLatLong,
        polylines: _polyLineState?.polylines ?? {},
        isLoading: true));
    //  }
  }

  // used to call whenever map updated
  void onMapCreated(GoogleMapController controller) {
    _googleMapController = controller;
    if (!_controller?.isCompleted) {
      _controller.complete(controller);
    }
    //   _startTimer();
    _polylineBloc?.emitEvent(FetchPolyLineEvent(
        wayPosPoints: widget.wayPosPoints,
        travelMode: widget.travelMode,
        googleApiKey: widget.googleApiKey,
        originLatLong: widget.originLatLong,
        destLatLong: widget.destLatLong,
        polylines: _polyLineState?.polylines ?? {},
        isLoading: true));
  }

  // used to set map pins
  void _setMapPins() {
    _markers.clear();
    // source pin
    _markers.add(Marker(
        infoWindow: InfoWindow(
            title: AppLocalizations.of(context).polyline.text.shopAddress, snippet: widget.originLatLong?.address),
        markerId: MarkerId('sourcePin'),
        position: new LatLng(
            widget.originLatLong.latitude, widget.originLatLong.longitude),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed)));

    // way points pin
    if (_polyLineState?.wayPosPoints?.isNotEmpty == true) {
      for (var latLong in _polyLineState?.wayPosPoints) {
        _markers.add(Marker(
            markerId: MarkerId(latLong.toString()),
            position: new LatLng(latLong.latitude, latLong.longitude),
            icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueYellow)));
      }
    }

    // destination pin
    _markers.add(Marker(
        infoWindow: InfoWindow(
            title: AppLocalizations.of(context).polyline.text.deliveryAddress, snippet: widget.destLatLong?.address),
        markerId: MarkerId('destPin'),
        position: new LatLng(
            widget.destLatLong.latitude, widget.destLatLong.longitude),
        icon:
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen)));

    print("markers ${_markers?.length}");

    CameraUpdate u2 = CameraUpdate.newLatLngZoom(
        LatLng(widget.originLatLong.latitude, widget.originLatLong.longitude),
        11);
    _googleMapController?.animateCamera(u2);
  }
}

// used to get latest poly points
class UpdatePolyPointsCallback {
  void updatePolyDetails(
      {String googleApiKey,
      PolyPointLatLngModel originLatLong,
      PolyPointLatLngModel destLatLong,
      TravelMode travelMode,
      List<PolyPointLatLngModel> wayPosPoints}) {}
}
