follow this link to add location dependency

https://pub.dev/packages/location#-readme-tab-


# import location: ^3.0.2 on yaml file

And to use it in iOS, you have to add this permission in Info.plist :

NSLocationWhenInUseUsageDescription
NSLocationAlwaysUsageDescription


We just need to use this PolyLineLatLongMapPage  as a widget and pass the variable

if you want to update ui when location change without calling set state of parent

UpdatePolyPointsCallback  this call back getting registered  on the PolyLineLatLongMapPage , you just need to use callback instance and send variable